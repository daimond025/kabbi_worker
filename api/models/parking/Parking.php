<?php

namespace app\models\parking;

use Yii;

/**
 * This is the model class for table "{{%parking}}".
 *
 * @property string $parking_id
 * @property string $tenant_id
 * @property string $city_id
 * @property string $name
 * @property string $polygon
 * @property integer $sort
 * @property string $type
 * @property bool is_active
 *
 * @property TblCabLine[] $tblCabLines
 * @property TblFixTariff[] $tblFixTariffs
 * @property TblCity $city
 * @property TblTenant $tenant
 */
class Parking extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%parking}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'city_id', 'name', 'polygon', 'type'], 'required'],
            [['tenant_id', 'city_id', 'sort'], 'integer'],
            [['polygon'], 'string'],
            [['name', 'type'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parking_id' => Yii::t('parking', 'Parking ID'),
            'tenant_id'  => Yii::t('parking', 'Tenant ID'),
            'city_id'    => Yii::t('parking', 'City ID'),
            'name'       => Yii::t('parking', 'Name'),
            'polygon'    => Yii::t('parking', 'Polygon'),
            'sort'       => Yii::t('parking', 'Sort'),
            'type'       => Yii::t('parking', 'Type'),
            'is_active'  => Yii::t('parking', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCabLines()
    {
        return $this->hasMany(CabLine::className(), ['parking_id' => 'parking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixFrom()
    {
        return $this->hasMany(\app\modules\tariff\models\FixTariff::className(), ['from' => 'parking_id']);
    }

    public function getFixTo()
    {
        return $this->hasMany(\app\modules\tariff\models\FixTariff::className(), ['to' => 'parking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(TblCity::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(TblTenant::className(), ['tenant_id' => 'tenant_id']);
    }

    public static function getParkingArrInCity($tenant_id, $city_id)
    {
        return Parking::find()
                        ->where(['tenant_id' => $tenant_id])
                        ->andWhere(['city_id' => $city_id])
                        ->andWhere(['type' => 'city'])
                        ->orderBy('sort DESC')
                        ->asArray()
                        ->all();
    }

    public static function getParkingArrOutCity($tenant_id, $city_id)
    {
        return Parking::find()
                        ->where(['tenant_id' => $tenant_id])
                        ->andWhere(['city_id' => $city_id])
                        ->andWhere(['type' => 'outCity'])
                        ->orderBy('sort DESC')
                        ->asArray()
                        ->all();
    }

    public static function getParkingArrAirports($tenant_id, $city_id)
    {
        return Parking::find()
                        ->where(['tenant_id' => $tenant_id])
                        ->andWhere(['city_id' => $city_id])
                        ->andWhere(['type' => 'airport'])
                        ->orderBy('sort DESC')
                        ->asArray()
                        ->all();
    }

    public static function getParkingArrStations($tenant_id, $city_id)
    {
        return Parking::find()
                        ->where(['tenant_id' => $tenant_id])
                        ->andWhere(['city_id' => $city_id])
                        ->andWhere(['type' => 'station'])
                        ->orderBy('sort DESC')
                        ->asArray()
                        ->all();
    }

    public static function getParkingArrBase($tenant_id, $city_id)
    {
        return Parking::find()
                        ->where(['tenant_id' => $tenant_id])
                        ->andWhere(['city_id' => $city_id])
                        ->andWhere(['type' => 'basePolygon'])
                        ->orderBy('sort DESC')
                        ->asArray()
                        ->one();
    }

    public static function getParkingArrAll($tenant_id, $city_id)
    {
        return Parking::find()
                        ->where(['tenant_id' => $tenant_id])
                        ->andWhere(['city_id' => $city_id])
                        ->andWhere('type <> :type', [':type' => 'reseptionArea'])
                        ->andWhere(['is_active' => 1])
                        ->orderBy('sort DESC')
                        ->asArray()
                        ->all();
    }

    public static function getParkingArrAllWithoutBase($tenant_id, $city_id)
    {
        return Parking::find()
                        ->where(['tenant_id' => $tenant_id])
                        ->andWhere(['city_id' => $city_id])
                        ->andWhere('type <> :type', [':type' => 'basePolygon'])
                        ->orderBy('sort DESC')
                        ->asArray()
                        ->all();
    }

    public static function countFreeOrdersOnParking($key, $freeOrders)
    {
        $count = 0;
        if (is_array($freeOrders)) {
            foreach ($freeOrders as $order) {
                if ($order["parking_id"] == $key) {
                    $count = $count + 1;
                }
            }
        }
        return $count;
    }

}
