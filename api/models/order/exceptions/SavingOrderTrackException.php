<?php

namespace api\models\order\exceptions;

use yii\base\Exception;

/**
 * Class SavingOrderTrackException
 * @package api\models\order\exceptions
 */
class SavingOrderTrackException extends Exception
{

}