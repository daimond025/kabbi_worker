<?php

namespace app\models\order;

use api\models\order\exceptions\QueueIsNotExistsException;
use PhpAmqpLib\Exception\AMQPProtocolChannelException;
use Ramsey\Uuid\Uuid;
use yii\base\Object;

/**
 * Class OrderUpdateEvent
 * @package app\models\order
 */
class OrderUpdateEvent extends Object
{
    public $requestId;
    public $tenantId;
    public $orderId;
    public $senderId;
    public $lastUpdateTime;
    public $lang;

    private $_repository;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->setRepository();
    }

    /**
     * Add order event for update.
     *
     * @param array $data
     *
     * @return string
     */
    public function addEvent(array $data)
    {
        $this->addEventToQueue($data);
    }

    /**
     * Add event to rabbit queue
     *
     * @param array $data
     *
     * @return mixed
     * @throws QueueIsNotExistsException
     */
    private function addEventToQueue(array $data)
    {
        $data = $this->prepareData($data);

        try {
            return \Yii::$app->amqp->send($data, 'order_' . $this->orderId, '', false);
        } catch (AMQPProtocolChannelException $ex) {
            throw new QueueIsNotExistsException('Queue is not exists', 0, $ex);
        }
    }

    /**
     * Getting response of executing order event
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getResponse($key)
    {
        return $this->getRepository()->get($key);
    }

    public function removeResponse($key)
    {
        return $this->getRepository()->delete($key);
    }

    /**
     * @return string
     */
    private function generateUuid()
    {
        return Uuid::uuid4()->toString();
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function prepareData(array $data)
    {
        return [
            'uuid'             => $this->requestId === null ? $this->generateUuid() : $this->requestId,
            'type'             => 'order_event',
            'timestamp'        => time(),
            'sender_service'   => 'worker_service',
            'command'          => 'update_order_data',
            'order_id'         => $this->orderId,
            'tenant_id'        => $this->tenantId,
            'change_sender_id' => $this->senderId,
            'last_update_time' => $this->lastUpdateTime,
            'lang'             => $this->lang,
            'params'           => $data,
        ];
    }

    /**
     * @return OrderUpdateEventRepository
     */
    private function getRepository()
    {
        if (empty($this->_repository)) {
            $this->setRepository();
        }

        return $this->_repository;
    }

    private function setRepository()
    {
        $this->_repository = new OrderUpdateEventRepository();
    }

}