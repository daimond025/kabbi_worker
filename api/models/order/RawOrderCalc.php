<?php

namespace app\models\order;

use Yii;

/**
 * This is the model class for table "tbl_raw_order_calc".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $raw_cacl_data
 * @property string $create_date
 *
 * @property Order $order
 */
class RawOrderCalc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_raw_order_calc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'raw_cacl_data'], 'required'],
            [['order_id'], 'integer'],
            [['raw_cacl_data'], 'string'],
            [['create_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'raw_cacl_data' => 'Raw Cacl Data',
            'create_date' => 'Create Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }
}
