<?php

namespace app\models\order;

use Yii;

/**
 * This is the model class for table "{{%tbl_order_has_option}}".
 *
 * @property string $order_id
 * @property integer $option_id
 *
 * @property TblCarOption $option
 * @property TblOrder $order
 */
class OrderHasOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_has_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'option_id'], 'required'],
            [['order_id', 'option_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('order', 'Order ID'),
            'option_id' => Yii::t('order', 'Option ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(CarOption::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

    /**
     * Allow to save multiple models
     * @param array $arOptions
     * @param array $order_id
     * @return boolean
     */
    public static function manySave($arOptions, $order_id)
    {
        if(is_array($arOptions) && !empty($arOptions))
        {
            $insertValue = [];
            $connection = Yii::$app->db;

            foreach($arOptions as $option_id)
            {
                if($option_id > 0)
                    $insertValue[] = [$order_id, $option_id];
            }

            if(!empty($insertValue))
            {
                $connection->createCommand()->batchInsert(self::tableName(), ['order_id', 'option_id'], $insertValue)->execute();

                return true;
            }
        }

        return false;
    }
}
