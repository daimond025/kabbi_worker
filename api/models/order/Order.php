<?php

namespace app\models\order;

use api\components\orderApi\interfaces\createOrder\ApiOrderInterface;
use api\components\orderApi\OrderApi;
use api\models\order\exceptions\PromoException;
use app\models\promo\Promo;
use app\models\client\ClientCompany;
use app\models\tenant\TenantTariffService;
use app\models\traits\ApiOrderTrait;
use app\models\worker\Position;
use app\models\worker\Worker;
use v3\components\repositories\PositionRepository;
use v3\components\taxiApiRequest\TaxiErrorCode;
use v3\components\WorkerService;
use Yii;
use yii\base\ErrorException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use app\models\tenant\TenantSetting;
use app\models\address\City;
use app\models\order\OrderStatus;
use app\models\client\Client;
use yii\db\Exception;
use yii\db\Expression;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property string            $order_id
 * @property string            $currency_id
 * @property string            $car_id
 * @property string            $tenant_id
 * @property string            $client_id
 * @property string            $worker_id
 * @property string            $city_id
 * @property string            $tariff_id
 * @property string            $user_id
 * @property string            $user_create
 * @property integer           $status_id
 * @property string            $user_modifed
 * @property string            $company_id
 * @property string            $parking_id
 * @property string            $address
 * @property string            $commentc
 * @property string            $predv_price
 * @property string            $device
 * @property string            $order_number
 * @property string            $payment
 * @property integer           $show_phone
 * @property integer           $viewed
 * @property string            $create_time
 * @property string            $order_time
 * @property string            $status_time
 * @property string            $time_to_client
 * @property string            $phone
 * @property integer           $time_offset
 * @property integer           $position_id
 *
 * @property ClientReview[]    $clientReviews
 * @property Exchange[]        $exchanges
 * @property City              $city
 * @property ClientCompany     $company
 * @property Worker            $worker
 * @property OrderStatus       $status
 * @property Parking           $parking
 * @property TaxiTariff        $tariff
 * @property Tenant            $tenant
 * @property User              $userCreate
 * @property User              $userModifed
 * @property OrderChange[]     $orderChanges
 * @property OrderHasOption[]  $orderHasOptions
 * @property CarOption[]       $options
 * @property OrderStatusTime[] $orderStatusTimes
 * @property ParkingOrder[]    $parkingOrders
 * @property Position          $position
 */
class Order extends \yii\db\ActiveRecord implements ApiOrderInterface
{

    use ApiOrderTrait;

    const ORDER_ACTION = 'EXECUTING_ORDER';
    const ORDER_PREE = 'pree';
    const ORDER_FREE= 'free';

    const DEVICE_0 = 'DISPATCHER';
    const DEVICE_1 = 'IOS';
    const DEVICE_2 = 'ANDROID';
    const DEVICE_WORKER = 'WORKER';

    const PRE_ODRER_TIME = 1800;
    const PICK_UP_TIME = 300;
    const STATUS_NEW_FREE = 4;
    const STATUS_NEW_EXPIRED = 52;
    const STATUS_NEW_PRDV = 6;
    const STATUS_NEW_PRDV_FREE = 15;
    const STATUS_NEW_PRDV_NO_PARKING = 16;

    const STATUS_PRE_ORDER_FOR_HOSPITAL = 203;
    const STATUS_PRE_ORDER_FOR_COMPANY = 205;

    const STATUS_FREE_FOR_HOSPITAL = 204;
    const STATUS_FREE_FOR_COMPANY = 206;

    public $additional_option = [];
    // public $phone;
    public $order_date;
    public $order_hours;
    public $order_minutes;
    public $status_reject;
    private $pickUp;
    private $orderOffset;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'status_id'], 'required'],
            [
                ['order_number'],
                function ($attribute) {
                    if (ctype_digit($this->order_number) || $this->order_number instanceof Expression) {
                        return true;
                    }

                    $this->addError(\Yii::t('order', 'Invalid "{attribute}" format', [
                        'attribute' => $this->getAttributeLabel($attribute)
                    ]));
                }
            ],
            [
                [
                    'tenant_id',
                    'worker_id',
                    'city_id',
                    'tariff_id',
                    'client_id',
                    'user_create',
                    'status_id',
                    'user_modifed',
                    'company_id',
                    'parking_id',
                    'show_phone',
                    'create_time',
                    'order_time',
                    'time_offset',
                    'position_id',
                ],
                'integer',
            ],
            [['address', 'comment', 'payment', 'phone'], 'string'],
            [
                ['show_phone', 'additional_option', 'order_date', 'order_hours', 'order_minutes', 'status_reject'],
                'safe',
            ],
            [['predv_price'], 'number'],
            [['device'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id'         => 'Order ID',
            'tenant_id'        => 'Tenant ID',
            'worker_id'        => 'Worker ID',
            'city_id'          => 'City ID',
            'tariff_id'        => 'Tariff',
            'client_id'        => 'Client ID',
            'user_create'      => 'User Create',
            'user_modifed'     => 'User Modifed',
            'status_id'        => 'Status',
            'address'          => 'Address',
            'comment'          => 'Comment',
            'predv_price'      => 'Predv Price',
            'create_time'      => 'Create Time',
            'device'           => 'Device',
            'payment'          => 'Payment',
            'show_phone'       => 'Show client phone to worker',
            'parking_id'       => 'Parking',
            'preliminary_calc' => 'Preliminary calculation',
            'phone'            => 'Phone',
            'position_id'      => 'Position ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(ClientCompany::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientReview()
    {
        return $this->hasOne(\app\models\client\ClientReview::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExchanges()
    {
        return $this->hasMany(Exchange::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParking()
    {
        return $this->hasOne(\app\models\parking\Parking::className(), ['parking_id' => 'parking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(\app\models\order\OrderStatus::className(), ['status_id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(\app\models\tariff\TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(\app\models\tenant\Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(\app\models\tenant\User::className(), ['user_id' => 'user_create']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModifed()
    {
        return $this->hasOne(\app\models\tenant\User::className(), ['user_id' => 'user_modifed']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(\app\models\address\City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderChanges()
    {
        return $this->hasMany(\app\models\order\OrderChange::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHasOptions()
    {
        return $this->hasMany(\app\models\order\OrderHasOption::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(\app\models\car\CarOption::className(),
            ['option_id' => 'option_id'])->viaTable('{{%order_has_option}}', ['order_id' => 'order_id']);
    }

    public function getOrderDetailCost()
    {
        return $this->hasOne(OrderDetailCost::className(), ['order_id' => 'order_id']);
    }

    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }


    public static function getPickUp($tenantId, $cityId, $positionId)
    {
        $pickUp = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_PICK_UP, $cityId, $positionId);

        return empty($pickUp) ? self::PICK_UP_TIME : $pickUp;
    }

    public static function addOrderTime($orderData)
    {
        $offset                  = City::getTimeOffset($orderData->city_id);
        $time                    = time() + $offset;
        $orderData->create_time  = $time;
        $orderData->updated_time = $time;
        $orderData->status_time  = $time;

        return $orderData;
    }

    public static function getLastOrderNumber($tenantId)
    {
        $connection = Yii::$app->db;
        $command    = $connection->createCommand('SELECT MAX(order_number) FROM ' . Order::tableName() . 'WHERE `tenant_id`=' . $tenantId);

        return $command->queryScalar();
    }


    /**
     * Getting orders by status from Redis
     *
     * @param int   $tenantId
     * @param array $statuses
     *
     * @return array
     */
    private static function getOrdersByStatusFromRedis($tenantId, array $statuses)
    {
        $orders = \Yii::$app->redis_orders_active->executeCommand('HVALS', [$tenantId]);

        $result = [];
        if (is_array($orders)) {
            foreach ($orders as $order) {
                try {
                    $order = unserialize($order);
                    if ($order !== false) {
                        if (in_array($order['status']['status_id'], $statuses)) {
                            $result[] = $order;
                        }
                    }
                } catch (\Exception $ex) {
                    Yii::error('Order unserialize exception: ' . $ex->getMessage());
                }
            }
        }

        return $result;
    }

    /**
     * Getting orders by worker
     *
     * @param int $tenantId
     * @param int $workerCallsign
     *
     * @return array
     */
    public static function getOrdersByWorkerFromRedis($tenantId, $workerCallsign)
    {
        $workerOrders = \Yii::$app->redis_worker_orders->executeCommand('SMEMBERS', ["{$tenantId}_{$workerCallsign}"]);
        if (empty($workerOrders)) {
            return [];
        }

        $orders = \Yii::$app->redis_orders_active->executeCommand('HMGET', array_merge([$tenantId], $workerOrders));

        $result = [];
        if (is_array($orders)) {
            foreach ($orders as $order) {
                try {
                    $model = unserialize($order);
                    if ($model !== false) {
                        $result[] = $model;
                    }
                } catch (\Exception $ex) {
                    Yii::error('Order unserialize exception: ' . $ex->getMessage());
                }
            }
        }

        return $result;
    }

    /**
     * Получение из редиса всех  свободных заказов (status_id = 4)
     *
     * @param string $tenantId
     *
     * @return array
     */
    public static function getFreeOrders($tenantId)
    {
        return self::getOrdersByStatusFromRedis($tenantId, [
            self::STATUS_NEW_FREE,
            self::STATUS_NEW_EXPIRED,
            self::STATUS_FREE_FOR_COMPANY,
            self::STATUS_FREE_FOR_HOSPITAL,
        ]);
    }

    /**
     * Получение из редиса всех  свободных  предварительных заказов  (status_id = 6 -новый предвар-й,  15 - предв-й
     * свободный)
     *
     * @param string $tenantId
     *
     * @return array
     */
    public static function getFreePredvOrders($tenantId)
    {
        return self::getOrdersByStatusFromRedis($tenantId, [
            self::STATUS_NEW_PRDV,
            self::STATUS_NEW_PRDV_FREE,
            self::STATUS_NEW_PRDV_NO_PARKING,
            self::STATUS_PRE_ORDER_FOR_COMPANY,
        ]);
    }


    /**
     * Getting worker pre-order list
     *
     * @param int $tenantId
     * @param int $workerCallsign
     *
     * @return array
     */
    public static function getWorkerPreOrders($tenantId, $workerCallsign)
    {
        $orders = self::getOrdersByStatusFromRedis($tenantId, [
            OrderStatus::STATUS_PRE_WORKER_ACCEPT_ORDER,
            OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT,
            OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD,
            OrderStatus::STATUS_WAITING_FOR_PRE_ORDER_CONFIRMATION,
            OrderStatus::STATUS_WORKER_ACCEPTED_PRE_ORDER_CONFIRMATION,
        ]);

        return array_values(array_filter($orders, function ($order) use ($workerCallsign) {
            return isset($order['worker']['callsign'])
                && $order['worker']['callsign'] == $workerCallsign;
        }));
    }


    /**
     * Получение из редиса всех свободный заказов (status_id = 4) на парковке
     *
     * @param string $tenantId
     * @param string $parkingId
     *
     * @return array
     */
    public static function getFreeOrdersAtParking($tenantId, $parkingId)
    {
        $orders = \Yii::$app->redis_orders_active->executeCommand('HVALS', [$tenantId]);
        if (empty($orders)) {
            return null;
        }
        $freeOrders = [];
        foreach ($orders as $order) {
            try {
                $order = unserialize($order);
                if ($order) {
                    if (($order['status']['status_id'] == self::STATUS_NEW_FREE || $order['status']['status_id'] == self::STATUS_NEW_EXPIRED) && $order['parking_id'] == $parkingId) {
                        $freeOrders[] = $order;
                    }
                }
            } catch (\Exception $ex) {
                Yii::error('Order unserialize exception: ' . $ex->getMessage());
            }
        }

        return $freeOrders;
    }

    /**
     * Получить заказа из редиса по Ид
     * @param $orderId
     * @param $tenantId
     * @return mixed|null
     */
    public static function getOrderForId($orderId, $tenantId)
    {
        $order = \Yii::$app->redis_orders_active->executeCommand('HGET', [$tenantId, $orderId]);
        try {
            $order = unserialize($order);
            if (empty($order)) {
                return null;
            }
            return $order;
        } catch (\Exception $ex) {
            Yii::error('Order unserialize exception: ' . $ex->getMessage());
        }
    }

    public static function filterAddress($addressArray)
    {
        if (is_array($addressArray)) {
            $newArray = ['address' => []];
            foreach ($addressArray['address'] as $address) {
                $newAddress = [];
                foreach ($address as $key => $paramVal) {
                    if (!empty($paramVal)) {
                        $paramVal = trim(strip_tags(stripcslashes($paramVal)));

                        if ($key == "city") {
                            $paramVal = mb_substr($paramVal, 0, 50, 'utf-8');
                        }
                        if ($key == "street") {
                            $paramVal = mb_substr($paramVal, 0, 100, 'utf-8');
                        }
                        if (in_array($key, ['house', 'housing', 'porch', 'apt'])) {
                            $paramVal = mb_substr($paramVal, 0, 10, 'utf-8');
                        }
                    }
                    $newAddress[$key] = $paramVal;
                }
                $newArray['address'] [] = $newAddress;
            }

            return $newArray;
        }
    }


    /**
     * Фильтрация отдачи инфы по заказу
     * @param $orderData
     * @param $tenantId
     * @param $language
     * @return mixed
     * @throws PromoException
     * @throws \api\components\orderApi\exceptions\OrderApiException
     * @throws \yii\base\InvalidConfigException
     */
    public static function filterOrderData($orderData, $tenantId, $language)
    {
        //Добавляем инфу о скидке промокода, если есть
        $orderData['promo_code_discount'] = null;

        if ($orderData['promo_code_id']) {
            /* @var $orderApi OrderApi */
            $orderApi  = Yii::$app->get('orderApi');
            $promoCode = $orderApi->getInfoPromoCode($orderData['promo_code_id']);

            if ($promoCode['code'] !== TaxiErrorCode::OK) {
                throw new PromoException('Discount failed');
            }

            if ($promoCode['result']['promo']['activation_type_id'] == Promo::FIELD_TYPE_ACTIVATION_DISCOUNT) {
                $orderData['promo_code_discount'] = $promoCode['result']['promo']['client_discount'];
            }
        }


        //Проверям, нужно ли добавить инфу по бонсуам клиента
        if (!empty($orderData['bonus_payment'])
            && $orderData['payment'] !== 'CORP_BALANCE'
        ) {
            $bonusData = Client::getBonusPayment(
                $tenantId,
                $orderData['order_id'],
                $orderData['client_id'],
                $orderData['currency_id'],
                $orderData['tariff_id']
            );
        } else {
            $bonusData = [];
        }

        $cityId     = $orderData['city_id'];
        $positionId = $orderData['position_id'];


        $propertyForDelete = [
            //'city_id',
            'tariff_id',
            'parking_id',
            //'worker_id',
            'car_id',
            'user_id',
            'user_create',
            'status_id',
            'user_modifed',
            'create_time',
            'updated_time',
            'client_device_token',
            'userCreated',
            'city',
            'worker',
            'car',
            'call_warning_id',
            'status_time',
            'time_to_client',
            'tenant_id',
            'company_id',
            'client_id',
            'predv_price',
            'predv_distance',
            'predv_time',
            'workers_offered',
            'workers_offered_responses',
            'server_id',
            'process_id',


        ];

        foreach ($propertyForDelete as $property) {
            if (array_key_exists($property, $orderData)) {
                unset($orderData[$property]);
            }
        }

        if (isset($orderData['status']['dispatcher_sees'])) {
            unset($orderData['status']['dispatcher_sees']);
        }
        if (isset($orderData['tariff']['tenant_id'])) {
            unset($orderData['tariff']['tenant_id']);
        }
        if (isset($orderData['tariff']['block'])) {
            unset($orderData['tariff']['block']);
        }
        if (isset($orderData['tariff']['class']['class'])) {
            $orderData['tariff']['class']['class'] = Yii::t('car', $orderData['tariff']['class']['class'], null,
                $language);
        }

        if (!empty($orderData['options'])) {
            $newOptions = [];
            foreach ($orderData['options'] as $optionData) {
                if (isset($optionData["name"])) {
                    $optionData["name"] = Yii::t('car-options', $optionData["name"], null, $language);
                }
                if (isset($optionData["additionalOptions"])) {
                    $additionalOptions = $optionData["additionalOptions"];
                    if (isset($additionalOptions[0])) {
                        if (isset($additionalOptions[0]["price"])) {
                            $optionData["price"] = $additionalOptions[0]["price"];
                        }
                    }
                }
                unset($optionData["additionalOptions"]);
                unset($optionData["option_id"]);
                $newOptions[] = $optionData;
            }
            $orderData['options'] = $newOptions;
        }

        if (isset($orderData['client']['client_id'])) {
            unset($orderData['client']['client_id']);
        }

        if (empty($orderData['deny_refuse_order'])) {
            $orderData['deny_refuse_order'] = '0';
        }

        if (empty($orderData['tariff']['auto_downtime'])) {
            $orderData['tariff']['auto_downtime'] = '0';
        }

        $showWorkerPhone = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_SHOW_WORKER_PHONE, $cityId, $positionId);

        if (isset($orderData['costData']['is_fix'])) {
            $orderData['costData']['is_fix'] = (int)$orderData['costData']['is_fix'];
        }

        if (isset($showWorkerPhone)) {
            if ($showWorkerPhone == "0") {
                if (isset($orderData["phone"])) {
                    unset($orderData["phone"]);
                }
            }
        } else {
            if (isset($orderData["phone"])) {
                unset($orderData["phone"]);
            }
        }
        unset($orderData['show_phone']);

        try {
            $orderData['address'] = unserialize($orderData['address']);
        } catch (\Exception $exc) {
            Yii::error($exc);
        }

        $showForWorkerFioClient = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_SHOW_WORKER_CLIENT, $cityId, $positionId);
        if (empty($showForWorkerFioClient)) {
            unset($orderData['client']);
        }

        $orderData['order_time'] = date('d.m.Y H:i', $orderData['order_time']);

        //Нужно отфильтровать тарифы от лишних полей в зависимости от isDay
        if (isset($orderData['costData']['tariffInfo']['isDay'])) {
            $isDay         = $orderData['costData']['tariffInfo']['isDay'];
            $tariffInCity  = isset($orderData['costData']['tariffInfo']['tariffDataCity']) ? $orderData['costData']['tariffInfo']['tariffDataCity'] : null;
            $tariffOutCity = isset($orderData['costData']['tariffInfo']['tariffDataTrack']) ? $orderData['costData']['tariffInfo']['tariffDataTrack'] : null;
            $tariffArr     = [
                'inCity'  => $tariffInCity,
                'outCity' => $tariffOutCity,
            ];

            $newTariffArr = [];
            foreach ($tariffArr as $key => $tariff) {
                if ($isDay == 1) {
                    $propertyForDelete = [
                        'planting_price_night',
                        'planting_include_night',
                        'next_km_price_night',
                        'min_price_night',
                        'supply_price_night',
                        'wait_time_night',
                        'wait_driving_time_night',
                        'wait_price_night',
                        'speed_downtime_night',
                        'rounding_night',
                        'allow_day_night',
                        'active',
                        'next_cost_unit_night',
                        'next_km_price_night_time',
                        'planting_include_night_time',
                        'rounding_type_night',
                    ];
                } else {
                    $propertyForDelete = [
                        'planting_price_day',
                        'planting_include_day',
                        'next_km_price_day',
                        'min_price_day',
                        'supply_price_day',
                        'wait_time_day',
                        'wait_driving_time_day',
                        'wait_price_day',
                        'speed_downtime_day',
                        'rounding_day',
                        'allow_day_night',
                        'active',
                        'next_cost_unit_day',
                        'next_km_price_day_time',
                        'planting_include_day_time',
                        'rounding_type_day',
                    ];
                }
                foreach ($propertyForDelete as $property) {
                    if (array_key_exists($property, $tariff)) {
                        unset($tariff["$property"]);
                    }
                }
                $newTariffArr[$key] = $tariff;
            }
            if (isset($newTariffArr['inCity']['accrual'])) {
                if ($newTariffArr['inCity']['accrual'] == "INTERVAL") {
                    if (isset($newTariffArr['inCity']['next_km_price_day'])) {
                        $nextKm                                      = unserialize($newTariffArr['inCity']['next_km_price_day']);
                        $newTariffArr['inCity']['next_km_price_day'] = $nextKm;
                    } else {
                        if (isset($newTariffArr['inCity']['next_km_price_night'])) {
                            $nextKm                                        = unserialize($newTariffArr['inCity']['next_km_price_night']);
                            $newTariffArr['inCity']['next_km_price_night'] = $nextKm;
                        }
                    }
                }

                #todo 1
                 if($newTariffArr['inCity']['accrual'] == "DISTANCE"){
                    if (isset($newTariffArr['inCity']['next_km_price_day'])) {
                        $nextKm   = is_numeric($newTariffArr['inCity']['next_km_price_day']) ? (float)$newTariffArr['inCity']['next_km_price_day'] : 0.0;
                        $newTariffArr['inCity']['next_km_price_day'] = $nextKm;
                    } else {
                        if (isset($newTariffArr['inCity']['next_km_price_night'])) {
                            $nextKm   = is_numeric($newTariffArr['inCity']['next_km_price_night']) ? (float)$newTariffArr['inCity']['next_km_price_night'] : 0.0;
                            $newTariffArr['inCity']['next_km_price_night'] = $nextKm;
                        }
                    }
                }
            }
            if (isset($newTariffArr['outCity']['accrual'])) {
                if ($newTariffArr['outCity']['accrual'] == "INTERVAL") {
                    if (isset($newTariffArr['outCity']['next_km_price_day'])) {
                        $nextKm                                       = unserialize($newTariffArr['outCity']['next_km_price_day']);
                        $newTariffArr['outCity']['next_km_price_day'] = $nextKm;
                    } else {
                        if (isset($newTariffArr['outCity']['next_km_price_night'])) {
                            $nextKm                                         = unserialize($newTariffArr['outCity']['next_km_price_night']);
                            $newTariffArr['outCity']['next_km_price_night'] = $nextKm;
                        }
                    }
                }

                #todo 2
                if($newTariffArr['outCity']['accrual'] == "DISTANCE"){
                    if (isset($newTariffArr['outCity']['next_km_price_day'])) {
                        $nextKm   = is_numeric($newTariffArr['outCity']['next_km_price_day']) ? (float)$newTariffArr['outCity']['next_km_price_day'] : 0.0;
                        $newTariffArr['outCity']['next_km_price_day'] = $nextKm;
                    } else {
                        if (isset($newTariffArr['outCity']['next_km_price_night'])) {
                            $nextKm   = is_numeric($newTariffArr['outCity']['next_km_price_night']) ? (float)$newTariffArr['outCity']['next_km_price_night'] : 0.0;
                            $newTariffArr['outCity']['next_km_price_night'] = $nextKm;
                        }
                    }
                }
            }

            $orderData['costData']['tariffInfo']['tariffDataCity']  = $newTariffArr['inCity'];
            $orderData['costData']['tariffInfo']['tariffDataTrack'] = $newTariffArr['outCity'];
        }
        $orderData['bonusData'] = $bonusData;

        return $orderData;
    }

    public static function saveOrderToRedis($orderId, $costData)
    {
        /**
         * @var $workerService WorkerService
         */
        $workerService = \Yii::$container->get(WorkerService::className());

        $tariffType = isset($costData["tariffInfo"] ["tariffType"]) ? $costData["tariffInfo"] ["tariffType"] : null;
        $tariffId   = isset($costData["tariffInfo"]["tariffDataCity"]["tariff_id"]) ? $costData["tariffInfo"]["tariffDataCity"]["tariff_id"] : null;

        //АЛЕКСЕЮ ТАК ЖЕ ПРИ ДОБАВЛЕНИИ ЗАКАЗА ПИСТЬ В РЕДИС ВОТ ТАКУЮ Штуку
        $order = self::find()
            ->where([
                'order_id' => $orderId,
            ])
            ->with([
                'status',
                'tariff',
                'tariff.class'              => function ($query) {
                    $query->select(['class_id', 'class']);
                },
                'client'                    => function ($query) {
                    $query->select(['client_id', 'last_name', 'name', 'second_name', 'lang', 'send_to_email', 'email']);
                },
                'userCreated'               => function ($query) {
                    $query->select(['user_id', 'last_name', 'name', 'second_name']);
                },
                'options.additionalOptions' => function ($query) use ($tariffId) {
                    $query->select(['id', 'additional_option_id', 'price'])->where([
                        'tariff_id'   => $tariffId,
                        'tariff_type' => "CURRENT",
                    ]);
                },
                'tenant'                    => function ($query) {
                    $query->select(['domain', 'company_name', 'contact_phone', 'contact_email', 'site']);
                },
                'worker',
            ])
            ->asArray()
            ->one();

        $order['tenant_login'] = $order['tenant']['domain'];
        $order['costData']     = $costData;
        if (!empty($order['worker'])) {
            $worker = $workerService->getWorkerFromRedis(
                $order['tenant_id'], $order['worker']['callsign']);

            $order["car_id"] = $worker["car"]["car_id"];
            $order["worker"] = $worker["worker"];
            $order["car"]    = $worker["car"];
        }
        $orderSerialized = serialize($order);
        $result          = \Yii::$app->redis_orders_active->executeCommand('hset',
            [$order['tenant_id'], $orderId, $orderSerialized]);

        return $result;
    }

    public static function getOrderNumber($orderId)
    {
        return self::find()
            ->select('order_number')
            ->where(['order_id' => $orderId])
            ->scalar();
    }

    protected function delay()
    {
        usleep(mt_rand(0, 500000));
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->isNewRecord) {
            $retryCount = \Yii::$app->params['saveOrder.retryCount'];

            for ($i = 0; $i < $retryCount; $i++) {
                $this->order_number = new Expression('(SELECT * FROM (SELECT COALESCE (MAX(order_number), 0) FROM tbl_order WHERE tenant_id = ' . $this->tenant_id . ' FOR UPDATE) AS t) + 1');
                try {
                    $res = parent::save($runValidation, $attributeNames)
                        && $this->order_number = (int)self::getOrderNumber($this->order_id);

                    return $res;
                } catch (Exception $ex) {
                    if (strpos($ex->getMessage(), 'ui_order__tenant_id__order_number') === false
                        && strpos($ex->getMessage(),'Deadlock found when trying to get lock; try restarting transaction') === false) {
                        throw $ex;
                    } else {
                        $this->delay();
                        continue;
                    }
                }
            }

            return false;
        }

        return parent::save($runValidation, $attributeNames);
    }
}
