<?php

namespace app\models\order;

use api\models\order\exceptions\SavingOrderTrackException;
use app\models\order\ar\mongodb\OrderTrack;

/**
 * Class OrderTrackService
 * @package app\models\order
 */
class OrderTrackService
{
    /**
     * Getting order track
     *
     * @param int $orderId
     *
     * @return array
     */
    public function getTrack($orderId)
    {
        $record = OrderTrack::find()
            ->where(['order_id' => $orderId])
            ->asArray()
            ->one();

        return empty($record['tracking']) ? [] : $record['tracking'];
    }

    /**
     * Setting order track
     *
     * @param int   $orderId
     * @param array $track
     *
     * @return bool
     * @throws \api\models\order\exceptions\SavingOrderTrackException
     */
    public function setTrack($orderId, $track)
    {
        $record = OrderTrack::find()
            ->where(['order_id' => $orderId])
            ->one();

        if (empty($record)) {
            $record = new OrderTrack(['order_id' => $orderId]);
        }

        $track = array_map(function ($item) {
            if (isset($item['lat'])) {
                $item['lat'] = (string)round($item['lat'], 5);
            }

            if (isset($item['lat'])) {
                $item['lon'] = (string)round($item['lon'], 5);
            }

            return $item;
        }, $track);

        $record->tracking = $track;
        $record->time     = time();

        if (!$record->save()) {
            throw new SavingOrderTrackException(implode('; ', $record->getFirstErrors()));
        }
    }
}