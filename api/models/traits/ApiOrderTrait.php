<?php
namespace app\models\traits;

use app\models\balance\Transaction;
use app\models\order\Order;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $order_id
 * @property integer $tenant_id
 * @property integer $worker_id
 * @property integer $city_id
 * @property integer $tariff_id
 * @property integer $client_id
 * @property string $phone
 * @property integer $user_create
 * @property integer $status_id
 * @property integer $user_modifed
 * @property integer $company_id
 * @property integer $parking_id
 * @property string $address
 * @property string $comment
 * @property string $predv_price
 * @property string $predv_distance
 * @property string $predv_time
 * @property string $device
 * @property string $order_number
 * @property string $payment
 * @property integer $show_phone
 * @property string $create_time
 * @property string $updated_time
 * @property string $order_time
 * @property string $time_to_client
 * @property integer $time_offset
 * @property integer $position_id
 * @property integer $is_fix
 * @property integer $update_time
 * @property integer $deny_refuse_order
 * @property integer $promo_code_id
 * @property float $predv_price_no_discount
 * @property integer $processed_exchange_program_id
 * @property integer $bonus_payment
 * @property integer $orderAction
 */
trait ApiOrderTrait
{
    public function getCityId()
    {
        return $this->city_id;
    }

    public function getOrderNow()
    {
        return 1;
    }

    public function getOrderDate()
    {
        return $this->order_date;
    }

    public function getOrderHours()
    {
        return $this->order_hours;
    }

    public function getOrderMinutes()
    {
        return $this->order_minutes;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getClientId()
    {
        return $this->client_id;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function getPayment()
    {
        return Transaction::CASH_PAYMENT;
    }

    public function getBonusPayment()
    {
        return $this->bonus_payment;
    }

    public function getPositionId()
    {
        return $this->position_id;
    }

    public function getTariffId()
    {
        return $this->tariff_id;
    }

    public function getPredvPrice()
    {
        return $this->predv_price;
    }

    public function getPredvDistance()
    {
        return $this->predv_distance;
    }

    public function getPredvTime()
    {
        return $this->predv_time;
    }

    public function getPredvPriceNoDiscount()
    {
        return $this->predv_price_no_discount;
    }

    public function getIsFix()
    {
        return $this->is_fix;
    }

    public function getParkingId()
    {
        return $this->parking_id;
    }

    public function getCompanyId()
    {
        return $this->company_id;
    }

    public function getDevice()
    {
        return $this->device;
    }

    public function getOrderAction()
    {
        return Order::ORDER_ACTION;
    }

    public function getAddressOriginal()
    {
        return $this->address;
    }

    public function getTenantId()
    {
        return $this->tenant_id;
    }

    public function getUserCreatedId()
    {
        return null;
    }

    public function getUserModifedId()
    {
        return null;
    }

    public function getCallId()
    {
        return null;
    }

    public function getWorkerId()
    {
        return $this->worker_id;
    }

    public function getCarId()
    {
        return $this->car_id;
    }

    public function getAdditionalOption()
    {
        return $this->additional_option;
    }
}
