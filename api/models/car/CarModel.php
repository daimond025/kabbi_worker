<?php

namespace app\models\car;

use Yii;

/**
 * This is the model class for table "tbl_car_model".
 *
 * @property integer $model_id
 * @property integer $brand_id
 * @property string $name
 * @property integer $type_id
 *
 * @property TransportType $type
 * @property CarBrand $brand
 */
class CarModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_car_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_id', 'name'], 'required'],
            [['brand_id', 'type_id'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['brand_id', 'name'], 'unique', 'targetAttribute' => ['brand_id', 'name'], 'message' => 'The combination of Brand ID and Name has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_id' => 'Model ID',
            'brand_id' => 'Brand ID',
            'name' => 'Name',
            'type_id' => 'Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TransportType::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(CarBrand::className(), ['brand_id' => 'brand_id']);
    }
}
