<?php

namespace app\models\car;

use app\components\behavior\ActiveRecordBehavior;
use app\models\address\City;
use app\models\tenant\Tenant;
use app\models\worker\Worker;
use app\models\worker\WorkerHasCar;
use Yii;

/**
 * This is the model class for table "tbl_car".
 *
 * @property string         $car_id
 * @property string         $tenant_id
 * @property integer        $class_id
 * @property string         $city_id
 * @property string         $name
 * @property string         $gos_number
 * @property string         $color
 * @property string         $year
 * @property string         $photo
 * @property integer        $owner
 * @property integer        $raiting
 * @property string         $create_time
 * @property integer        $active
 * @property string         $license
 * @property string         $license_scan
 *
 * @property CarClass       $class
 * @property Tenant         $tenant
 * @property City           $city
 * @property CarHasOption[] $tblCarHasOptions
 * @property CarOption[]    $options
 * @property Driver[]       $tblDrivers
 */
class Car extends \yii\db\ActiveRecord
{
    const OWNER_COMPANY = 'COMPANY';
    const OWNER_WORKER = 'WORKER';

    public $brand;
    public $model_id;
    public $options = [];
    public $new_options = 0;
    public $block;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'gos_number', 'year', 'owner'], 'required'],
            [['tenant_id', 'class_id', 'city_id', 'raiting', 'active'], 'integer'],
            [['year', 'brand', 'options', 'block', 'new_options'], 'safe'],
            [['gos_number', 'color'], 'string', 'max' => 10],
            [['license'], 'string', 'max' => 45],
            [['photo', 'license_scan'], 'file', 'extensions' => ['png', 'jpeg', 'jpg']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'car_id'       => Yii::t('car', 'Car ID'),
            'tenant_id'    => Yii::t('car', 'Tenant ID'),
            'class_id'     => Yii::t('car', 'Type of car'),
            'city_id'      => Yii::t('car', 'City'),
            'name'         => Yii::t('car', 'Car name'),
            'gos_number'   => Yii::t('car', 'Gos Number'),
            'color'        => Yii::t('car', 'Color'),
            'year'         => Yii::t('car', 'Year'),
            'photo'        => Yii::t('car', 'Photo'),
            'owner'        => Yii::t('car', 'Whose car'),
            'raiting'      => Yii::t('car', 'Priority'),
            'create_time'  => Yii::t('car', 'Create Time'),
            'active'       => Yii::t('car', 'Active'),
            'license'      => Yii::t('car', 'License'),
            'license_scan' => Yii::t('car', 'Scan'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarHasOptions()
    {
        return $this->hasMany(CarHasOption::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(CarOption::className(), ['option_id' => 'option_id'])->viaTable('{{%car_has_option}}',
            ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasCars()
    {
        return $this->hasMany(WorkerHasCar::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkers()
    {
        return $this->hasMany(Worker::className(), ['worker_id' => 'worker_id'])->via('workerHasCars');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCanWatchOrderWithCarClassId()
    {
        return $this->hasMany(CarHasWorkerGroupClass::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerTariffs()
    {
        return $this->hasMany(CarHasWorkerGroupTariff::className(), ['car_id' => 'car_id']);
    }

    public function getCarModel()
    {
        return $this->hasOne(CarModel::className(), ['model_id' => 'model_id']);
    }

    public function behaviors()
    {
        return [
            'common' => [
                'class' => ActiveRecordBehavior::className(),
            ],
        ];
    }

    /**
     * For getting variants for field $owner
     * @return array
     */
    public function getVariantsOfOwnerCar()
    {
        return [
            'COMPANY' => t('car', 'Company'),
            'DRIVER'  => t('car', 'Driver'),
        ];
    }

    /**
     * For getting variants of car color
     * @return array
     */
    public function getColorVariants()
    {
        return [
            'black' => t('car', 'Black'),
            'blue'  => t('car', 'Blue'),
            'red'   => t('car', 'Red'),
            'white' => t('car', 'White'),
        ];
    }

    /**
     * Translate color form db value.
     *
     * @param string $color
     *
     * @return string
     */
    public function getColorText($color)
    {
        $variants = $this->getColorVariants();

        return $variants[$color];
    }

    public static function getGridData($cars, $city_list = true, $type_list = true)
    {
        $arCars['CARS'] = [];

        if ($city_list) {
            $arCity = [];
        }

        if ($type_list) {
            $arTypes = [];
        }

        foreach ($cars as $car) {
            if ($city_list && !array_key_exists($car->city['city_id'], $arCity)) {
                $arCity[$car->city['city_id']] = $car->city['name'];
            }

            if ($type_list && !array_key_exists($car->class['class_id'], $arTypes)) {
                $arTypes[$car->class['class_id']] = $car->class['class'];
            }

            $arCars['CARS'][] = [
                'ID'         => $car->car_id,
                'PHOTO'      => $car->getPictureHtml($car->photo),
                'NAME'       => $car->name,
                'RAITING'    => $car->raiting,
                'GOV_NUMBER' => $car->gos_number,
                'CLASS'      => $car->class['class'],
            ];
        }

        if ($city_list) {
            asort($arCity);
            $arCars['CITI_LIST'] = $arCity;
        }

        if ($type_list) {
            asort($arTypes);
            $arCars['TYPE_LIST'] = $arTypes;
        }

        return $arCars;
    }

    /**
     * It needs for car form.
     * @return array
     */
    public function getCarParams()
    {
        //Массив возвращаемых параметров
        $data = [];

        //Получение списка моделей городов тенанта
        $tenant_city_list = \app\modules\tenant\models\TenantHasCity::getCityList();

        foreach ($tenant_city_list as $tenant_city_model) {
            $data['TENANT_CITY_LIST'][$tenant_city_model['city_id']] = $tenant_city_model['city']['name'];
        }

        //Получение списка типов авто
        $data['CAR_TYPES'] = \yii\helpers\ArrayHelper::map(\app\modules\car\models\CarClass::find()->all(), 'class_id',
            'class');

        //Получение списка марок авто
        $data['BRAND_LIST'] = \yii\helpers\ArrayHelper::map(\app\modules\car\models\CarBrand::find()->all(), 'brand_id',
            'name');

        //При необходимости модельный ряд авто дополняется самостоятельно. Отсюда удалять нельзя т.к. ключ участвует в форме.
        $data['MODEL_LIST'] = [];

        //Получение списка опций авто, которые выбрал тенант для работы
        $data['TENANT_CAR_OPTION'] = [];
        $tenant_car_options = \app\modules\tenant\models\TenantHasCarOption::find()->where(['tenant_id' => user()->tenant_id])->with('option')->all();

        foreach ($tenant_car_options as $option_model) {
            $data['TENANT_CAR_OPTION'][$option_model->option_id] = [
                'NAME'  => $option_model->option['name'],
                'VALUE' => $option_model->value,
            ];
        }

        return $data;
    }

    /**
     *
     * @param array $arOptions [option_id, option_id...]
     *
     * @return integer
     */
    public function calculateRating($arOptions)
    {
        $rating = 0;

        if (!empty($arOptions)) {
            $tenant_car_options = \app\modules\tenant\models\TenantHasCarOption::find()->where(['tenant_id' => user()->tenant_id])->all();
            $options = \yii\helpers\ArrayHelper::map($tenant_car_options, 'option_id', 'value');

            foreach ($arOptions as $option_id) {
                $rating += $options[$option_id];
            }
        }

        return $rating;
    }

    public function beforeSave($insert)
    {
        if ($this->new_options) {
            $this->raiting = $this->calculateRating($this->options);
        }

        return parent::beforeSave($insert);
    }

    public static function isCarBusy($tenantId, $carId)
    {
        $carBusyArr = \Yii::$app->redis_workers->executeCommand('HVALS', [$tenantId]);
        if (!empty($carBusyArr)) {
            foreach ($carBusyArr as $carData) {
                $carData = unserialize($carData);
                $redisCarId = isset($carData['car']['car_id']) ? $carData['car']['car_id'] : null;
                if ($redisCarId == $carId) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Получение url photo of car
     *
     * @param int    $carId
     * @param string $tenantLogin
     *
     * @return string|null
     */
    public static function getPhotoUrl($carId, $tenantLogin)
    {
        $carData = self::findOne($carId);
        if (!empty($carData)) {
            $photo = $carData->photo;
            if (!empty($photo)) {
                $url = Yii::$app->params['frontend.protocol'] . '://' . $tenantLogin . '.' . Yii::$app->params['frontend.domain'] . '/file/show-external-file?filename=thumb_' . $photo . '&id=' . $carData->tenant_id;

                return $url;
            }
        }

        return null;
    }

    public function isCompanyCar()
    {
        return $this->owner === Car::OWNER_COMPANY;
    }

}
