<?php

namespace app\models\car;

use Yii;

/**
 * This is the model class for table "tbl_car_color".
 *
 * @property integer $color_id
 * @property string  $name
 *
 * @property Car[]   $cars
 */
class CarColor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_car_color';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'color_id' => 'Color ID',
            'name'     => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['color' => 'color_id']);
    }

    /**
     * Getting car color name
     *
     * @param $carColorId
     *
     * @return null|string
     */
    public static function getCarColorName($carColorId)
    {
        return self::find()
            ->select('name')
            ->where(['color_id' => $carColorId])
            ->scalar();
    }
}
