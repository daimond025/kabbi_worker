<?php

namespace app\models\car;

use app\models\worker\WorkerTariff;
use Yii;

/**
 * This is the model class for table "tbl_car_has_worker_group_tariff".
 *
 * @property integer      $car_id
 * @property integer      $tariff_id
 *
 * @property Car          $car
 * @property WorkerTariff $tariff
 */
class CarHasWorkerGroupTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_car_has_worker_group_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_id', 'tariff_id'], 'required'],
            [['car_id', 'tariff_id'], 'integer'],
            [
                ['car_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Car::className(),
                'targetAttribute' => ['car_id' => 'car_id'],
            ],
            [
                ['tariff_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => WorkerTariff::className(),
                'targetAttribute' => ['tariff_id' => 'tariff_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'car_id'    => 'Car ID',
            'tariff_id' => 'Tariff ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(WorkerTariff::className(), ['tariff_id' => 'tariff_id']);
    }
}
