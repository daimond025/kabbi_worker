<?php

namespace app\models\car;

use Yii;

/**
 * This is the model class for table "tbl_car_has_worker_group_class".
 *
 * @property integer  $car_id
 * @property integer  $class_id
 *
 * @property Car      $car
 * @property CarClass $class
 */
class CarHasWorkerGroupClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_car_has_worker_group_class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_id', 'class_id'], 'required'],
            [['car_id', 'class_id'], 'integer'],
            [
                ['car_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Car::className(),
                'targetAttribute' => ['car_id' => 'car_id'],
            ],
            [
                ['class_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CarClass::className(),
                'targetAttribute' => ['class_id' => 'class_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'car_id'   => 'Car ID',
            'class_id' => 'Class ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }
}
