<?php

namespace app\models\car;

use Yii;

/**
 * This is the model class for table "{{%car_brand}}".
 *
 * @property integer $brand_id
 * @property string $name
 *
 */
class CarBrand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_brand}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 45],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'brand_id' => Yii::t('car', 'Brand ID'),
            'name'     => Yii::t('car', 'Name'),
        ];
    }
}
