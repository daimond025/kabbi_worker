<?php

namespace app\models\car;

use app\models\order\Order;
use app\models\order\OrderHasOption;
use Yii;

/**
 * This is the model class for table "{{%tbl_car_option}}".
 *
 * @property integer                 $option_id
 * @property string                  $name
 *
 * @property TblAdditionalOption[]   $tblAdditionalOptions
 * @property TblCarHasOption[]       $tblCarHasOptions
 * @property TblCar[]                $cars
 * @property TblOrderHasOption[]     $tblOrderHasOptions
 * @property TblOrder[]              $orders
 * @property TblTenantHasCarOption[] $tblTenantHasCarOptions
 * @property TblTenant[]             $tenants
 */
class CarOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id' => Yii::t('car', 'Option ID'),
            'name'      => Yii::t('car', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalOptions()
    {
        return $this->hasMany(\app\models\tariff\AdditionalOption::className(),
            ['additional_option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarHasOptions()
    {
        return $this->hasMany(CarHasOption::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['car_id' => 'car_id'])->via('carHasOptions');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHasOptions()
    {
        return $this->hasMany(OrderHasOption::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['order_id' => 'order_id'])->via('OrderHasOptions');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblTenantHasCarOptions()
    {
        return $this->hasMany(TblTenantHasCarOption::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenants()
    {
        return $this->hasMany(TblTenant::className(),
            ['tenant_id' => 'tenant_id'])->viaTable('{{%tbl_tenant_has_car_option}}', ['option_id' => 'option_id']);
    }
}
