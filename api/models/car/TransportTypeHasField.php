<?php

namespace app\models\car;

use Yii;

/**
 * This is the model class for table "tbl_transport_type_has_field".
 *
 * @property integer $id
 * @property integer $type_id
 * @property integer $field_id
 *
 * @property TransportFieldEnum[] $transportFieldEnums
 * @property TransportTypeFieldValue[] $transportTypeFieldValues
 * @property TransportField $field
 * @property TransportType $type
 */
class TransportTypeHasField extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_transport_type_has_field';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'field_id'], 'required'],
            [['type_id', 'field_id'], 'integer'],
            [['type_id', 'field_id'], 'unique', 'targetAttribute' => ['type_id', 'field_id'], 'message' => 'The combination of Type ID and Field ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Type ID',
            'field_id' => 'Field ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransportFieldEnums()
    {
        return $this->hasMany(TransportFieldEnum::className(), ['type_has_field_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransportTypeFieldValues()
    {
        return $this->hasMany(TransportTypeFieldValue::className(), ['has_field_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getField()
    {
        return $this->hasOne(TransportField::className(), ['field_id' => 'field_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TransportType::className(), ['type_id' => 'type_id']);
    }
}
