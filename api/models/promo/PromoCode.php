<?php

namespace app\models\promo;



/**
 * This is the model class for table "{{%promo_code}}".
 *
 * @property integer     $code_id
 * @property integer     $promo_id
 * @property string      $code
 * @property integer     $created_at
 * @property integer     $updated_at
 * @property integer     $worker_id
 * @property integer     $car_class_id
 *
 */
class PromoCode extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_code}}';
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromo()
    {
        return $this->hasOne(Promo::className(), ['promo_id' => 'promo_id']);
    }


}
