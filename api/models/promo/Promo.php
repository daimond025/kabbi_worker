<?php

namespace app\models\promo;

/**
 * This is the model class for table "{{%promo}}".
 *
 * @property integer              $promo_id
 * @property integer              $tenant_id
 * @property integer              $type_id
 * @property integer              $position_id
 * @property string               $name
 * @property integer              $period_type_id
 * @property integer              $period_start
 * @property integer              $period_end
 * @property integer              $action_count_id
 * @property integer              $action_clients_id
 * @property integer              $action_no_other_codes
 * @property integer              $activation_type_id
 * @property integer              $client_bonuses
 * @property integer              $client_discount
 * @property integer              $worker_commission
 * @property integer              $worker_bonuses
 * @property integer              $blocked
 * @property integer              $created_at
 * @property integer              $updated_at
 * @property integer              $count_codes
 * @property integer              $count_symbols_in_code
 */
class Promo extends \yii\db\ActiveRecord
{
    const FIELD_PROMO_TYPE_MANUALLY = 1;
    const FIELD_PROMO_TYPE_GENERATION = 2;
    const FIELD_PROMO_TYPE_DRIVERS = 3;

    const FIELD_PERIOD_NO_LIMITED = 1;
    const FIELD_PERIOD_LIMITED = 2;

    const FIELD_TYPE_ACTIVATION_BONUSES = 1;
    const FIELD_TYPE_ACTIVATION_DISCOUNT = 2;

    const FIELD_TYPE_PROMO_MANUALLY = 1;
    const FIELD_TYPE_PROMO_GENERATION = 2;
    const FIELD_TYPE_PROMO_DRIVERS = 3;

    const FIELD_ACTION_NO_OTHER_CODES = 1;

    const SCENARIO_UPDATE = 'scenarioUpdate';

    const FORMAT_DATE = 'd.m.Y';
    const FORMAT_TIME = 'H:i';
    const FORMAT_DATETIME = 'd.m.YH:i';


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }




    public function getPromoCodes()
    {
        return $this->hasMany(PromoCode::className(), ['promo_id' => 'promo_id']);
    }


}
