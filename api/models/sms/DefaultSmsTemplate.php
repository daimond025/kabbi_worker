<?php

namespace app\models\sms;

use Yii;

/**
 * This is the model class for table "tbl_default_sms_template".
 *
 * @property integer $template_id
 * @property string $type
 * @property string $text
 * @property string $description
 */
class DefaultSmsTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_default_sms_template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'text'], 'required'],
            [['text', 'description'], 'string'],
            [['type'], 'string', 'max' => 45],
            [['type'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'template_id' => 'Template ID',
            'type' => 'Type',
            'text' => 'Text',
            'description' => 'Description',
        ];
    }
}
