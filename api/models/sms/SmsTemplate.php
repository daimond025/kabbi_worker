<?php

namespace app\models\sms;

use Yii;

/**
 * This is the model class for table "tbl_sms_template".
 *
 * @property integer $template_id
 * @property integer $tenant_id
 * @property string $type
 * @property string $text
 *
 * @property Tenant $tenant
 */
class SmsTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_sms_template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'type', 'text'], 'required'],
            [['tenant_id'], 'integer'],
            [['text'], 'string'],
            [['type'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'template_id' => 'Template ID',
            'tenant_id' => 'Tenant ID',
            'type' => 'Type',
            'text' => 'Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }
    
}
