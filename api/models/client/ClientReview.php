<?php

namespace app\models\client;

use Yii;

/**
 * This is the model class for table "tbl_client_review".
 *
 * @property integer $review_id
 * @property integer $client_id
 * @property integer $order_id
 * @property string $text
 * @property double $rating
 * @property integer $create_time
 *
 * @property Client $client
 * @property Order $order
 */
class ClientReview extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_client_review';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'order_id'], 'required'],
            [['client_id', 'order_id'], 'integer'],
            [['text'], 'string'],
            [['rating'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'review_id'   => 'Review ID',
            'client_id'   => 'Client ID',
            'order_id'    => 'Order ID',
            'text'        => 'Text',
            'rating'      => 'Rating',
            'create_time' => 'Create Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

}
