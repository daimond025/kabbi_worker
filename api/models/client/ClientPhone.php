<?php

namespace app\models\client;

use Yii;

/**
 * This is the model class for table "tbl_client_phone".
 *
 * @property string $phone_id
 * @property string $client_id
 * @property string $value
 *
 * @property Client $client
 * @property Order[] $orders
 */
class ClientPhone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_client_phone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'value'], 'required'],
            [['client_id'], 'integer'],
            [['value'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'phone_id' => 'Phone ID',
            'client_id' => 'Client ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['phone_id' => 'phone_id']);
    }
}
