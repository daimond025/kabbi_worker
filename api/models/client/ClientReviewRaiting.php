<?php

namespace app\models\client;

use Yii;

/**
 * This is the model class for table "tbl_client_review_raiting".
 *
 * @property integer $raitng_id
 * @property integer $client_id
 * @property integer $one
 * @property integer $two
 * @property integer $three
 * @property integer $four
 * @property integer $five
 * @property integer $count
 *
 * @property Client $client
 */
class ClientReviewRaiting extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_client_review_raiting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id'], 'required'],
            [['client_id', 'one', 'two', 'three', 'four', 'five', 'count'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'raitng_id' => 'Raitng ID',
            'client_id' => 'Client ID',
            'one'       => 'One',
            'two'       => 'Two',
            'three'     => 'Three',
            'four'      => 'Four',
            'five'      => 'Five',
            'count'     => 'Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

}
