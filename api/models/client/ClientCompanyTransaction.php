<?php

namespace app\models\client;

use Yii;

/**
 * This is the model class for table "tbl_client_company_transaction".
 *
 * @property integer $transaction_id
 * @property integer $balance_id
 * @property string $operation
 * @property integer $transaction_time
 * @property string $value
 * @property string $comment
 * @property string $income_type
 * @property integer $user_create
 *
 * @property ClientCompanyBalance $balance
 * @property User $userCreate
 */
class ClientCompanyTransaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_client_company_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['balance_id', 'operation', 'transaction_time', 'value'], 'required'],
            [['balance_id', 'transaction_time', 'user_create'], 'integer'],
            [['operation', 'comment'], 'string'],
            [['value'], 'number'],
            [['income_type'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'transaction_id' => 'Transaction ID',
            'balance_id' => 'Balance ID',
            'operation' => 'Operation',
            'transaction_time' => 'Transaction Time',
            'value' => 'Value',
            'comment' => 'Comment',
            'income_type' => 'Income Type',
            'user_create' => 'User Create',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBalance()
    {
        return $this->hasOne(ClientCompanyBalance::className(), ['balance_id' => 'balance_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreate()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_create']);
    }
}
