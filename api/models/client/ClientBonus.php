<?php

namespace app\models\client;

use Yii;

/**
 * This is the model class for table "tbl_client_bonus".
 *
 * @property integer $bonus_id
 * @property integer $city_id
 * @property integer $class_id
 * @property integer $tenant_id
 * @property string $name
 * @property string $actual_date
 * @property string $bonus_type
 * @property string $bonus
 * @property string $min_cost
 * @property string $payment_method
 * @property string $max_payment_type
 * @property string $max_payment
 * @property string $bonus_app_type
 * @property string $bonus_app
 * @property integer $blocked
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property CarClass $class
 * @property Tenant $tenant
 * @property ClientBonusHasTariff[] $clientBonusHasTariffs
 * @property OrderDetailCost[] $orderDetailCosts
 */
class ClientBonus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_client_bonus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'class_id', 'tenant_id', 'name', 'bonus', 'created_at', 'updated_at'], 'required'],
            [['city_id', 'class_id', 'tenant_id', 'blocked', 'created_at', 'updated_at'], 'integer'],
            [['actual_date', 'bonus_type', 'payment_method', 'max_payment_type', 'bonus_app_type'], 'string'],
            [['bonus', 'min_cost', 'max_payment', 'bonus_app'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bonus_id' => 'Bonus ID',
            'city_id' => 'City ID',
            'class_id' => 'Class ID',
            'tenant_id' => 'Tenant ID',
            'name' => 'Name',
            'actual_date' => 'Actual Date',
            'bonus_type' => 'Bonus Type',
            'bonus' => 'Bonus',
            'min_cost' => 'Min Cost',
            'payment_method' => 'Payment Method',
            'max_payment_type' => 'Max Payment Type',
            'max_payment' => 'Max Payment',
            'bonus_app_type' => 'Bonus App Type',
            'bonus_app' => 'Bonus App',
            'blocked' => 'Blocked',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientBonusHasTariffs()
    {
        return $this->hasMany(ClientBonusHasTariff::className(), ['bonus_id' => 'bonus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDetailCosts()
    {
        return $this->hasMany(OrderDetailCost::className(), ['bonus_id' => 'bonus_id']);
    }
}
