<?php

namespace app\models\client;

use Yii;

/**
 * This is the model class for table "tbl_client_temp".
 *
 * @property string $client_temp_id
 * @property string $tenant_id
 * @property string $phone
 * @property string $password
 */
class ClientTemp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_client_temp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'phone', 'password'], 'required'],
            [['tenant_id'], 'integer'],
            [['phone'], 'string', 'max' => 30],
            [['password'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_temp_id' => 'Client Temp ID',
            'tenant_id' => 'Tenant ID',
            'phone' => 'Phone',
            'password' => 'Password',
        ];
    }
}
