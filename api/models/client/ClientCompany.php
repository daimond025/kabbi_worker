<?php

namespace app\models\client;

use Yii;

/**
 * This is the model class for table "tbl_client_company".
 *
 * @property integer $company_id
 * @property integer $tenant_id
 * @property string $name
 * @property string $full_name
 * @property string $legal_address
 * @property string $post_address
 * @property string $work_phone
 * @property string $inn
 * @property string $bookkeeper
 * @property string $kpp
 * @property string $ogrn
 * @property string $site
 * @property string $director
 * @property string $director_post
 * @property string $email
 * @property string $contact_last_name
 * @property string $contact_name
 * @property string $contact_second_name
 * @property string $contact_phone
 * @property string $contact_email
 * @property integer $block
 * @property string $logo
 *
 * @property Tenant $tenant
 * @property ClientCompanyBalance[] $clientCompanyBalances
 * @property ClientCompanyHasCity[] $clientCompanyHasCities
 * @property ClientCompanyHasTariff[] $clientCompanyHasTariffs
 * @property CarClass[] $tariffs
 * @property ClientHasCompany[] $clientHasCompanies
 * @property Client[] $clients
 * @property Order[] $orders
 */
class ClientCompany extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_client_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'name', 'legal_address'], 'required'],
            [['tenant_id', 'block'], 'integer'],
            [['name', 'full_name', 'legal_address', 'post_address', 'logo'], 'string', 'max' => 255],
            [['work_phone', 'inn'], 'string', 'max' => 15],
            [['bookkeeper', 'site', 'director', 'director_post', 'email', 'contact_last_name', 'contact_name', 'contact_second_name', 'contact_phone', 'contact_email'], 'string', 'max' => 45],
            [['kpp'], 'string', 'max' => 10],
            [['ogrn'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id'          => 'Company ID',
            'tenant_id'           => 'Tenant ID',
            'name'                => 'Name',
            'full_name'           => 'Full Name',
            'legal_address'       => 'Legal Address',
            'post_address'        => 'Post Address',
            'work_phone'          => 'Work Phone',
            'inn'                 => 'Inn',
            'bookkeeper'          => 'Bookkeeper',
            'kpp'                 => 'Kpp',
            'ogrn'                => 'Ogrn',
            'site'                => 'Site',
            'director'            => 'Director',
            'director_post'       => 'Director Post',
            'email'               => 'Email',
            'contact_last_name'   => 'Contact Last Name',
            'contact_name'        => 'Contact Name',
            'contact_second_name' => 'Contact Second Name',
            'contact_phone'       => 'Contact Phone',
            'contact_email'       => 'Contact Email',
            'block'               => 'Block',
            'logo'                => 'Logo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCompanyBalances()
    {
        return $this->hasMany(ClientCompanyBalance::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCompanyHasCities()
    {
        return $this->hasMany(ClientCompanyHasCity::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCompanyHasTariffs()
    {
        return $this->hasMany(ClientCompanyHasTariff::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffs()
    {
        return $this->hasMany(CarClass::className(), ['class_id' => 'tariff_id'])->viaTable('tbl_client_company_has_tariff', ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientHasCompanies()
    {
        return $this->hasMany(ClientHasCompany::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Client::className(), ['client_id' => 'client_id'])->viaTable('tbl_client_has_company', ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['company_id' => 'company_id']);
    }

    /**
     * Транзакция на вычет из баланса компании
     * @param type $companyId
     * @param type $value
     * @param type $comment
     * @param type $lang
     */
    public static function expenseTransaction($companyId, $value, $comment, $lang)
    {
        $balance = ClientCompanyBalance::find()
                ->where(["company_id" => $companyId])
                ->one();
        $balance->value = $balance->value - $value;
        if ($balance->save()) {
            $balaceId = $balance->balance_id;
            $transaction = new ClientCompanyTransaction();
            $transaction->balance_id = $balaceId;
            $transaction->operation = "expenses";
            $transaction->balance_id = (int) $balaceId;
            $transaction->transaction_time = (int) time();
            $transaction->value = (string) $value;
            $transaction->comment = $comment;
            $transaction->income_type = null;
            if ($transaction->save()) {
                return true;
            }
        }
        return false;
    }

}
