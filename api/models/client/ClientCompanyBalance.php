<?php

namespace app\models\client;

use Yii;

/**
 * This is the model class for table "tbl_client_company_balance".
 *
 * @property integer $balance_id
 * @property integer $company_id
 * @property string $value
 * @property string $currency
 *
 * @property ClientCompany $company
 * @property ClientCompanyTransaction[] $clientCompanyTransactions
 */
class ClientCompanyBalance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_client_company_balance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['company_id'], 'integer'],
            [['value'], 'number'],
            [['currency'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'balance_id' => 'Balance ID',
            'company_id' => 'Company ID',
            'value' => 'Value',
            'currency' => 'Currency',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(ClientCompany::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCompanyTransactions()
    {
        return $this->hasMany(ClientCompanyTransaction::className(), ['balance_id' => 'balance_id']);
    }
}
