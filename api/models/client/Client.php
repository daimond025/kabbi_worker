<?php

namespace app\models\client;

use app\models\balance\Account;
use app\models\client\ClientPhone;
use bonusSystem\BonusSystem;
use Yii;

/**
 * This is the model class for table "tbl_client".
 *
 * @property string                $client_id
 * @property string                $tenant_id
 * @property string                $city_id
 * @property string                $photo
 * @property string                $last_name
 * @property string                $name
 * @property string                $second_name
 * @property string                $email
 * @property integer               $black_list
 * @property integer               $priority
 * @property string                $create_time
 * @property integer               $active
 * @property string                $success_order
 * @property string                $fail_driver_order
 * @property string                $fail_client_order
 * @property string                $birth
 * @property integer               $password
 *
 * @property Tenant                $tenant
 * @property ClientHasBonus[]      $clientHasBonuses
 * @property ClientBonus[]         $bonuses
 * @property ClientHasCompany[]    $clientHasCompanies
 * @property ClientCompany[]       $companies
 * @property ClientPhone[]         $clientPhones
 * @property ClientReview[]        $clientReviews
 * @property ClientReviewRaiting[] $clientReviewRaitings
 * @property OrderChange[]         $orderChanges
 * @property OrderHistory[]        $orderHistories
 */
class Client extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id'], 'required'],
            [
                [
                    'tenant_id',
                    'city_id',
                    'black_list',
                    'priority',
                    'active',
                    'success_order',
                    'fail_worker_order',
                    'fail_client_order',
                    'password',
                ],
                'integer',
            ],
            [['create_time', 'birth'], 'safe'],
            [['photo'], 'string', 'max' => 255],
            [['last_name', 'name', 'second_name'], 'string', 'max' => 45],
            [['email'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_id'         => 'Client ID',
            'tenant_id'         => 'Tenant ID',
            'city_id'           => 'City ID',
            'photo'             => 'Photo',
            'last_name'         => 'Last Name',
            'name'              => 'Name',
            'second_name'       => 'Second Name',
            'email'             => 'Email',
            'black_list'        => 'Black List',
            'priority'          => 'Priority',
            'create_time'       => 'Create Time',
            'active'            => 'Active',
            'success_order'     => 'Success Order',
            'fail_worker_order' => 'Fail Worker Order',
            'fail_client_order' => 'Fail Client Order',
            'birth'             => 'Birth',
            'password'          => 'Password',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientHasBonuses()
    {
        return $this->hasMany(ClientHasBonus::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonuses()
    {
        return $this->hasMany(ClientBonus::className(), ['bonus_id' => 'bonus_id'])->viaTable('tbl_client_has_bonus',
            ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientHasCompanies()
    {
        return $this->hasMany(ClientHasCompany::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(ClientCompany::className(),
            ['company_id' => 'company_id'])->viaTable('tbl_client_has_company', ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientPhones()
    {
        return $this->hasMany(ClientPhone::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientReviews()
    {
        return $this->hasMany(ClientReview::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientReviewRaitings()
    {
        return $this->hasMany(ClientReviewRaiting::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderChanges()
    {
        return $this->hasMany(OrderChange::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistories()
    {
        return $this->hasMany(OrderHistory::className(), ['client_id' => 'client_id']);
    }

    public static function findByClientId($client_id, $select = [])
    {
        $clientPhone = ClientPhone::find()
            ->where(['client_id' => $client_id])
            ->with([
                'client' => function ($query) use ($select) {
                    $query->select($select);
                },
            ])
            ->one();

        return $clientPhone->client;
    }

    private static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t($category, $message, $params, $language);
    }

    /**
     * Получение баланса денег клиента
     *
     * @param int $clientId
     * @param int $tenantId
     *
     * @return int
     */
    public static function getClientMoneyBalance($clientId, $tenantId)
    {
        $bonusData = Account::findOne([
            'owner_id'    => $clientId,
            'tenant_id'   => $tenantId,
            'acc_kind_id' => Account::CLIENT_KIND,
        ]);
        if (isset($bonusData->balance)) {
            return $bonusData->balance;
        }

        return 0;
    }

    /**
     * Получение баланса бонусов клиента
     *
     * @param int $clientId
     * @param int $tenantId
     * @param int $currencyId
     *
     * @return int
     */
    public static function getClientBonusBalance($clientId, $tenantId, $currencyId)
    {
        $bonusData = Account::findOne([
            'owner_id'    => $clientId,
            'tenant_id'   => $tenantId,
            'acc_kind_id' => Account::CLIENT_BONUS,
            'currency_id' => $currencyId,
        ]);

        return isset($bonusData->balance) ? (string)$bonusData->balance : '0';
    }

    /**
     * Получение бонусного тарифа
     *
     * @param int $clientTariffId - ид клиентского тарифа
     *
     * @return array
     */
    public static function getClientBonusTariff($clientTariffId)
    {
        return ClientBonusHasTariff::find()
            ->where([
                'tariff_id' => $clientTariffId,
            ])
            ->joinWith('bonus')
            ->andWhere([
                ClientBonus::tableName() . '.blocked' => 0,
            ])
            ->orderBy('created_at ASC')
            ->asArray()
            ->one();
    }

    /**
     * Getting bonus payment data
     *
     * @param int $tenantId
     * @param int $orderId
     * @param int $clientId
     * @param int $currencyId
     * @param int $clientTariffId
     *
     * @return array
     */
    public static function getBonusPayment($tenantId, $orderId, $clientId, $currencyId, $clientTariffId)
    {
        try {
            /** @var $bonusSystem BonusSystem */
            $bonusSystem = \Yii::createObject(BonusSystem::class, [$tenantId]);

            if ($bonusSystem->isUDSGameBonusSystem()
                && !$bonusSystem->isRegisteredOrderForWriteOff($orderId)
            ) {
                return [];
            }

            $clientBonusBalance = $bonusSystem->getBalance($clientId, $currencyId);
            $paymentStrategy    = $bonusSystem->getPaymentStrategy($clientTariffId);

            return [
                'bonus_id'              => $paymentStrategy->getBonusId(),
                'payment_method'        => $paymentStrategy->getPaymentMethod(),
                'max_payment_type'      => $paymentStrategy->getMaxPaymentType(),
                'max_payment'           => $paymentStrategy->getMaxPayment(),
                'client_bonus_balance ' => $clientBonusBalance,
            ];
        } catch (\Exception $ex) {
            return [];
        }
    }

}
