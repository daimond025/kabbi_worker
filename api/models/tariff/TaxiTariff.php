<?php

namespace app\models\tariff;

use app\models\worker\Worker;
use v3\components\WorkerTariffService;
use Yii;

/**
 * This is the model class for table "tbl_taxi_tariff".
 *
 * @property string              $tariff_id
 * @property string              $tenant_id
 * @property integer             $class_id
 * @property integer             $position_id
 *
 * @property AdditionalOption[]  $additionalOptions
 * @property OptionActiveDate[]  $optionActiveDates
 * @property OptionTariff[]      $optionTariffs
 * @property Order[]             $orders
 * @property OrderChange[]       $orderChanges
 * @property OrderHistory[]      $orderHistories
 * @property PhoneLine[]         $phoneLines
 * @property CarClass            $class
 * @property Tenant              $tenant
 * @property TaxiTariffHasCity[] $taxiTariffHasCities
 */
class TaxiTariff extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_taxi_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'class_id'], 'required'],
            [['tenant_id', 'class_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tariff_id' => 'Tariff ID',
            'tenant_id' => 'Tenant ID',
            'class_id'  => 'Class ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalOptions()
    {
        return $this->hasMany(AdditionalOption::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptionActiveDates()
    {
        return $this->hasMany(OptionActiveDate::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptionTariffs()
    {
        return $this->hasMany(OptionTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderChanges()
    {
        return $this->hasMany(OrderChange::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistories()
    {
        return $this->hasMany(OrderHistory::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoneLines()
    {
        return $this->hasMany(PhoneLine::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxiTariffHasCities()
    {
        return $this->hasMany(TaxiTariffHasCity::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * Получить тип тарифа по времени
     *
     * @param string $tariffId Ид тарифа
     * @param string $orderTime timestamp
     *
     * @return string $tariffType тип тарифа  ENUM('CURRENT', 'HOLIDAYS', 'EXCEPTIONS')
     */
    public static function getClientTariffTypeByTime($tariffId, $orderTime)
    {
        $time = new \DateTime();
        $time->setTimestamp($orderTime);
        $dateFull        = $time->format('d.m.Y');
        $dateShort       = $time->format('d.m.');
        $dayOfWeek       = date('l', strtotime($dateFull));
        $toDay           = [$dayOfWeek, $dateShort, $dateFull];
        $records         = OptionActiveDate::find()
            ->where('tariff_id = :tariffId', [':tariffId' => $tariffId])
            ->andWhere(['OR LIKE', 'active_date', [$toDay['0'], $toDay['1'], $toDay['2']]])
            ->asArray()
            ->all();
        $goodTariffTypes = [];
        if (!empty($records)) {
            foreach ($records as $tariff) {
                $activeDate    = $tariff['active_date'];
                $activeDateArr = explode(";", $activeDate);
                foreach ($activeDateArr as $activeDateValue) {
                    foreach ($toDay as $toDayValue) {
                        if (!strstr($activeDateValue, "|")) {
                            if ($activeDateValue == $toDayValue) {
                                array_push($goodTariffTypes, $tariff['tariff_type']);
                            }
                        } else {
                            $activeDateValueArr = explode("|", $activeDateValue);
                            if (isset($activeDateValueArr[1])) {
                                if ($activeDateValueArr[0] == $toDayValue) {
                                    $activeDateInterval = $activeDateValueArr[1];

                                    /* @var $tariffService WorkerTariffService */
                                    $tariffService = \Yii::createObject(WorkerTariffService::className());
                                    if ($tariffService->timestampInInterval($activeDateInterval, $orderTime)) {
                                        array_push($goodTariffTypes, $tariff['tariff_type']);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (count($goodTariffTypes) != 0) {
            if (in_array('EXCEPTIONS', $goodTariffTypes)) {
                if (self::checkTariffIsActive($tariffId, 'EXCEPTIONS')) {
                    return 'EXCEPTIONS';
                }
            }
            if (in_array('HOLIDAYS', $goodTariffTypes)) {
                if (self::checkTariffIsActive($tariffId, 'HOLIDAYS')) {
                    return 'HOLIDAYS';
                }
            }
        }

        return "CURRENT";
    }

    public static function checkTariffIsActive($tariffId, $tariffType)
    {
        $isActive = OptionTariff::findBySql("SELECT * from " . OptionTariff::tableName() . " where tariff_id= :tariffId  and tariff_type= :tariffType and (active = 1 OR active IS NULL) ",
            [':tariffId' => $tariffId, ':tariffType' => $tariffType])->asArray()->all();
        if (!empty($isActive)) {
            return true;
        }

        return false;
    }

    public static function getTariffData($tariffId, $area, $tariffType)
    {
        return OptionTariff::findBySql("SELECT * from " . OptionTariff::tableName() . " where tariff_id= :tariffId and area = :area and tariff_type= :tariffType ",
            [':tariffId' => $tariffId, ':area' => $area, ':tariffType' => $tariffType])->asArray()->one();
    }

    /**
     * День/ночь?
     *
     * @param array  $tariffData
     * @param string $orderTime timestamp
     *
     * @return boolean
     */
    public static function isTariffDay($tariffData, $orderTime)
    {
        $allowDayNight = $tariffData['allow_day_night'];
        if ($allowDayNight != 1) {
            return 1;
        } else {
            $orderDate = new \DateTime();
            $orderDate->setTimestamp($orderTime);
            $orderHouers = $orderDate->format('H');
            $startDay    = $tariffData['start_day'];
            if (empty($startDay)) {
                return 1;
            } else {
                $startDay = explode(':', $startDay);
                $startDay = (int)$startDay[0];
            }
            $endDay = $tariffData['end_day'];
            if (empty($endDay)) {
                return 1;
            } else {
                $endDay = explode(':', $endDay);
                $endDay = (int)$endDay[0];
            }

            if ($orderHouers >= $startDay && $orderHouers <= $endDay) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    /**
     * Фильтрация данных тарифа с учетом для ночи, если стоит галка allow_day_night - то выдаем
     * тариф в исходном виде, если не стоит, то ставим поля ночи = поля дня
     *
     * @param array $tariffData
     */
    public static function filterTariffDataAllowDayNigth($tariffData)
    {
        if (is_array($tariffData)) {
            if ($tariffData['allow_day_night']) {
                return $tariffData;
            } else {
                $tariffData['planting_price_night']    = $tariffData['planting_price_day'];
                $tariffData['planting_include_night']  = $tariffData['planting_include_day'];
                $tariffData['next_km_price_night']     = $tariffData['next_km_price_day'];
                $tariffData['min_price_night']         = $tariffData['min_price_day'];
                $tariffData['supply_price_night']      = $tariffData['supply_price_day'];
                $tariffData['wait_time_night']         = $tariffData['wait_time_day'];
                $tariffData['wait_driving_time_night'] = $tariffData['wait_driving_time_day'];
                $tariffData['wait_price_night']        = $tariffData['wait_price_day'];
                $tariffData['speed_downtime_night']    = $tariffData['speed_downtime_day'];
                $tariffData['rounding_night']          = $tariffData['rounding_day'];
                $tariffData['speed_downtime_night']    = $tariffData['speed_downtime_day'];

                return $tariffData;
            }
        }

        return $tariffData;
    }

    /**
     * UnSerialize tariff interval data
     *
     * @param array $tariffData
     *
     * @return array
     */
    public static function unSerializeIntervalData($tariffData)
    {
        if (isset($tariffData['accrual']) && $tariffData['accrual'] === 'INTERVAL') {
            if (isset($tariffData['next_km_price_day'])) {
                $tariffData['next_km_price_day'] = unserialize($tariffData['next_km_price_day']);
            }
            if (isset($tariffData['next_km_price_night'])) {
                $tariffData['next_km_price_night'] = unserialize($tariffData['next_km_price_night']);
            }
        }

        return $tariffData;
    }

    /**
     * Getting tariffs by car classes
     *
     * @param int   $tenantId
     * @param array $classIds
     * @param int   $cityId
     *
     * @return array
     */
    public static function getTariffsByCarClasses($tenantId, $classIds, $cityId)
    {
        return self::find()
            ->alias('t')
            ->joinWith('taxiTariffHasCities c')
            ->where([
                't.tenant_id'      => $tenantId,
                't.class_id'       => $classIds,
                'c.city_id'        => $cityId,
                't.block'          => 0,
                't.enabled_bordur' => 1,
            ])
            ->orderBy(['IFNULL(t.sort, 0)' => SORT_ASC, 't.name' => SORT_ASC])
            ->asArray()
            ->all();
    }

    /**
     * Getting tariffs by position classes
     *
     * @param int   $tenantId
     * @param array $positionIds
     * @param int   $cityId
     *
     * @return array
     */
    public static function getTariffsByPositions($tenantId, $positionIds, $cityId)
    {
        return self::find()
            ->alias('t')
            ->joinWith('taxiTariffHasCities c')
            ->where([
                't.tenant_id'      => $tenantId,
                't.position_id'    => $positionIds,
                'c.city_id'        => $cityId,
                't.enabled_bordur' => 1,
                't.block'          => 0,
            ])
            ->orderBy(['t.sort' => SORT_DESC])
            ->asArray()
            ->all();
    }

    public static function isExistsTariffsByCarClasses($tenantId, $classIds, $cityId, $tariffId = null)
    {
        return self::find()
            ->alias('t')
            ->joinWith('taxiTariffHasCities c')
            ->where([
                't.tenant_id'      => $tenantId,
                't.class_id'       => $classIds,
                'c.city_id'        => $cityId,
                't.block'          => 0,
                't.enabled_bordur' => 1,
            ])
            ->andFilterWhere(['t.tariff_id' => $tariffId])
            ->exists();
    }

    public static function isExistsTariffsByPositions($tenantId, $positionIds, $cityId, $tariffId = null)
    {
        return self::find()
            ->alias('t')
            ->joinWith('taxiTariffHasCities c')
            ->where([
                't.tenant_id'      => $tenantId,
                't.position_id'    => $positionIds,
                'c.city_id'        => $cityId,
                't.enabled_bordur' => 1,
                't.block'          => 0,
            ])
            ->andFilterWhere(['t.tariff_id' => $tariffId])
            ->exists();
    }
}