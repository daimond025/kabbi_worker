<?php

namespace app\models\tariff;

use Yii;

/**
 * This is the model class for table "tbl_option_active_date".
 *
 * @property string $date_id
 * @property string $tariff_id
 * @property string $active_date
 * @property string $tariff_type
 *
 * @property TaxiTariff $tariff
 */
class OptionActiveDate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%option_active_date}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'active_date', 'tariff_type'], 'required'],
            [['tariff_id'], 'integer'],
            [['tariff_type'], 'string'],
            [['active_date'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date_id' => Yii::t('tariff', 'Date ID'),
            'tariff_id' => Yii::t('tariff', 'Tariff ID'),
            'active_date' => Yii::t('tariff', 'Active Date'),
            'tariff_type' => Yii::t('tariff', 'Tariff Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * Allow to save multiple rows
     * @param array $arOptions
     * @return bool|integer number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public function manySave(array $arOptions)
    {
        if(!empty($arOptions))
        {
            $arOptions = array_unique($arOptions);
            $insertValue = [];
            $connection = app()->db;

            foreach($arOptions as $date)
            {
                if(!empty($date))
                    $insertValue[] = [$this->tariff_id, $date, $this->tariff_type];
            }

            if(!empty($insertValue))
            {
                return $connection->createCommand()->batchInsert(self::tableName(), ['tariff_id', 'active_date', 'tariff_type'], $insertValue)->execute();
            }
        }

        return false;
    }
}
