<?php

namespace app\models\tariff;

use Yii;

/**
 * This is the model class for table "{{%holiday}}".
 *
 * @property integer $holiday_id
 * @property string $date
 */
class Holiday extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%holiday}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'required'],
            [['date'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'holiday_id' => 'Holiday ID',
            'date' => 'Date',
        ];
    }
}
