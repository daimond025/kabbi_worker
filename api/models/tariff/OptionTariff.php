<?php

namespace app\models\tariff;

use Yii;

/**
 * This is the model class for table "{{%option_tariff}}".
 *
 * @property string $option_id
 * @property string $tariff_id
 * @property string $accrual
 * @property string $area
 * @property string $planting_price_day
 * @property string $planting_price_night
 * @property string $planting_include_day
 * @property string $planting_include_night
 * @property string $next_km_price_day
 * @property string $next_km_price_night
 * @property string $min_price_day
 * @property string $min_price_night
 * @property string $supply_price_day
 * @property string $supply_price_night
 * @property integer $wait_time_day
 * @property integer $wait_time_night
 * @property integer $wait_driving_time_day
 * @property integer $wait_driving_time_night
 * @property string $wait_price_day
 * @property string $wait_price_night
 * @property integer $speed_downtime_day
 * @property integer $speed_downtime_night
 * @property string $rounding_day
 * @property string $rounding_night
 * @property string $tariff_type
 * @property string $allow_day_night
 * @property string $start_day
 * @property string $end_day
 *
 * @property TblFixHasOption[] $tblFixHasOptions
 * @property TblFixTariff[] $fixes
 * @property TblTaxiTariff $tariff
 */
class OptionTariff extends \yii\db\ActiveRecord {

    //$accrual
    const ACCRUAL_FIX = 'FIX';
    const ACCRUAL_DISTANCE = 'DISTANCE';
    const ACCRUAL_TIME = 'TIME';
    //$tariff_type
    const CURRENT_TYPE = 'CURRENT';
    const HOLIDAYS_TYPE = 'HOLIDAYS';
    const EXCEPTIONS_TYPE = 'EXCEPTIONS';
    //$area
    const AREA_CITY = 'CITY';
    const AREA_TRACK = 'TRACK';
    const AREA_RAILWAY = 'RAILWAY';
    const AREA_AIRPORT = 'AIRPORT';

    private $_old_accrual;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%option_tariff}}';
    }

    public function init()
    {
        parent::init();
        $this->allow_day_night = 1;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['area', 'tariff_type', 'accrual'], 'required'],
            [['tariff_id', 'wait_time_day', 'wait_time_night', 'wait_driving_time_day', 'wait_driving_time_night', 'speed_downtime_day', 'speed_downtime_night'], 'integer'],
            [['accrual', 'area', 'tariff_type', 'start_day', 'end_day'], 'string'],
            [['planting_price_day', 'planting_price_night', 'planting_include_day', 'planting_include_night', 'next_km_price_day', 'next_km_price_night', 'min_price_day', 'min_price_night', 'supply_price_day', 'supply_price_night', 'wait_price_day', 'wait_price_night', 'rounding_day', 'rounding_night'], 'number'],
            [['planting_price_day', 'planting_price_night', 'planting_include_day', 'planting_include_night', 'next_km_price_day', 'next_km_price_night', 'min_price_day', 'min_price_night', 'supply_price_day', 'supply_price_night', 'wait_price_day', 'wait_price_night', 'rounding_day', 'rounding_night', 'wait_time_day', 'wait_time_night', 'wait_driving_time_day', 'wait_driving_time_night', 'speed_downtime_day', 'speed_downtime_night'], 'default', 'value' => 0],
            [['allow_day_night'], 'default', 'value' => 1],
            ['allow_day_night', 'boolean'],
            [[], 'match', 'pattern' => '/^[a-z]\w*$/i']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id'               => Yii::t('taxi_tariff', 'Option ID'),
            'tariff_id'               => Yii::t('taxi_tariff', 'Tariff ID'),
            'accrual'                 => Yii::t('taxi_tariff', 'Accrual'),
            'area'                    => Yii::t('taxi_tariff', 'Area'),
            'planting_price_day'      => Yii::t('taxi_tariff', 'Planting price'),
            'planting_price_night'    => Yii::t('taxi_tariff', 'Planting price'),
            'planting_include_day'    => Yii::t('taxi_tariff', 'Included in the cost of planting'),
            'planting_include_night'  => Yii::t('taxi_tariff', 'Included in the cost of planting'),
            'next_km_price_day'       => Yii::t('taxi_tariff', 'Price next'),
            'next_km_price_night'     => Yii::t('taxi_tariff', 'Price next'),
            'min_price_day'           => Yii::t('taxi_tariff', 'Min price of trip'),
            'min_price_night'         => Yii::t('taxi_tariff', 'Min price of trip'),
            'supply_price_day'        => Yii::t('taxi_tariff', 'Cost of 1 km to the feed address'),
            'supply_price_night'      => Yii::t('taxi_tariff', 'Cost of 1 km to the feed address'),
            'wait_time_day'           => Yii::t('taxi_tariff', 'Free waiting time before a trip'),
            'wait_time_night'         => Yii::t('taxi_tariff', 'Free waiting time before a trip'),
            'wait_driving_time_day'   => Yii::t('taxi_tariff', 'Free waiting time during a trip one time'),
            'wait_driving_time_night' => Yii::t('taxi_tariff', 'Free waiting time during a trip one time'),
            'wait_price_day'          => Yii::t('taxi_tariff', 'Cost of 1 minute waiting'),
            'wait_price_night'        => Yii::t('taxi_tariff', 'Cost of 1 minute waiting'),
            'speed_downtime_day'      => Yii::t('taxi_tariff', 'Speed downtime'),
            'speed_downtime_night'    => Yii::t('taxi_tariff', 'Speed downtime'),
            'rounding_day'            => Yii::t('taxi_tariff', 'Rounding'),
            'rounding_night'          => Yii::t('taxi_tariff', 'Rounding'),
            'tariff_type'             => Yii::t('taxi_tariff', 'Tariff Type'),
            'allow_day_night'         => Yii::t('taxi_tariff', 'Allow day and night'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixHasOptions()
    {
        return $this->hasMany(FixHasOption::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixes()
    {
        return $this->hasMany(FixTariff::className(), ['fix_id' => 'fix_id'])->viaTable('{{%fix_has_option}}', ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * Variants of tariff accruals
     * @return array
     */
    public static function getAccruals()
    {
        return [
            self::ACCRUAL_DISTANCE => t('taxi_tariff', 'Distance'),
            self::ACCRUAL_TIME     => t('taxi_tariff', 'Time'),
            self::ACCRUAL_FIX      => t('taxi_tariff', 'Fix'),
        ];
    }

    public function is_fix()
    {
        return $this->accrual == self::ACCRUAL_FIX ? true : false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!$insert) {
            if ($this->_old_accrual == self::ACCRUAL_FIX && $this->accrual != self::ACCRUAL_FIX) {
                FixHasOption::deleteAll(['option_id' => $this->option_id]);
            }
        }
    }

    public function beforeSave($insert)
    {
        if (!$insert)
            $this->_old_accrual = $this->getOldAttribute('accrual');

        return parent::beforeSave($insert);
    }

}
