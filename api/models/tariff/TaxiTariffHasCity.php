<?php

namespace app\models\tariff;

use Yii;

/**
 * This is the model class for table "tbl_taxi_tariff_has_city".
 *
 * @property integer $tariff_id
 * @property integer $city_id
 *
 * @property TaxiTariff $tariff
 */
class TaxiTariffHasCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_taxi_tariff_has_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'city_id'], 'required'],
            [['tariff_id', 'city_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tariff_id' => 'Tariff ID',
            'city_id' => 'City ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }
}
