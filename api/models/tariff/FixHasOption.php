<?php

namespace app\models\tariff;

use Yii;

/**
 * This is the model class for table "{{%fix_has_option}}".
 *
 * @property string $option_id
 * @property string $fix_id
 * @property string $price_to
 * @property string $price_back
 *
 * @property OptionTariff $option
 * @property FixTariff $fix
 */
class FixHasOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%fix_has_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_id', 'fix_id', 'price_to', 'price_back'], 'required'],
            [['option_id', 'fix_id'], 'integer'],
            [['price_to', 'price_back'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id' => 'Option ID',
            'fix_id' => 'Fix ID',
            'price_to' => 'Price To',
            'price_back' => 'Price Back',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(OptionTariff::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFix()
    {
        return $this->hasOne(FixTariff::className(), ['fix_id' => 'fix_id']);
    }

    /**
     * Allow to save multiple rows
     * @param array $arCity
     * @param array $tariff_id
     * @return integer number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public static function manySave(array $arFixOption, $option_id)
    {
        $insertValue = [];
        $connection = app()->db;

        foreach($arFixOption as $fix_id => $fix_option)
        {
            $price_to = empty($fix_option['PRICE_TO']) ? 0 : $fix_option['PRICE_TO'];
            $price_back = empty($fix_option['PRICE_BACK']) ? $price_to : $fix_option['PRICE_BACK'];

            $insertValue[] = [$option_id, $fix_id, $price_to, $price_back];
        }

        return $connection->createCommand()->batchInsert(self::tableName(), ['option_id', 'fix_id', 'price_to', 'price_back'], $insertValue)->execute();
    }
}
