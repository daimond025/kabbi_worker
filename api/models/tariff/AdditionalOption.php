<?php

namespace app\models\tariff;

use Yii;
use app\models\car\CarOption;
use app\models\tariff\TaxiTariff;

/**
 * This is the model class for table "{{%tbl_additional_option}}".
 *
 * @property string $id
 * @property string $tariff_id
 * @property integer $additional_option_id
 * @property string $price
 * @property string $tariff_type
 *
 * @property TblCarOption $additionalOption
 * @property TblTaxiTariff $tariff
 */
class AdditionalOption extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%additional_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'additional_option_id', 'tariff_type'], 'required'],
            [['tariff_id', 'additional_option_id'], 'integer'],
            [['price'], 'number'],
            [['tariff_type'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                   => Yii::t('tariff', 'ID'),
            'tariff_id'            => Yii::t('tariff', 'Tariff ID'),
            'additional_option_id' => Yii::t('tariff', 'Additional Option ID'),
            'price'                => Yii::t('tariff', 'Price'),
            'tariff_type'          => Yii::t('tariff', 'Tariff Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(CarOption::className(), ['option_id' => 'additional_option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * Allow to save multiple rows
     * @param array $arOptions
     * @return bool|integer number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public function manySave(array $arOptions)
    {
        if (!empty($arOptions)) {
            $insertValue = [];
            $connection = app()->db;

            foreach ($arOptions as $option_id => $price) {
                if ($price > -1)
                    $insertValue[] = [$this->tariff_id, $option_id, $price, $this->tariff_type];
            }

            if (!empty($insertValue)) {
                return $connection->createCommand()->batchInsert(self::tableName(), ['tariff_id', 'additional_option_id', 'price', 'tariff_type'], $insertValue)->execute();
            }
        }

        return false;
    }

}
