<?php

namespace app\models\tariff;

use Yii;

/**
 * This is the model class for table "{{%fix_tariff}}".
 *
 * @property string $fix_id
 * @property string $from
 * @property string $to
 * @property string $price_to
 * @property string $price_back
 *
 * @property FixHasOption[] $FixHasOptions
 * @property OptionTariff[] $options
 * @property Parking $from0
 * @property Parking $to0
 */
class FixTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%fix_tariff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from', 'to', 'price_to', 'price_back'], 'required'],
            [['from', 'to'], 'integer'],
            [['price_to', 'price_back'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fix_id' => Yii::t('tariff', 'Fix ID'),
            'from' => Yii::t('tariff', 'From'),
            'to' => Yii::t('tariff', 'To'),
            'price_to' => Yii::t('tariff', 'Price To'),
            'price_back' => Yii::t('tariff', 'Price Back'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixHasOptions()
    {
        return $this->hasMany(FixHasOption::className(), ['fix_id' => 'fix_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(OptionTariff::className(), ['option_id' => 'option_id'])->viaTable('{{%fix_has_option}}', ['fix_id' => 'fix_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrom()
    {
        return $this->hasOne(Parking::className(), ['parking_id' => 'from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTo()
    {
        return $this->hasOne(Parking::className(), ['parking_id' => 'to']);
    }

    /**
     * Getting fix tariffs for form
     * @param integer $city_id
     * @param string $parking_type
     * @return array
     */
    public static function getFixTariffs($city_id, $parking_type)
    {
        $arFixTariff = [];

        //Формирование фильтра связей. Например: Для парковок типа "Город" выводим связи между пакроковами только типа "Город"
        switch ($parking_type)
        {
            case 'city':
                $parkingTypeFilter = ['city'];
                break;
            case 'outCity':
                $parkingTypeFilter = ['city', 'outCity'];
                break;
            default:
                $parkingTypeFilter = ['city', 'outCity', 'airport', 'station'];
        }

        //Получение всех парковок
        $parkingModels = \app\modules\parking\models\Parking::find()
          ->with(['fixFrom', 'fixTo'])
          ->where([
              'tenant_id' => user()->tenant_id,
              'city_id' => $city_id,
          ])
          ->andWhere('type != "basePolygon"')
          ->select([
              'parking_id',
              'name',
              'sort',
              'type',
              'is_active'
          ])
          ->orderBy('sort')
          ->all();

        //Формирование массива-карты по первичному ключу парковок. Для того чтобы, подставляя id парковки в массив, получать необходимую информацию.
        $arParking = \yii\helpers\ArrayHelper::index($parkingModels, 'parking_id');

        //Формирование результирующего массива
        foreach($parkingModels as $key => $model)
        {
            //Фильтруем массив по необходимым параметрам
            if($model->type != $parking_type || !$model->is_active || (empty($model->fixFrom) && empty($model->fixTo)))
                continue;

            //Корень
            $arFixTariff[$key] = [
                'PARENT_ID' => $model->parking_id,
                'NAME' => $arParking[$model->parking_id]->name,
                'TYPE' => $arParking[$model->parking_id]->type,
            ];

            //Флаг для исключения нескольких одинаковых значений from_id и to_id
            $identical_values = false;

            //Формирование массива связей
            foreach($model->fixFrom as $tariff)
            {
                //Фильтруем массив по необходимым параметрам
                if(!in_array($arParking[$tariff->from]->type, $parkingTypeFilter)
                  || !in_array($arParking[$tariff->to]->type, $parkingTypeFilter)
                  || !$arParking[$tariff->to]->is_active
                  || !$arParking[$tariff->from]->is_active)
                    continue;

                if($tariff->from == $tariff->to)
                    $identical_values = true;

                $arFixTariff[$key]['ITEMS'][] = [
                    'FIX_ID' => $tariff->fix_id,
                    'FROM_ID' => $tariff->from,
                    'TO_ID' => $tariff->to,
                    'FROM' => $arParking[$tariff->from]->name,
                    'TO' => $arParking[$tariff->to]->name,
                ];
            }

            //Добавление массива связей
            foreach($model->fixTo as $tariff)
            {
                //Фильтруем массив по необходимым параметрам
                if($tariff->from == $tariff->to
                  || !in_array($arParking[$tariff->from]->type, $parkingTypeFilter)
                  || !in_array($arParking[$tariff->to]->type, $parkingTypeFilter)
                  || !$arParking[$tariff->to]->is_active
                  || !$arParking[$tariff->from]->is_active)
                    continue;

                $arFixTariff[$key]['ITEMS'][] = [
                    'FIX_ID' => $tariff->fix_id,
                    'FROM_ID' => $tariff->to,
                    'TO_ID' => $tariff->from,
                    'FROM' => $arParking[$tariff->to]->name,
                    'TO' => $arParking[$tariff->from]->name,
                ];
            }

            //Сортировка, для того чтобы связь "По району" была в начале.
            usort($arFixTariff[$key]['ITEMS'], function ($a){
                return $a['FROM_ID'] == $a['TO_ID'] ? 0 : 1;
            });
        }

        return $arFixTariff;
    }
}
