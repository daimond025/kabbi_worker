<?php

namespace app\models\tariff;

use app\models\car\Car;
use app\models\client\ClientCompany;
use Yii;

/**
 * This is the model class for table "tbl_car_class".
 *
 * @property integer                  $class_id
 * @property string                   $class
 *
 * @property Car[]                    $cars
 * @property ClientCompanyHasTariff[] $clientCompanyHasTariffs
 * @property ClientCompany[]          $companies
 * @property TaxiTariff[]             $taxiTariffs
 */
class CarClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_car_class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class'], 'required'],
            [['class'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'class_id' => 'Class ID',
            'class'    => 'Class',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCompanyHasTariffs()
    {
        return $this->hasMany(ClientCompanyHasTariff::className(), ['tariff_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(ClientCompany::className(),
            ['company_id' => 'company_id'])->viaTable('tbl_client_company_has_tariff', ['tariff_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxiTariffs()
    {
        return $this->hasMany(TaxiTariff::className(), ['class_id' => 'class_id']);
    }
}
