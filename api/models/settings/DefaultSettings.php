<?php

namespace app\models\settings;

use Yii;

/**
 * This is the model class for table "tbl_default_settings".
 *
 * @property string $name
 * @property string $value
 */
class DefaultSettings extends \yii\db\ActiveRecord
{
    const SETTING_ANDROID_WORKER_APP_VERSION = 'ANDROID_WORKER_APP_VERSION';
    const SETTING_IOS_WORKER_APP_VERSION = 'IOS_WORKER_APP_VERSION';
    const SETTING_MAX_NUMBER_OF_ADDRESS_POINTS = 'MAX_NUMBER_OF_ADDRESS_POINTS';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_default_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'value'], 'required'],
            [['value'], 'string'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'  => 'Name',
            'value' => 'Value',
        ];
    }

    public static function getSetting($name, $defaultValue = null)
    {
        $value = self::find()->select('value')->where(['name' => $name])->scalar();

        return $value === false ? $defaultValue : $value;
    }

    public static function getCachedSetting($name, $defaultValue = null)
    {
        $cache    = Yii::$app->cache;
        $cacheKey = self::class . '_' . $name;

        $value = $cache->get($cacheKey);
        if ($value === false) {
            $value = self::getSetting($name, $defaultValue);
            $cache->set($cacheKey, $value, 15 * 60);
        }

        return $value;
    }
}
