<?php

namespace app\models\balance;

use app\models\tenant\Tenant;
use Yii;

/**
 * This is the model class for table "{{%account}}".
 *
 * @property integer       $account_id
 * @property integer       $acc_kind_id
 * @property integer       $acc_type_id
 * @property integer       $owner_id
 * @property integer       $currency_id
 * @property integer       $tenant_id
 * @property double        $balance
 *
 * @property AccountKind   $accKind
 * @property AccountType   $accType
 * @property Currency      $currency
 * @property Tenant        $tenant
 * @property Operation[]   $operations
 * @property Transaction[] $transactions
 */
class Account extends \yii\db\ActiveRecord
{

    const ACTIVE_TYPE = 1;
    const PASSIVE_TYPE = 2;

    const TENANT_KIND = 1;
    const WORKER_KIND = 2;
    const CLIENT_KIND = 3;
    const COMPANY_KIND = 4;
    const SYSTEM_KIND = 5;

    const CLIENT_BONUS = 7;
    const SYSTEM_BONUS = 8;

    const DEFAULT_CURRENCY = 1;

    public $sum;
    public $transaction_comment;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%account}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['acc_kind_id', 'acc_type_id', 'currency_id'], 'required'],
            [['acc_kind_id', 'acc_type_id', 'owner_id', 'currency_id', 'tenant_id'], 'integer'],
            ['transaction_comment', 'string'],
            [['balance', 'sum'], 'number'],
            [['balance'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'account_id'  => 'Account ID',
            'acc_kind_id' => 'Acc Kind ID',
            'acc_type_id' => 'Acc Type ID',
            'owner_id'    => 'Owner ID',
            'currency_id' => 'Currency ID',
            'tenant_id'   => 'Tenant ID',
            'balance'     => 'Balance',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccKind()
    {
        return $this->hasOne(AccountKind::className(), ['kind_id' => 'acc_kind_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccType()
    {
        return $this->hasOne(AccountType::className(), ['type_id' => 'acc_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperations()
    {
        return $this->hasMany(Operation::className(), ['account_id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['sender_acc_id' => 'account_id']);
    }

    /**
     * Getting balance
     * @return float
     */
    public function getFormattedBalance()
    {
        $balance   = empty($this->balance) ? 0 : $this->balance;
        $minorUnit = empty($this->currency->minor_unit) ? 0 : $this->currency->minor_unit;

        return floor(round($balance * pow(10, $minorUnit), 1)) / pow(10, $minorUnit);
    }

}