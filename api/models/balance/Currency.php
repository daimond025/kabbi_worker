<?php

namespace app\models\balance;

use Yii;

/**
 * This is the model class for table "tbl_currency".
 *
 * @property integer         $currency_id
 * @property integer         $type_id
 * @property string          $name
 * @property string          $code
 * @property string          $symbol
 * @property int             $minor_unit
 * @property int             $number_code
 *
 * @property Account[]       $accounts
 * @property CurrencyType    $type
 * @property TenantHasCity[] $tenantHasCities
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'name'], 'required'],
            [['type_id'], 'integer'],
            [['name', 'code', 'symbol'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'currency_id' => 'Currency ID',
            'type_id'     => 'Type ID',
            'name'        => 'Name',
            'code'        => 'Code',
            'symbol'      => 'Symbol',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(Account::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(CurrencyType::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCities()
    {
        return $this->hasMany(TenantHasCity::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * Getting currency id by currency code (character)
     *
     * @param string $code
     *
     * @return false|null|string
     */
    public static function getCurrencyIdByCode($code)
    {
        return self::find()
            ->select('currency_id')
            ->where(['code' => $code])
            ->scalar();
    }
}
