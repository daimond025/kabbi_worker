<?php

namespace api\models\balance;

use app\models\balance\Account;
use app\models\balance\Transaction;
use yii\base\Object;

/**
 * Class WorkerTransaction
 * @package api\models\balance
 */
class WorkerTransaction extends Object
{
    /**
     * Buy worker tariff
     *
     * @param int    $tenantId
     * @param int    $workerId
     * @param string $tariff
     * @param float  $tariffCost
     * @param int    $currencyId
     * @param string $requestId
     *
     * @return int
     */
    public function buyWorkerTariff($tenantId, $workerId, $tariff, $tariffCost, $currencyId, $requestId = '')
    {
        $transactionData = [
            'type_id'            => Transaction::WITHDRAWAL_TYPE,
            'payment_method'     => Transaction::PAYMENT_WITHDRAWAL_FOR_TARIFF,
            'tenant_id'          => $tenantId,
            'sender_owner_id'    => $workerId,
            'sender_acc_kind_id' => Account::WORKER_KIND,
            'sum'                => $tariffCost,
            'currency_id'        => $currencyId,
            'comment'            => "Payment for tariff||{$tariff}",
            'request_id'         => $requestId,
        ];

        return Transaction::createTransaction($transactionData);
    }

    /**
     * Close order
     *
     * @param int $orderId
     * @param int $statusNewId
     *
     * @return bool|string
     */
    public function closeOrder($orderId, $statusNewId)
    {
        $transactionData = [
            'order_id'        => $orderId,
            'order_status_id' => $statusNewId,
        ];

        $responseCode = Transaction::createTransaction($transactionData);
        if ($responseCode == Transaction::SUCCESS_RESPONSE) {
            return true;
        } else {
            \Yii::error("ERROR AT closeOrderTransaction. Error code: " . $responseCode);

            return $responseCode;
        }
    }

    /**
     * Refill account
     *
     * @param int    $tenantId
     * @param int    $workerId
     * @param string $requestId
     * @param string $pan
     * @param string $sum
     * @param int    $currencyId
     *
     * @return string
     */
    public function refillAccount($tenantId, $workerId, $requestId, $pan, $sum, $currencyId)
    {
        $transactionData = [
            'type_id'            => Transaction::DEPOSIT_TYPE,
            'payment_method'     => Transaction::CARD_PAYMENT,
            'tenant_id'          => $tenantId,
            'sender_owner_id'    => $workerId,
            'sender_acc_kind_id' => Account::WORKER_KIND,
            'request_id'         => $requestId,
            'pan'                => $pan,
            'sum'                => $sum,
            'currency_id'        => $currencyId,
        ];

        return Transaction::createTransaction($transactionData);
    }

}