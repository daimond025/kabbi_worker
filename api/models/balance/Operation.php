<?php

namespace app\models\balance;

use Yii;

/**
 * This is the model class for table "{{%operation}}".
 *
 * @property integer $operation_id
 * @property integer $type_id
 * @property integer $account_id
 * @property integer $transaction_id
 * @property float $sum
 * @property float $saldo
 * @property string $comment
 * @property string $date
 *
 * @property Account $account
 * @property Transaction $transaction
 * @property OperationType $type
 */
class Operation extends \yii\db\ActiveRecord
{
    /**
     * Приход
     */
    const INCOME_TYPE = 1;
    /**
     * Расход
     */
    const EXPENSES_TYPE = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%operation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'account_id', 'transaction_id'], 'required'],
            [['type_id', 'account_id', 'transaction_id'], 'integer'],
            [['sum'], 'number'],
            [['date'], 'safe'],
            [['comment'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'operation_id' => 'Operation ID',
            'type_id' => 'Type ID',
            'account_id' => 'Account ID',
            'transaction_id' => 'Transaction ID',
            'sum' => 'Sum',
            'saldo' => 'Saldo',
            'comment' => 'Comment',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['account_id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['transaction_id' => 'transaction_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(OperationType::className(), ['type_id' => 'type_id']);
    }

   
}