<?php

namespace app\models\worker;

use Yii;

/**
 * This is the model class for table "tbl_worker_has_document".
 *
 * @property integer                    $id
 * @property integer                    $worker_id
 * @property integer                    $document_id
 * @property integer                    $has_position_id
 *
 * @property WorkerDocumentScan[]       $workerDocumentScans
 * @property WorkerDriverLicense[]      $workerDriverLicenses
 * @property Document                   $document
 * @property Worker                     $worker
 * @property WorkerInn[]                $workerInns
 * @property WorkerMedicalCertificate[] $workerMedicalCertificates
 * @property WorkerOgrnip[]             $workerOgrnips
 * @property WorkerOsago[]              $workerOsagos
 * @property WorkerPassport[]           $workerPassports
 * @property WorkerSnils[]              $workerSnils
 */
class WorkerHasDocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker_has_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worker_id', 'document_id'], 'required'],
            [['worker_id', 'document_id', 'has_position_id'], 'integer'],
            [
                ['worker_id', 'document_id', 'has_position_id'],
                'unique',
                'targetAttribute' => ['worker_id', 'document_id', 'has_position_id'],
                'message'         => 'The combination of Worker ID, Document ID and Has Position ID has already been taken.',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'worker_id'       => 'Worker ID',
            'document_id'     => 'Document ID',
            'has_position_id' => 'Has Position ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerDocumentScans()
    {
        return $this->hasMany(WorkerDocumentScan::className(), ['worker_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerDriverLicenses()
    {
        return $this->hasMany(WorkerDriverLicense::className(), ['worker_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['document_id' => 'document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerInns()
    {
        return $this->hasMany(WorkerInn::className(), ['worker_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerMedicalCertificates()
    {
        return $this->hasMany(WorkerMedicalCertificate::className(), ['worker_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerOgrnips()
    {
        return $this->hasMany(WorkerOgrnip::className(), ['worker_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerOsagos()
    {
        return $this->hasMany(WorkerOsago::className(), ['worker_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerPassports()
    {
        return $this->hasMany(WorkerPassport::className(), ['worker_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerSnils()
    {
        return $this->hasMany(WorkerSnils::className(), ['worker_document_id' => 'id']);
    }
}
