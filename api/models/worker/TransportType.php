<?php

namespace app\models\worker;

use app\models\car\CarModel;
use app\models\car\CarOption;
use app\models\car\TransportTypeHasField;
use app\models\tariff\CarClass;
use Yii;

/**
 * This is the model class for table "tbl_transport_type".
 *
 * @property integer                 $type_id
 * @property string                  $name
 *
 * @property CarClass[]              $carClasses
 * @property CarModel[]              $carModels
 * @property CarOption[]             $carOptions
 * @property Position[]              $positions
 * @property TransportTypeHasField[] $transportTypeHasFields
 */
class TransportType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_transport_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_id' => 'Type ID',
            'name'    => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarClasses()
    {
        return $this->hasMany(CarClass::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModels()
    {
        return $this->hasMany(CarModel::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarOptions()
    {
        return $this->hasMany(CarOption::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(Position::className(), ['transport_type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransportTypeHasFields()
    {
        return $this->hasMany(TransportTypeHasField::className(), ['type_id' => 'type_id']);
    }
}
