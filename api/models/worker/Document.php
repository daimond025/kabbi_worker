<?php

namespace app\models\worker;

use Yii;

/**
 * This is the model class for table "tbl_document".
 *
 * @property integer               $document_id
 * @property string                $name
 * @property integer               $code
 *
 * @property PositionHasDocument[] $positionHasDocuments
 * @property Position[]            $positions
 * @property WorkerHasDocument[]   $workerHasDocuments
 */
class Document extends \yii\db\ActiveRecord
{
    const CODE_DRIVER_LICENSE = 7;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['code'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'document_id' => 'Document ID',
            'name'        => 'Name',
            'code'        => 'Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositionHasDocuments()
    {
        return $this->hasMany(PositionHasDocument::className(), ['document_id' => 'document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(Position::className(),
            ['position_id' => 'position_id'])->viaTable('tbl_position_has_document', ['document_id' => 'document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasDocuments()
    {
        return $this->hasMany(WorkerHasDocument::className(), ['document_id' => 'document_id']);
    }
}
