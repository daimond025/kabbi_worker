<?php

namespace app\models\worker;

use Yii;

/**
 * This is the model class for table "tbl_worker_review_rating".
 *
 * @property integer  $rating_id
 * @property integer  $worker_id
 * @property integer  $one
 * @property integer  $two
 * @property integer  $three
 * @property integer  $four
 * @property integer  $five
 * @property integer  $count
 * @property integer  $position_id
 *
 * @property Position $position
 * @property Worker   $worker
 */
class WorkerReviewRating extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker_review_rating';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worker_id'], 'required'],
            [['worker_id', 'one', 'two', 'three', 'four', 'five', 'count', 'position_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rating_id'   => 'Rating ID',
            'worker_id'   => 'Worker ID',
            'one'         => 'One',
            'two'         => 'Two',
            'three'       => 'Three',
            'four'        => 'Four',
            'five'        => 'Five',
            'count'       => 'Count',
            'position_id' => 'Position ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    public function addRating($rating)
    {
        switch ($rating) {
            case 1:
                $this->one++;
                $this->count++;
                break;
            case 2:
                $this->two++;
                $this->count++;
                break;
            case 3:
                $this->three++;
                $this->count++;
                break;
            case 4:
                $this->four++;
                $this->count++;
                break;
            case 5:
                $this->five++;
                $this->count++;
                break;
        }
    }

    public function getAverageRating()
    {
        if ($this->count == 0) {
            return 0;
        }

        return ($this->one * 1 + $this->two * 2 + $this->three * 3 + $this->four * 4 + $this->five * 5) / $this->count;
    }

    public static function makeWorkerReviewRating($workerId, $positionId)
    {
        return new self([
            'worker_id'   => $workerId,
            'one'         => 0,
            'two'         => 0,
            'three'       => 0,
            'four'        => 0,
            'five'        => 0,
            'count'       => 0,
            'position_id' => $positionId,
        ]);
    }

    public function annulmentRating()
    {
        $this->one   = 0;
        $this->two   = 0;
        $this->three = 0;
        $this->four  = 0;
        $this->five  = 0;
        $this->count = 0;
    }
}
