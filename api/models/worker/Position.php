<?php

namespace app\models\worker;

use app\models\tenant\TenantCityHasPosition;
use Yii;

/**
 * This is the model class for table "tbl_position".
 *
 * @property integer               $position_id
 * @property string                $name
 * @property integer               $module_id
 * @property string                $code
 * @property integer               $has_car
 * @property integer               $transport_type_id
 *
 * @property TransportType         $transportType
 * @property PositionHasDocument[] $positionHasDocuments
 * @property Document[]            $documents
 * @property PositionHasField[]    $positionHasFields
 * @property PositionField[]       $fields
 * @property TaxiTariff[]          $taxiTariffs
 * @property WorkerGroup[]         $workerGroups
 * @property WorkerHasPosition[]   $workerHasPositions
 * @property WorkerTariff[]        $workerTariffs
 */
class Position extends \yii\db\ActiveRecord
{
    const POSITION_ID_DRIVER = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_position';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'module_id', 'code'], 'required'],
            [['module_id', 'has_car', 'transport_type_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['code'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'position_id'       => 'Position ID',
            'name'              => 'Name',
            'module_id'         => 'Module ID',
            'code'              => 'Code',
            'has_car'           => 'Has Car',
            'transport_type_id' => 'Transport Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransportType()
    {
        return $this->hasOne(TransportType::className(), ['type_id' => 'transport_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositionHasDocuments()
    {
        return $this->hasMany(PositionHasDocument::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Document::className(),
            ['document_id' => 'document_id'])->viaTable('tbl_position_has_document', ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositionHasFields()
    {
        return $this->hasMany(PositionHasField::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFields()
    {
        return $this->hasMany(PositionField::className(),
            ['field_id' => 'field_id'])->viaTable('tbl_position_has_field', ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxiTariffs()
    {
        return $this->hasMany(TaxiTariff::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerGroups()
    {
        return $this->hasMany(WorkerGroup::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasPositions()
    {
        return $this->hasMany(WorkerHasPosition::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerTariffs()
    {
        return $this->hasMany(WorkerTariff::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(TenantCityHasPosition::className(), ['position_id' => 'position_id']);
    }

}
