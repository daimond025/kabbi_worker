<?php

namespace app\models\worker;

use Yii;

/**
 * This is the model class for table "tbl_position_field".
 *
 * @property integer                    $field_id
 * @property string                     $name
 * @property string                     $type
 *
 * @property PositionHasField[]         $positionHasFields
 * @property Position[]                 $positions
 * @property WorkerPositionFieldValue[] $workerPositionFieldValues
 */
class PositionField extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_position_field';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['type'], 'string'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'field_id' => 'Field ID',
            'name'     => 'Name',
            'type'     => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositionHasFields()
    {
        return $this->hasMany(PositionHasField::className(), ['field_id' => 'field_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(Position::className(),
            ['position_id' => 'position_id'])->viaTable('tbl_position_has_field', ['field_id' => 'field_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerPositionFieldValues()
    {
        return $this->hasMany(WorkerPositionFieldValue::className(), ['field_id' => 'field_id']);
    }
}
