<?php

namespace app\models\worker;


/**
 * This is the model class for table "tbl_worker_option_tariff".
 *
 * @property integer                $option_id
 * @property integer                $tariff_id
 * @property string                 $tariff_type
 * @property string                 $commission_type
 * @property string                 $commission
 * @property string                 $active_date
 * @property int                    $sort
 *
 * @property WorkerOptionDiscount[] $workerOptionDiscounts
 * @property WorkerTariff           $tariff
 */
class WorkerOptionTariff extends \yii\db\ActiveRecord
{
    const TYPE_CURRENT = 'CURRENT';
    const TYPE_EXCEPTION = 'EXCEPTIONS';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker_option_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'tariff_type'], 'required'],
            [['tariff_id', 'sort'], 'integer'],
            [['tariff_type', 'increase_sum_fix_type', 'active_date'], 'string'],
            [['increase_order_sum', 'increase_sum_limit', 'increase_sum_fix'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id'       => 'Option ID',
            'tariff_id'       => 'Tariff ID',
            'tariff_type'     => 'Tariff Type',
            'commission_type' => 'Commission Type',
            'commission'      => 'Commission',
            'active_date'     => 'Active Date',
            'sort'            => 'Sort',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerOptionDiscounts()
    {
        return $this->hasMany(WorkerOptionDiscount::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(WorkerTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffCommission()
    {
        return $this->hasMany(TariffCommission::className(), ['option_id' => 'option_id']);
    }

    /**
     * @param int $tariffId
     *
     * @return array
     */
    public static function getCurrentTariffOption($tariffId)
    {
        $option = WorkerOptionTariff::find()
            ->with('tariffCommission')
            ->where([
                'tariff_id'   => $tariffId,
                'tariff_type' => self::TYPE_CURRENT,
            ])
            ->asArray()
            ->one();

        return empty($option) ? null : $option;
    }

}
