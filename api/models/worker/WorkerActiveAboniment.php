<?php

namespace app\models\worker;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_worker_active_aboniment".
 *
 * @property integer      $aboniment_id
 * @property integer      $tariff_id
 * @property integer      $worker_id
 * @property integer      $count_active
 * @property integer      $expired_at
 * @property integer      $shift_valid_to
 * @property integer      $created_at
 * @property integer      $updated_at
 *
 * @property string       $type
 * @property string       $action_new_shift
 * @property string       $period_type
 * @property string       $period
 * @property string       $cost
 * @property string       $subscription_limit_type
 * @property integer      $days
 *
 * @property WorkerTariff $tariff
 * @property Worker       $worker
 */
class WorkerActiveAboniment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker_active_aboniment';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'worker_id'], 'required'],
            [['tariff_id', 'worker_id', 'count_active', 'expired_at', 'shift_valid_to'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'aboniment_id'   => 'Aboniment ID',
            'tariff_id'      => 'Tariff ID',
            'worker_id'      => 'Worker ID',
            'count_active'   => 'Count Active',
            'expired_at'     => 'Expired At',
            'shift_valid_to' => 'Shift Valid To',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(WorkerTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

}
