<?php

namespace app\models\worker;

use Yii;

/**
 * This is the model class for table "tbl_worker_has_position".
 *
 * @property integer     $id
 * @property integer     $worker_id
 * @property integer     $position_id
 * @property integer     $active
 * @property integer     $group_id
 * @property double      $rating
 *
 * @property Position    $position
 * @property Worker      $worker
 * @property WorkerGroup $group
 */
class WorkerHasPosition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker_has_position';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worker_id', 'position_id'], 'required'],
            [['worker_id', 'position_id', 'active', 'group_id'], 'integer'],
            [['rating'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'worker_id'   => 'Worker ID',
            'position_id' => 'Position ID',
            'active'      => 'Active',
            'group_id'    => 'Group ID',
            'rating'      => 'Rating',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(WorkerGroup::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasCars()
    {
        return $this->hasMany(WorkerHasCar::className(), ['has_position_id' => 'id']);
    }
}
