<?php

namespace app\models\worker;

use app\models\address\City;
use app\models\tenant\Tenant;
use Yii;

/**
 * This is the model class for table "tbl_worker".
 *
 * @property integer                 $worker_id
 * @property integer                 $tenant_id
 * @property integer                 $callsign
 * @property string                  $password
 * @property string                  $last_name
 * @property string                  $name
 * @property string                  $second_name
 * @property string                  $phone
 * @property string                  $photo
 * @property string                  $device
 * @property string                  $device_token
 * @property string                  $lang
 * @property string                  $description
 * @property string                  $device_info
 * @property string                  $email
 * @property string                  $partnership
 * @property string                  $birthday
 * @property integer                 $block
 * @property integer                 $create_time
 * @property integer                 $tenant_company_id
 * @property integer                 $activate
 *
 * @property City[]                  $cities
 * @property Tenant                  $tenant
 * @property WorkerActiveAboniment[] $workerActiveAboniments
 * @property WorkerBlock[]           $workerBlocks
 * @property WorkerHasDocument[]     $workerHasDocuments
 * @property WorkerHasPosition[]     $workerHasPositions
 * @property WorkerRefuseOrder[]     $workerRefuseOrders
 * @property WorkerReviewRating[]    $workerReviewRatings
 */
class Worker extends \yii\db\ActiveRecord
{
    const STATUS_FREE = 'FREE';
    const STATUS_BLOCKED = 'BLOCKED';
    const STATUS_ON_BREAK = 'ON_BREAK';
    const STATUS_ON_ORDER = 'ON_ORDER';
    const STATUS_ACCEPT_PREORDER = 'ACCEPT_PREORDER';
    const STATUS_REFUSE_PREORDER = 'REFUSE_PREORDER';

    const ACTION_PAUSE = 'pause';
    const ACTION_WORK = 'work';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'callsign', 'phone'], 'required'],
            [['tenant_id', 'callsign', 'block', 'create_time'], 'integer'],
            [['device', 'device_token', 'partnership'], 'string'],
            [['birthday'], 'safe'],
            [['password', 'photo', 'device_info'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 500],
            [['last_name', 'name', 'second_name', 'email'], 'string', 'max' => 45],
            [['phone'], 'string', 'max' => 15],
            [['lang'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'worker_id'    => 'Worker ID',
            'tenant_id'    => 'Tenant ID',
            'callsign'     => 'Callsign',
            'password'     => 'Password',
            'last_name'    => 'Last Name',
            'name'         => 'Name',
            'second_name'  => 'Second Name',
            'phone'        => 'Phone',
            'photo'        => 'Photo',
            'device'       => 'Device',
            'device_token' => 'Device Token',
            'lang'         => 'Lang',
            'description'  => 'Description',
            'device_info'  => 'Device Info',
            'email'        => 'Email',
            'partnership'  => 'Partnership',
            'birthday'     => 'Birthday',
            'block'        => 'Block',
            'create_time'  => 'Create Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['city_id' => 'city_id'])->via('workerHasCity');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasCities()
    {
        return $this->hasMany(WorkerHasCity::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerActiveAboniments()
    {
        return $this->hasMany(WorkerActiveAboniment::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerBlocks()
    {
        return $this->hasMany(WorkerBlock::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasDocuments()
    {
        return $this->hasMany(WorkerHasDocument::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasPositions()
    {
        return $this->hasMany(WorkerHasPosition::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerRefuseOrders()
    {
        return $this->hasMany(WorkerRefuseOrder::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerReviewRatings()
    {
        return $this->hasMany(WorkerReviewRating::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * Getting photo url
     *
     * @param int    $workerId
     * @param string $workerDomain
     *
     * @return null|string
     */
    public static function getPhotoUrl($workerId, $workerDomain)
    {
        $worker = self::findOne($workerId);

        if (!empty($worker)) {
            $photo = $worker->photo;

            if (!empty($photo)) {
                $url = Yii::$app->params['frontend.protocol'] . '://' . $workerDomain . '.' . Yii::$app->params['frontend.domain'] . '/file/show-external-file?filename=thumb_' . $photo . '&id=' . $worker->tenant_id;

                return $url;
            }
        }

        return null;
    }

    /**
     * Getting worker device info (user-agent)
     * @return string | null
     */
    public static function getWorkerDeviceInfo()
    {
        return \Yii::$app->request->headers->get('user-agent', null, true);
    }

}
