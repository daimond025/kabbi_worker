<?php

namespace app\models\worker;

use Yii;

/**
 * This is the model class for table "tbl_position_has_field".
 *
 * @property integer       $position_id
 * @property integer       $field_id
 *
 * @property Position      $position
 * @property PositionField $field
 */
class PositionHasField extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_position_has_field';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position_id', 'field_id'], 'required'],
            [['position_id', 'field_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'position_id' => 'Position ID',
            'field_id'    => 'Field ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getField()
    {
        return $this->hasOne(PositionField::className(), ['field_id' => 'field_id']);
    }
}
