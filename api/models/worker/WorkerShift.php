<?php

namespace app\models\worker;

use Yii;

/**
 * This is the model class for table "tbl_worker_shift".
 *
 * @property integer $id
 * @property integer $worker_id
 * @property integer $car_id
 * @property integer $city_id
 * @property integer $start_work
 * @property integer $end_work
 * @property integer $tariff_id
 * @property string  $pause_data
 * @property integer $position_id
 * @property integer $group_id
 * @property integer $group_priority
 * @property integer $position_priority
 */
class WorkerShift extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker_shift';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'worker_id',
                    'car_id',
                    'city_id',
                    'start_work',
                    'end_work',
                    'tariff_id',
                    'position_id',
                    'group_id',
                    'group_priority',
                    'position_priority',
                ],
                'integer',
            ],
            [['start_work', 'tariff_id'], 'required'],
            [['pause_data'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'worker_id'   => 'Worker ID',
            'car_id'      => 'Car ID',
            'city_id'     => 'City ID',
            'start_work'  => 'Start Work',
            'end_work'    => 'End Work',
            'tariff_id'   => 'Tariff ID',
            'pause_data'  => 'Pause Data',
            'position_id' => 'Position ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['id' => 'position_id'])->inverseOf('workerShifts');
    }
}
