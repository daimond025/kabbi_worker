<?php

namespace app\models\worker;

use app\models\worker\WorkerOptionTariff;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;

/**
 * This is the model class for table "tbl_tariff_commission".
 *
 * @property integer            $id
 * @property integer            $option_id
 * @property string             $type
 * @property string             $value
 * @property string             $from
 * @property string             $to
 *
 * @property WorkerOptionTariff $option
 */
class TariffCommission extends ActiveRecord
{
    const TYPE_PERCENT = 'PERCENT';
    const TYPE_MONEY = 'MONEY';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tariff_commission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_id'], 'required'],
            [['option_id'], 'integer'],
            [['type'], 'string'],
            [['value', 'from', 'to'], 'number', 'min' => 0],
            [
                ['option_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => WorkerOptionTariff::className(),
                'targetAttribute' => ['option_id' => 'option_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'option_id' => 'Option ID',
            'type'      => 'Type',
            'value'     => 'Value',
            'from'      => 'From',
            'to'        => 'To',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(WorkerOptionTariff::className(), ['option_id' => 'option_id']);
    }

}
