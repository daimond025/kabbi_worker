<?php

namespace app\models\worker;

use Yii;

/**
 * This is the model class for table "tbl_worker_group_has_tariff".
 *
 * @property integer      $group_id
 * @property integer      $tariff_id
 *
 * @property WorkerGroup  $group
 * @property WorkerTariff $tariff
 */
class WorkerGroupHasTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker_group_has_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'tariff_id'], 'required'],
            [['group_id', 'tariff_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id'  => 'Group ID',
            'tariff_id' => 'Tariff ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(WorkerGroup::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(WorkerTariff::className(), ['tariff_id' => 'tariff_id']);
    }
}
