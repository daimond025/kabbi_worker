<?php

namespace app\models\worker;

use Yii;

/**
 * This is the model class for table "tbl_worker_tariff_has_city".
 *
 * @property integer      $tariff_id
 * @property integer      $city_id
 *
 * @property City         $city
 * @property WorkerTariff $tariff
 */
class WorkerTariffHasCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker_tariff_has_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'city_id'], 'required'],
            [['tariff_id', 'city_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tariff_id' => 'Tariff ID',
            'city_id'   => 'City ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(WorkerTariff::className(), ['tariff_id' => 'tariff_id']);
    }
}
