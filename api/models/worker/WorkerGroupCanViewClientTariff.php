<?php

namespace app\models\worker;

use app\models\car\CarClass;
use Yii;

/**
 * This is the model class for table "tbl_worker_group_can_view_client_tariff".
 *
 * @property integer     $group_id
 * @property integer     $class_id
 *
 * @property CarClass    $class
 * @property WorkerGroup $group
 */
class WorkerGroupCanViewClientTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker_group_can_view_client_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'class_id'], 'required'],
            [['group_id', 'class_id'], 'integer'],
            [
                ['class_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CarClass::className(),
                'targetAttribute' => ['class_id' => 'class_id'],
            ],
            [
                ['group_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => WorkerGroup::className(),
                'targetAttribute' => ['group_id' => 'group_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id' => 'Group ID',
            'class_id' => 'Class ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(WorkerGroup::className(), ['group_id' => 'group_id']);
    }
}
