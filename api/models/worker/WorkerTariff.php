<?php

namespace app\models\worker;

use app\models\address\City;
use Yii;

/**
 * This is the model class for table "tbl_worker_tariff".
 *
 * @property integer                   $tariff_id
 * @property integer                   $tenant_id
 * @property integer                   $class_id
 * @property string                    $name
 * @property string                    $type
 * @property integer                   $block
 * @property integer                   $days
 * @property string                    $subscription_limit_type
 * @property string                    $description
 * @property integer                   $position_id
 * @property string                    $period_type
 * @property string                    $period
 * @property string                    $cost
 * @property string                    $action_new_shift
 *
 * @property CarHasWorkerGroupTariff[] $carHasWorkerGroupTariffs
 * @property Car[]                     $cars
 * @property WorkerActiveAboniment[]   $workerActiveAboniments
 * @property WorkerGroupHasTariff[]    $workerGroupHasTariffs
 * @property WorkerOptionTariff[]      $workerOptionTariffs
 * @property CarClass                  $class
 * @property Tenant                    $tenant
 * @property Position                  $position
 * @property WorkerTariffHasCity[]     $workerTariffHasCities
 * @property City[]                    $cities
 */
class WorkerTariff extends \yii\db\ActiveRecord
{
    const TYPE_ONCE = 'ONCE';
    const TYPE_SUBSCRIPTION = 'SUBSCRIPTION';

    const SUBSCRIPTION_LIMIT_TYPE_SHIFT = 'SHIFT_COUNT';
    const SUBSCRIPTION_LIMIT_TYPE_DAY = 'DAY_COUNT';

    const ACTION_NEW_SHIFT_CONTINUE_PERIOD = 'CONTINUE_PERIOD';
    const ACTION_NEW_SHIFT_NEW_PERIOD = 'NEW_PERIOD';

    const PERIOD_TYPE_INTERVAL = 'INTERVAL';
    const PERIOD_TYPE_HOURS = 'HOURS';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'position_id'], 'required'],
            [['tenant_id', 'class_id', 'block', 'days', 'subscription_limit_type', 'position_id'], 'integer'],
            [['type', 'description'], 'string'],
            [['name'], 'string', 'max' => 45],
            [['period_type'], 'required'],
            [['period_type'], 'string'],
            [['cost'], 'number'],
            [['period'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tariff_id'               => 'Tariff ID',
            'tenant_id'               => 'Tenant ID',
            'class_id'                => 'Class ID',
            'name'                    => 'Name',
            'type'                    => 'Type',
            'block'                   => 'Block',
            'days'                    => 'Days',
            'subscription_limit_type' => 'Subscription Limit Type',
            'description'             => 'Description',
            'position_id'             => 'Position ID',
            'period_type'             => 'Period Type',
            'period'                  => 'Period',
            'cost'                    => 'Cost',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarHasWorkerGroupTariffs()
    {
        return $this->hasMany(CarHasWorkerGroupTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['car_id' => 'car_id'])->viaTable('tbl_car_has_worker_group_tariff',
            ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerActiveAboniments()
    {
        return $this->hasMany(WorkerActiveAboniment::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerGroupHasTariffs()
    {
        return $this->hasMany(WorkerGroupHasTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerOptionTariffs()
    {
        return $this->hasMany(WorkerOptionTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerTariffHasCities()
    {
        return $this->hasMany(WorkerTariffHasCity::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['city_id' => 'city_id'])->viaTable('tbl_worker_tariff_has_city',
            ['tariff_id' => 'tariff_id']);
    }
}