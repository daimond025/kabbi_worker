<?php

namespace app\models\worker;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%car_mileage}}".
 *
 * @property integer     $id
 * @property integer     $car_id
 * @property integer     $shift_id
 * @property double      $begin_value
 * @property double      $end_value
 * @property integer     $created_at
 * @property integer     $updated_at
 *
 * @property WorkerShift $shift
 */
class CarMileage extends \yii\db\ActiveRecord
{
    const MILEAGE_MAX_VALUE = 9999999999;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_mileage}}';
    }

    /**
     * @inherited
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            TimestampBehavior::className(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_id', 'shift_id', 'begin_value'], 'required'],
            [['car_id', 'shift_id', 'created_at', 'updated_at'], 'integer'],
            [['begin_value', 'end_value'], 'number', 'max' => self::MILEAGE_MAX_VALUE],
            [['shift_id'], 'unique'],
            [
                ['shift_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => WorkerShift::className(),
                'targetAttribute' => ['shift_id' => 'id'],
            ],
            ['end_value', 'compare', 'compareAttribute' => 'begin_value', 'operator' => '>=', 'type' => 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'car_id'      => 'Car ID',
            'shift_id'    => 'Shift ID',
            'begin_value' => 'Begin Value',
            'end_value'   => 'End Value',
            'created_at'  => 'Created At',
            'updated_at'  => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShift()
    {
        return $this->hasOne(WorkerShift::className(), ['id' => 'shift_id']);
    }

    public static function getByShiftId(int $shiftId): ?self
    {
        $model = self::find()->where(['shift_id' => $shiftId])->one();

        return empty($model) ? null : $model;
    }

    public static function getLastShiftWithMileage(int $carId): ?int
    {
        $lastValue = self::find()
            ->alias('t')
            ->select(['t.shift_id'])
            ->where(['t.car_id' => $carId])
            ->orderBy(['t.shift_id' => SORT_DESC])
            ->limit(1)
            ->scalar();

        return empty($lastValue) ? null : (int)$lastValue;
    }

    public static function getCurrentMileage(int $carId): float
    {
        $lastValue = self::find()
            ->alias('t')
            ->select(['ifnull(t.end_value, t.begin_value)'])
            ->where(['t.car_id' => $carId])
            ->orderBy(['t.shift_id' => SORT_DESC])
            ->limit(1)
            ->scalar();

        return empty($lastValue) ? 0 : (float)$lastValue;
    }

    public static function existsByShiftId(int $shiftId): bool
    {
        return self::find()->where(['shift_id' => $shiftId])->exists();
    }

    public static function createCarMileage(int $carId, int $shiftId, float $beginValue): void
    {
        $model = new self([
            'car_id'      => $carId,
            'shift_id'    => $shiftId,
            'begin_value' => $beginValue,
        ]);

        if (!$model->save()) {
            throw new Exception(implode('; ', $model->getFirstErrors()));
        }
    }

    public static function setEndValue(int $carId, int $shiftId, float $endValue): void
    {
        $model = self::find()
            ->where([
                'car_id'   => $carId,
                'shift_id' => $shiftId,
            ])
            ->one();

        if (empty($model)) {
            throw new Exception("Car mileage model not found: carId={$carId}, shiftId={$shiftId}");
        }

        $model->end_value = $endValue;

        if (!$model->save()) {
            throw new Exception(implode('; ', $model->getFirstErrors()));
        }
    }
}
