<?php

namespace app\models\worker;

use app\models\order\Order;
use app\models\order\OrderDetailCost;
use app\models\worker\WorkerShift;
use Yii;

/**
 * This is the model class for table "{{%worker_shift_order}}".
 *
 * @property integer     $id
 * @property integer     $shift_id
 * @property integer     $order_id
 * @property integer     $created_at
 * @property integer     $updated_at
 *
 * @property Order       $order
 * @property WorkerShift $shift
 */
class WorkerShiftOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_shift_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shift_id', 'order_id'], 'required'],
            [['shift_id', 'order_id', 'created_at', 'updated_at'], 'integer'],
            [['order_id'], 'unique'],
            [
                ['order_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Order::className(),
                'targetAttribute' => ['order_id' => 'order_id'],
            ],
            [
                ['shift_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => WorkerShift::className(),
                'targetAttribute' => ['shift_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'shift_id'   => 'Shift ID',
            'order_id'   => 'Order ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShift()
    {
        return $this->hasOne(WorkerShift::className(), ['id' => 'shift_id']);
    }

    public function getOrderDetailCost()
    {
        return $this->hasOne(OrderDetailCost::className(), ['order_id' => 'order_id']);
    }
}
