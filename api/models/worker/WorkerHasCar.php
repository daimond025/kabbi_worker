<?php

namespace app\models\worker;

use app\models\car\Car;
use Yii;

/**
 * This is the model class for table "tbl_worker_has_car".
 *
 * @property integer $has_position_id
 * @property integer $car_id
 *
 * @property Car     $car
 */
class WorkerHasCar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker_has_car';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['has_position_id', 'car_id'], 'required'],
            [['has_position_id', 'car_id'], 'integer'],
            [
                ['car_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Car::className(),
                'targetAttribute' => ['car_id' => 'car_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'has_position_id' => 'Has Position ID',
            'car_id'          => 'Car ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasPosition()
    {
        return $this->hasOne(WorkerHasPosition::className(), ['id' => 'has_position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['car_id' => 'car_id']);
    }
}
