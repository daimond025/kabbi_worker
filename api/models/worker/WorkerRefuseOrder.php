<?php

namespace app\models\worker;

use Yii;

/**
 * This is the model class for table "tbl_worker_refuse_order".
 *
 * @property integer $refuse_id
 * @property integer $shift_id
 * @property integer $order_id
 * @property integer $refuse_time
 * @property string  $refuse_subject
 * @property integer $worker_id
 *
 * @property Worker  $worker
 */
class WorkerRefuseOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker_refuse_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shift_id', 'order_id', 'refuse_time', 'refuse_subject', 'worker_id'], 'required'],
            [['shift_id', 'order_id', 'refuse_time', 'worker_id'], 'integer'],
            [['refuse_subject'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'refuse_id'      => 'Refuse ID',
            'shift_id'       => 'Shift ID',
            'order_id'       => 'Order ID',
            'refuse_time'    => 'Refuse Time',
            'refuse_subject' => 'Refuse Subject',
            'worker_id'      => 'Worker ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }
}
