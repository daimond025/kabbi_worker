<?php

namespace bankCard;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%card_payment}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string  $pan
 * @property float   $payment_sum
 * @property string  $payment_order_id
 * @property string  $created_at
 * @property string  $updated_at
 *
 * @property Order   $order
 */
class CardPayment extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%card_payment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'pan'], 'required'],
            [['order_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['payment_sum'], 'number'],
            [['pan'], 'string', 'max' => 25],
            [['payment_order_id'], 'string', 'max' => 36],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'ID',
            'order_id'         => 'Order ID',
            'pan'              => 'Pan',
            'payment_order_id' => 'Payment Order ID',
            'created_at'       => 'Created At',
            'updated_at'       => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

}