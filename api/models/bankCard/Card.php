<?php

declare(strict_types=1);

namespace bankCard;

use api\components\curl\exceptions\RestException;
use api\components\curl\RestCurl;
use yii\base\NotSupportedException;

/**
 * Class Card
 * @package bankCard
 */
class Card
{
    /**
     * @var RestCurl
     */
    private $restCurl;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $lang;

    public function __construct(RestCurl $restCurl, string $url, string $lang)
    {
        $this->restCurl = $restCurl;
        $this->url      = $url;
        $this->lang = $lang;
    }

    public function getCards(string $tenantId, string $profile, string $workerId): array
    {
        $response = $this->restCurl->get("{$this->url}{$this->getBaseUrl()}/{$profile}/worker_{$workerId}");

        return $response['result'] ?? [];
    }

    public function create(string $tenantId, string $profile, string $workerId): array
    {
        $url = "{$this->url}{$this->getBaseUrl()}/{$profile}/worker_{$workerId}";

        // empty array is bugfix for common\components\curl\Curl
        $postParams = ['empty' => 'empty'];

        return $this->doAction(CardPaymentLog::TYPE_ADD_CARD, $tenantId, $profile, $workerId, $url, $postParams);
    }

    public function check(string $tenantId, string $profile, string $workerId, string $orderId): array
    {
        $url = "{$this->url}{$this->getBaseUrl()}/{$profile}/worker_{$workerId}";

        return $this->doAction(CardPaymentLog::TYPE_CHECK_CARD, $tenantId, $profile, $workerId, $url, compact('orderId'));
    }

    public function delete(string $tenantId, string $profile, string $workerId, string $pan): array
    {
        $url = "{$this->url}{$this->getBaseUrl()}/{$profile}/worker_{$workerId}/{$pan}";

        return $this->doAction(CardPaymentLog::TYPE_REMOVE_CARD, $tenantId, $profile, $workerId, $url);
    }

    private function getBaseUrl(): string
    {
        return 'v0/cards';
    }

    private function doAction(
        string $actionType,
        string $tenantId,
        string $profile,
        string $workerId,
        string $url,
        ?array $postParams = []
    ): array {
        try {
            $this->restCurl->setHeaders([
                'Accept-Language' => $this->lang,
            ]);
            switch ($actionType) {
                case CardPaymentLog::TYPE_ADD_CARD:
                    $response = $this->restCurl->post($url, $postParams);
                    break;
                case CardPaymentLog::TYPE_CHECK_CARD:
                    $response = $this->restCurl->put($url, $postParams);
                    break;
                case CardPaymentLog::TYPE_REMOVE_CARD:
                    $response = $this->restCurl->delete($url, $postParams);
                    break;
                default:
                    throw new NotSupportedException('Type of action is not supported by log');
            }

            $result = CardPaymentLog::RESULT_OK;
            CardPaymentLog::log($tenantId, $profile, null, null, $workerId, $actionType, $postParams, $response, $result);

            return $response;
        } catch (RestException $ex) {
            $response  = [
                'message'  => $ex->getMessage(),
                'code'     => $ex->getCode(),
                'response' => $ex->getResponse(),
            ];
            $exception = $ex;
            $result    = CardPaymentLog::RESULT_ERROR;
        } catch (\Exception $ex) {
            $response  = [
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode(),
            ];
            $exception = $ex;
            $result    = CardPaymentLog::RESULT_ERROR;
        }

        CardPaymentLog::log($tenantId, $profile, null, null, $workerId, $actionType, $postParams, $response, $result);

        \Yii::error(implode("\n",
            [
                "An error has occurred in step '{$actionType}'",
                "Url: {$url}",
                print_r($postParams, true),
                print_r($response, true),
            ]), 'bank-card');

        throw $exception;
    }

}