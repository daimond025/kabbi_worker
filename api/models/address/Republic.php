<?php

namespace app\models\address;

use Yii;

/**
 * This is the model class for table "{{%tbl_republic}}".
 *
 * @property string $republic_id
 * @property integer $country_id
 * @property string $name
 * @property string $timezone
 *
 * @property TblCity[] $tblCities
 * @property TblCountry $country
 */
class Republic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%republic}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'name'], 'required'],
            [['country_id'], 'integer'],
            [['name', 'timezone'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'republic_id' => Yii::t('city', 'Republic ID'),
            'country_id' => Yii::t('city', 'Country ID'),
            'name' => Yii::t('city', 'Name'),
            'timezone' => Yii::t('city', 'Timezone'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasMany(City::className(), ['republic_id' => 'republic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }
}
