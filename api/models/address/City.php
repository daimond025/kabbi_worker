<?php

namespace app\models\address;

use app\models\worker\WorkerHasCity;
use api\components\TimeZone;

/**
 * This is the model class for table "tbl_city".
 *
 * @property integer         $city_id
 * @property integer         $republic_id
 * @property string          $name
 * @property string          $shortname
 * @property string          $lat
 * @property string          $lon
 * @property string          $fulladdress
 * @property string          $fulladdress_reverse
 * @property string          $sort
 *
 * @property Republic        $republic
 * @property Order[]         $orders
 * @property PhoneLine[]     $phoneLines
 * @property TenantHasCity[] $tenantHasCities
 * @property WorkerHasCity[] $workerHasCities
 * @property Tenant[]        $tenants
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['republic_id', 'name'], 'required'],
            [['republic_id'], 'integer'],
            [['name', 'shortname', 'lat', 'lon'], 'string', 'max' => 100],
            [['fulladdress', 'fulladdress_reverse'], 'string', 'max' => 200],
            [['sort'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id'             => 'City ID',
            'republic_id'         => 'Republic ID',
            'name'                => 'Name',
            'shortname'           => 'Shortname',
            'lat'                 => 'Lat',
            'lon'                 => 'Lon',
            'fulladdress'         => 'Fulladdress',
            'fulladdress_reverse' => 'Fulladdress Reverse',
            'sort'                => 'Sort',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepublic()
    {
        return $this->hasOne(Republic::className(), ['republic_id' => 'republic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoneLines()
    {
        return $this->hasMany(PhoneLine::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCities()
    {
        return $this->hasMany(TenantHasCity::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasCities()
    {
        return $this->hasMany(WorkerHasCity::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenants()
    {
        return $this->hasMany(Tenant::className(), ['tenant_id' => 'tenant_id'])->viaTable('tbl_tenant_has_city',
            ['city_id' => 'city_id']);
    }

    public static function getCityNameById($id)
    {
        $cityData = self::findOne($id);
        if ($cityData) {
            return $cityData->name;
        }

        return null;
    }

    /**
     * Getting city time offset
     *
     * @param int $city_id
     *
     * @return int
     */
    public static function getTimeOffset($city_id)
    {
        try {
            /**
             * @var $component TimeZone
             */
            //            $component = \Yii::$container->get(TimeZone::className());

            $timeOffset = self::find()
                ->alias('t')
                ->select('r.timezone')
                ->joinWith('republic r')
                ->where(['t.city_id' => $city_id])
                ->scalar();

            //            $timeOffset = $component->getTimeOffsetByTimezone($timezone);
        } catch (\Exception $ex) {
            $timeOffset = 0;
        }

        return empty($timeOffset) ? 0 : $timeOffset * 3600;
    }

}
