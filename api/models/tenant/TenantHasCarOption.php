<?php

namespace app\models\tenant;

use Yii;

/**
 * This is the model class for table "tbl_tenant_has_car_option".
 *
 * @property integer      $tenant_id
 * @property integer      $option_id
 * @property string       $value
 * @property integer      $city_id
 *
 * @property TblCarOption $option
 * @property TblTenant    $tenant
 */
class TenantHasCarOption extends \yii\db\ActiveRecord
{
    /**
     * @var array Car option list
     */
    public $option_list;

    /**
     * @var array Tenant option list
     */
    public $tenant_options;

    /**
     * @var array Tenant option list
     */
    public $tenant_options_active;

    public static function tableName()
    {
        return '{{%tenant_has_car_option}}';
    }

    public function rules()
    {
        return [
            [['value'], 'required'],
            [['tenant_id', 'option_id', 'value', 'city_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'tenant_id' => Yii::t('tenant', 'Tenant ID'),
            'option_id' => Yii::t('tenant', 'Option ID'),
            'value'     => Yii::t('tenant', 'Value'),
            'city_id'   => Yii::t('tenant', 'City ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(CarOption::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCity()
    {
        return $this->hasMany(TenantHasCity::className(), ['tenant_id' => 'tenant_id']);
    }

}
