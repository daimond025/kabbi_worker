<?php

namespace app\models\tenant;

use app\models\address\City;
use app\models\worker\Position;

/**
 * This is the model class for table "{{%tenant_city_has_position}}".
 *
 * @property integer  $id
 * @property integer  $tenant_id
 * @property integer  $city_id
 * @property integer  $position_id
 *
 * @property City     $city
 * @property Position $position
 * @property Tenant   $tenant
 */
class TenantCityHasPosition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_city_has_position}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'city_id', 'position_id'], 'required'],
            [['tenant_id', 'city_id', 'position_id'], 'integer'],
            [
                ['tenant_id', 'city_id', 'position_id'],
                'unique',
                'targetAttribute' => ['tenant_id', 'city_id', 'position_id'],
                'message'         => 'The combination of Tenant ID, City ID and Position ID has already been taken.',
            ],
            [
                ['city_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => City::className(),
                'targetAttribute' => ['city_id' => 'city_id'],
            ],
            [
                ['position_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Position::className(),
                'targetAttribute' => ['position_id' => 'position_id'],
            ],
            [
                ['tenant_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tenant::className(),
                'targetAttribute' => ['tenant_id' => 'tenant_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'tenant_id'   => 'Tenant ID',
            'city_id'     => 'City ID',
            'position_id' => 'Position ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }
}
