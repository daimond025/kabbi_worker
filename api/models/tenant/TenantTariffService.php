<?php

namespace app\models\tenant;

use yii\base\Object;
use yii\db\Connection;

/**
 * Class TenantTariffService
 * @package tenantTariff
 */
class TenantTariffService extends Object
{
    const MODULE_TAXI = 1;

    const PERMISSION_WORKERS_ONLINE = 1;
    const PERMISSION_USERS_ONLINE = 2;

    const SQL_CURRENT_TARIFF = <<<SQL
select `tt`.`id`
from `tbl_tenant_tariff` `tt`
join `tbl_tariff` `t` on `tt`.`tariff_id` = `t`.`id`
join `tbl_module` `m` on `t`.`module_id` = `m`.`id`
                and `m`.`code` = :module
                and `m`.`active` = 1
where `tt`.`tenant_id` = :tenantId
    and `tt`.`started_at` <= :date
    and `tt`.`expiry_date` >= :date
    and `tt`.`active` = 1

group by `m`.`code`
order by `tt`.`started_at` desc
SQL;

    const SQL_PERMISSION = <<<SQL
select `ttp`.`value`
from `tbl_tenant_permission` `ttp`
join `tbl_tariff_permission` `tp` on `ttp`.`permission_id` = `tp`.`id`
join `tbl_permission` `p` on `tp`.`permission_id` = `p`.`id`
                        and `p`.`code` = :permission
where `ttp`.`tenant_tariff_id` = :tariffId
SQL;

    const SQL_DEFAULT_PERMISSION = <<<SQL
select `tp`.`value`
from `tbl_tariff` `t`
join `tbl_module` `m` on `t`.`module_id` = `m`.`id`
                    and `m`.`code` = :module
                    and `m`.`active` = 1
join `tbl_tariff_permission` `tp` on `t`.`id` = `tp`.`tariff_id`
                                and `tp`.`active` = 1
join `tbl_permission` `p` on `tp`.`permission_id` = `p`.`id`
                        and `p`.`code` = :permission
where `t`.`default` = 1
group by `m`.`code`
SQL;

    /**
     * @var Connection
     */
    public $db;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (empty($this->db)) {
            $this->db = \Yii::$app->db;
        }

        parent::init();
    }

    /**
     * Getting permission value
     *
     * @param integer    $tenantId
     * @param integer    $module
     * @param integer    $permission
     * @param integer    $time
     * @param Connection $db
     *
     * @return string|false
     */
    public function getPermission($tenantId, $module, $permission, $time = null)
    {
        $date = date('d.m.Y', empty($time) ? time() : $time);

        $tariffId = $this->db->createCommand(self::SQL_CURRENT_TARIFF, [
            ':tenantId' => $tenantId,
            ':module'   => $module,
            ':date'     => strtotime($date),
        ])->queryScalar();

        if ($tariffId === false) {
            return $this->db->createCommand(self::SQL_DEFAULT_PERMISSION, [
                ':module'     => $module,
                ':permission' => $permission,
            ])->queryScalar();
        } else {
            return $this->db->createCommand(self::SQL_PERMISSION, [
                ':tariffId'   => $tariffId,
                ':permission' => $permission,
            ])->queryScalar();
        }
    }
}