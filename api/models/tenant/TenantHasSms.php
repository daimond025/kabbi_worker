<?php

namespace app\models\tenant;

use Yii;

/**
 * This is the model class for table "tbl_tenant_has_sms".
 *
 * @property string $id
 * @property integer $server_id
 * @property string $tenant_id
 * @property string $login
 * @property string $password
 * @property integer $active
 *
 * @property SmsServer $server
 * @property Tenant $tenant
 */
class TenantHasSms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tenant_has_sms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['server_id', 'tenant_id', 'login', 'password', 'active'], 'required'],
            [['server_id', 'tenant_id', 'active'], 'integer'],
            [['login', 'password'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'server_id' => 'Server ID',
            'tenant_id' => 'Tenant ID',
            'login' => 'Login',
            'password' => 'Password',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServer()
    {
        return $this->hasOne(SmsServer::className(), ['server_id' => 'server_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }
}
