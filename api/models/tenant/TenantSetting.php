<?php

namespace app\models\tenant;

use app\models\balance\Currency;
use yii\web\ServerErrorHttpException;

/**
 * This is the model class for table "tbl_tenant_setting".
 *
 * @property integer $setting_id
 * @property integer $tenant_id
 * @property string $name
 * @property string $value
 *
 * @property Tenant $tenant
 */
class TenantSetting extends \yii\db\ActiveRecord
{

    public const CACHE_TIME = 3600;
    public const CACHE_ON   = true;

    public const DEFAULT_CURRENCY_CODE = 'RUB';

    public const SETTING_GEOCODER_TYPE = "GEOCODER_TYPE";

    public const SETTING_PICK_UP   = 'PICK_UP';
    public const SETTING_PRE_ORDER = 'PRE_ORDER';

    public const SETTING_CURRENCY = 'CURRENCY';

    public const SETTING_ORDER_OFFER_SEC = 'ORDER_OFFER_SEC';

    public const SETTING_OFFER_ORDER_TYPE              = 'OFFER_ORDER_TYPE';
    public const SETTING_OFFER_ORDER_IN_ROTATION_TYPE_ = 'queue';
    public const SETTING_OFFER_ORDER_MULTIPLE_TYPE     = 'batch';

    public const SETTING_TYPE_OF_DISTRIBUTION_ORDER           = 'TYPE_OF_DISTRIBUTION_ORDER';
    public const SETTING_TYPE_OF_DISTRIBUTION_ORDER_DISTANCE  = 1;
    public const SETTING_TYPE_OF_DISTRIBUTION_ORDER_DISTRICTS = 2;

    public const SETTING_DISTANCE_FOR_SEARCH                = 'DISTANCE_FOR_SEARCH';
    public const SETTING_CHECK_WORKER_RATING_TO_OFFER_ORDER = 'CHECK_WORKER_RATING_TO_OFFER_ORDER';

    public const SETTING_WORKER_API_KEY     = 'WORKER_API_KEY';
    public const SETTING_WORKER_MIN_BAlANCE = 'WORKER_MIN_BAlANCE';

    public const SETTING_WORKER_TYPE_BLOCK            = 'WORKER_TYPE_BLOCK';
    public const SETTING_WORKER_COUNT_TO_BLOCK        = 'WORKER_COUNT_TO_BLOCK';
    public const SETTING_WORKER_TIME_OF_BLOCK         = 'WORKER_TIME_OF_BLOCK';
    public const SETTING_WORKER_PRE_ORDER_REMINDER    = 'WORKER_PRE_ORDER_REMINDER';
    public const SETTING_WORKER_PRE_ORDER_BLOCK_HOURS = 'WORKER_PRE_ORDER_BLOCK_HOUER';
    public const SETTING_WORKER_PRE_ORDER_REJECT_MIN  = 'WORKER_PRE_ORDER_REJECT_MIN';
    public const SETTING_WORKER_ARRIVAL_TIME          = 'WORKER_ARRIVAL_TIME';

    public const SETTING_SHOW_WORKER_PHONE   = 'SHOW_WORKER_PHONE';
    public const SETTING_SHOW_WORKER_ADDRESS = 'SHOW_WORKER_ADDRESS';
    public const SETTING_SHOW_WORKER_CLIENT  = 'SHOW_WORKER_CLIENT';

    public const SETTING_DISTANCE_TO_FILTER_FREE_ORDERS = 'DISTANCE_TO_FILTER_FREE_ORDERS';

    public const SETTING_ALLOW_WORK_WITHOUT_GPS            = 'ALLOW_WORK_WITHOUT_GPS';
    public const SETTING_ALLOW_USE_BANK_CARD               = 'ALLOW_USE_BANK_CARD';
    public const SETTING_ALLOW_WORKER_TO_CANCEL_ORDER      = 'ALLOW_WORKER_TO_CANCEL_ORDER';
    public const SETTING_TIME_ALLOW_WORKER_TO_CANCEL_ORDER = 'TIME_ALLOW_WORKER_TO_CANCEL_ORDER';
    public const SETTING_SHOW_CHAT_DISPATCHER_EXIT         = 'SHOW_CHAT_DISPATCHER_EXIT';
    public const SETTING_SHOW_CHAT_DISPATCHER_SHIFT        = 'SHOW_CHAT_DISPATCHER_SHIFT';
    public const SETTING_SHOW_GENERAL_CHAT_SHIFT           = 'SHOW_GENERAL_CHAT_SHIFT';
    public const SETTING_START_TIME_BY_ORDER               = 'START_TIME_BY_ORDER';
    public const SETTING_SHOW_ESTIMATION                   = 'SHOW_ESTIMATION';
    public const SETTING_ALLOW_EDIT_ORDER                  = 'ALLOW_EDIT_ORDER';
    public const SETTING_SHOW_URGENT_ORDER_TIME            = 'SHOW_URGENT_ORDER_TIME';

    public const SETTING_DELAY_CIRCLES_DISTRIBUTION_ORDER        = 'DELAY_CIRCLES_DISTRIBUTION_ORDER';
    public const SETTING_FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES = 'FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES';

    public const SETTING_ALLOW_WORKER_TO_CREATE_ORDER           = 'ALLOW_WORKER_TO_CREATE_ORDER';
    public const SETTING_DENY_FAKEGPS                           = 'DENY_FAKEGPS';
    public const SETTING_DENY_SETTING_ARRIVAL_STATUS_EARLY      = 'DENY_SETTING_ARRIVAL_STATUS_EARLY';
    public const SETTING_MIN_DISTANCE_TO_SET_ARRIVAL_STATUS     = 'MIN_DISTANCE_TO_SET_ARRIVAL_STATUS';
    public const REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION = 'REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION';
    public const PRINT_CHECK                                    = 'PRINT_CHECK';
    public const SETTING_CONTROL_OWN_CAR_MILEAGE                = 'CONTROL_OWN_CAR_MILEAGE';
    public const SETTING_RESTRICT_VISIBILITY_OF_PRE_ORDER       = 'RESTRICT_VISIBILITY_OF_PRE_ORDER';
    public const SETTING_TIME_OF_PRE_ORDER_VISIBILITY           = 'TIME_OF_PRE_ORDER_VISIBILITY';
    public const SHOW_DRIVER_PRIVACY_POLICY                     = 'SHOW_DRIVER_PRIVACY_POLICY';
    public const ALLOW_EDIT_COST_ORDER                          = 'ALLOW_EDIT_COST_ORDER';
    public const ADD_INTERMEDIATE_POINT_IN_ORDER_FOR_STOP       = 'ADD_INTERMEDIATE_POINT_IN_ORDER_FOR_STOP';


    public const SETTING_ALLOW_RESERVE_URGENT_ORDERS = 'ALLOW_RESERVE_URGENT_ORDERS';
    public const SETTING_WORKER_REVIEW_COUNT         = 'WORKER_REVIEW_COUNT';
    public const SETTING_WORKER_MIN_REVIEW_RATING    = 'WORKER_MIN_REVIEW_RATING';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tenant_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'name', 'value'], 'required'],
            [['tenant_id'], 'integer'],
            [['name', 'value'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting_id' => 'Setting ID',
            'tenant_id'  => 'Tenant ID',
            'name'       => 'Name',
            'value'      => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * Getting setting value.
     *
     * @param int $tenantId
     * @param string $setting
     * @param int $cityId
     * @param int $positionId
     *
     * @return string
     */
    public static function getSettingValue($tenantId, $setting, $cityId = null, $positionId = null)
    {
        $value = self::find()
            ->select('value')
            ->where([
                'tenant_id' => $tenantId,
                'name'      => $setting,
            ])
            ->andFilterWhere([
                'city_id'     => $cityId,
                'position_id' => $positionId,
            ])
            ->scalar();

        return $value;
    }


    public static function getTenantCurrencyId($tenantId, $cityId)
    {
        return self::getSettingValue($tenantId, self::SETTING_CURRENCY, $cityId);
    }

    public static function getTenantCurrencyCode($tenantId, $cityId)
    {
        $currencyId = self::getTenantCurrencyId($tenantId, $cityId);
        $code = Currency::find()
            ->select('code')
            ->where(['currency_id' => $currencyId])
            ->scalar();

        return empty($code) ? self::DEFAULT_CURRENCY_CODE : $code;
    }

    /**
     * @param $tenantId
     * @param $cityId
     * @param $positionId
     * @return int
     * @throws ServerErrorHttpException
     */
    public static function getIsViewQueue($tenantId, $cityId, $positionId): ?int
    {
        $typeOfDistributionOrder = self::getSettingValue(
            $tenantId, self::SETTING_TYPE_OF_DISTRIBUTION_ORDER, $cityId, $positionId);

        switch ($typeOfDistributionOrder) {
            case self::SETTING_TYPE_OF_DISTRIBUTION_ORDER_DISTANCE:
                return 0;
            case self::SETTING_TYPE_OF_DISTRIBUTION_ORDER_DISTRICTS:
                return 1;

            default:
                \Yii::error('There is no such type of distribution of orders: ' . $typeOfDistributionOrder);
                throw new ServerErrorHttpException();
        }
    }

}
