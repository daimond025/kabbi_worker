<?php

namespace app\models\tenant;

use Yii;
use app\models\address\City;

/**
 * This is the model class for table "tbl_tenant_has_city".
 *
 * @property integer $tenant_id
 * @property integer $city_id
 *
 * @property Tenant $tenant
 * @property City $city
 */
class TenantHasCity extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tenant_has_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'city_id'], 'required'],
            [['tenant_id', 'city_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tenant_id' => 'Tenant ID',
            'city_id'   => 'City ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

}
