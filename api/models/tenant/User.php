<?php

namespace app\models\tenant;

use yii\web\IdentityInterface;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%tbl_user}}".
 *
 * @property string $user_id
 * @property string $tenant_id
 * @property string $position_id
 * @property string $email
 * @property string $email_confirm
 * @property string $password
 * @property string $last_name
 * @property string $name
 * @property string $second_name
 * @property string $phone
 * @property string $photo
 * @property integer $active
 * @property string $birth
 * @property string $address
 * @property string $create_time
 * @property string $auth_key
 * @property string $password_reset_token
 * @property timestamp $active_time
 *
 * @property Order[] $Orders
 * @property OrderHistory[] $OrderHistories
 * @property Support[] $Supports
 * @property SupportFeedback[] $SupportFeedbacks
 * @property Tenant $tenant
 * @property UserPosition $position
 * @property UserHasCity[] $UserHasCities
 * @property City[] $cities
 * @property UserSetting[] $UserSettings
 * @property UserWorkingTime[] $UserWorkingTimes
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * position_id должности администратора
     */
    const ADMIN = 7;

    public $password_repeat;
    public $city_list = [];
    public $position_list = [];
    /**
     * Flag of block user
     * @var bool
     */
    public $block;
    /**
     *Is user online
     * @var string on|off The value immediately substituted in name of css class
     */
    public $online = 'off';

    /**
     * Vars for user birth
     */
    public $birth_day;
    public $birth_month;
    public $birth_year;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password', 'name', 'last_name'], 'required', 'on' => 'insert'],
            [['phone'], 'filter', 'filter' => function($value){ return preg_replace("/[^0-9]/", '', $value);}],
            [['tenant_id', 'position_id', 'active'], 'integer'],
            [['password_repeat', 'city_list', 'block'], 'safe'],
            [['email'], 'string', 'max' => 20],
            [['address'], 'string', 'max' => 255],
            [['last_name', 'name', 'second_name'], 'string', 'max' => 45],
            [['phone'], 'string', 'max' => 15],
            [['email'], 'unique'],
            [['email'], 'email'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'on' => 'update'],
            ['password', 'string', 'min' => 6],
            ['photo', 'file', 'extensions' => ['png', 'jpeg', 'jpg']],
        ];
    }

    /**
    * @return array customized attribute labels (name=>label)
    */
    public function attributeLabels()
    {
       return array(
           'password' => t('user', 'Password'),
           'password_repeat' => t('user', 'Repeat password'),
           'address' => t('user', 'Address'),
           'birth' => t('user', 'Birth'),
           'name' => t('user', 'Name'),
           'last_name' => t('user', 'Last name'),
           'second_name' => t('user', 'Second name'),
           'create_time' => t('user', 'Registration date'),
           'position_id' => t('user', 'Position'),
           'position.name' => t('user', 'Position'),
           'city' => t('user', 'City'),
           'email' => 'E-mail',
           'phone' => t('user', 'Mobile phone'),
           'active' => t('user', 'Active user'),
           'photo' => t('user', 'Photo'),
           'contact_name' => t('user', 'Contact name'),
           'active_time' => 'Online',
           'city_list' => t('user', 'Working in cities')
       );
    }

    /**
     * Get user position list
     * @return array [position_id => name]
     */
    public function getPositionList()
    {
        return ArrayHelper::map(UserPosition::find()->all(), 'position_id', 'name');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['user_modifed' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistories()
    {
        return $this->hasMany(OrderHistory::className(), ['user_modifed' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportFeedbacks()
    {
        return $this->hasMany(SupportFeedback::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(UserPosition::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserHasCities()
    {
        return $this->hasMany(UserHasCity::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(\app\modules\city\models\City::className(), ['city_id' => 'city_id'])->viaTable('{{%user_has_city}}', ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSettings()
    {
        return $this->hasMany(UserSetting::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserWorkingTimes()
    {
        return $this->hasMany(UserWorkingTime::className(), ['user_id' => 'user_id']);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public static function findIdentity($id)
    {
        return static::findOne(['user_id' => $id]);
    }

    public static function findIdentityByEmail($email)
    {
        $user = static::find()
          ->where([
              static::tableName() . '.email' => $email,
              'active' => 1
          ])
          ->joinWith(['tenant' => function($query) {
                $query->andWhere('status!="REMOVED"');
            }
          ], false)
          ->one();

        if(is_null($user) || ($user->position_id != self::ADMIN && $user->tenant->status == Tenant::BLOCKED))
            return false;

        return $user;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    //---------------------------------------------------------

    //Recovery password

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'active' => 1,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }



    public function setEmailToken()
    {
        $this->email_confirm = md5($this->email . time());
    }

    /**
     * Format array of user models for table.
     * @param array $users Array of \app\modules\tenant\models\User
     * @param bool $city_list Return result with city list from all users.
     * @param bool $position_list Return result with position list from all users.
     * @return array
     */
    public static function getGridData($users, $city_list = true, $position_list = true)
    {
        $arUsers['USERS'] = [];

        if($city_list)
            $arCity = [];

        if($position_list)
            $arPosition = [];

        foreach($users as $user)
        {
            $arUserCity = [];

            foreach($user->cities as $city)
            {
                $arUserCity[] = $city->name;

                if($city_list && !array_key_exists($city->city_id, $arCity))
                    $arCity[$city->city_id] = $city->name;
            }

            if($position_list && !array_key_exists($user['position']['position_id'], $arPosition))
                $arPosition[$user['position']['position_id']] = $user['position']['name'];

            asort($arUserCity);

            $arUsers['USERS'][] = [
                'ID' => $user->user_id,
                'CITIES' => implode(', ', $arUserCity),
                'PHOTO' => $user->getPictureHtml($user->photo),
                'ONLINE' => time() - $user['active_time'] < app()->params['online'] ? 'on' : 'off',
                'NAME' => trim($user->last_name . ' ' . $user->name . ' ' . $user->second_name),
                'POSITION' => $user->position->name
            ];
        }

        if($city_list)
        {
            asort($arCity);
            $arUsers['CITI_LIST'] = $arCity;
        }

        if($position_list)
        {
            asort($arPosition);
            $arUsers['POSITION_LIST'] = $arPosition;
        }

        return $arUsers;
    }

    /**
     * Send invite to email
     * @param string $password You can send a user password to email.
     */
    public function sendInvite($password = null)
    {
        $params = [
            'TEMPLATE' => 'confirmUserEmail',
            'TO' => $this->email,
            'SUBJECT' => t('app', 'Confirmation of registration for {name}', ['name' => '"' . app()->name . '"']),
            'DATA' => [
                'URL' => app()->urlManager->createAbsoluteUrl(['tenant/user/confirm-email', 'token' => $this->email_confirm]),
                'NAME' => $this->last_name . ' ' . $this->name,
                'PASSWORD' => $password
            ]
        ];

        Yii::$app->gearman->doBackground(\app\components\gearman\Gearman::EMAIL_TASK, $params);
    }

    /**
     * Check user invite status
     * @return bool
     */
    public function isUserInvited()
    {
        return $this->email_confirm != 1 && $this->active == 0;
    }

    /**
     *
     * @return array ['city_id => 'name']
     */
    public function getUserCityList()
    {
        return ArrayHelper::map($this->cities, 'city_id', 'name');
    }

    /**
     *
     * @return array
     */
    public function getUserCityListWithRepublic()
    {
        $user = self::find()
          ->where(['user_id' => $this->id])
          ->with([
              'cities' => function($query){
                    $query->select(['city_id', 'name', 'republic_id']);
              },
              'cities.republic' => function($query){
                $query->select(['republic_id', 'timezone']);
            }])
          ->select(['user_id'])
          ->asArray()
          ->one();

        return ArrayHelper::getValue($user, 'cities');
    }

    public function getFullName()
    {
        return trim($this->last_name . ' ' . $this->name . ' ' . $this->second_name);
    }
}
