<?php

namespace app\models\tenant;

use app\models\worker\Worker;
use Yii;

/**
 * This is the model class for table "tbl_tenant".
 *
 * @property string $tenant_id
 * @property string $company_name
 * @property string $full_company_name
 * @property string $legal_address
 * @property string $post_address
 * @property string $contact_name
 * @property string $contact_second_name
 * @property string $contact_last_name
 * @property string $contact_phone
 * @property integer $contact_phone_confirm
 * @property string $contact_email
 * @property string $company_phone
 * @property string $domain
 * @property string $email
 * @property string $email_confirm
 * @property string $inn
 * @property string $create_time
 * @property string $bookkeeper
 * @property string $kpp
 * @property string $ogrn
 * @property string $site
 * @property string $archive
 * @property string $logo
 * @property string $director
 * @property string $director_position
 * @property string $status
 * @property integer $sms_code
 * @property string $sms_code_expire
 *
 * @property Car[] $cars
 * @property Client[] $clients
 * @property ClientCompany[] $clientCompanies
 * @property Driver[] $drivers
 * @property DriverTariff[] $driverTariffs
 * @property MessageCity[] $messageCities
 * @property Order[] $orders
 * @property OrderHistory[] $orderHistories
 * @property OrderViews[] $orderViews
 * @property Parking[] $parkings
 * @property PaymentTnDr[] $paymentTnDrs
 * @property PaymentTnSt[] $paymentTnSts
 * @property PaymentTnTn[] $paymentTnTns
 * @property PhoneLine[] $phoneLines
 * @property Support[] $supports
 * @property TaxiTariff[] $taxiTariffs
 * @property TenantBalance[] $tenantBalances
 * @property TenantHasCarOption[] $tenantHasCarOptions
 * @property CarOption[] $options
 * @property TenantHasCity[] $tenantHasCities
 * @property City[] $cities
 * @property TenantHasSms[] $tenantHasSms
 * @property TenantSetting[] $tenantSettings
 * @property TenantTariff[] $tenantTariffs
 * @property User[] $users
 */
class Tenant extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tenant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_phone', 'domain', 'email'], 'required'],
            [['contact_phone_confirm', 'sms_code', 'sms_code_expire'], 'integer'],
            [['create_time', 'archive'], 'safe'],
            [['status'], 'string'],
            [['company_name', 'full_company_name', 'legal_address', 'post_address', 'logo'], 'string', 'max' => 255],
            [['contact_name', 'contact_second_name', 'contact_last_name', 'email_confirm', 'bookkeeper', 'director_position'], 'string', 'max' => 45],
            [['contact_phone', 'company_phone', 'inn'], 'string', 'max' => 15],
            [['contact_email', 'email', 'ogrn', 'site'], 'string', 'max' => 20],
            [['domain', 'kpp'], 'string', 'max' => 10],
            [['director'], 'string', 'max' => 50],
            [['domain'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tenant_id'             => 'Tenant ID',
            'company_name'          => 'Company Name',
            'full_company_name'     => 'Full Company Name',
            'legal_address'         => 'Legal Address',
            'post_address'          => 'Post Address',
            'contact_name'          => 'Contact Name',
            'contact_second_name'   => 'Contact Second Name',
            'contact_last_name'     => 'Contact Last Name',
            'contact_phone'         => 'Contact Phone',
            'contact_phone_confirm' => 'Contact Phone Confirm',
            'contact_email'         => 'Contact Email',
            'company_phone'         => 'Company Phone',
            'domain'                => 'Domain',
            'email'                 => 'Email',
            'email_confirm'         => 'Email Confirm',
            'inn'                   => 'Inn',
            'create_time'           => 'Create Time',
            'bookkeeper'            => 'Bookkeeper',
            'kpp'                   => 'Kpp',
            'ogrn'                  => 'Ogrn',
            'site'                  => 'Site',
            'archive'               => 'Archive',
            'logo'                  => 'Logo',
            'director'              => 'Director',
            'director_position'     => 'Director Position',
            'status'                => 'Status',
            'sms_code'              => 'Sms Code',
            'sms_code_expire'       => 'Sms Code Expire',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Client::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCompanies()
    {
        return $this->hasMany(ClientCompany::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkers()
    {
        return $this->hasMany(Worker::className(), ['tenant_id' => 'tenant_id']);
    }

//    /**
    //     * @return \yii\db\ActiveQuery
    //     */
    //    public function getDriverTariffs()
    //    {
    //        return $this->hasMany(DriverTariff::className(), ['tenant_id' => 'tenant_id']);
    //    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessageCities()
    {
        return $this->hasMany(MessageCity::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistories()
    {
        return $this->hasMany(OrderHistory::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderViews()
    {
        return $this->hasMany(OrderViews::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParkings()
    {
        return $this->hasMany(Parking::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTnDrs()
    {
        return $this->hasMany(PaymentTnDr::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTnSts()
    {
        return $this->hasMany(PaymentTnSt::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTnTns()
    {
        return $this->hasMany(PaymentTnTn::className(), ['recipient' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoneLines()
    {
        return $this->hasMany(PhoneLine::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxiTariffs()
    {
        return $this->hasMany(TaxiTariff::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantBalances()
    {
        return $this->hasMany(TenantBalance::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCarOptions()
    {
        return $this->hasMany(TenantHasCarOption::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(CarOption::className(), ['option_id' => 'option_id'])->viaTable('tbl_tenant_has_car_option', ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCities()
    {
        return $this->hasMany(TenantHasCity::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['city_id' => 'city_id'])->viaTable('tbl_tenant_has_city', ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasSms()
    {
        return $this->hasMany(TenantHasSms::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantSettings()
    {
        return $this->hasMany(TenantSetting::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantTariffs()
    {
        return $this->hasMany(TenantTariff::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * Получение пути лого файла
     * @param type $tenantId
     */
    public static function getLogoUrl($tenantId, $tenantDomain)
    {
        $tenantData = self::findOne($tenantId);
        if (!empty($tenantData)) {
            $logo = $tenantData->logo;
            if (!empty($logo)) {
                $url = Yii::$app->params['frontend.protocol'] . '://' .  $tenantDomain . '.' . Yii::$app->params['frontend.domain'] . '/file/show-external-file?filename=thumb_' . $logo . '&id=' . $tenantId;
                return $url;
            }
        }
        return '';
    }

}
