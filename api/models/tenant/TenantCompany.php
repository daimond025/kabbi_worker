<?php

namespace app\models\tenant;


use app\models\car\Car;
use app\models\order\Order;
use app\models\worker\Worker;
use app\models\worker\WorkerTariff;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "{{%tenant_company}}".
 *
 * @property integer        $tenant_company_id
 * @property integer        $tenant_id
 * @property string         $name
 * @property integer        $sort
 * @property integer        $block
 * @property integer        $logo
 * @property integer        $use_logo_company
 * @property integer        $phone
 * @property integer        $user_contact
 *
 * @property Car[]          $cars
 * @property Order[]        $orders
 * @property Tenant         $tenant
 * @property User[]         $users
 * @property Worker[]       $workers
 * @property WorkerTariff[] $workerTariffs
 */
class TenantCompany extends ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_company}}';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkers()
    {
        return $this->hasMany(Worker::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerTariffs()
    {
        return $this->hasMany(WorkerTariff::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

}