<?php

return [
    'id'         => 'basic-tests',
    'basePath'   => dirname(__DIR__),
    'aliases'    => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'language'   => 'en-US',
    'components' => [
        'db'         => null,
        'mailer'     => [
            'useFileTransport' => true,
        ],
        'urlManager' => [
            'showScriptName' => true,
        ],
        'user'       => [
            'identityClass' => 'app\models\User',
        ],
        'request'    => [
            'cookieValidationKey'  => 'test',
            'enableCsrfValidation' => false,
        ],
    ],

    'params' => require __DIR__ . '/params.php',
];