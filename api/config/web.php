<?php

$db = require __DIR__ . '/db.php';

$logVars = array_merge(
    ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
    require __DIR__ . '/logVarsFilter.php'
);

$config = [
    'id'         => 'api_worker',
    'basePath'   => dirname(__DIR__),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap'  => ['log'],
    'aliases'    => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        // Yii2 components
        'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl'  => '@web/assets',
        ],
        'cache'        => [
            'class'     => 'yii\redis\Cache',
            'redis'     => $db['redisCache'],
            'keyPrefix' => getenv('REDIS_CACHE_KEY_PREFIX'),
        ],

        'errorHandler' => [
            'errorAction' => '/v3/api/error',
        ],

        'i18n' => [
            'translations' => [
                '*'    => [
                    'class'            => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                ],
                'app*' => [
                    'class'            => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                ],
            ],
        ],

        'log' => [
            'traceLevel' => 3,
            'targets'    => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'except'  => ['yii\debug\Module::checkAccess'],
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                ],
                [
                    'class'   => 'yii\log\EmailTarget',
                    'except'  => ['yii\debug\Module::checkAccess'],
                    'mailer'  => 'mailer',
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                    'message' => [
                        'from'    => [getenv('MAIL_SUPPORT_USERNAME')],
                        'to'      => [getenv('MAIL_SUPPORT_USERNAME')],
                        'subject' => getenv('MAIL_SUPPORT_SUBJECT'),
                    ],
                ],
                [
                    'class'   => 'notamedia\sentry\SentryTarget',
                    'dsn'     => getenv('SENTRY_DSN'),
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                    'except'  => [
                        'yii\web\HttpException:400',
                        'yii\web\HttpException:401',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                    ],
                    'clientOptions' => [
                        'environment' => getenv('ENVIRONMENT_NAME'),
                    ],
                ],
            ],
        ],

        'mailer' => [
            'class'            => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport'        => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => getenv('MAIL_SUPPORT_HOST'),
                'port'       => getenv('MAIL_SUPPORT_PORT'),
                'username'   => getenv('MAIL_SUPPORT_USERNAME'),
                'password'   => getenv('MAIL_SUPPORT_PASSWORD'),
                'encryption' => getenv('MAIL_SUPPORT_ENCRYPTION'),
            ],
        ],

        'request' => [
            'baseUrl'              => '',
            'cookieValidationKey'  => getenv('COOKIE_VALIDATION_KEY'),
            'enableCsrfValidation' => false,
        ],

        'response' => [
            'class'        => '\yii\web\Response',
            'on afterSend' => function () {
                /** @var $apiLogger \logger\ApiLogger */
                $apiLogger = \Yii::$app->apiLogger;
                $apiLogger->setRequest(\Yii::$app->request);
                $apiLogger->setResponse(\Yii::$app->response);
                $apiLogger->export();
            },
        ],

        'urlManager'                  => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
                'version'     => '/api/version',
                'status'      => '/api/status',
                'v3/<action>' => '/v3/api/<action>',
            ],
        ],

        // Databases
        'db'                          => $db['dbMain'],
        'mongodb'                     => $db['mongodbMain'],
        'redis_pub_sub'               => $db['redisMainPubSub'],
        'redis_workers'               => $db['redisMainWorkers'],
        'redis_orders_active'         => $db['redisMainOrdersActive'],
        'redis_phone_scenario'        => $db['redisMainPhoneScenario'],
        'redis_active_call'           => $db['redisMainActiveCall'],
        'redis_queue_call'            => $db['redisMainQueueCall'],
        'redis_active_dispetcher'     => $db['redisMainActiveDispatcher'],
        'redis_free_dispetcher_queue' => $db['redisMainDispatcherQueue'],
        'redis_worker_shift'          => $db['redisMainWorkerShift'],
        'redis_parking_hashtable'     => $db['redisMainParkingHashTable'],
        'redis_order_event'           => $db['redisMainOrderEvent'],
        'redis_worker_orders'         => $db['redisMainWorkerOrders'],
        'redis_main_check_status'     => $db['redisMainCheckStatus'],
        'redis_cache_check_status'    => $db['redisCacheCheckStatus'],

        // Other components and services
        'amqp'                        => [
            'class'    => 'app\components\rabbitmq\Amqp',
            'host'     => getenv('RABBITMQ_MAIN_HOST'),
            'port'     => getenv('RABBITMQ_MAIN_PORT'),
            'user'     => getenv('RABBITMQ_MAIN_USER'),
            'password' => getenv('RABBITMQ_MAIN_PASSWORD'),
        ],

        'apiLogger' => logger\ApiLogger::class,

        'gearman' => [
            'class' => 'app\components\gearman\Gearman',
            'host'  => getenv('GEARMAN_MAIN_HOST'),
            'port'  => getenv('GEARMAN_MAIN_PORT'),
        ],

        'pusher' => [
            'class' => 'app\components\taxiPushComponent\TaxiPusherComponent',
            'url'   => getenv('SERVICE_PUSH_URL'),
        ],

        'restCurl' => 'api\components\curl\RestCurl',

        'routeAnalyzer' => [
            'class' => 'app\components\routeAnalyzer\TaxiRouteAnalyzer',
            'url'   => getenv('API_SERVICE_URL'),
        ],

        'consulService' => [
            'class'           => 'app\components\consul\ConsulService',
            'consulAgentHost' => getenv('CONSUL_MAIN_HOST'),
            'consulAgentPort' => getenv('CONSUL_MAIN_PORT'),
        ],

        'tenantTariffService' => 'app\models\tenant\TenantTariffService',

        'orderApi' => [
            'class'   => 'api\components\orderApi\OrderApi',
            'baseUrl' => getenv('API_ORDER_URL'),
        ],
    ],

    'modules' => [
        'v3' => 'v3\Module',
    ],

    'params' => require __DIR__ . '/params.php',
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => ['192.168.1.*'],
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class'      => 'yii\gii\Module',
        'allowedIPs' => ['192.168.1.*'],
    ];
}

return $config;