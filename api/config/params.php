<?php

return [
    'frontend.protocol' => getenv('FRONTEND_PROTOCOL'),
    'frontend.domain'   => getenv('FRONTEND_DOMAIN'),

    'paymentGateApi.url' => getenv('API_PAYGATE_URL'),

    'bonusSystem.UDSGame.baseUrl'           => 'https://udsgame.com/v1/partner/',
    'bonusSystem.UDSGame.connectionTimeout' => 15,
    'bonusSystem.UDSGame.timeout'           => 15,

    'supportedLanguages'   => ['ru', 'en', 'az', 'de', 'se', 'sr', 'ro'],
    'checkSignature'       => true,
    'saveOrder.retryCount' => 5,

    'version' => require __DIR__ . '/version.php',
];
