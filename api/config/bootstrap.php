<?php

\Yii::setAlias('api', __DIR__ . '/../../api');
\Yii::setAlias('app', '@api');
\Yii::setAlias('v3', '@api/modules/v3');
\Yii::setAlias('bankCard', '@api/models/bankCard');
\Yii::setAlias('logger', '@api/components/logger');
\Yii::setAlias('bonusSystem', '@api/components/bonusSystem');
\Yii::setAlias('healthCheck', '@api/components/healthCheck');
\Yii::setAlias('addressHistory', '@api/components/addressHistory');
\Yii::setAlias('paymentGate', '@api/components/paymentGate');


\logger\ApiLogger::setupStartTimeForStatistic();