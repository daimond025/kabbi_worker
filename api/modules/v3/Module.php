<?php

namespace v3;

use v3\components\CityService;
use v3\components\OrderService;
use v3\components\ParkingService;
use v3\components\TenantService;
use v3\components\TimeHelper;
use v3\components\WorkerService;
use v3\components\WorkerShiftService;
use v3\components\WorkerTariffService;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'v3\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->setupDependencies();

        \Yii::configure($this, require(__DIR__ . '/config.php'));
    }

    /**
     * Setup dependencies
     */
    private function setupDependencies()
    {
        \Yii::$container->setSingleton('workerService', WorkerService::className());
        \Yii::$container->setSingleton('workerTariffService', WorkerTariffService::className());
        \Yii::$container->setSingleton('workerShiftService', WorkerShiftService::className());
        \Yii::$container->setSingleton('orderService', OrderService::className());
        \Yii::$container->setSingleton('cityService', CityService::className());
        \Yii::$container->setSingleton('parkingService', ParkingService::className());
        \Yii::$container->setSingleton('tenantService', TenantService::className());

        \Yii::$container->setSingleton('timeHelper', TimeHelper::className());
    }
}
