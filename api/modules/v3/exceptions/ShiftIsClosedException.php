<?php

namespace v3\exceptions;

use yii\base\Exception;

/**
 * Class ShiftIsClosedException
 * @package v3\exceptions
 */
class ShiftIsClosedException extends Exception
{

}