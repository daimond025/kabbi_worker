<?php

namespace v3\components;

use app\models\tenant\TenantHasCity;
use yii\base\Object;

/**
 * Class TenantService
 * @package v3\components
 */
class TenantService extends Object
{
    /**
     * Is city blocked
     *
     * @param int $tenantId
     * @param int $cityId
     *
     * @return bool
     */
    public function isCityBlocked($tenantId, $cityId)
    {
        return TenantHasCity::find()
            ->where([
                'tenant_id' => $tenantId,
                'city_id'   => $cityId,
                'block'     => 1,
            ])
            ->exists();
    }
}