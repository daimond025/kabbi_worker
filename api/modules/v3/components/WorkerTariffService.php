<?php

namespace v3\components;

use api\models\balance\WorkerTransaction;
use app\models\address\City;
use app\models\car\Car;
use app\models\car\CarHasOption;
use app\models\car\CarOption;
use app\models\tariff\TaxiTariff;
use app\models\tenant\TenantHasCarOption;
use app\models\tenant\TenantSetting;
use app\models\worker\TariffCommission;
use app\models\worker\WorkerActiveAboniment;
use app\models\worker\WorkerGroup;
use app\models\worker\WorkerOptionDiscount;
use app\models\worker\WorkerOptionTariff;
use app\models\worker\WorkerTariff;
use v3\components\repositories\WorkerActiveAbonimentRepository;
use v3\exceptions\ActivateAbonimentShiftException;
use yii\base\InvalidConfigException;
use yii\base\Object;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

/**
 * Class WorkerTariffService
 * @package v3\components
 */
class WorkerTariffService extends Object
{

    /* @var $workerActiveAbonimentRepository WorkerActiveAbonimentRepository */
    protected $workerActiveAbonimentRepository;
    /** @var TaxiRouteAnalyzer */
    protected $routeAnalizer;

    public function init()
    {
        parent::init();
        $this->routeAnalizer = \Yii::$app->routeAnalyzer;
        $this->workerActiveAbonimentRepository = \Yii::$container->get(WorkerActiveAbonimentRepository::class);
    }


    /**
     * Getting worker tariff
     *
     * @param int $workerTariffId
     * @param int $workerCityId
     *
     * @return array|null
     */
    public function getWorkerTariff($workerTariffId, $workerCityId = null)
    {

        return WorkerTariff::find()
            ->alias('t')
            ->joinWith('workerTariffHasCities w', false)
            ->where(['t.tariff_id' => $workerTariffId])
            ->andFilterWhere(['w.city_id' => $workerCityId])
            ->asArray()
            ->one();
    }

    /**
     * @param string $periodType
     * @param string $period
     * @param int    $time
     *
     * @return bool
     */
    public function isValidPeriodTime($periodType, $period, $time)
    {
        return $periodType === WorkerTariff::PERIOD_TYPE_HOURS
            || ($periodType === WorkerTariff::PERIOD_TYPE_INTERVAL && $this->timestampInInterval($period, $time));
    }

    /**
     * Has active aboniment
     *
     * @param int $workerId
     * @param int $tariffId
     * @param int $currentTime
     *
     * @return bool
     */
    public function hasActiveAboniment($workerId, $tariffId, $currentTime)
    {
        /* @var $activeAboniment WorkerActiveAboniment */
        $activeAboniment = WorkerActiveAboniment::find()
            ->where([
                'worker_id' => $workerId,
                'tariff_id' => $tariffId,
            ])
            ->andWhere([
                'or',
                ['>', 'shift_valid_to', $currentTime],
                ['and', ['expired_at' => null], ['>', 'count_active', 0]],
                ['and', ['count_active' => null], ['>', 'expired_at', $currentTime]],
                ['and', ['>', 'count_active', 0], ['>', 'expired_at', $currentTime]],
            ])
            ->one();

        if ($activeAboniment
            && $activeAboniment->tariff->type == WorkerTariff::TYPE_ONCE
            && $activeAboniment->tariff->action_new_shift == WorkerTariff::ACTION_NEW_SHIFT_NEW_PERIOD
        ) {
            return false;
        }

        return (bool)$activeAboniment;
    }

    public function isValidWorkerForBorderTariff(array $activeWorker, $tariffId)
    {
        $cityId    = isset($activeWorker['worker']['city_id']) ? $activeWorker['worker']['city_id'] : null;
        $classes   = isset($activeWorker['position']['classes']) ? $activeWorker['position']['classes'] : null;
        $positions = isset($activeWorker['position']['positions']) ? $activeWorker['position']['positions'] : null;
        $tenantId  = isset($activeWorker['worker']['tenant_id']) ? $activeWorker['worker']['tenant_id'] : null;

        if ($activeWorker['position']['has_car'] == 1) {
            return TaxiTariff::isExistsTariffsByCarClasses($tenantId, $classes, $cityId, $tariffId);
        }

        return TaxiTariff::isExistsTariffsByPositions($tenantId, $positions, $cityId, $tariffId);
    }

    /**
     * @param int $workerId
     * @param int $currentTime
     */
    public function deleteInactiveAboniments($workerId, $currentTime)
    {
        $this->workerActiveAbonimentRepository->deleteOnceWithNewPeriod($workerId);

        try {
            WorkerActiveAboniment::deleteAll([
                'and',
                ['worker_id' => $workerId],
                ['<', 'shift_valid_to', $currentTime],
                [
                    'or',
                    ['and', ['expired_at' => null], ['<', 'count_active', 1]],
                    ['and', ['count_active' => null], ['<', 'expired_at', $currentTime]],
                    [
                        'and',
                        ['not', ['count_active' => null]],
                        ['not', ['expired_at' => null]],
                        ['or', ['<', 'count_active', 1], ['<', 'expired_at', $currentTime]],
                    ],
                ],
            ]);
        } catch (\Exception $ex) {
            \Yii::error("Delete inactive aboniment error: error={$ex->getMessage()}", 'worker-tariff-service');
        }
    }

    /**
     * Create worker aboniment
     *
     * @param int $workerId
     * @param int $workerTariffId
     * @param int $countActive
     * @param int $expiredAt
     * @param int $shiftValidTo
     * @param array $workerTariff
     *
     * @return bool
     */
    public function createWorkerAboniment($workerId, $workerTariffId, $countActive, $expiredAt, $shiftValidTo, $workerTariff)
    {
        $aboniment = new WorkerActiveAboniment();

        $aboniment->worker_id      = $workerId;
        $aboniment->tariff_id      = $workerTariffId;
        $aboniment->count_active   = $countActive;
        $aboniment->expired_at     = $expiredAt;
        $aboniment->shift_valid_to = $shiftValidTo;

        $aboniment->type                    = $workerTariff['type'];
        $aboniment->action_new_shift        = $workerTariff['action_new_shift'];
        $aboniment->period_type             = $workerTariff['period_type'];
        $aboniment->period                  = $workerTariff['period'];
        $aboniment->cost                    = $workerTariff['cost'];
        $aboniment->subscription_limit_type = $workerTariff['subscription_limit_type'];
        $aboniment->days                    = $workerTariff['days'];

        return $aboniment->save();
    }

    /**
     * @param int $workerId
     * @param int $tariffId
     *
     * @return int
     */
    public function getAbonimentShiftValidDate($workerId, $tariffId)
    {
        $model = WorkerActiveAboniment::find()
            ->where([
                'worker_id' => $workerId,
                'tariff_id' => $tariffId,
            ])
            ->one();

        return $model->shift_valid_to ?? 0;
    }

    /**
     * @param int $workerId
     * @param int $workerTariffId
     * @param int $shiftValidTo
     *
     * @return int
     * @throws ActivateAbonimentShiftException
     * @throws Exception
     */
    public function activateAbonimentShiftAndGetValidDate($workerId, $workerTariffId, $shiftValidTo)
    {
        $aboniment = WorkerActiveAboniment::findOne([
            'worker_id' => $workerId,
            'tariff_id' => $workerTariffId,
        ]);

        if (empty($aboniment)) {
            throw new ActivateAbonimentShiftException('Aboniment not found');
        }

        $aboniment->shift_valid_to = $shiftValidTo;

        if ($aboniment->count_active !== null) {
            $aboniment->count_active -= 1;
        }

        if ($aboniment->expired_at !== null && $shiftValidTo > $aboniment->expired_at) {
            $aboniment->shift_valid_to = $aboniment->expired_at;
        }

        if (!$aboniment->save()) {
            throw new ActivateAbonimentShiftException('Save aboniment information error');
        }

        return $aboniment->shift_valid_to;
    }

    /**
     * Getting car
     *
     * @param int $carId
     *
     * @return array|null
     */
    public function getCar($carId)
    {
        return Car::find()
            ->alias('t')
            ->with('workerTariffs')
            ->where(['t.car_id' => $carId])
            ->asArray()
            ->one();
    }

    public function getWorkerGroup($workerId, $positionId)
    {
        return WorkerGroup::find()
            ->alias('g')
            ->joinWith('workerHasPositions wp')
            ->with('workerGroupHasTariffs')
            ->where([
                'wp.worker_id'   => $workerId,
                'wp.position_id' => $positionId,
                'wp.active'      => 1,
            ])
            ->asArray()
            ->one();
    }

    /**
     * Checking is valid car for tariff
     *
     * @param int $tariffId
     * @param int $carId
     * @param int $workerCityId
     *
     * @return bool
     */
    public function isValidCarForTariff($tariffId, $carId, $workerCityId)
    {
        $tariff = $this->getWorkerTariff($tariffId);
        $car    = $this->getCar($carId);

        if (empty($tariff) || empty($car) || $tariff['block'] == 1) {
            return false;
        } elseif (empty($car['workerTariffs'])) {
            return $car['class_id'] == $tariff['class_id']
                && $car['city_id'] == $workerCityId;

        } else {
            $tariffs = ArrayHelper::getColumn($car['workerTariffs'], 'tariff_id');

            return in_array($tariffId, $tariffs, false)
                && $car['city_id'] == $workerCityId;
        }
    }

    /**
     * Checking is valid position for tariff
     *
     * @param int $workerId
     * @param int $tariffId
     * @param int $positionId
     *
     * @return bool
     */
    public function isValidPositionForTariff($workerId, $tariffId, $positionId)
    {
        $tariff      = $this->getWorkerTariff($tariffId);
        $workerGroup = $this->getWorkerGroup($workerId, $positionId);

        if (empty($tariff) || $tariff['block'] == 1) {
            return false;
        } elseif (empty($workerGroup['workerGroupHasTariffs'])) {
            return $positionId == $tariff['position_id'];
        } else {
            $tariffIds = ArrayHelper::getColumn($workerGroup['workerGroupHasTariffs'], 'tariff_id');

            return in_array($tariffId, $tariffIds, false);
        }
    }

    /**
     * Getting data of tariff option
     *
     * @param int $workerTariffId
     * @param int $time
     *
     * @return array
     * @throws InvalidConfigException
     */
    public function getTariffOptionData($workerTariffId, $time)
    {
        $date = new \DateTime();
        $date->setTimestamp($time);

        $dateFull    = $date->format('d.m.Y');
        $dateShort   = $date->format('d.m');
        $dayOfWeek   = date('l', strtotime($dateFull));
        $currentTime = $date->format('H:i');

        $optionId = $this->getTariffOptionId($workerTariffId, [$dayOfWeek, $dateShort, $dateFull], $currentTime);

        return WorkerOptionTariff::find()
            ->with('tariffCommission')
            ->where([
                'option_id' => $optionId,
            ])
            ->asArray()
            ->one();
    }

    /**
     * @param int    $tariffId
     * @param array  $currentDays
     * @param string $currentTime
     *
     * @return int
     * @throws InvalidConfigException
     */
    private function getTariffOptionId($tariffId, $currentDays, $currentTime)
    {
        $currentOptionId = WorkerOptionTariff::find()
            ->select('option_id')
            ->where([
                'tariff_id'   => $tariffId,
                'tariff_type' => WorkerOptionTariff::TYPE_CURRENT,
            ])
            ->orderBy(['sort' => SORT_ASC, 'option_id' => SORT_ASC])
            ->scalar();

        $options = WorkerOptionTariff::find()
            ->select(['option_id', 'active_date'])
            ->where([
                'and',
                ['tariff_id' => $tariffId],
                ['!=', 'tariff_type', WorkerOptionTariff::TYPE_CURRENT],
            ])
            ->orderBy(['sort' => SORT_ASC, 'option_id' => SORT_ASC])
            ->asArray()
            ->all();

        if (!empty($options)) {
            foreach ($options as $option) {
                $dates = empty($option['active_date']) ? [] : explode(';', $option['active_date']);

                foreach ($dates as $date) {

                    $array = explode('|', $date);
                    $day   = ArrayHelper::getValue($array,0);
                    $time  = ArrayHelper::getValue($array, 1, '00:00-23:59');

                    if (!in_array((string)$day, $currentDays, true)) {
                        continue;
                    }

                    $timeInterval = explode('-', $time);
                    if ($timeInterval[0] <= $currentTime && $timeInterval[1] >= $currentTime) {
                        return $option['option_id'];
                    }
                }
            }
        }

        return $currentOptionId;
    }

    /**
     * Попадает ли время в интервал
     *
     * @param string $period - 'h-i:h-i', example: "12-00:23:00"
     * @param string $time - timestamp
     *
     * @return boolean
     */
    public function timestampInInterval($period, $time)
    {
        $periodArr   = explode("-", $period);
        $periodStart = explode(":", $periodArr[0]);
        $periodEnd   = explode(":", $periodArr[1]);

        $startH = (int)$periodStart[0];
        $startM = (int)$periodStart[1];
        $endH   = (int)$periodEnd[0];
        $endM   = (int)$periodEnd[1];

        $datetime = \DateTime::createFromFormat('U', $time);
        $nowH     = (int)$datetime->format('H');
        $nowM     = (int)$datetime->format('i');

        if ($endH > $startH) {
            if (($nowH == $startH && $nowM >= $startM)
                || ($nowH > $startH && $nowH < $endH)
                || ($nowH == $endH && $nowM < $endM)
            ) {
                return true;
            }
        } else {
            if ($endH == $startH) {
                if ($nowH == $startH && $nowM >= $startM && $nowM < $endM) {
                    return true;
                }
            } else {
                $startH1 = $startH;
                $startM1 = $startM;
                $endH1   = 23;
                $endM1   = 59;
                $startH2 = 0;
                $startM2 = 0;
                $endH2   = $endH;
                $endM2   = $endM;

                if (($nowH == $startH1 && $nowM >= $startM1)
                    || ($nowH > $startH1 && $nowH < $endH1)
                    || ($nowH == $endH1 && $nowM < $endM1)
                ) {
                    return true;
                } else {
                    if (($nowH == $startH2 && $nowM >= $startM2)
                        || ($nowH > $startH2 && $nowH < $endH2)
                        || ($nowH == $endH2 && $nowM < $endM2)
                    ) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Getting discount for car options
     *
     * @param int   $tenantId
     * @param int   $cityId
     * @param int   $workerOptionTariffId
     * @param int   $workerCarId
     * @param float $baseCost
     *
     * @return float
     */
    public function getDiscountForCarOptions(
        $tenantId,
        $cityId,
        $workerOptionTariffId,
        $workerCarId,
        $baseCost
    ) {
        $discountCost    = 0;
        $discountPercent = 0;

        $tenantCarOptions = TenantHasCarOption::find()
            ->select('option_id')
            ->where([
                'tenant_id' => $tenantId,
                'city_id'   => $cityId,
                'is_active' => 1,
            ])
            ->asArray()
            ->all();

        $discounts = WorkerOptionDiscount::find()
            ->select(['discount_line_type', 'discount_line'])
            ->alias('t')
            ->joinWith(['carOption co', 'carOption.cars c'], false)
            ->where([
                't.option_id'     => $workerOptionTariffId,
                't.car_option_id' => isset($tenantCarOptions)
                    ? ArrayHelper::getColumn($tenantCarOptions, 'option_id') : null,
                'c.car_id'        => $workerCarId,
            ])
            ->asArray()
            ->all();

        if (is_array($discounts)) {
            foreach ($discounts as $discount) {
                switch ($discount['discount_line_type']) {
                    case WorkerOptionDiscount::TYPE_MONEY:
                        $discountCost += $discount['discount_line'];
                        break;
                    case WorkerOptionDiscount::TYPE_PERCENT:
                        $discountPercent += $discount['discount_line'];
                        break;
                }
            }
        }

        $result = $discountCost + ($baseCost * $discountPercent / 100);

        return $result > $baseCost ? $baseCost : $result;
    }

    /**
     * Buy worker tariff
     *
     * @param int    $tenantId
     * @param int    $workerId
     * @param int    $workerCityId
     * @param string $tariff
     * @param float  $tariffCost
     * @param string $requestId
     *
     * @return int
     * @throws InvalidConfigException
     */
    public function buyWorkerTariff(
        $tenantId,
        $workerId,
        $workerCityId,
        $tariff,
        $tariffCost,
        $requestId = ''
    ) {
        $currencyId = TenantSetting::getTenantCurrencyId($tenantId, $workerCityId);

        /** @var $workerTransaction WorkerTransaction */
        $workerTransaction = \Yii::$container->get(WorkerTransaction::className());

        return $workerTransaction->buyWorkerTariff($tenantId, $workerId, $tariff, $tariffCost, $currencyId,
            $requestId);
    }

    public function getAdditionalsOptions(int $tariffId, string $lang, int $time)
    {
        $addoptions = $this->routeAnalizer->addOptions($tariffId, date('d.m.Y H:i:s', $time));


        $carOptions = CarOption::find()
            ->where(['option_id' => $addoptions])
            ->asArray()
            ->all();


        $result = [];
        foreach ($carOptions as $item) {
            $result[] = [
                'option' => \Yii::t('car-options', $item['name'], null, $lang),
                'id' => $item['option_id']
            ];
        }

        return $result;
    }

}