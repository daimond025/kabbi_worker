<?php

namespace v3\components\services\worker\preparingRedisData;


use app\models\tenant\TenantCompany;

class TenantCompanyBuild
{
    protected $data;
    protected $worker;

    public function __construct($data, $worker)
    {
        $this->data   = $data;
        $this->worker = $worker;
    }


    public function assemble()
    {
        $this->prepareData();
        $tenantCompany = $this->getTenantCompany();

        if (!$tenantCompany) {
            return $this->data;
        }

        $this->build($tenantCompany);

        return $this->data;
    }


    protected function build($tenantCompany)
    {
        $this->data['worker']['tenant_company']['company_id'] = isset($tenantCompany->tenant_company_id) ?  $tenantCompany->tenant_company_id : "" ;
        $this->data['worker']['tenant_company']['name']       = isset($tenantCompany->name) ?  $tenantCompany->name : "" ;
        $this->data['worker']['tenant_company']['phone']      = isset($tenantCompany->phone) ? $tenantCompany->phone : "" ;
        $this->data['worker']['tenant_company']['user_contact'] = isset($tenantCompany->user_contact) ?  $tenantCompany->user_contact : "" ;
    }

    protected function getTenantCompany(): ?TenantCompany
    {
        return TenantCompany::find()
            ->where([
                'tenant_company_id' => $this->worker['tenant_company_id'],
                'block'             => 0,
            ])->one();
    }

    protected function prepareData()
    {
        $this->data['worker']['tenant_company'] = null;
    }

}