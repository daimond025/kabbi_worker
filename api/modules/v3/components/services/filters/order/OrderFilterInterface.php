<?php

namespace v3\components\services\filters\order;


interface OrderFilterInterface
{
    public function run(array &$order): array;

}