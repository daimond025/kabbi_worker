<?php

namespace v3\components\services\filters\order;

use v3\components\helpers\MyArrayHelpers;
use v3\components\repositories\TenantSettingRepository;

class ClientNameFilter implements OrderFilterInterface
{

    private const SETTING_SHOW_WORKER_CLIENT = 'SHOW_WORKER_CLIENT';

    private const VARIANTS_SHOW_WORKER_CLIENT = [
        0 => [
            '!client.name',
            '!client.second_name',
            '!client.last_name',
        ],
        1 => [
            '!client.second_name',
            '!client.last_name',
        ],
        2 => [
            '!client.last_name',
        ],
        3 => []
    ];

    protected $workerFromRedis;

    public function __construct($workerFromRedis)
    {
        $this->workerFromRedis = $workerFromRedis;
    }

    /**
     * @param $order
     * @return array
     */
    public function run(array &$order): array
    {
        $valueSetting = TenantSettingRepository::getValueSettingAtTenant(
            $this->getTenantId(),
            self::SETTING_SHOW_WORKER_CLIENT,
            $this->getCityId(),
            $this->getPositionId()
        );

        return MyArrayHelpers::filter($order, self::VARIANTS_SHOW_WORKER_CLIENT[$valueSetting]);
    }


    protected function getTenantId()
    {
        return $this->workerFromRedis['worker']['tenant_id'];
    }

    protected function getCityId()
    {
        return $this->workerFromRedis['worker']['city_id'];
    }

    protected function getPositionId()
    {
        return $this->workerFromRedis['position']['position_id'];
    }

}