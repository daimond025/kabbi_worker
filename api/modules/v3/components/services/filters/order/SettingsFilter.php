<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 22.06.2018
 * Time: 11:54
 */

namespace v3\components\services\filters\order;

use app\models\tenant\TenantSetting;
use yii\helpers\ArrayHelper;

class SettingsFilter implements OrderFilterInterface
{

    public function run(array &$order): array
    {
        if (!$this->isHasSettings($order)) {
            return $order;
        }

        $offerSec                     = ArrayHelper::getValue($order['settings'], 'ORDER_OFFER_SEC');
        $offerOrderType               = ArrayHelper::getValue($order['settings'], 'OFFER_ORDER_TYPE');
        $requirePointConfirmationCode = ArrayHelper::getValue($order['settings'], 'REQUIRE_POINT_CONFIRMATION_CODE');


        $preOrderWorking = ArrayHelper::getValue($order['settings'], 'PRE_ORDER_WORKING');
        $preOrderStart = ArrayHelper::getValue($order['settings'], 'START_TIME_BY_ORDER');

        unset($order['settings']);

        $this->setSettingOrderOfferSec($order, $offerSec);
        $this->setSettingOfferOrderType($order, $offerOrderType);
        $this->setSettingRequirePointConfirmationCode($order, $requirePointConfirmationCode);
        $this->setPreOrderWorking($order, $preOrderWorking);
        $this->setStartTimeByOrder($order, $preOrderStart);

        return $order;
    }

    private function isHasSettings(array &$order): bool
    {
        return array_key_exists('settings', $order) && is_array($order['settings']);
    }

    private function setSettingOrderOfferSec(array &$order, $offerSec)
    {
        if ($offerSec !== null) {
            $order['settings']['order_offer_sec'] = (int)$offerSec;
        }
    }

    private function setSettingOfferOrderType(array &$order, $offerOrderType)
    {
        if ($offerOrderType !== null) {
            $order['settings']['show_search_worker_loader'] = (int)$this->showLoader($offerOrderType);
        }
    }

    private function setSettingRequirePointConfirmationCode(array &$order, $requirePointConfirmationCode)
    {
        if ($requirePointConfirmationCode !== null) {
            $order['settings']['require_point_confirmation_code'] = (int)$requirePointConfirmationCode;
        }
    }
    private function setPreOrderWorking(array &$order, $pre_order_working)
    {
        if ($pre_order_working !== null) {
            $order['settings']['pre_order_working'] = (int)$pre_order_working;
        }
    }
    private function setStartTimeByOrder(array &$order, $set_start_time_by_order)
    {
        if ($set_start_time_by_order !== null) {
            $order['settings']['set_start_time_by_order'] = (int)$set_start_time_by_order;
        }
    }

    /**
     * @param string $offerOrderType
     *
     * @return int
     */
    private function showLoader(string $offerOrderType): int
    {
        $show = 0;
        if ($offerOrderType === TenantSetting::SETTING_OFFER_ORDER_MULTIPLE_TYPE) {
            $show = 1;
        }

        return $show;
    }
}
