<?php

namespace v3\components\services\filters\order;

use app\models\address\City;
use app\models\order\OrderStatus;
use yii\helpers\ArrayHelper;

class HospitalAddressFilter implements OrderFilterInterface
{
    static public $hospital_type = 'HOSPITAL';



    public function run(array &$order, array $worker = []): array
    {
        $device = ArrayHelper::getValue($order, 'device');


        $order_worker_id = ArrayHelper::getValue($order, 'worker_id');
        $worker_id = ArrayHelper::getValue($worker, 'worker.worker_id');
        $city_id = ArrayHelper::getValue($order, 'city_id');

        // это больничнаЯ поездка и это предаказ
        if(($device === self::$hospital_type) &&
            (is_null($order_worker_id) &&  $order_worker_id != $worker_id )){

            $city_info = City::find()
                ->where(['city_id' => $city_id])
                ->one();
            $address = [];
            foreach($order['address'] as $point => $value){

                $correct_adress['city_id'] = isset($value['city_id']) ? $value['city_id'] : '' ;
                $correct_adress['city'] = isset($value['city']) ? $value['city'] : '' ;
                $correct_adress['street'] = isset($value['street']) ? $value['street'] : '' ;
                $correct_adress['housing'] = '';
                $correct_adress['house'] =  isset($value['house']) ? $value['house'] : '' ;
                $correct_adress['porch'] = '';
                $correct_adress['apt'] = '';
                $correct_adress['parking'] = '';
                $correct_adress['lat'] = $city_info->lat;
                $correct_adress['lon'] = $city_info->lon;
                $correct_adress['lon'] = $city_info->lon;

                $correct_adress['house_n'] = isset($value['house_n']) ? $value['house_n'] : '' ;
                $correct_adress['street_n'] = isset($value['street_n']) ? $value['street_n'] : '' ;
                $correct_adress['city_n'] = isset($value['city_n']) ? $value['city_n'] : '' ;
                $correct_adress['code_n'] = isset($value['code_n']) ? $value['code_n'] : '' ;
                $correct_adress['name_n'] = isset($value['name_n']) ? $value['name_n'] : '' ;
                $correct_adress['placeId'] = isset($value['placeId']) ? $value['placeId'] : '' ;

                $address[$point] = $correct_adress;
            }
            $order['address'] = self::correctAddressAll($address); ;
        }
        elseif($device === self::$hospital_type){
            $order['address'] = self::correctAddressAll($order['address']);
        }

        return $order;
    }

    static public function hospitalAddress(array $order, $address, array $worker)
    {
        $order_worker_id = ArrayHelper::getValue($order, 'worker_id');
        $worker_id = ArrayHelper::getValue($worker, 'worker.worker_id');
        $city_id = ArrayHelper::getValue($order, 'city_id');

        $city_info = City::find()
            ->where(['city_id' => $city_id])
            ->one();

        // заказа водителя
        if($order_worker_id != $worker_id  ){
            $correct_adress['city_id'] = isset($address['city_id']) ? $address['city_id'] : '' ;
            $correct_adress['city'] = isset($address['city']) ? $address['city'] : '' ;
            $correct_adress['street'] = isset($address['street']) ? $address['street'] : '' ;
            $correct_adress['housing'] = '';
            $correct_adress['house'] = isset($address['house']) ? $address['house'] : '' ;
            $correct_adress['porch'] = '';
            $correct_adress['apt'] = '';
            $correct_adress['parking'] = '';
            $correct_adress['lat'] = $city_info->lat;
            $correct_adress['lon'] = $city_info->lon;

            $correct_adress['house_n'] = isset($address['house_n']) ? $address['house_n'] : '' ;
            $correct_adress['street_n'] = isset($address['street_n']) ? $address['street_n'] : '' ;
            $correct_adress['city_n'] = isset($address['city_n']) ? $address['city_n'] : '' ;
            $correct_adress['code_n'] = isset($address['code_n']) ? $address['code_n'] : '' ;
            $correct_adress['name_n'] = isset($address['name_n']) ? $address['name_n'] : '' ;
            $correct_adress['placeId'] = isset($address['placeId']) ? $address['placeId'] : '' ;

            $address = self::correctAddress($correct_adress);
        }else{
            $address = self::correctAddress($address);
        }
        return $address;
    }

    static public function correctAddressAll($address)
    {
        $correct_address = [];
        foreach($address as $point => $value){
            $correct_address[$point] = self::correctAddress($value);
        }
        return $correct_address;
    }

    static public function correctAddress($address_from)
    {
        $address_correct = $address_from;

        $city = isset($address_from['city']) ? $address_from['city'] : '';
        $street = isset($address_from['street']) ? $address_from['street'] : '';

        if(isset($address_from['house']) && !empty($address_from['house'])){
            $street = $street.', '.$address_from['house'];
        }
        if(!empty($city)){
            $street = $city.', '.$street;
        }

        $address_correct['street'] = $street;
        $address_correct['parking'] = isset($address_from['parking']) ? $address_from['parking'] : '' ;
        return $address_correct;

    }
}