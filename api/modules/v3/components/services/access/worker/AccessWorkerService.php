<?php

namespace v3\components\services\access\worker;


use v3\components\services\access\AccessException;
use v3\components\taxiApiRequest\TaxiErrorCode;

class AccessWorkerService
{

    public function isAccessAuth($worker)
    {
        if (empty($worker)) {
            throw new AccessException(TaxiErrorCode::WORKER_NOT_EXIST);
        }

        if ($worker['block'] == 1) {
            throw new AccessException(TaxiErrorCode::WORKER_BLOCKED);
        }

        if ($worker['activate'] == 0) {
            throw new AccessException(TaxiErrorCode::WORKER_NOT_ACTIVATED);
        }

        if ($worker['block'] == 0 && $worker['activate'] == 1) {
            return true;
        }

        throw new AccessException(TaxiErrorCode::INTERNAL_ERROR);
    }

}