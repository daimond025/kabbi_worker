<?php

namespace v3\components\services\auth;


use app\models\tenant\Tenant;
use app\models\tenant\TenantCompany;
use Yii;

class SignCompany
{

    const LOGO_COMPANY_NOT = 0;
    const LOGO_COMPANY_YES = 1;

    protected $workerAttributes = null;
    protected $isShowLogoCompany = null;
    protected $tenantCompany = null;

    public function __construct(array $workerAttributes)
    {
        $this->workerAttributes = $workerAttributes;
    }


    public function getCompanyName()
    {
        if ($this->isShowSignCompany()) {
            return $this->getTenantCompany()->name;
        }

        return $this->getMainCompanyName();
    }


    public function getCompanyLogo($tenantDomain)
    {
        if ($this->isShowSignCompany()) {
            return $this->getCompanyLogoUrl($this->getTenantId(), $tenantDomain);
        }

        return Tenant::getLogoUrl($this->getTenantId(), $tenantDomain);
    }

    /**
     * @return bool
     */
    public function isShowSignCompany()
    {
        if (!is_null($this->isShowLogoCompany)) {
            return $this->isShowLogoCompany;
        }

        if (!$this->getTenantCompany()) {
            $this->isShowLogoCompany = false;
            return false;
        }

        if ($this->getTenantCompany()->use_logo_company == self::LOGO_COMPANY_YES) {
            return true;
        }

        return false;
    }


    protected function getCompanyLogoUrl($tenantId, $tenantDomain)
    {
        $tenantCompany = $this->getTenantCompany();
        if (!empty($tenantCompany)) {
            $logo = $tenantCompany->logo;
            if (!empty($logo)) {
                $url = Yii::$app->params['frontend.protocol'] . '://' .  $tenantDomain . '.' . Yii::$app->params['frontend.domain'] . '/file/show-external-file?filename=thumb_' . $logo . '&id=' . $tenantId;
                return $url;
            }
        }
        return '';
    }


    /**
     * @return null|TenantCompany
     */
    protected function getTenantCompany()
    {
        if (!is_null($this->tenantCompany)) {
            return $this->tenantCompany;
        }

        $this->tenantCompany = is_null($this->getTenantCompanyId())
            ? false
            : TenantCompany::findOne(['tenant_company_id' => $this->getTenantCompanyId()]);

        return $this->tenantCompany;
    }


    protected function getTenantCompanyId()
    {
        return $this->workerAttributes['tenant_company_id'];
    }

    protected function getMainCompanyName()
    {
        return $this->workerAttributes['tenant']['company_name'];
    }

    protected function getTenantId()
    {
        return $this->workerAttributes['tenant_id'];
    }
}