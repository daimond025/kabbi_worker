<?php
namespace v3\components\services\promo;


use v3\components\repositories\PromoRepository;
use v3\components\WorkerService;

class PromoService
{
    public function getPromoCodeByWorker($tenantId, $workerCallsign)
    {

        $promoCode = null;
        if ($workerFromRedis = (new WorkerService())->getWorkerFromRedis($tenantId, $workerCallsign)) {
            $carClass = array_key_exists('class_id', $workerFromRedis['car'])
                ? $workerFromRedis['car']['class_id'] : null;

            $promoCode = (new PromoRepository())->getPromoCodeWorker(
                $workerFromRedis['worker']['tenant_id'],
                $workerFromRedis['worker']['worker_id'],
                $workerFromRedis['position']['position_id'],
                $carClass
            );

            $promoCode = $promoCode ? $promoCode->code : null;
        }

        return $promoCode;
    }
}