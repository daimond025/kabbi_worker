<?php

namespace v3\components;

use app\models\address\City;
use app\models\balance\Account;
use app\models\car\Car;
use app\models\car\CarColor;
use app\models\car\CarHasWorkerGroupClass;
use app\models\client\ClientReview;
use app\models\settings\DefaultSettings;
use app\models\tenant\TenantHasCarOption;
use app\models\tenant\TenantHasCity;
use app\models\tenant\TenantSetting;
use app\models\tenant\TenantTariffService;
use app\models\worker\CarMileage;
use app\models\worker\Position;
use app\models\worker\TariffCommission;
use app\models\worker\Worker;
use app\models\worker\WorkerActiveAboniment;
use app\models\worker\WorkerBlock;
use app\models\worker\WorkerDriverLicense;
use app\models\worker\WorkerGroup;
use app\models\worker\WorkerGroupCanViewClientTariff;
use app\models\worker\WorkerHasCar;
use app\models\worker\WorkerHasCity;
use app\models\worker\WorkerHasPosition;
use app\models\worker\WorkerOptionTariff;
use app\models\worker\WorkerReviewRating;
use app\models\worker\WorkerShift;
use app\models\worker\WorkerShiftOrder;
use app\models\worker\WorkerTariff;
use v3\components\services\promo\PromoService;
use v3\components\services\worker\preparingRedisData\TenantCompanyBuild;
use v3\exceptions\ActivateAbonimentShiftException;
use v3\exceptions\CityBlockedException;
use v3\exceptions\ShiftIsClosedException;
use yii\base\InvalidConfigException;
use yii\base\Object;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\db\Query;
use yii\di\NotInstantiableException;
use yii\helpers\ArrayHelper;
use yii\redis\Connection;

/**
 * Class WorkerService
 * @package v3\components
 */
class WorkerService extends Object
{
    public $lang;

    /** @var ApiLogger */
    protected $apiLogger;

    /** @var WorkerTariffService */
    protected $workerTariffService;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        $this->workerTariffService          = \Yii::createObject(WorkerTariffService::class);
        $this->apiLogger                    = \Yii::$app->get('apiLogger');
        parent::init();
    }

    /**
     * Проверка версии приложения
     *
     * @param string $appVersion
     * @param string $workerDevice
     * @param integer $tenantId
     *
     * @return bool
     */
    public function isValidAppVersion($appVersion, $workerDevice, $tenantId)
    {
        $appVersion   = (float)$appVersion;
        $workerDevice = mb_strtoupper($workerDevice);

        switch ($workerDevice) {
            case 'ANDROID':
                $settingName = DefaultSettings::SETTING_ANDROID_WORKER_APP_VERSION;
                break;
            case 'IOS':
                $settingName = DefaultSettings::SETTING_IOS_WORKER_APP_VERSION;
                break;
            default:
                return false;
        }

        $currentVersion = $this->getCurrentVersionApp($settingName, $tenantId);

        return isset($currentVersion) && $appVersion >= (float)$currentVersion;
    }

    protected function getCurrentVersionApp($settingName, $tenantId)
    {
        $currentVersion = TenantSetting::find()
            ->select('value')
            ->where([
                'name'      => $settingName,
                'tenant_id' => $tenantId,
            ])
            ->orderBy('setting_id ASC ')
            ->scalar();

        if ($currentVersion) {
            return $currentVersion;
        }

        return DefaultSettings::find()
            ->select('value')
            ->where(['name' => $settingName])
            ->scalar();
    }

    /**
     * Исполнитель на смене
     *
     * @param string $tenantLogin
     * @param string $workerCallsign
     *
     * @return bool
     */
    public function isWorkerOnShift($tenantLogin, $workerCallsign)
    {
        $shift_data = \Yii::$app->redis_worker_shift
            ->executeCommand('GET', [$tenantLogin . '_' . $workerCallsign]);

        return !empty($shift_data);
    }

    /**
     * Checking is valid worker car
     *
     * @param int $workerId
     * @param int $positionId
     * @param int $workerCarId
     *
     * @return bool
     */
    public function isValidWorkerCar($workerId, $positionId, $workerCarId)
    {
        return WorkerHasCar::find()
            ->alias('t')
            ->joinWith('workerHasPosition w')
            ->joinWith('car c')
            ->where([
                'w.worker_id'   => $workerId,
                'w.position_id' => $positionId,
                't.car_id'      => $workerCarId,
                'c.active'      => 1,
            ])
            ->exists();
    }

    /**
     * Checking is valid worker position
     *
     * @param int $workerId
     * @param int $positionId
     *
     * @return bool
     */
    public function isValidWorkerPosition($workerId, $positionId)
    {
        return WorkerHasPosition::find()
            ->where([
                'worker_id'   => $workerId,
                'position_id' => $positionId,
                'active'      => 1,
            ])
            ->exists();
    }

    /**
     * Получение исполнителя
     *
     * @param integer $tenantId
     * @param integer $workerCallsign
     *
     * @return array
     */
    public function getWorker($tenantId, $workerCallsign)
    {
        return Worker::find()
            ->alias('t')
            ->joinWith('tenant')
            ->where([
                't.tenant_id' => $tenantId,
                't.callsign'  => $workerCallsign,
            ])
            ->asArray()
            ->one();
    }

    /**
     * Получение исполнителя из редиса
     *
     * @param string $tenantId
     * @param string $workerCallsign
     *
     * @return array|false
     */
    public function getWorkerFromRedis($tenantId, $workerCallsign)
    {
        $workerData = \Yii::$app->redis_workers
            ->executeCommand('hget', [$tenantId, $workerCallsign]);
        $workerData = unserialize($workerData);

        if (!empty($workerData)) {
            return $workerData;
        } else {
            return false;
        }
    }

    /**
     * Save worker to redis
     *
     * @param int   $tenantId
     * @param int   $workerCallsign
     * @param array $worker
     *
     * @return bool
     */
    public function saveWorkerToRedis($tenantId, $workerCallsign, $worker)
    {
        $workerData = serialize($worker);
        $result     = \Yii::$app->redis_workers->executeCommand(
            'hset', [$tenantId, $workerCallsign, $workerData]);

        return $result == 0 || $result == 1;
    }

    /**
     * Сохраняем информацию об устройстве исполнителя (телефоне)
     *
     * @param integer $workerId
     * @param string  $device
     * @param string  $deviceToken
     * @param string  $lang
     */
    public function saveWorkerDeviceInfo($workerId, $device, $deviceToken, $lang = null)
    {
        $worker = Worker::findOne($workerId);

        if (!empty($worker)) {
            $device = mb_strtoupper($device);
            if (in_array($device, ['IOS', 'ANDROID'])) {
                $worker->device = $device;
            }
            $worker->device_info  = Worker::getWorkerDeviceInfo();
            $worker->device_token = $deviceToken;
            if (isset($lang)) {
                $worker->lang = $lang;
            }
            $worker->save();
        }
    }

    /**
     * Проверка лимита активных смен исполнителя арендодатора.
     *
     * @param integer $tenantId
     *
     * @return boolean
     */
    public function isExceededOnlineWorkersLimit($tenantId)
    {
        $onlineWorkers  = 0;
        $workersOnShift = \Yii::$app->redis_workers->executeCommand('HKEYS', [$tenantId]);

        if (is_array($workersOnShift)) {
            $onlineWorkers = count($workersOnShift);
        }

        /**
         * @var $service TenantTariffService
         */
        $service        = \Yii::$app->tenantTariffService;
        $maxOnlineUsers = $service->getPermission($tenantId, $service::MODULE_TAXI,
            $service::PERMISSION_WORKERS_ONLINE);

        return $maxOnlineUsers !== false && $onlineWorkers >= $maxOnlineUsers;
    }

    /**
     * Проверка баланса исполнителя в зависимости от тарифа и действия
     *
     * @param int   $tenantId
     * @param int   $workerId
     * @param int   $workerCityId
     * @param int   $positionId
     * @param float $tariffCost
     *
     * @return bool
     */
    public function isGoodWorkerBalance($tenantId, $workerId, $workerCityId, $positionId, $tariffCost)
    {
        $currencyId = TenantSetting::getTenantCurrencyId($tenantId, $workerCityId);

        $workerAccount   = Account::findOne([
            'owner_id'    => $workerId,
            'tenant_id'   => $tenantId,
            'acc_kind_id' => Account::WORKER_KIND,
            'currency_id' => $currencyId,
        ]);
        $minBalanceValue = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_WORKER_MIN_BAlANCE, $workerCityId, $positionId);

        $balance    = empty($workerAccount) ? 0 : $workerAccount->balance;
        $minBalance = empty($minBalanceValue) ? 0 : $minBalanceValue;

        return $balance >= $tariffCost + $minBalance;
    }

    /**
     * Checking is worker blocked
     *
     * @param int    $workerId
     * @param int    $positionId
     * @param string $typeBlock
     *
     * @return bool
     */
    public function isWorkerBlocked($workerId, $positionId, $typeBlock = null)
    {
        return WorkerBlock::find()
            ->alias('t')
            ->joinWith('workerShift w')
            ->where([
                't.worker_id'    => $workerId,
                'w.position_id'  => $positionId,
                't.is_unblocked' => 0,
            ])
            ->andWhere(['>', 't.end_block', time()])
            ->andFilterWhere(['t.type_block' => $typeBlock])
            ->exists();
    }

    /**
     * Getting prepared data of tariff commission
     *
     * @param array $tariffCommission
     *
     * @return array
     */
    public function getTariffCommission($tariffCommission)
    {
        $commission = !empty($tariffCommission) ? $tariffCommission : [];

        return array_reduce($commission, function ($result, $item) {
            if (isset($item['value'])
                && (float)$item['value'] > (float)$result['value']
            ) {
                return [
                    'value' => $item['value'],
                    'type'  => $item['type'],
                ];
            } else {
                return $result;
            }
        }, [
            'value' => 0,
            'type'  => TariffCommission::TYPE_PERCENT,
        ]);
    }

    /**
     * Getting worker profile
     *
     * @param int $tenantId
     * @param int $workerCallsign
     * @param int $workerCityId
     * @param int $positionId
     * @param int $currentTime
     *
     * @return array
     * @throws CityBlockedException
     */
    public function getWorkerProfile($tenantId, $workerCallsign, $workerCityId, $positionId, $currentTime)
    {
        $worker = Worker::find()
            ->with('tenant')
            ->where([
                'tenant_id' => $tenantId,
                'callsign'  => $workerCallsign,
            ])
            ->asArray()
            ->one();

        if (empty($worker)) {
            return null;
        }

        $workerCityIds = ArrayHelper::getColumn(
            $this->getWorkerCities($worker['tenant_id'], $worker['worker_id']), 'city_id');

        if ($workerCityId !== null) {
            $cityId = $workerCityId;
        } elseif (!empty($worker['city_id'])) {
            $cityId = $worker['city_id'];
        } else {
            $cityId = $this->getLastActiveCityId($worker['worker_id']);
        }

        if (!in_array($cityId, $workerCityIds, false)) {
            throw new CityBlockedException('City is blocked');
        }

        $activeTariffs = WorkerActiveAboniment::find()
            ->alias('a')
            ->joinWith([
                'tariff t',
                'tariff.workerOptionTariffs',
                'tariff.workerOptionTariffs.tariffCommission tc',
                'tariff.workerTariffHasCities c',
            ], true)
            ->where([
                'a.worker_id'   => $worker['worker_id'],
                't.position_id' => $positionId,
            ])
            ->andWhere([
                'or',
                ['>', 'shift_valid_to', $currentTime],
                ['and', ['expired_at' => null], ['>', 'count_active', 0]],
                ['and', ['count_active' => null], ['>', 'expired_at', $currentTime]],
                ['and', ['>', 'count_active', 0], ['>', 'expired_at', $currentTime]],
            ])
            ->asArray()
            ->all();

        $workerActiveTariffs = [];
        $activeTariffIds     = [];
        if (is_array($activeTariffs)) {
            foreach ($activeTariffs as $tariff) {
                $tariffCityIds = !empty($tariff['tariff']['workerTariffHasCities'])
                    ? ArrayHelper::getColumn($tariff['tariff']['workerTariffHasCities'], 'city_id') : [];

                if (!empty($tariff['tariff']['workerOptionTariffs'][0])
                    && in_array($cityId, $tariffCityIds, false)
                ) {
                    $activeTariffIds[] = $tariff['tariff_id'];
                    $tariffOption      = WorkerOptionTariff::getCurrentTariffOption($tariff['tariff_id']);
                    $commission        = $this->getTariffCommission($tariffOption['tariffCommission']);

                    $countActive = 0;
                    if ($tariff['count_active'] !== null) {
                        $countActive = $tariff['count_active'];
                    } elseif ($tariff['expired_at'] !== null) {
                        $expiredDate = (new \DateTime())->setTimestamp($tariff['expired_at']);
                        $currentDate = (new \DateTime())->setTimestamp($currentTime);
                        $interval    = $expiredDate->diff($currentDate);
                        $countActive = $interval && $interval->d > 0 ? $interval->d : 0;
                    }

                    $workerActiveTariffs[] = [
                        'worker_tariff_id'            => $tariff['tariff_id'],
                        'worker_tariff_type'          => $tariff['tariff']['type'],
                        'worker_car_class'            => $tariff['tariff']['class_id'],
                        'worker_tariff_period_type'   => $tariff['tariff']['period_type'],
                        'worker_tariff_period_info'   => $tariff['tariff']['period'],
                        'worker_tariff_name'          => $tariff['tariff']['name'],
                        'worker_tariff_cost'          => $tariff['tariff']['cost'],
                        'worker_order_comission'      => $commission['value'],
                        'worker_order_comission_type' => $commission['type'],
                        'worker_count_shift'          => $tariff['tariff']['days'],
                        'worker_count_closed_shift'   =>
                            isset($tariff['tariff']['days']) ? (string)($tariff['tariff']['days'] - $countActive) : null,
                        'worker_count_active_shift'   => $countActive,
                    ];
                }
            }
        }

        $position = Position::findOne($positionId);
        $hasCar   = isset($position->has_car) ? (bool)$position->has_car : false;

        $workerCars       = [];
        $availableTariffs = [];
        if ($hasCar) {
            $cars = Car::find()
                ->alias('car')
                ->joinWith([
                    'workerHasCars',
                    'workerHasCars.workerHasPosition w',
                    //                'workerHasCars.workerHasPosition.position p',
                    'workerTariffs',
                    'workerTariffs.tariff t' => function ($query) use ($positionId) {
                        /* @var $query ActiveQuery */
                        $query->andOnCondition([
                            't.block'       => 0,
                            't.position_id' => $positionId,
                        ]);
                    },
                    'workerTariffs.tariff.workerOptionTariffs.tariffCommission tc',
                    'workerTariffs.tariff.workerTariffHasCities wc'
                    //                'workerTariffs.tariff.position p2',
                ], true)
                ->where([
                    'w.worker_id'   => $worker['worker_id'],
                    'w.position_id' => $positionId,
                    'w.active'      => 1,
                    'car.active'      => 1,
                ])
                ->asArray()
                ->all();

            if (is_array($cars)) {
                foreach ($cars as $car) {
                    if (empty($car['workerTariffs'])) {
                        $availableTariffsByClass = WorkerTariff::find()
                            ->alias('t')
                            ->joinWith('workerTariffHasCities c', true)
                            ->where([
                                't.tenant_id'   => $tenantId,
                                'c.city_id'     => $cityId,
                                't.position_id' => $positionId,
                                't.class_id'    => $car['class_id'],
                                't.block'       => 0,
                            ])
                            ->asArray()
                            ->all();

                        if (is_array($availableTariffsByClass)) {
                            foreach ($availableTariffsByClass as $tariff) {
                                if (empty($availableTariffs[$tariff['tariff_id']])) {
                                    $availableTariffs[$tariff['tariff_id']] = $tariff;
                                }
                            }
                        }
                    } else {
                        foreach ($car['workerTariffs'] as $tariff) {
                            if (!empty($tariff['tariff']) && empty($availableTariffs[$tariff['tariff_id']])
                            ) {
                                $availableTariffs[$tariff['tariff_id']] = $tariff['tariff'];
                            }
                        }
                    }

                    $workerCars[] = [
                        'worker_car_id'    => $car['car_id'],
                        'worker_car_class' => $car['class_id'],
                        'worker_car_descr' => $car['name'] . ' ' . $car['gos_number'],
                        'is_company_car'   => $car['owner'] === Car::OWNER_COMPANY,
                    ];
                }
            }
        } else {
            $groupTariffs = WorkerTariff::find()
                ->alias('t')
                ->joinWith('workerTariffHasCities c')
                ->joinWith('workerGroupHasTariffs.group.workerHasPositions wp', false)
                ->where([
                    'wp.worker_id'   => $worker['worker_id'],
                    'c.city_id'      => $cityId,
                    'wp.position_id' => $positionId,
                    'wp.active'      => 1,
                    't.block'        => 0,
                ])
                ->asArray()
                ->all();

            if (!empty($groupTariffs)) {
                $availableTariffs = ArrayHelper::index($groupTariffs, 'tariff_id');
            } else {
                $tariffs = WorkerTariff::find()
                    ->alias('t')
                    ->joinWith('workerTariffHasCities')
                    ->joinWith('workerTariffHasCities c', false)
                    ->where([
                        't.tenant_id'   => $tenantId,
                        'c.city_id'     => $cityId,
                        't.position_id' => $positionId,
                        't.class_id'    => null,
                        't.block'       => 0,
                    ])
                    ->asArray()
                    ->all();

                if (!empty($tariffs)) {
                    $availableTariffs = ArrayHelper::index($tariffs, 'tariff_id');
                }
            }
        }

        $currentTime = time() + City::getTimeOffset($cityId);

        $workerAvailableTariffs = [];
        foreach ($availableTariffs as $tariff) {
            $tariffCityIds = !empty($tariff['workerTariffHasCities'])
                ? ArrayHelper::getColumn($tariff['workerTariffHasCities'], 'city_id') : [];

            if (!in_array($tariff['tariff_id'], $activeTariffIds, false)
                && in_array($cityId, $tariffCityIds, false)
            ) {
                $tariffOption = WorkerOptionTariff::getCurrentTariffOption($tariff['tariff_id']);
                $commission   = $this->getTariffCommission($tariffOption['tariffCommission']);

                $workerAvailableTariffs[] = [
                    'worker_tariff_id'            => $tariff['tariff_id'],
                    'worker_tariff_type'          => $tariff['type'],
                    'worker_car_class'            => $tariff['class_id'],
                    'worker_tariff_period_type'   => $tariff['period_type'],
                    'worker_tariff_period_info'   => $tariff['period'],
                    'worker_tariff_name'          => $tariff['name'],
                    'worker_tariff_cost'          => $tariff['cost'],
                    'worker_order_comission'      => $commission['value'],
                    'worker_order_comission_type' => $commission['type'],
                    'worker_count_shift'          => $tariff['days'],
                ];
            }
        }

        $currency = TenantSetting::getTenantCurrencyCode($tenantId, $cityId);

        $offerSec = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_ORDER_OFFER_SEC, $cityId, $positionId);

        $allowWorkWithoutGPS           = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_ALLOW_WORK_WITHOUT_GPS, $cityId, $positionId);
        $allowRefillBalanceByBankCard  = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_ALLOW_USE_BANK_CARD, $cityId, $positionId);
        $allowCancelOrder              = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_ALLOW_WORKER_TO_CANCEL_ORDER, $cityId, $positionId);
        $allowCancelOrderAfterTime     = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_TIME_ALLOW_WORKER_TO_CANCEL_ORDER, $cityId, $positionId);
        $showChatWithDispatcherOnShift = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_SHOW_CHAT_DISPATCHER_SHIFT, $cityId, $positionId);
        $showGeneralChatOnShift        = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_SHOW_GENERAL_CHAT_SHIFT, $cityId, $positionId);
        $startTimeByOrder              = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_START_TIME_BY_ORDER, $cityId, $positionId);
        $workerArrivalTime             = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_WORKER_ARRIVAL_TIME, $cityId, $positionId);

        $showEstimation                = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_SHOW_ESTIMATION, $cityId, $positionId);
        $allowEditOrder                = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_ALLOW_EDIT_ORDER, $cityId, $positionId);
        $showUrgentOrderTime           = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_SHOW_URGENT_ORDER_TIME, $cityId, $positionId);
        $showForWorkerEndAddress       = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_SHOW_WORKER_ADDRESS, $cityId, $positionId);
        $allowWorkerToCreateOrder      = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_ALLOW_WORKER_TO_CREATE_ORDER, $cityId, $positionId);
        $denyFakegps                   = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_DENY_FAKEGPS, $cityId, $positionId);
        $denySettingArrivalStatusEarly = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_DENY_SETTING_ARRIVAL_STATUS_EARLY, $cityId, $positionId);
        $minDistanceToSetArrivalStatus = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_MIN_DISTANCE_TO_SET_ARRIVAL_STATUS, $cityId, $positionId);
        $controlOwnCarMileage          = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_CONTROL_OWN_CAR_MILEAGE, $cityId, $positionId);

        $showChatWithDispatcherBeforeStartShift   = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_SHOW_CHAT_DISPATCHER_EXIT, $cityId, $positionId);
        $requirePasswordEveryTimeLogInApplication = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION, $cityId, $positionId);
        $printCheck                               = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::PRINT_CHECK, $cityId, $positionId);
        $showDriverPrivacyPolicy                  = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SHOW_DRIVER_PRIVACY_POLICY, $cityId, $positionId);
        $allowEditCostOrder                       = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::ALLOW_EDIT_COST_ORDER, $cityId, $positionId);
        $allowReserveUrgentOrders                 = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_ALLOW_RESERVE_URGENT_ORDERS, $cityId, $positionId);
        $addIntermediatePointInOrderForStop       = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::ADD_INTERMEDIATE_POINT_IN_ORDER_FOR_STOP, $cityId, $positionId);


        $workerArrivalTime = array_map(function ($item) {
            return (int)$item;
        }, explode(';', $workerArrivalTime));
        sort($workerArrivalTime);

        $driverLicenses = WorkerDriverLicense::getDriverLicenses($worker['worker_id']);

        $workerExperience = isset($driverLicenses[0]['start_date']) ? $driverLicenses[0]['start_date'] : null;
        $workerPhoto      = Worker::getPhotoUrl($worker['worker_id'], $worker['tenant']['domain']);

        $promoCode = (new PromoService())->getPromoCodeByWorker($tenantId, $workerCallsign);

        $isViewQueue = TenantSetting::getIsViewQueue($tenantId, $cityId, $positionId);


        return [
            'worker_fio'                                   => $worker['last_name'] . ' ' . $worker['name'] . ' ' . $worker['second_name'],
            'worker_experience'                            => $workerExperience,
            'worker_city_id'                               => $cityId,
            'worker_city_name'                             => City::getCityNameById($cityId),
            'worker_cars'                                  => $workerCars,
            'worker_phone'                                 => $worker['phone'],
            'worker_photo'                                 => $workerPhoto,
            'worker_email'                                 => $worker['email'],
            'worker_currency'                              => empty($currency) ? 'RUB' : $currency,
            'worker_arrival_time'                          => $workerArrivalTime,
            'promo_code'                                   => $promoCode,
            'order_offer_sec'                              => $offerSec,
            'allow_work_without_gps'                       => $allowWorkWithoutGPS,
            'allow_refill_balance_by_bank_card'            => $allowRefillBalanceByBankCard,
            'allow_cancel_order'                           => $allowCancelOrder,
            'allow_cancel_order_after_time'                => $allowCancelOrderAfterTime,
            'show_chat_with_dispatcher_before_start_shift' => $showChatWithDispatcherBeforeStartShift,
            'show_chat_with_dispatcher_on_shift'           => $showChatWithDispatcherOnShift,
            'show_general_chat_on_shift'                   => $showGeneralChatOnShift,
            'start_time_by_order'                          => $startTimeByOrder,
            'show_estimation'                              => $showEstimation,
            'allow_edit_order'                             => $allowEditOrder,
            'show_urgent_order_time'                       => $showUrgentOrderTime,
            'show_worker_address'                          => $showForWorkerEndAddress,
            'allow_worker_to_create_order'                 => $allowWorkerToCreateOrder,
            'deny_fakegps'                                 => $denyFakegps,
            'deny_setting_arrival_status_early'            => $denySettingArrivalStatusEarly,
            'min_distance_to_set_arrival_status'           => $minDistanceToSetArrivalStatus,
            'control_own_car_mileage'                      => $controlOwnCarMileage,
            'require_password_every_time_log_in_application' => $requirePasswordEveryTimeLogInApplication,
            'print_check'                                    => $printCheck,
            'show_driver_privacy_policy'                     => $showDriverPrivacyPolicy,
            'allow_edit_cost_order'                          => $allowEditCostOrder,
            'is_view_queue'                                  => $isViewQueue,
            'add_intermediate_point_in_order_for_stop'       => $addIntermediatePointInOrderForStop,
            'allow_reserve_urgent_orders'                    => $allowReserveUrgentOrders,

            'worker_tariffs' => [
                'active' => $workerActiveTariffs,
                'to_buy' => $workerAvailableTariffs,
            ],
        ];
    }


    /**
     * Getting last active city id
     *
     * @param int $workerId
     *
     * @return null|string
     */
    public function getLastActiveCityId($workerId)
    {
        $cityId = WorkerHasCity::find()->alias('whc')
            ->select('`whc`.`city_id`')
            ->leftJoin(
                Worker::tableName() . ' `w`',
                '`w`.`worker_id` = `whc`.`worker_id`'
            )
            ->leftJoin(
                TenantHasCity::tableName() . ' `thc`',
                '`w`.`tenant_id` = `thc`.`tenant_id`'
            )
            ->where([
                '`whc`.`worker_id`' => $workerId,
                '`thc`.`block`'     => 0,
            ])
            ->andWhere('`whc`.`city_id` = `thc`.`city_id`')
            ->orderBy([
                '`whc`.`last_active`' => SORT_DESC,
                '`whc`.`city_id`'     => SORT_ASC,
            ])
            ->limit(1)
            ->scalar();

        return $cityId === false ? null : $cityId;
    }

    /**
     * Getting worker cities
     *
     * @param int $tenantId
     * @param int $workerId
     *
     * @return array
     */
    public function getWorkerCities($tenantId, $workerId)
    {
        $prefix = empty($this->lang) || $this->lang === 'ru' ? '' : "_{$this->lang}";

        $workerCities = City::find()
            ->alias('c')
            ->select(['c.city_id', "c.name{$prefix} as name", 'c.shortname', 'c.lat', 'c.lon'])
            ->innerJoinWith([
                'workerHasCities w' => function ($query) use ($tenantId, $workerId) {
                    $query->select(['w.city_id'])
                        ->where([
                            'and',
                            ['w.worker_id' => $workerId],
                            [
                                'in',
                                'w.city_id',
                                TenantHasCity::find()
                                    ->alias('t')
                                    ->select('t.city_id')
                                    ->where([
                                        't.tenant_id' => $tenantId,
                                        't.block'     => 0,
                                    ]),
                            ],
                        ]);
                },
            ], false)
            ->asArray()
            ->all();

        return empty($workerCities) ? [] : $workerCities;
    }


    /**
     * Getting worker positions
     *
     * @param int $tenantId
     * @param int $workerId
     * @param int $cityId
     *
     * @return array
     */
    public function getWorkerPositions($tenantId, $workerId, $cityId)
    {
        $positions = Position::find()
            ->select(['p.position_id', 'p.name', 'p.has_car'])
            ->distinct()
            ->joinWith(['cities c', 'workerHasPositions w', 'workerHasPositions.position p'], false)
            ->where([
                'c.city_id'   => $cityId,
                'c.tenant_id' => $tenantId,
                'w.worker_id' => $workerId,
                'w.active'    => 1,
            ])
            ->asArray()
            ->all();

        return empty($positions) ? [] : array_map(function ($position) {
            return [
                'position_id' => $position['position_id'] ?? null,
                'name'        => \Yii::t('employee', $position['name'] ?? null, null, $this->lang),
                'has_car'     => (bool)($position['has_car'] ?? false),
            ];
        }, $positions);
    }


    /**
     * Пересчет рэйтинга исполнителя
     *
     * @param array $ratings -  массив рейтингов
     * @param int $workerId -   ид исполнителя
     * @param int $positionId
     * @param int $workerCarId - ид автомобиля
     */
    protected function recountWorkerReviewRating($ratings, $workerId, $positionId, $workerCarId)
    {
        $carRating    = 0;

        $car = Car::findOne($workerCarId);

        if (!empty($car)) {
            $carRating = $car->raiting;
        }

        $reviewRating = WorkerReviewRating::find()
            ->where([
                'worker_id'   => $workerId,
                'position_id' => $positionId,
            ])
            ->one();

        if (!$reviewRating) {
            $reviewRating = WorkerReviewRating::makeWorkerReviewRating($workerId, $positionId);
        }

        $reviewRating->annulmentRating();
        foreach ($ratings as $rating) {
            $reviewRating->addRating($rating);
        }

        /** @var WorkerHasPosition $workerHasPosition */
        $workerHasPosition = WorkerHasPosition::find()
            ->where([
                'worker_id'   => $workerId,
                'position_id' => $positionId,
            ])
            ->one();

        $workerRating = (int)$reviewRating->getAverageRating() * 10;

        $workerHasPosition->rating = $workerRating + $carRating;
        $workerHasPosition->save();
        $reviewRating->save();
    }

    public function isAllowedStartShiftByRating($tenantId, $cityId, $positionId, $workerId, $workerCarId)
    {
        $workerReviewCount = TenantSetting::getSettingValue(
            $tenantId,
            TenantSetting::SETTING_WORKER_REVIEW_COUNT,
            $cityId,
            $positionId
        );

        $workerMinReviewRating = TenantSetting::getSettingValue(
            $tenantId,
            TenantSetting::SETTING_WORKER_MIN_REVIEW_RATING,
            $cityId,
            $positionId
        );

        $ratings = ClientReview::find()
            ->select(['rating'])
            ->where([
                'worker_id'   => $workerId,
                'position_id' => $positionId,
                'active'      => ClientReview::STATUS_ACTIVE
            ])
            ->orderBy(['create_time' => SORT_DESC])
            ->limit($workerReviewCount)
            ->column();

        $this->recountWorkerReviewRating($ratings, $workerId, $positionId, $workerCarId);

        if (count($ratings) == 0) {
            return true;
        }

        $averageRating = array_sum($ratings) / count($ratings);

        return $workerMinReviewRating <= $averageRating;
    }


    /**
     * @param int $workerShiftId
     * @param int $groupId
     * @param int $groupPriority
     * @param int $positionPriority
     */
    private function updateWorkerShiftInfo($workerShiftId, $groupId, $groupPriority, $positionPriority)
    {
        try {
            $shift = WorkerShift::findOne($workerShiftId);

            $shift->group_id          = $groupId;
            $shift->group_priority    = $groupPriority;
            $shift->position_priority = $positionPriority;

            if (!$shift->save()) {
                throw new \DomainException(implode(';', $shift->getFirstErrors()));
            }
        } catch (\Exception $ex) {
            \Yii::error("Error update worker shift information: shiftId={$workerShiftId}, groupId={$groupId}, groupPriority={$groupPriority}, positionPriority={$positionPriority}, error={$ex->getMessage()}",
                'worker-shift');
        }
    }

    /**
     * Set worker to online
     *
     * @param string $tenantLogin
     * @param int    $workerId
     * @param int    $positionId
     * @param int    $tariffId
     * @param int    $workerCarId
     * @param int    $workerCityId
     * @param int    $workerShiftId
     *
     * @return bool
     */
    public function setWorkerToOnline(
        $tenantLogin,
        $workerId,
        $positionId,
        $tariffId,
        $workerCarId,
        $workerCityId,
        $workerShiftId,
        $newShiftStart,
        $shiftLifeTime
    ) {
        $worker = Worker::find()
            ->where(['worker_id' => $workerId])
            ->asArray()
            ->one();

        $settings = TenantSetting::find()
            ->where([
                'tenant_id'   => $worker['tenant_id'],
                'city_id'     => $workerCityId,
                'position_id' => $positionId,
            ])
            ->asArray()
            ->all();

        $workerTypeBlock          = null;
        $workerCountToBlock       = null;
        $workerTimeOfBlock        = null;
        $workerPreOrderReminder   = null;
        $workerPreOrderBlockHours = null;
        $workerPreOrderRejectMin  = null;

        if (is_array($settings)) {
            foreach ($settings as $setting) {
                switch ($setting['name']) {
                    case TenantSetting::SETTING_WORKER_TYPE_BLOCK:
                        $workerTypeBlock = $setting['value'];
                        break;
                    case TenantSetting::SETTING_WORKER_COUNT_TO_BLOCK:
                        $workerCountToBlock = $setting['value'];
                        break;
                    case TenantSetting::SETTING_WORKER_TIME_OF_BLOCK:
                        $workerTimeOfBlock = $setting['value'];
                        break;
                    case TenantSetting::SETTING_WORKER_PRE_ORDER_REMINDER:
                        $workerPreOrderReminder = $setting['value'];
                        break;
                    case TenantSetting::SETTING_WORKER_PRE_ORDER_BLOCK_HOURS:
                        $workerPreOrderBlockHours = $setting['value'];
                        break;
                    case TenantSetting::SETTING_WORKER_PRE_ORDER_REJECT_MIN:
                        $workerPreOrderRejectMin = $setting['value'];
                        break;
                }
            }
        }

        $car = Car::find()
            ->alias('t')
            ->joinWith([
                'carModel',
                'carModel.brand'
            ])
            ->with([
                'carHasOptions' => function ($query) use ($worker, $workerCityId) {
                    $query->where([
                        'in',
                        'option_id',
                        TenantHasCarOption::find()->select('option_id')
                            ->where([
                                'tenant_id' => $worker['tenant_id'],
                                'city_id'   => $workerCityId,
                                'is_active' => 1,
                            ]),
                    ]);
                },
            ])
            ->where(['t.car_id' => $workerCarId])
            ->asArray()
            ->one();

        $data['worker']                          = $worker;
        $data['worker']['tenant_login']          = $tenantLogin;
        $data['worker']['status']                = Worker::STATUS_FREE;
        $data['worker']['photo_url']             = Worker::getPhotoUrl($workerId, $tenantLogin);
        $data['worker']['worker_tariff_id']      = $tariffId;
        $data['worker']['worker_shift_id']       = $workerShiftId;
        $data['worker']['worker_shift_start']    = $newShiftStart;
        $data['worker']['worker_shift_time']    = $shiftLifeTime;
        $data['worker']['device_info']           = Worker::getWorkerDeviceInfo();
        $data['worker']['worker_type_block']     = $workerTypeBlock;
        $data['worker']['worker_count_to_block'] = $workerCountToBlock;
        $data['worker']['worker_time_of_block']  = $workerTimeOfBlock;
        $data['worker']['pre_order_reminder']    = unserialize($workerPreOrderReminder);
        $data['worker']['pre_order_block_houer'] = $workerPreOrderBlockHours;
        $data['worker']['pre_order_reject_min']  = $workerPreOrderRejectMin;
        $data['worker']['summ_reject']           = null;
        $data['worker']['in_row_reject']         = null;

        $data['worker']['city_id'] = $workerCityId;
        $data['worker']['cities']  = $this->getWorkerCities($worker['tenant_id'], $worker['worker_id']);

        $data['car']              = $car;
        $data['car']['photo_url'] = Car::getPhotoUrl($workerCarId, $tenantLogin);
        $data['car']['car_time']  = null;
        $data['car']['color']     = CarColor::getCarColorName($car['color']);
        $data['car']['callsign']  = $car['callsign'];

        $data['geo']['parking_id'] = null;
        $data['geo']['lat']        = null;
        $data['geo']['lon']        = null;
        $data['geo']['degree']     = null;
        $data['geo']['speed']      = null;

        $position = WorkerHasPosition::find()
            ->alias('t')
            ->with(['position', 'group'])
            ->where([
                't.worker_id'   => $workerId,
                't.position_id' => $positionId,
                't.active'      => 1,
            ])
            ->asArray()
            ->one();

        $data['position']                = [];
        $data['position']['position_id'] = $position['position_id'];
        $data['position']['module_id']   = $position['position']['module_id'];
        $data['position']['name']        = $position['position']['name'];
        $data['position']['has_car']     = $position['position']['has_car'];
        $data['position']['rating']      = $position['rating'];

        if ((int)$data['position']['has_car'] === 1) {
            $groupId = $car['group_id'];
            $group   = WorkerGroup::findOne($groupId);

            $data['worker']['group_id'] = $groupId;

            $data['position']['group_priority'] = empty($group['priority']) ? 0 : (int)$group['priority'];
            $data['position']['group_block']    = empty($group['block']) ? 0 : (int)$group['block'];
            $data['position']['classes']        = ArrayHelper::getColumn(
                CarHasWorkerGroupClass::findAll(['car_id' => $workerCarId]), 'class_id');
            $data['position']['positions']      = [];

            if (!in_array($car['class_id'], $data['position']['classes'], false)) {
                $data['position']['classes'][] = $car['class_id'];
            }
        } else {
            $groupId = empty($position['group']['group_id']) ? null : $position['group']['group_id'];

            $data['worker']['group_id'] = $groupId;

            $data['position']['group_priority'] = empty($position['group']['priority']) ? 0 : (int)$position['group']['priority'];
            $data['position']['group_block']    = empty($position['group']['block']) ? 0 : (int)$position['group']['block'];
            $data['position']['classes']        = [];
            $data['position']['positions']      = ArrayHelper::getColumn(
                WorkerGroupCanViewClientTariff::findAll(['group_id' => $position['group_id']]), 'class_id');

            if (!in_array($position['position_id'], $data['position']['positions'], false)) {
                $data['position']['positions'][] = $position['position_id'];
            }
        }

        $this->updateWorkerShiftInfo($workerShiftId, $groupId, $data['position']['group_priority'],
            $data['position']['rating']);

        $tenantCompanyBuild = \Yii::createObject(TenantCompanyBuild::class, [$data, $worker]);
        $data = $tenantCompanyBuild->assemble();

        $result = \Yii::$app->redis_workers->executeCommand(
            'HSET', [$worker['tenant_id'], $worker['callsign'], serialize($data)]);

        return $result == 0 || $result == 1;
    }

    /**
     * Setup current city
     *
     * @param int $workerId
     * @param int $cityId
     */
    public function setupCurrentCity($workerId, $cityId)
    {
        WorkerHasCity::updateAll(['last_active' => 0], ['worker_id' => $workerId]);

        /* @var $model WorkerHasCity */
        $model              = WorkerHasCity::find()
            ->where([
                'worker_id' => $workerId,
                'city_id'   => $cityId,
            ])
            ->one();
        $model->last_active = 1;

        if (!$model->save()) {
            \Yii::error(implode('; ', $model->getFirstErrors()), 'setup-current-city');
        }
    }

    private function tryCloseShift($workerShiftService, $tenantId, $tenantLogin, $workerCallsign)
    {
        try {
            $workerShiftService->closeWorkerShift($tenantId, $tenantLogin, $workerCallsign);
        } catch (\Exception $ex) {
            \Yii::error($ex, 'worker-start-work');
        }
    }

    /**
     * Start worker shift
     *
     * @param int    $tenantId
     * @param string $tenantLogin
     * @param int    $workerId
     * @param int    $workerCallsign
     * @param array  $workerTariff
     * @param int    $workerCarId
     * @param int    $workerCityId
     * @param bool   $hasActiveAboniment
     * @param float  $carMileage
     *
     * @return bool
     * @throws NotInstantiableException
     * @throws Exception
     * @throws ShiftIsClosedException
     * @throws InvalidConfigException
     */
    public function startShift(
        $tenantId,
        $tenantLogin,
        $workerId,
        $workerCallsign,
        $workerTariff,
        $workerCarId,
        $workerCityId,
        $hasActiveAboniment,
        $carMileage
    ) {
        /**
         * @var $workerTariffService WorkerTariffService
         */
        $workerTariffService = \Yii::$container->get(WorkerTariffService::className());

        /**
         * @var $workerShiftService WorkerShiftService
         */
        $workerShiftService = \Yii::$container->get(WorkerShiftService::className());

        $tariffId      = $workerTariff['tariff_id'];
        $periodType    = $workerTariff['period_type'];
        $currentTime   = time() + City::getTimeOffset($workerCityId);
        $shiftLifeTime = $workerShiftService->getShiftLifeTime($workerTariff['period'], $periodType, $currentTime);

        $workerTariffService->deleteInactiveAboniments($workerId, $currentTime);

        if ($hasActiveAboniment) {
            $validDate = $workerTariffService->getAbonimentShiftValidDate($workerId, $tariffId);

            if ($currentTime > $validDate) {
                try {
                    $validDate = $workerTariffService->activateAbonimentShiftAndGetValidDate($workerId, $tariffId,
                        $currentTime + $shiftLifeTime);
                } catch (\Exception $ex) {
                    return false;
                }
            }
            $shiftLifeTime = $validDate - $currentTime;
        } else {
            $days         = (int)$workerTariff['days'] < 1 ? 0 : (int)$workerTariff['days'] - 1;
            $shiftValidTo = $currentTime + $shiftLifeTime;

            $countActive = null;
            $expiredAt   = null;

            if ($workerTariff['type'] === WorkerTariff::TYPE_ONCE) {
                $countActive = 0;
            } else {
                if ($periodType === WorkerTariff::PERIOD_TYPE_HOURS
                    || $workerTariff['subscription_limit_type'] === WorkerTariff::SUBSCRIPTION_LIMIT_TYPE_SHIFT) {
                    $countActive = $days;
                }
                if ($workerTariff['subscription_limit_type'] === WorkerTariff::SUBSCRIPTION_LIMIT_TYPE_DAY) {
                    if ($periodType === WorkerTariff::PERIOD_TYPE_HOURS) {
                        $days++;
                    }
                    $expiredAt = strtotime("+ {$days} day", $shiftValidTo);
                }
            }

            if (!$workerTariffService->createWorkerAboniment($workerId, $tariffId, $countActive, $expiredAt,
                $shiftValidTo, $workerTariff)
            ) {
                return false;
            }
        }


       list( $newShiftId, $newShiftStart)  = $workerShiftService->createWorkerShift(
            $tenantLogin, $workerId, $workerCallsign, $workerCarId, $workerCityId, $workerTariff, $shiftLifeTime);


        if ($carMileage !== null) {
            try {
                CarMileage::createCarMileage((int)$workerCarId, (int)$newShiftId, (float)$carMileage);
            } catch (\Exception $ex) {
                $this->tryCloseShift($workerShiftService, $tenantId, $tenantLogin, $workerCallsign);
            }
        }


        if (empty($newShiftId)) {
            $this->tryCloseShift($workerShiftService, $tenantId, $tenantLogin, $workerCallsign);

            return false;
        }

        $this->setupCurrentCity($workerId, $workerCityId);

        if ($this->setWorkerToOnline(
            $tenantLogin,
            $workerId,
            $workerTariff['position_id'],
            $tariffId,
            $workerCarId,
            $workerCityId,
            $newShiftId,
            $newShiftStart,
            $shiftLifeTime)
        ) {
            if ($workerShiftService->sendWorkerShiftToNode(
                $tenantId, $tenantLogin, $workerCallsign, $newShiftId, $shiftLifeTime)
            ) {
                return true;
            } else {
                $workerShiftService->closeWorkerShift($tenantId, $tenantLogin, $workerCallsign);
            }
        }

        return false;
    }

    /**
     * Close worker shift
     *
     * @param int    $tenantId
     * @param string $tenantLogin
     * @param string $workerCallsign
     *
     * @return bool
     */
    public function closeShift($tenantId, $tenantLogin, $workerCallsign)
    {
        /**
         * @var $workerShiftService WorkerShiftService
         */
        $workerShiftService = \Yii::$container->get(WorkerShiftService::className());

        $worker = $this->getWorkerFromRedis($tenantId, $workerCallsign);

        // Если исполнитель на смене заказе, то не закрываем заказ,
        //а ставим флаг, что нужно будет закрыть после заказа
        if (isset($worker['worker']['status'])) {
            if ($worker['worker']['status'] == Worker::STATUS_ON_ORDER) {
                $worker['worker']['need_close_shift'] = 1;

                $worker = serialize($worker);
                $result = \Yii::$app->redis_workers->executeCommand(
                    'HSET', [$tenantId, $workerCallsign, $worker]);

                return $result == 0 || $result == 1;
            }
        }

        return $workerShiftService->closeWorkerShift($tenantId, $tenantLogin, $workerCallsign);
    }

    /**
     * @param int   $parkingId
     * @param int   $callsign
     * @param float $rating
     *
     * @return bool
     * @throws Exception
     */
    private function saveWorkerToParking($parkingId, $callsign, $rating)
    {
        /** @var Connection $db */
        $db     = \Yii::$app->redis_parking_hashtable;
        $result = (int)$db->executeCommand('HSET', [$parkingId, $callsign, $rating]);

        return $result === 0 || $result === 1;
    }

    /**
     * @param int $parkingId
     * @param int $callsign
     *
     * @throws Exception
     */
    private function removeWorkerFromParking($parkingId, $callsign)
    {
        /** @var Connection $db */
        $db = \Yii::$app->redis_parking_hashtable;
        $db->executeCommand('HDEL', [$parkingId, $callsign]);
    }

    /**
     * Set worker to parking
     *
     * @param int   $tenantId
     * @param int   $workerCallsign
     * @param int   $parkingId
     * @param float $workerRating
     * @param bool  $needCheckWorkerRating
     *
     * @return bool
     * @throws Exception
     */
    public function setWorkerToParking($tenantId, $workerCallsign, $parkingId, $workerRating, $needCheckWorkerRating)
    {
        if ($parkingId === null || $this->saveWorkerToParking($parkingId, $workerCallsign, $workerRating)) {
            $worker = $this->getWorkerFromRedis($tenantId, $workerCallsign);
            if (!empty($worker)) {
                $workerOldParkingId = isset($worker['geo']['parking_id']) ? (int)$worker['geo']['parking_id'] : null;
                if ($workerOldParkingId !== $parkingId) {
                    $this->removeWorkerFromParking($workerOldParkingId, $workerCallsign);

                    $worker['geo']['parking_id'] = $parkingId;
                    $updateWorkerResult          = $this->saveWorkerToRedis($tenantId, $workerCallsign, $worker);

                    if ($updateWorkerResult == 0 || $updateWorkerResult == 1) {
                        // self::pushUpdateOneParking($tenantId, $parkingId, $needCheckDriverRating);

                        return true;
                    }
                } else {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Возвращает массив исполнителей на парковке в отсортированном порядке если это нужно (по убыванию рейтинга)
     *
     * @param int  $parkingId
     * @param bool $needCheckWorkerRating
     *
     * @return array
     */
    public function getParkingWorkerQueue($parkingId, $needCheckWorkerRating)
    {
        $keys = \Yii::$app->redis_parking_hashtable->executeCommand('HKEYS', [$parkingId]);
        $vals = \Yii::$app->redis_parking_hashtable->executeCommand('HVALS', [$parkingId]);

        if (!$needCheckWorkerRating) {
            return $keys;
        } else {
            $resultArr = [];
            foreach ($keys as $keyI => $keyVal) {
                $resultArr[$keyVal] = $vals[$keyI];
            }
            arsort($resultArr);
            $resultArr = array_keys($resultArr);

            return $resultArr;
        }
    }

    /**
     * Clear information after worker was deleted from mobile application
     *
     * @param int $tenantId
     * @param int $workerCallsign
     */
    public function clearInformationAfterWorkerDelete($tenantId, $workerCallsign)
    {
        \Yii::$app->getDb()->createCommand()->update('{{%worker}}', [
            'device'       => null,
            'device_token' => null,
            'device_info'  => null,
        ], [
            'tenant_id' => $tenantId,
            'callsign'  => $workerCallsign,
        ])->execute();
    }

    public function getAdditionOptions($tariffId, $tenantId, $workerCallsign, $getLang)
    {
        $activeWorker = $this->getWorkerFromRedis($tenantId, $workerCallsign);

        if ((int)$activeWorker['position']['has_car'] !== 1) {
            return [];
        }

        $optionsTariff = ArrayHelper::index(
            $this->workerTariffService->getAdditionalsOptions(
                $tariffId,
                $getLang,
                time() + City::getTimeOffset($activeWorker['worker']['city_id'])
            ),
            'id'
        );

        $carOptions = ArrayHelper::getColumn($activeWorker['car']['carHasOptions'], 'option_id');
        $allowedOptions = array_intersect_key($optionsTariff, array_flip($carOptions));

        return array_values($allowedOptions);
    }

    // получение количество заказов  и общее время работы
    public function getStaticOfWorker($worker_id, $start_work, $end_work){

        try {
            $total_count_orders = 0;
            $total_working= 0;
            $earnings= 0;


            $shifts = WorkerShift::find()
                ->where(['>=','start_work',$start_work])
                ->andWhere(['<=','end_work',$end_work])
                ->andWhere(['worker_id' => $worker_id])
                ->all();

            $shift_array = [];
            foreach ($shifts as $shift){
                $total_count_orders += $shift->completed_order_count;
                $shift_duration = (int)$shift->end_work - (int)$shift->start_work;
                $total_working +=$shift_duration;

                if($shift->completed_order_count  > 0){
                    $shift_array[] = $shift->id;
                }
            }

            if(count($shift_array) > 0){
                $shifts_orders = WorkerShiftOrder::find()
                    ->with('orderDetailCost')
                    ->where(['in','shift_id',$shift_array])
                    ->all();

                foreach ($shifts_orders as $shifts_order){
                    $earnings += $shifts_order->orderDetailCost->summary_cost;
                }
            }

            return [$earnings , $total_count_orders, $total_working ];

        } catch (\Exception $ex) {
            \Yii::error("Error update worker shift information: shiftId={$workerShiftId}, groupId={$groupId}, groupPriority={$groupPriority}, positionPriority={$positionPriority}, error={$ex->getMessage()}",
                'worker-shift');
        }
    }

    // получение списка заказов
    public function getWorker_report($worker_id, $start_work, $end_work, $page){

        $countPerPage = 10;
        $total = 0;

        // неправильная нумерация на сервере начинается с 0 а апп с 1
        if($page > 0){
            $page = $page -1;
            if($page < 0) {
                $page = 0;
            }
        }

        try {
            $total_count_orders = 0;

            $shifts = WorkerShift::find()
                ->where(['>=','start_work',$start_work])
                ->andWhere(['<=','end_work',$end_work])
                ->andWhere(['worker_id' => $worker_id])
                ->andWhere(['>','completed_order_count',0])
                ->all();

            $shift_array = [];
            foreach ($shifts as $shift){
                $shift_array[] = $shift->id;
            }

            $rezult['data'] = [];
            $rezult['current_page'] = $page;
            $rezult['last_page'] = floor( $total/$countPerPage);
            $rezult['per_page'] = $countPerPage;
            $rezult['total'] = $total;

            if(count($shift_array) > 0){
                $shifts_orders = WorkerShiftOrder::find()
                    ->with('order')
                    ->with('orderDetailCost')
                    ->where(['in','shift_id',$shift_array]);
                   // ->all();

                $dataProvider = new ActiveDataProvider([
                    'query' => $shifts_orders,
                    'sort' => [
                        'defaultOrder' => ['order_id' => SORT_ASC ],
                    ],
                    'pagination' => [
                        'pageSize' => $countPerPage
                    ],
                ]);

                $dataProvider->pagination->page = $page;
                $model = $dataProvider->refresh();
                $models = $dataProvider->getModels();
                $total = $dataProvider->getTotalCount();

                $rezult['current_page'] = $page;
                $rezult['last_page'] = floor( $total/$countPerPage);
                $rezult['per_page'] = $countPerPage;
                $rezult['total'] = $total;

                foreach ($models as $model){
                    $item_order = [];
                    $address = unserialize( $model->order->address);
                    $item_order['address']['A']['city'] = isset($address['A']['city']) ? $address['A']['city'] : '';
                    $item_order['address']['A']['street'] = isset($address['A']['street']) ? $address['A']['street'] : '';
                    $item_order['address']['B']['city'] = isset($address['B']['city']) ? $address['B']['city'] : '';
                    $item_order['address']['B']['street'] = isset($address['B']['street']) ? $address['B']['street'] : '';

                    $item_order['create_time'] = ArrayHelper::getValue($model->order, 'create_time', 0);
                    $item_order['order_id'] = ArrayHelper::getValue($model->order, 'order_id', 0);

                    $item_order['earnings']  =  number_format((float)ArrayHelper::getValue($model->order, 'predv_price', 0), 2, '.', '');
                    $item_order['summary_cost'] = number_format((float)ArrayHelper::getValue($model->orderDetailCost, 'summary_cost', 0), 2, '.', '');
                    $rezult['data'][] = $item_order;
                }
                return $rezult;
            } else{
                return $rezult;
            }

        } catch (\Exception $ex) {
            \Yii::error("Error update worker shift information: worker_id={$worker_id}, error={$ex->getMessage()}",
                'worker-shift');
        }
    }
}