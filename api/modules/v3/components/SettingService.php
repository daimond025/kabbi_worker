<?php

namespace v3\components;


use app\models\worker\Worker;
use v3\components\repositories\CityRepository;
use v3\components\repositories\PositionRepository;
use v3\components\repositories\TenantSettingRepository;
use v3\components\repositories\WorkerRepository;
use v3\components\taxiApiRequest\TaxiApiRequest;
use v3\components\taxiApiRequest\TaxiErrorCode;
use v3\exceptions\ExceptionConfidentialPage;
use yii\base\Object;

class SettingService extends Object
{

    const SETTING_SHOW_DRIVER_PRIVACY_POLICY = 'SHOW_DRIVER_PRIVACY_POLICY';
    const SETTING_DRIVER_PRIVACY_POLICY = 'DRIVER_PRIVACY_POLICY';

    public function getConfidentialPage($tenantId, $workerCallsign, $workerCityId, $positionId)
    {

        if (WorkerRepository::isBlockedWorker($tenantId, $workerCallsign)) {
            throw new ExceptionConfidentialPage(TaxiErrorCode::WORKER_BLOCKED);
        }

        if (CityRepository::isBlockedCityAtTenant($workerCityId, $tenantId)) {
            throw new ExceptionConfidentialPage(TaxiErrorCode::CITY_BLOCKED);
        }

        if (!PositionRepository::isActivePositionInCity($tenantId, $workerCityId, $positionId)) {
            throw new ExceptionConfidentialPage(TaxiErrorCode::FORBIDDEN_ACTION);
        }

        if (!TenantSettingRepository::isSettingAllowed(
            $tenantId, self::SETTING_SHOW_DRIVER_PRIVACY_POLICY, $workerCityId, $positionId)
        ) {
            throw new ExceptionConfidentialPage(TaxiErrorCode::ACCESS_DENIED);
        }

        return TenantSettingRepository::getValueSettingAtTenant($tenantId, self::SETTING_DRIVER_PRIVACY_POLICY);
    }
}