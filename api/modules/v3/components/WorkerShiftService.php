<?php

namespace v3\components;

use app\components\serviceEngine\ServiceEngine;
use app\models\address\City;
use app\models\worker\CarMileage;
use app\models\worker\Worker;
use app\models\worker\WorkerOptionTariff;
use app\models\worker\WorkerShift;
use app\models\worker\WorkerTariff;
use v3\components\taxiApiRequest\TaxiErrorCode;
use v3\exceptions\ShiftIsClosedException;
use yii\base\Object;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

/**
 * Class WorkerShiftService
 * @package v3\components
 */
class WorkerShiftService extends Object
{

    const WORKER_ON_ORDER = "ON_ORDER";
    /**
     * Getting lifetime of worker shift
     *
     * @param int    $period
     * @param string $periodType
     * @param int    $currentTime
     *
     * @return int
     */
    public function getShiftLifeTime($period, $periodType, $currentTime)
    {
        if ($periodType === WorkerTariff::PERIOD_TYPE_HOURS) {
            return $period * 3600;
        } else {
            $periodArr   = explode("-", $period);
            $periodStart = explode(":", $periodArr[0]);
            $periodEnd   = explode(":", $periodArr[1]);

            $startH = (int)$periodStart[0];
            //$startM = (int)$periodStart[1];

            $endH = (int)$periodEnd[0];
            $endM = (int)$periodEnd[1];

            //$time = new \DateTime();

            if ($endH >= $startH) {
                // $resTime = $time->format("d.m.Y $endH:$endM:00");
                $resTime = date("d.m.Y $endH:$endM:00", $currentTime);

                return strtotime($resTime) - $currentTime;
            } else {
                // $resTime = $time->format("d.m.Y 23:59:00");
                $resTime = date("d.m.Y 23:59:00", $currentTime);

                return strtotime($resTime) - $currentTime + ($endH * 3600 + $endM * 60);
            }
        }
    }

    /**
     * Create worker shift and return shift ID if shift was created successful
     *
     * @param string $tenantLogin
     * @param int    $workerId
     * @param int    $workerCallsign
     * @param int    $workerCarId
     * @param int    $workerCityId
     * @param array  $workerTariff
     * @param int    $shiftLifeTime
     *
     * @return int
     * @throws \yii\db\Exception
     */
    public function createWorkerShift(
        $tenantLogin,
        $workerId,
        $workerCallsign,
        $workerCarId,
        $workerCityId,
        $workerTariff,
        $shiftLifeTime
    ) {
        $transaction = \Yii::$app->db->beginTransaction();

        $shift = new WorkerShift();

        $shift->worker_id   = $workerId;
        $shift->car_id      = $workerCarId;
        $shift->city_id     = $workerCityId;
        $shift->tariff_id   = $workerTariff['tariff_id'];
        $shift->start_work  = time();
        $shift->position_id = $workerTariff['position_id'];

        if ($shift->save()) {
            $redisWorkerShift = \Yii::$app->redis_worker_shift;
            $shiftKey         = $tenantLogin . '_' . $workerCallsign;

            if ($redisWorkerShift->executeCommand('SET', [$shiftKey, $shift->id])
                && $redisWorkerShift->executeCommand('EXPIRE', [$shiftKey, $shiftLifeTime])
            ) {
                $transaction->commit();

                return [$shift->id, $shift->start_work];
            }
        }

        $transaction->rollBack();
    }

    /**
     * Close worker shift
     *
     * @param int    $tenantId
     * @param string $tenantLogin
     * @param int    $workerCallsign
     *
     * @return bool
     * @throws \v3\exceptions\ShiftIsClosedException
     * @throws \yii\base\InvalidConfigException
     */
    public function closeWorkerShift($tenantId, $tenantLogin, $workerCallsign)
    {
        $redisWorkers     = \Yii::$app->redis_workers;
        $redisWorkerShift = \Yii::$app->redis_worker_shift;

        $workerData = $redisWorkers->executeCommand('HGET', [$tenantId, $workerCallsign]);
        $workerData = unserialize($workerData);

        if (!empty($workerData)) {
            $workerShiftId = isset($workerData['worker']['worker_shift_id']) ? $workerData['worker']['worker_shift_id'] : null;
            $workerId      = isset($workerData['worker']['workerId']) ? $workerData['worker']['workerId'] : null;
        }

        if (empty($workerId)) {
            /* @var $workerService WorkerService */
            $workerService = \Yii::$container->get(WorkerService::className());
            $worker        = $workerService->getWorker($tenantId, $workerCallsign);
            $workerId      = isset($worker['worker_id']) ? $worker['worker_id'] : null;
        }

        if (empty($workerShiftId)) {
            $workerShiftId = $redisWorkerShift->executeCommand('GET',
                [$tenantLogin . '_' . $workerCallsign]);
        }

        if (empty($workerShiftId)) {
            throw new ShiftIsClosedException('Shift is already closed');
        }

        /* @var $serviceEngine ServiceEngine */
        $serviceEngine = \Yii::$container->get(ServiceEngine::className());

        return $serviceEngine->closeWorkerShift($tenantId, $tenantLogin, $workerId, $workerCallsign, $workerShiftId);
    }

    /**
     * Send worker shift to nodejs
     *
     * @param int    $tenantId
     * @param string $tenantLogin
     * @param int    $workerCallsign
     * @param int    $workerShiftId
     * @param int    $shiftLifeTime
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function sendWorkerShiftToNode($tenantId, $tenantLogin, $workerCallsign, $workerShiftId, $shiftLifeTime)
    {
        /* @var $serviceEngine ServiceEngine */
        $serviceEngine = \Yii::$container->get(ServiceEngine::className());

        return $serviceEngine->watchWorkerShift(
            $tenantId, $tenantLogin, $workerCallsign, $workerShiftId, $shiftLifeTime);
    }

    public function getCurrentShiftId(int $tenantId, string $tenantLogin, int $workerCallsign): ?int
    {
        $workerShiftId = \Yii::$app->redis_worker_shift->executeCommand('GET', [$tenantLogin . '_' . $workerCallsign]);
        if (!empty($workerShiftId)) {
            return (int)$workerShiftId;
        }

        $workerData = \Yii::$app->redis_workers->executeCommand('HGET', [$tenantId, $workerCallsign]);
        $workerData = unserialize($workerData);
        if (!empty($workerData)) {
            return $workerData['worker']['worker_shift_id'] ?? null;
        }

        return null;
    }

     public function workerOnOrder(int $tenantId,int $workerCallsign ){
         $workerData = \Yii::$app->redis_workers->executeCommand('HGET', [$tenantId, $workerCallsign]);

         $workerData = unserialize($workerData);
         if(empty($workerData)){
             return TaxiErrorCode::SHIFT_NEED_CLOSED;
         }

         $worker_id = ArrayHelper::getValue($workerData, 'worker.worker_id', null);
         $worker_status = ArrayHelper::getValue($workerData, 'worker.status', '');
         $active_orders = ArrayHelper::getValue($workerData, 'worker.active_orders', []);

         // проверка что активных заказов у водителя нет
         $last_order_id = ArrayHelper::getValue($workerData, 'worker.last_order_id');

         $active = false;
         foreach($active_orders as $order_id){
             $order = \Yii::$app->redis_orders_active->executeCommand('HGET', [$tenantId, $order_id]);
             if(!is_null($order)){
                 $order_detail = unserialize($order);
                 $order_worker = ArrayHelper::getValue($order_detail, 'worker_id', null);
                 if( !is_null($order_worker) && ((int)$order_worker === (int)$worker_id)){
                     $active = true; break;
                 }
             }
         }

         if($active){
             return TaxiErrorCode::WORKER_ALREADY_HAS_ACTIVE_ORDER;
         }

        /* if($last_order_id){
             $order = \Yii::$app->redis_orders_active->executeCommand('HGET', [$tenantId, $last_order_id]);
         }

         if($last_order_id &&(!is_null($order))){
             return TaxiErrorCode::WORKER_ALREADY_HAS_ACTIVE_ORDER;
         }


         $statusWorker = ArrayHelper::getValue($workerData, 'worker.status');
         if (isset($statusWorker) && (strcasecmp($statusWorker, $this::WORKER_ON_ORDER) === 0 )) {
              return TaxiErrorCode::WORKER_ALREADY_HAS_ACTIVE_ORDER;
         }*/

         return 0;
     }


    /**
     * Getting worker shift id from Redis
     *
     * @param string $tenantLogin
     * @param int    $workerCallsign
     *
     * @return string
     */
    public function getWorkerShiftIdFromRedis($tenantLogin, $workerCallsign)
    {
        $shiftKey = $tenantLogin . '_' . $workerCallsign;

        return \Yii::$app->redis_worker_shift->executeCommand('GET', [$shiftKey]);
    }

    /**
     * Set pause data to Redis and MySql
     *
     * @param int    $tenantId
     * @param string $tenantLogin
     * @param int    $workerCallsign
     * @param string $comment
     * @param string $action
     *
     */
    public function setPauseData($tenantId, $tenantLogin, $workerCallsign, $comment, $action)
    {
        /**
         * @var $workerService WorkerService
         */
        $workerService = \Yii::$container->get(WorkerService::className());

        $worker = $workerService->getWorkerFromRedis($tenantId, $workerCallsign);
        if (!empty($worker)) {
            $pauseData = [];

            if (!empty($worker['worker']['pause_data'])) {
                $pauseData = $worker['worker']['pause_data'];
                $count     = count($pauseData);

                if ($pauseData[$count - 1]['pause_end'] === null) {
                    $pauseData[$count - 1]['pause_end'] = (string)time();
                }
            }

            if ($action === Worker::ACTION_PAUSE) {
                $pauseData[] = [
                    'pause_start'  => (string)time(),
                    'pause_end'    => null,
                    'pause_reason' => $comment,
                ];
            }

            $worker['worker']['pause_data'] = $pauseData;
            $workerService->saveWorkerToRedis($tenantId, $workerCallsign, $worker);

            $shift = WorkerShift::findone($this->getWorkerShiftIdFromRedis($tenantLogin, $workerCallsign));

            if (!empty($shift)) {
                $shift->pause_data = serialize($pauseData);
                $shift->save();
            }
        }
    }

}