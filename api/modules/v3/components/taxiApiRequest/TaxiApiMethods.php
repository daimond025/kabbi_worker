<?php

namespace v3\components\taxiApiRequest;

use yii\base\Object;
use Yii;

/*
 * Для общей работы с набором методов АПИ, их параметров
 */

class TaxiApiMethods extends Object
{
    const REQUARED = '!REQ!';

    public $validateData;

    /**
     * Карта для исправления и автозаполнения пареметров коммнад апи
     * @var array
     */
    private $_fixParamsMap = [
        'auth_worker'                           => [
            'tenant_login'    => TaxiApiMethods::REQUARED,
            'worker_login'    => TaxiApiMethods::REQUARED,
            'worker_password' => TaxiApiMethods::REQUARED,
            'device_token'    => TaxiApiMethods::REQUARED,
        ],
        'set_worker_position'                   => [
            'tenant_login'        => TaxiApiMethods::REQUARED,
            'worker_login'        => TaxiApiMethods::REQUARED,
            'worker_lat'          => TaxiApiMethods::REQUARED,
            'worker_lon'          => TaxiApiMethods::REQUARED,
            'worker_degree'       => TaxiApiMethods::REQUARED,
            'worker_speed'        => TaxiApiMethods::REQUARED,
            'update_time'         => null,
            'coordinate_accuracy' => null,
        ],
        'get_worker_cities'                     => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
        ],
        'get_worker_positions'                  => [
            'tenant_login'   => TaxiApiMethods::REQUARED,
            'worker_login'   => TaxiApiMethods::REQUARED,
            'worker_city_id' => TaxiApiMethods::REQUARED,
        ],
        'get_worker_profile'                    => [
            'tenant_login'   => TaxiApiMethods::REQUARED,
            'worker_login'   => TaxiApiMethods::REQUARED,
            'worker_city_id' => null,
            'position_id'    => null,
        ],
        'get_worker_balance'                    => [
            'tenant_login'   => TaxiApiMethods::REQUARED,
            'worker_login'   => TaxiApiMethods::REQUARED,
            'worker_city_id' => null,
        ],
        'worker_start_work'                     => [
            'tenant_login'     => TaxiApiMethods::REQUARED,
            'worker_login'     => TaxiApiMethods::REQUARED,
            'worker_tariff_id' => TaxiApiMethods::REQUARED,
            'worker_car_id'    => null,
            'worker_city_id'   => TaxiApiMethods::REQUARED,
            'device_token'     => null,
            'app_version'      => null,
            'car_mileage'      => null,
        ],
        'worker_end_work'                       => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'car_mileage'  => null,
        ],
        'set_car_mileage' => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'shift_id'     => TaxiApiMethods::REQUARED,
            'car_mileage'  => TaxiApiMethods::REQUARED,
        ],
        'get_parkings_list'                     => [
            'tenant_login'   => TaxiApiMethods::REQUARED,
            'worker_login'   => TaxiApiMethods::REQUARED,
            'worker_city_id' => TaxiApiMethods::REQUARED,
            'parking_id'     => null,
        ],
        'get_city_polygone'                     => [
            'tenant_login'   => TaxiApiMethods::REQUARED,
            'worker_login'   => TaxiApiMethods::REQUARED,
            'worker_city_id' => TaxiApiMethods::REQUARED,
        ],
        'set_parking_queue'                     => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'parking_id'   => null,
        ],
        'get_parkings_orders_queues'            => [
            'tenant_login'   => TaxiApiMethods::REQUARED,
            'worker_login'   => TaxiApiMethods::REQUARED,
            'worker_city_id' => TaxiApiMethods::REQUARED,
            'parking_id'     => null,
        ],
        'get_own_orders'                        => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
        ],
        'get_orders_open_list'                  => [
            'tenant_login'   => TaxiApiMethods::REQUARED,
            'worker_login'   => TaxiApiMethods::REQUARED,
            'worker_city_id' => null,
            'worker_lat'     => null,
            'worker_lon'     => null,
            'parking_id'     => null,
        ],
        'get_orders_pretime_list'               => [
            'tenant_login'   => TaxiApiMethods::REQUARED,
            'worker_login'   => TaxiApiMethods::REQUARED,
            'worker_city_id' => null,
        ],
        'get_order_for_id'                      => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'order_id'     => TaxiApiMethods::REQUARED,
        ],
        'set_order_status'                      => [
            'tenant_login'      => TaxiApiMethods::REQUARED,
            'worker_login'      => TaxiApiMethods::REQUARED,
            'order_id'          => TaxiApiMethods::REQUARED,
            'status_new_id'     => TaxiApiMethods::REQUARED,
            'time_to_client'    => null,
            'detail_order_data' => null,
            'order_refuse' => null,
        ],
        'set_order_route'                       => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'order_id'     => TaxiApiMethods::REQUARED,
            'order_route'  => TaxiApiMethods::REQUARED,
        ],
        'set_order_route_file'                  => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'order_id'     => TaxiApiMethods::REQUARED,
        ],
        'check_worker_on_work'                  => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
        ],
        'set_signal_sos'                        => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'worker_lat'   => TaxiApiMethods::REQUARED,
            'worker_lon'   => TaxiApiMethods::REQUARED,
        ],
        'unset_signal_sos'                      => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'worker_lat'   => TaxiApiMethods::REQUARED,
            'worker_lon'   => TaxiApiMethods::REQUARED,
        ],
        'get_city_list'                         => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'city_part'    => TaxiApiMethods::REQUARED,
        ],
        'get_geoobjects_list'                   => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'city_id'      => TaxiApiMethods::REQUARED,
            'street_part'  => TaxiApiMethods::REQUARED,
        ],
        'set_worker_state'                      => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'action'       => TaxiApiMethods::REQUARED,
            'comment'      => null,
        ],
        'get_client_tariff'                     => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
        ],
        'call_cost'                             => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'tariff_id'    => TaxiApiMethods::REQUARED,
            'address'      => TaxiApiMethods::REQUARED,
            'order_date'   => null,
        ],
        'create_order'                          => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'tariff_id'    => TaxiApiMethods::REQUARED,
            'address'      => TaxiApiMethods::REQUARED,
            'client_phone' => TaxiApiMethods::REQUARED,
        ],
        'update_order'                          => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'order_id'     => TaxiApiMethods::REQUARED,
            'address'      => TaxiApiMethods::REQUARED,
        ],
        'update_order_cost'                     => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'order_id'     => TaxiApiMethods::REQUARED,
            'predv_price'  => TaxiApiMethods::REQUARED,
        ],
        'set_order_raw_calc'                    => [
            'tenant_login'    => TaxiApiMethods::REQUARED,
            'worker_login'    => TaxiApiMethods::REQUARED,
            'order_id'        => TaxiApiMethods::REQUARED,
            'order_calc_data' => TaxiApiMethods::REQUARED,
        ],
        'get_order_raw_calc'                    => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'order_id'     => TaxiApiMethods::REQUARED,
        ],
        'worker_order_unblock'                  => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'block_id'     => TaxiApiMethods::REQUARED,
        ],
        'worker_order_block'                    => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
        ],
        'worker_pre_order_unblock'              => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'block_id'     => TaxiApiMethods::REQUARED,
        ],
        'worker_pre_order_block'                => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
        ],

        'get_bank_cards'                        => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
        ],
        'create_bank_card'                      => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
        ],
        'check_bank_card'                       => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'order_id'     => TaxiApiMethods::REQUARED,
        ],
        'delete_bank_card'                      => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'pan'          => TaxiApiMethods::REQUARED,
        ],
        'refill_account'                        => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'pan'          => TaxiApiMethods::REQUARED,
            'sum'          => TaxiApiMethods::REQUARED,
            'currency'     => TaxiApiMethods::REQUARED,
        ],

        'get_request_result'                    => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'request_id'   => TaxiApiMethods::REQUARED,
        ],
        'clear_information_after_worker_delete' => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
        ],

        'get_confidential_page' => [
            'tenant_login'   => TaxiApiMethods::REQUARED,
            'worker_login'   => TaxiApiMethods::REQUARED,
            'worker_city_id' => TaxiApiMethods::REQUARED,
            'position_id'    => TaxiApiMethods::REQUARED,
        ],

        'get_additional_options' => [
            'tariff_id'   => TaxiApiMethods::REQUARED,
            'tenant_login'   => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
        ],
        'worker_statistic' => [
            'start_date' => TaxiApiMethods::REQUARED,
            'end_date' => TaxiApiMethods::REQUARED,
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
        ],

        'worker_report' => [
            'start_date' => TaxiApiMethods::REQUARED,
            'end_date' => TaxiApiMethods::REQUARED,
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
            'page' => null,
        ],

        'get_own_orders_hospital'      => [
            'tenant_login' => TaxiApiMethods::REQUARED,
            'worker_login' => TaxiApiMethods::REQUARED,
        ],
    ];

    public function __construct()
    {
        $this->validateData['code']  = null;
        $this->validateData['param'] = null;
        parent::__construct();
    }
    /*
     * GETTERS AND SETTERS
     */

    /*
     * METHODS
     */

    /**
     * Получить порядок по умолчанию для параметров метода, с дефолтными значениями
     *
     * @param string $commandName - метод
     *
     * @return array - array('name' => '', 'type' => null, 'orderId' => 0, ...)
     */
    private function defaultOrder($commandName)
    {
        $map = $this->_fixParamsMap[$commandName];

        return $map;
    }

    private function setValidateDataResult($code, $param = null)
    {
        $this->validateData['code']  = $code;
        $this->validateData['param'] = $param;

        return $this->validateData;
    }

    /**
     * Проверяет имена параметров на корректность
     *
     * @param type $commandName
     * @param type $params
     *
     * @return int TaxiErrorCode
     */
    private function validateParamName($commandName, $params)
    {
        $defaultOrder = $this->defaultOrder($commandName);
        foreach ($params as $param => $value) {
            if (!key_exists($param, $defaultOrder)) {
                return $this->setValidateDataResult(TaxiErrorCode::BAD_PARAM, $param);
            }
        }

        return $this->setValidateDataResult(TaxiErrorCode::OK);
    }

    /**
     * Проверка, заполнены ли обязательные параметры
     *
     * @param type $commandName
     * @param type $params
     *
     * @return int TaxiErrorCode
     */
    private function validateParamRequared($commandName, $params)
    {
        $defaultOrder = $this->defaultOrder($commandName);
        foreach ($defaultOrder as $defaultParam => $defaultValue) {
            if ($defaultValue == TaxiApiMethods::REQUARED) {
                if (!key_exists($defaultParam, $params)) {
                    return $this->setValidateDataResult(TaxiErrorCode::MISSING_INPUT_PARAMETER, $defaultParam);
                }
                if (key_exists($defaultParam, $params) && empty($params[$defaultParam])) {
                    return $this->setValidateDataResult(TaxiErrorCode::EMPTY_VALUE, $defaultParam);
                }
            }
        }

        return TaxiErrorCode::OK;
    }

    /**
     * Проверка входных парамтеров команды на корректность,
     * Если одна из проверок не проходит то возвращается код ошибки в
     * соответствующем методе
     *
     * @param strin $commandName - команда
     * @param array $params - параметры
     *
     * @return int TaxiErrorCode
     */
    public function validateParams($commandName, $params)
    {
        if (key_exists($commandName, $this->_fixParamsMap)) {
            //Проверяем параметры запроса на корректное название
            $validateData = $this->validateParamName($commandName, $params);
            if (!empty($validateData['code'])) {
                return $validateData;
            }
            //Проверяем, заполнены ли обязательные параметры
            $validateData = $this->validateParamRequared($commandName, $params);
            if (!empty($validateData['code'])) {
                return $validateData;
            }

            if (empty($validateData['code'])) {
                return $this->setValidateDataResult(TaxiErrorCode::OK);
            }
        } else {
            //Неизвестный запрос
            return $this->setValidateDataResult(TaxiErrorCode::UNKNOWN_REQUEST);
        }
    }
}