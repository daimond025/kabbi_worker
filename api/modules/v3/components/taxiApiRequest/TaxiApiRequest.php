<?php

namespace v3\components\taxiApiRequest;

use app\models\tenant\Tenant;
use app\models\tenant\TenantSetting;
use yii\base\Object;
use Yii;
use yii\web\Response;

/**
 * Класс обеспечивающий стандартизацию ответа от апи
 * Анализатор маршрута
 * @author Сергей К.
 */
class TaxiApiRequest extends Object
{

    public $methods;
    public $errCode;
    public $dataBaseLayer;
    public $validateData;

    public function __construct($config = [])
    {
        $this->methods              = new TaxiApiMethods();
        $this->errCode              = new TaxiErrorCode();
        $this->validateData['code'] = null;
        $this->validateData['info'] = null;
        parent::__construct($config);
    }

    private function setValidateDataFull($code, $param = null)
    {
        $this->validateData['code'] = $code;
        $this->validateData['info'] = $this->errCode->errorCodeData[$code];
        if (!empty($param)) {
            $this->validateData['info'] = $this->validateData['info'] . " $param";
        }

        return $this->validateData;
    }

    public function validateParams($commandName, $headers, $params, $needChekSign, $tenantLogin)
    {
        $signature = isset($headers['signature']['0']) ? $headers['signature']['0'] : null;

        Yii::info("HEEEADERS!: " . json_encode($headers));

        // TODO daimond
        /*if ($needChekSign) {
           if (empty($signature)) {
                return $this->setValidateDataFull(TaxiErrorCode::INCORRECTLY_SIGNATURE);
            }


            if (!(TaxiErrorCode::OK == $this->validateSignature($signature, $tenantLogin, $params))) {
                return $this->setValidateDataFull(TaxiErrorCode::INCORRECTLY_SIGNATURE);
            }
        }*/

        $validateResult = $this->methods->validateParams($commandName, $params);
        return $this->setValidateDataFull($validateResult['code'], $validateResult['param']);
    }

    public function filterParams($params)
    {
        $paramsFiltered = [];
        foreach ($params as $paramKey => $paramVal) {
            if (is_string($paramVal)) {
                $paramVal = urldecode($paramVal);
                $paramVal = str_replace('\\', '/', $paramVal);
                $paramVal = trim(strip_tags(stripcslashes($paramVal)));
            }
            $paramsFiltered[$paramKey] = $paramVal;
        }

        return $paramsFiltered;
    }

    public function getJsonAnswer($validateData, $result = null)
    {
        $response = [
            'code'   => $validateData['code'],
            'info'   => $validateData['info'],
            'result' => $result,
        ];

        return json_encode($response);
    }

    public function getJsonAnswerAdditional($validateData, $result = null, $additional = [])
    {
        $response = [
            'code'   => $validateData['code'],
            'info'   => $validateData['info'],
            'result' => $result,
        ];

        foreach ($additional as $key => $value){
            $response[$key] = $value;
        }

        return json_encode($response);
    }

    private function validateSignature($signature, $tenant_login, $params)
    {
        $apiKey = $this->getTenantApiKey($tenant_login);
        ksort($params);
        $params = http_build_query($params, '', '&', PHP_QUERY_RFC3986);
        $sign   = md5($params . $apiKey);
        Yii::info("PARAMS!: $params");
        Yii::info("API KEY!: $apiKey");
        Yii::info("MY SIGNATURE!: $sign");
        Yii::info("OUT SIGNATURE!: $signature");
        if ($sign == $signature) {
            return TaxiErrorCode::OK;
        } else {
            Yii::error("INCORRECTLY_SIGNATURE");
            Yii::error("PARAMS!: $params");
            Yii::error("API KEY!: $apiKey");
            Yii::error("MY SIGNATURE!: $sign");
            Yii::error("OUT SIGNATURE!: $signature");

            return TaxiErrorCode::INCORRECTLY_SIGNATURE;
        }
    }

    public static function getTenantApiKey($tenant_login)
    {
        $tenantData = Tenant::find()
            ->where(['domain' => $tenant_login])
            ->asArray()
            ->one();

        $tenantSettings = TenantSetting::find()
            ->where(['tenant_id' => $tenantData['tenant_id']])
            ->andWhere(['name' => TenantSetting::SETTING_WORKER_API_KEY])
            ->one();

        return empty($tenantSettings) ? null : $tenantSettings->value;
    }

}
