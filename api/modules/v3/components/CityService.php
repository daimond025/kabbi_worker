<?php

namespace v3\components;

use app\models\address\City;
use yii\base\Object;
use yii\caching\Cache;

/**
 * Class CityService
 * @package v3\components
 */
class CityService extends Object
{
    const TIME_OFFSET_CACHE_DURATION = 3600;

    /**
     * @var Cache
     */
    public $cache;

    /**
     * @var int
     */
    public $limit = 15;

    /**
     * Find city by part name
     *
     * @param string $name
     * @param string $lang
     * @param int    $limit
     *
     * @return array
     */
    public function findCities($name, $lang, $limit)
    {
        return City::find()
            ->select([
                'city_id',
                'city_name' => 'name' . $lang,
                'city_lat'  => 'lat',
                'city_lon'  => 'lon',
            ])
            ->where(['like', 'name' . $lang, $name])
            ->orderBy(['sort' => SORT_DESC])
            ->limit($limit)
            ->asArray()
            ->all();
    }

    /**
     * Find cities by part name (cache results)
     *
     * @param $name
     * @param $lang
     *
     * @return array
     */
    public function findCitiesCached($name, $lang)
    {
        $cache    = isset($this->cache) ? $this->cache : \Yii::$app->cache;
        $cacheKey = "{$name}_{$lang}_{$this->limit}";

        $cities = $cache->get($cacheKey);
        if ($cities === false) {
            $cities = $this->findCities($name, $lang, $this->limit);
            $cache->set($cacheKey, $cities, 8 * 3600);
        }

        return $cities;
    }


    /**
     * Getting city time offset
     *
     * @param int $cityId
     *
     * @return int|mixed
     */
    public function getTimeOffset($cityId)
    {
        return City::getTimeOffset($cityId);
    }

    /**
     * Getting city time offset from cache
     *
     * @param int $cityId
     *
     * @return int
     */
    public function getCachedTimeOffset($cityId)
    {
        $cache = $this->cache === null ? \Yii::$app->cache : $this->cache;

        $cacheKey   = "time offset city_id={$cityId}";
        $timeOffset = $cache->get($cacheKey);
        if ($timeOffset === false) {
            $timeOffset = $this->getTimeOffset($cityId);
            $cache->set($cacheKey, $timeOffset, self::TIME_OFFSET_CACHE_DURATION);
        }

        return $timeOffset;
    }

}