<?php

namespace v3\components;

use app\models\worker\Position;
use yii\base\Object;

/**
 * Class PositionService
 * @package v3\components
 */
class PositionService extends Object
{
    /**
     * Getting position
     *
     * @param integer $positionId
     *
     * @return array|null
     */
    public function getPosition($positionId)
    {
        return Position::find()
            ->where(['position_id' => $positionId])
            ->asArray()
            ->one();
    }
}