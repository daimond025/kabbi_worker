<?php

namespace v3\components;

use yii\base\Object;

/**
 * Class TimeHelper
 * @package v3\components
 */
class TimeHelper extends Object
{
    /**
     * Getting current time
     * @return int
     */
    function getCurrentTime()
    {
        return time();
    }
}