<?php

namespace v3\components\processingErrors;

use v3\components\taxiApiRequest\TaxiErrorCode;

class ProcessingErrorsApiOrder
{

    public $errors422 = [];

    public $errors400 = [
        OrderError::WORKER_SHIFT_IS_CLOSED          => TaxiErrorCode::SHIFT_IS_CLOSED,
        OrderError::WORKER_BLOCKED                  => TaxiErrorCode::WORKER_BLOCKED,
        OrderError::WORKER_ALREADY_HAS_ACTIVE_ORDER => TaxiErrorCode::WORKER_ALREADY_HAS_ACTIVE_ORDER,
        OrderError::TARIFF_ERROR                    => TaxiErrorCode::BAD_TARIFF,
    ];

    protected $statusCode;
    protected $message;
    protected $errors;

    public function setResponse($response)
    {
        $this->statusCode = $response['status'];
        $this->message    = $response['message'];
        $this->errors     = $response['errors'];
    }

    public function getError()
    {
        switch ($this->statusCode) {
            case 422:
                return $this->parseError422();

            case 400:
                return $this->parseError400();

            default:
                return TaxiErrorCode::INTERNAL_ERROR;
        }
    }

    protected function parseError422()
    {
        if (is_array($this->errors) && array_key_exists(current($this->errors)[0], $this->errors422)) {
            $errorKey = $this->errors422[current($this->errors)[0]];

            return $errorKey;
        }

        return TaxiErrorCode::INTERNAL_ERROR;
    }

    protected function parseError400()
    {
        if (array_key_exists((int)$this->message, $this->errors400)) {
            $errorKey = $this->errors400[$this->message];

            return $errorKey;
        }

        return TaxiErrorCode::INTERNAL_ERROR;
    }
}
