<?php

namespace v3\components;

use addressHistory\AddressHistoryService;
use addressHistory\models\Address;
use addressHistory\models\CityId;
use addressHistory\models\Client;
use addressHistory\models\DBAddressHistoryRepository;
use addressHistory\models\HistoryAddress;
use api\models\order\exceptions\SavingOrderTrackException;
use app\components\gearman\Gearman;
use app\models\address\City;
use app\models\order\Order;
use app\models\order\OrderStatus;
use app\models\order\OrderTrackService;
use app\models\order\OrderUpdateEvent;
use app\models\order\RawOrderCalc;
use app\models\tenant\TenantSetting;
use app\models\worker\Worker;
use v3\components\services\filters\order\ClientNameFilter;
use v3\components\services\filters\order\HospitalAddressFilter;
use v3\components\services\filters\order\SettingsFilter;
use yii\base\InvalidConfigException;
use yii\base\Object;
use yii\helpers\ArrayHelper;

/**
 * Class OrderService
 * @package v3\components
 */
class OrderService extends Object
{


    /**
     * Getting order from redis
     * @param $orderId
     * @param $tenantId
     * @return mixed|null
     */
    public function getOrderFromRedis($orderId, $tenantId)
    {
        $order = \Yii::$app->redis_orders_active->executeCommand('HGET', [$tenantId, $orderId]);
        $order = unserialize($order);

        return empty($order) ? null : $order;
    }

    /**
     *  Save order to redis
     * @param $orderId
     * @param $tenantId
     * @param $order
     * @return bool
     */
    public function saveOrderToRedis($orderId, $tenantId, $order): bool
    {
        $orderData = serialize($order);
        $result = \Yii::$app->redis_orders_active->executeCommand('HSET',
            [$tenantId, $orderId, $orderData]);

        return $result == 0 || $result == 1;
    }

    /**
     * @todo переделать на отдельные фильтры
     * Filter order data
     * @param $order
     * @param $tenantId
     * @param $language
     * @param $workerFromRedis
     * @return array
     * @throws InvalidConfigException
     * @throws \api\components\orderApi\exceptions\OrderApiException
     * @throws \api\models\order\exceptions\PromoException
     */
    public function filterOrder($order, $tenantId, $language, $workerFromRedis): array
    {
        $order = Order::filterOrderData($order, $tenantId, $language);
        (new SettingsFilter())->run($order);
        (new ClientNameFilter($workerFromRedis))->run($order);
       (new HospitalAddressFilter())->run($order,$workerFromRedis);
        return $order;
    }


    /**
     * Save address to history
     * @param $orderId
     * @param $tenantId
     * @param array $address
     */
    public function saveAddressToHistory($orderId, $tenantId, array $address): void
    {
        $order = $this->getOrderFromRedis($orderId, $tenantId);

        $service = new AddressHistoryService(new DBAddressHistoryRepository());

        foreach ($address as $item) {
            $clientId = $order['client_id'] ?? 0;
            $cityId = $item['city_id'] ?? 0;

            $city = $item['city'] ?? '';
            $street = $item['street'] ?? '';
            $house = $item['house'] ?? '';
            $lat = $item['lat'] ?? '';
            $lon = $item['lon'] ?? '';

            $service->save(new HistoryAddress(new Client((int)$clientId), new Address(new CityId((int)$cityId),
                (string)$city, (string)$street, (string)$house, (string)$lat, (string)$lon)));
        }
    }


    /**
     * Getting free orders
     * @param $tenantId
     * @param $workerCallsign
     * @param $workerCityId
     * @param $parkingId
     * @return array
     * @throws InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getFreeOrders($tenantId, $workerCallsign, $workerCityId, $parkingId): array
    {
        /**
         * @var $workerService WorkerService
         */
        $workerService = \Yii::$container->get(WorkerService::className());

        $freeOrders = isset($parkingId)
            ? Order::getFreeOrdersAtParking($tenantId, $parkingId)
            : Order::getFreeOrders($tenantId);

        $worker = $workerService->getWorkerFromRedis($tenantId, $workerCallsign);

        $positions = isset($worker['position']['positions']) ? $worker['position']['positions'] : [];
        $classes = isset($worker['position']['classes']) ? $worker['position']['classes'] : [];
        $hasCar = isset($worker['position']['has_car']) ? $worker['position']['has_car'] : null;

        $addOptions = isset($worker['car']['carHasOptions'])
            ? ArrayHelper::getColumn($worker['car']['carHasOptions'], 'option_id') : [];

        $result = [];
        if (!empty($freeOrders) && !(empty($classes) && empty($positions))) {
            foreach ($freeOrders as $order) {
                if ($order['city_id'] == $workerCityId) {
                    $classId = isset($order['tariff']['class']['class_id'])
                        ? $order['tariff']['class']['class_id'] : null;
                    $positionId = isset($order['tariff']['position_id'])
                        ? $order['tariff']['position_id'] : null;

                    if ((!empty($hasCar) && in_array($classId, $classes, false))
                        || (empty($hasCar) && in_array($positionId, $positions, false))
                    ) {
                        $isValidOptions = true;
                        $orderOptions = isset($order['options'])
                            ? ArrayHelper::getColumn($order['options'], 'option_id') : [];
                        foreach ($orderOptions as $option) {
                            if (!in_array($option, $addOptions, false)) {
                                $isValidOptions = false;
                                break;
                            }
                        }

                        $device = ArrayHelper::getValue($order, 'device');
                        // no company ore different company
                        if($device === HospitalAddressFilter::$hospital_type){
                            $isValidOptions = $this->showHospitalOrder($order, $worker, Order::ORDER_FREE );
                        }
                        if ($isValidOptions) {
                            $addressFrom = $order['address'];
                            $addressFrom = unserialize($addressFrom);
                            $addressFrom = isset($addressFrom['A']) ? $addressFrom['A'] : null;
                            $createTime = $order['order_time'];
                            $orderTime = date('d.m.Y H:i:s', $createTime);

                            $address_correct = ($device === HospitalAddressFilter::$hospital_type) ?
                                HospitalAddressFilter::hospitalAddress($order, $addressFrom, $worker) : $addressFrom ;

                            $result[] = [
                                'order_id'    => $order['order_id'],
                                'position_id' => $order['position_id'],
                                'order_from'  => $address_correct,
                                'order_time'  => $orderTime,
                                'payment'     => $order['payment'],
                                'exceptCarModels' => $order['exceptCarModels'],
                            ];
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Getting filtered by distance orders
     *
     * @param int $tenantId
     * @param int $cityId
     * @param float $workerLat
     * @param float $workerLon
     * @param array $orders
     *
     * @return array
     * @throws InvalidConfigException
     */
    public function getFilteredByDistanceOrders($tenantId, $cityId, $workerLat, $workerLon, array $orders): array
    {
        /** @var $geoService GeoService */
        $geoService = \Yii::createObject(GeoService::className());

        return array_values(array_filter($orders, function ($order)
        use ($geoService, $tenantId, $cityId, $workerLat, $workerLon) {
            $positionId = $order['position_id'] ?? null;

            $filterDistance = TenantSetting::getSettingValue(
                $tenantId, TenantSetting::SETTING_DISTANCE_TO_FILTER_FREE_ORDERS, $cityId, $positionId);
            $filterDistanceInKm = empty($filterDistance) ? 0 : $filterDistance / 1000;

            $orderLat = $order['order_from']['lat'];
            $orderLon = $order['order_from']['lon'];
            $distance = $geoService->getDistance(
                (float)$workerLat,
                (float)$workerLon,
                (float)$orderLat,
                (float)$orderLon
            );

            return $filterDistanceInKm >= $distance;
        }));
    }


    public function getFilteredByExceptCarModel(array $worker, array $orders): array
    {
        $workerCarModelId  = ArrayHelper::getValue($worker['car'], 'model_id');

        return array_values(array_filter($orders, function ($order) use ($workerCarModelId) {

            $exceptCarModelIds = ArrayHelper::getValue($order, 'exceptCarModels');
            if (!$workerCarModelId || !$exceptCarModelIds) {
                return true;
            }

            return !in_array($workerCarModelId, $exceptCarModelIds);
        }));
    }

    public function getFilteredOrders(array $orders): array
    {
        $result = [];
        foreach ($orders as $order) {
            $result[] = [
                'order_id'          => $order['order_id'],
                'position_id'       => $order['position_id'],
                'order_from'        => $order['order_from'],
                'order_time'        => $order['order_time'],
                'payment'           => $order['payment'],
                'except_car_models' => $order['exceptCarModels'],
            ];
        }

        return $result;
    }

    public function getFilteredPreOrders(array $preOrders): array
    {
        $result = [];
        foreach ($preOrders as $preOrder) {
            $result[] = [
                'order_id'          => $preOrder['order_id'],
                'order_from'        => $preOrder['order_from'],
                'order_time'        => $preOrder['order_time'],
                'payment'           => $preOrder['payment'],
                'except_car_models' => $preOrder['exceptCarModels'],
            ];
        }

        return $result;
    }

    /**
     * Getting free pre-orders
     * @param $tenantId
     * @param $workerCallsign
     * @param $workerCityId
     * @return array
     * @throws InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getFreePreOrders($tenantId, $workerCallsign, $workerCityId): array
    {
        /**
         * @var $workerService WorkerService
         */
        $workerService = \Yii::$container->get(WorkerService::className());

        $freePreOrders = Order::getFreePredvOrders($tenantId);

        $worker = $workerService->getWorkerFromRedis($tenantId, $workerCallsign);

        $classes = isset($worker['position']['classes']) ? $worker['position']['classes'] : [];
        $positions = isset($worker['position']['positions']) ? $worker['position']['positions'] : [];
        $hasCar = isset($worker['position']['has_car']) ? $worker['position']['has_car'] : null;
        $workerPositionId = isset($worker['position']['position_id']) ? $worker['position']['position_id'] : null;

        $addOptions = isset($worker['car']['carHasOptions'])
            ? ArrayHelper::getColumn($worker['car']['carHasOptions'], 'option_id') : [];
        $timeDiffOfPreOrderVisibility = null;
        $workerCityOffset = City::getTimeOffset($workerCityId);
        $restrictVisibilityOfPreOrder = TenantSetting::getSettingValue($tenantId, TenantSetting::SETTING_RESTRICT_VISIBILITY_OF_PRE_ORDER, $workerCityId, $workerPositionId);
        if ((int)$restrictVisibilityOfPreOrder === 1) {
            $timeOfPreOrderVisibilityInHours = TenantSetting::getSettingValue($tenantId, TenantSetting::SETTING_TIME_OF_PRE_ORDER_VISIBILITY, $workerCityId, $workerPositionId);
            $timeDiffOfPreOrderVisibility = $timeOfPreOrderVisibilityInHours ? $timeOfPreOrderVisibilityInHours * 60 * 60 : null;
        }
        $result = [];
        if (!empty($freePreOrders) && !(empty($classes) && empty($positions))) {
            foreach ($freePreOrders as $preOrder) {
                if ($preOrder['city_id'] == $workerCityId) {
                    if ($timeDiffOfPreOrderVisibility) {
                        $orderTime = $preOrder['order_time'];
                        $nowTime = time() + $workerCityOffset;
                        if ($orderTime - $nowTime > $timeDiffOfPreOrderVisibility) {
                            continue;
                        }
                    }
                    $classId = isset($preOrder['tariff']['class']['class_id'])
                        ? $preOrder['tariff']['class']['class_id'] : null;
                    $positionId = isset($preOrder['tariff']['position_id'])
                        ? $preOrder['tariff']['position_id'] : null;

                    if ((!empty($hasCar) && in_array($classId, $classes, false))
                        || (empty($hasCar) && in_array($positionId, $positions, false))
                    ) {
                        $isValidOptions = true;
                        $orderOptions = isset($preOrder['options'])
                            ? ArrayHelper::getColumn($preOrder['options'], 'option_id') : [];
                        foreach ($orderOptions as $option) {
                            if (!in_array($option, $addOptions, false)) {
                                $isValidOptions = false;
                                break;
                            }
                        }

                        $device = ArrayHelper::getValue($preOrder, 'device');
                        // no company ore different company
                        if($device === HospitalAddressFilter::$hospital_type){
                           $isValidOptions = $this->showHospitalOrder($preOrder, $worker, Order::ORDER_PREE );
                        }

                        if ($isValidOptions) {
                            $addressFrom = $preOrder['address'];
                            $addressFrom = unserialize($addressFrom);
                            $addressFrom = isset($addressFrom['A']) ? $addressFrom['A'] : null;
                            $createTime = $preOrder['order_time'];
                            $orderTime = date('d.m.Y H:i:s', $createTime);

                            $address_correct = ($device === HospitalAddressFilter::$hospital_type) ?
                                HospitalAddressFilter::hospitalAddress($preOrder, $addressFrom, $worker) : $addressFrom ;


                            $result[] = [
                                'order_id'   => $preOrder['order_id'],
                                'order_from' => $addressFrom,
                                'order_time' => $orderTime,
                                'payment'    => $preOrder['payment'],
                                'exceptCarModels' => $preOrder['exceptCarModels'],

                                'order_from' => $address_correct
                            ];
                        }
                    }
                }
            }
        }

        return $result;
    }


    /**
     * Getting worker pre-orders
     *
     * @param int $tenantId
     * @param int $workerCallsign
     * @param int $workerCityId
     *
     * @return array
     */
    public function getWorkerPreOrders($tenantId, $workerCallsign, $workerCityId): array
    {
        $orders = Order::getWorkerPreOrders($tenantId, $workerCallsign);

        $result = [];
        if (is_array($orders)) {
            foreach ($orders as $order) {
                if ($order['city_id'] == $workerCityId) {
                    $addressFrom = unserialize($order['address']);
                    $addressFrom = isset($addressFrom['A']) ? $addressFrom['A'] : null;
                    $createTime = $order['order_time'];
                    $orderTime = date('d.m.Y H:i:s', $createTime);

                    if(!is_null($addressFrom)){
                        $addressFrom = HospitalAddressFilter::correctAddress($addressFrom);
                    }

                    $result[] = [
                        'order_id'   => $order['order_id'],
                        'order_from' => $addressFrom,
                        'order_time' => $orderTime,
                        'payment'    => $order['payment'],
                    ];
                }
            }
        }

        return $result;
    }


    public function getOwnOrdersHospital($tenantId, $workerCallsign): array
    {
        $orders = Order::find()
            ->select('o.*')
            ->alias('o')
            ->leftJoin(
                Worker::tableName() . ' `w`',
                '`w`.`worker_id` = `o`.`worker_id`'
            )
            ->where(
                [
                    'o.tenant_id' => $tenantId,
                    'o.device' => HospitalAddressFilter::$hospital_type,
                    'w.callallsign' => $workerCallsign
                ]
            )
            ->where(['in', 'o.status_id', [
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD,
                OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT
            ]])
            ->all();

        $assignedOrders = [];
        foreach($orders as $order)
        {
            $address = unserialize($order->address);
            $addressFrom = isset($address['A']) ? $address['A'] : null;
            $addressTo = isset($address['B']) ? $address['B'] : null;
            $createTime = $order->order_time;
            $orderTime = date('d.m.Y H:i:s', $createTime);

            if(!is_null($addressFrom)){
                $addressFrom = HospitalAddressFilter::correctAddress($addressFrom);
            }
            if(!is_null($addressTo)){
                $addressTo = HospitalAddressFilter::correctAddress($addressTo);
            }
            $info = [
                'order_id'    => $order->order_id,
                'order_from'  => $addressFrom,
                'order_to'  => $addressTo,
                'order_time'  => $orderTime,
                'payment'     => $order->payment,
                'deny_refuse' => in_array($order->status_id, [
                    OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD,
                    OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD,
                ], false),
                'status_id'   => $order->status_id
            ];
            $assignedOrders[] = $info;
        }
        return [
            'assignedOrders'    => $assignedOrders
        ];

    }


    /**
     * Getting list of own orders
     *
     * @param int $tenantId
     * @param int $workerCallsign
     *
     * @return array
     */
    public function getOwnOrders($tenantId, $workerCallsign): array
    {
        $orders = Order::getOrdersByWorkerFromRedis($tenantId, $workerCallsign);

        $activeOrders = [];
        $assignedOrders = [];
        $assignedPreOrders = [];

        if (is_array($orders)) {
            foreach ($orders as $order) {
                $address = unserialize($order['address']);
                $addressFrom = isset($address['A']) ? $address['A'] : null;
                $addressTo = isset($address['B']) ? $address['B'] : null;
                $createTime = $order['order_time'];
                $orderTime = date('d.m.Y H:i:s', $createTime);

                if(!is_null($addressFrom)){
                    $addressFrom = HospitalAddressFilter::correctAddress($addressFrom);
                }
                if(!is_null($addressTo)){
                    $addressTo = HospitalAddressFilter::correctAddress($addressTo);
                }
                $info = [
                    'order_id'    => $order['order_id'],
                    'order_from'  => $addressFrom,
                    'order_to'  => $addressTo,
                    'order_time'  => $orderTime,
                    'payment'     => $order['payment'],
                    'deny_refuse' => in_array($order['status_id'], [
                        OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD,
                        OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD,
                    ], false),
                    'status_id'   => $order['status_id']
                ];

                switch ($order['status_id']) {
                    case OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD:
                    case OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT:
                    case OrderStatus::STATUS_PRE_WORKER_ACCEPT_ORDER:
                    case OrderStatus::STATUS_WAITING_FOR_PRE_ORDER_CONFIRMATION:
                    case OrderStatus::STATUS_WORKER_IGNORED_PRE_ORDER_CONFIRMATION:
                    case OrderStatus::STATUS_WORKER_REFUSED_PRE_ORDER_CONFIRMATION:
                    case OrderStatus::STATUS_WORKER_ACCEPTED_PRE_ORDER_CONFIRMATION:
                        $assignedPreOrders[] = $info;
                        break;
                    case OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD:
                    case OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT:
                        $assignedOrders[] = $info;
                        break;
                    default:
                        $activeOrders[] = $info;
                }
            }
        }

        return [
            'activeOrders'      => $activeOrders,
            'assignedOrders'    => $assignedOrders,
            'assignedPreOrders' => $assignedPreOrders,
        ];
    }


    /**
     * Set raw order calc
     *
     * @param int $orderId
     * @param string $orderCalcData
     *
     * @return bool
     */
    public function setRawOrderCalc($orderId, $orderCalcData): bool
    {
        $rawOrderCalc = RawOrderCalc::findOne(['order_id' => $orderId]);

        if (!empty($rawOrderCalc)) {
            return true;
        }

        $rawOrderCalc = new RawOrderCalc();
        $rawOrderCalc->order_id = $orderId;
        $rawOrderCalc->raw_cacl_data = $orderCalcData;

        $result = $rawOrderCalc->save();
        if (!$result) {
            \Yii::error("Error save Set_order_raw_calc . Order id = " . $orderId . " . Error:");
            \Yii::error($rawOrderCalc->errors);
        }

        return $result;
    }


    /**
     * Getting order route
     *
     * @param int $orderId
     *
     * @return array
     * @throws InvalidConfigException
     */
    public function getOrderRoute($orderId): array
    {
        /* @var $service OrderTrackService */
        $service = \Yii::createObject(OrderTrackService::class);

        return $service->getTrack($orderId);
    }


    /**
     * Set order route
     *
     * @param int $orderId
     * @param string $orderRoute
     *
     * @return bool
     * @throws InvalidConfigException
     */
    public function setOrderRoute($orderId, $orderRoute): bool
    {
        $data = [
            'orderId'    => $orderId,
            'orderTrack' => $orderRoute,
        ];

        $result = false;
        try {
            \Yii::$app->gearman->doBackground(Gearman::ORDER_TRACK_TASK, $data);
            $result = true;
        } catch (\Exception $ex) {
            \Yii::error("Error save Set_order_route . Order id = " . $orderId . " . Error:");
            \Yii::error($ex->getMessage());
        }

        return $result;
    }


    /**
     * Getting result of order update event
     *
     * @param string $requestId Uuid4
     *
     * @return string
     * @throws InvalidConfigException
     */
    public function getOrderUpdateEventResult($requestId): ?string
    {
        /* @var $event OrderUpdateEvent */
        $event = \Yii::createObject(OrderUpdateEvent::className());
        $response = $event->getResponse($requestId);

        return empty($response) ? null : $response;
    }


    /**
     * Show order to worker
     *
     * @param array $preOrder
     *
     * @param array  $$worker
     *
     * @return boolean
     */
    public function showHospitalOrder($preOrder = [],$worker= [], $type = 0 ){

        $device = ArrayHelper::getValue($preOrder, 'device');
        $order_company = ArrayHelper::getValue($preOrder, 'worker_company.tenant_company_id');
        $worker_company = ArrayHelper::getValue($worker, 'worker.tenant_company.company_id') ;

        $isValidOptions = true;
        // no company ore different company - for preorders
        // pre
        if(Order::ORDER_PREE == $type && $device === HospitalAddressFilter::$hospital_type){
            if(is_null($order_company) ||
                ($order_company != $worker_company && !is_null($order_company))){
                $isValidOptions = false;
            }
        }
        elseif(Order::ORDER_FREE == $type && $device === HospitalAddressFilter::$hospital_type){
            if($order_company != $worker_company && !is_null($order_company)){
                $isValidOptions = false;
            }
        }

        return $isValidOptions;
    }
}