<?php

namespace v3\components\repositories;


use app\models\tenant\TenantCityHasPosition;
use app\models\worker\Position;
use yii\helpers\ArrayHelper;

class PositionRepository
{

    public static function isActivePositionInCity($tenantId, $cityId, $positionId)
    {
        return TenantCityHasPosition::find()
            ->where([
                'tenant_id' => $tenantId,
                'city_id' => $cityId,
                'position_id' => $positionId
            ])
            ->exists();
    }

    public static function getPositionsHasCarClass()
    {
        return ArrayHelper::getColumn(Position::find()
                ->where(['not', ['transport_type_id' => null]])
                ->all(), 'transport_type_id');
    }
}