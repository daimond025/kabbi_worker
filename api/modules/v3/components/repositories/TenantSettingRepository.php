<?php

namespace v3\components\repositories;


use app\models\tenant\TenantSetting;

class TenantSettingRepository
{

    public static function isSettingAllowed($tenantId, $nameSetting, $cityId, $positionId)
    {
        return TenantSetting::find()
            ->where([
                'tenant_id'   => $tenantId,
                'name'        => $nameSetting,
                'city_id'     => $cityId,
                'position_id' => $positionId,
            ])
            ->andWhere(['value' => 1])
            ->exists();
    }


    public static function getValueSettingAtTenant($tenantId, $nameSetting, $cityId = null, $positionId = null)
    {
        return TenantSetting::find()
            ->select('value')
            ->where([
                'tenant_id' => $tenantId,
                'name'      => $nameSetting,
            ])
            ->andFilterWhere([
                'city_id'     => $cityId,
                'position_id' => $positionId,
            ])
            ->scalar();
    }
}