<?php

namespace v3\components\repositories;


use app\models\tenant\TenantHasCity;

class CityRepository
{
    public static function isBlockedCityAtTenant($cityId, $tenantId)
    {
        return TenantHasCity::find()
            ->where([
                'tenant_id' => $tenantId,
                'city_id' => $cityId
            ])
            ->andWhere(['block' => 1])
            ->exists();
    }
}