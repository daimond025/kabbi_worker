<?php

namespace v3\components\repositories;


use app\models\promo\PromoCode;

class PromoRepository
{

    public function getPromoCodeWorker($tenantId, $workerId, $positionId, $carClassId = null) : ?PromoCode
    {
        return PromoCode::find()->alias('pc')
            ->joinWith('promo p', false)
            ->where([
                'p.tenant_id'     => $tenantId,
                'p.position_id'   => $positionId,
                'p.blocked'       => 0,
                'pc.worker_id'    => $workerId,
            ])
            ->andFilterWhere(['pc.car_class_id' => $carClassId])
            ->one();
    }

}