<?php

namespace v3\components\repositories;


use app\models\worker\Worker;

class WorkerRepository
{
    public static function isBlockedWorker($tenantId, $workerCallsign)
    {
        return Worker::find()
            ->where([
                'tenant_id' => $tenantId,
                'callsign' => $workerCallsign
            ])
            ->andWhere(['block' => 1])
            ->exists();
    }


}