<?php
namespace v3\components\repositories;


use app\models\worker\WorkerTariff;

class WorkerActiveAbonimentRepository
{


    public function deleteOnceWithNewPeriod($workerId)
    {
        try {

            $sql = <<<SQL
DELETE FROM `tbl_worker_active_aboniment` 
WHERE `aboniment_id` IN (
    SELECT * FROM (
        SELECT `aboniment_id` 
        FROM `tbl_worker_active_aboniment` `waa` 
        LEFT JOIN `tbl_worker_tariff` `t` ON `waa`.`tariff_id` = `t`.`tariff_id` 
        WHERE (`t`.`type`='%s') 
            AND (`t`.`action_new_shift`='%s') 
            AND (`waa`.`worker_id`='%d')
    ) AS t
)
SQL;

            \Yii::$app->db
                ->createCommand(
                    sprintf(
                        $sql,
                        WorkerTariff::TYPE_ONCE,
                        WorkerTariff::ACTION_NEW_SHIFT_NEW_PERIOD,
                        $workerId
                    )
                )
                ->execute();

        } catch (\Exception $ex) {
            \Yii::error("Delete once with new period error: error={$ex->getMessage()}", 'worker-tariff-service');
        }
    }
}