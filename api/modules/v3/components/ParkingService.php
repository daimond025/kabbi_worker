<?php

namespace v3\components;

use app\models\parking\Parking;
use yii\base\Object;

/**
 * Class ParkingService
 * @package v3\components
 */
class ParkingService extends Object
{
    /**
     * Getting list of parking
     *
     * @param int $tenantId
     * @param int $cityId
     * @param int $parkingId
     *
     * @return array
     */
    public function getParkingList($tenantId, $cityId, $parkingId = null)
    {
        if (isset($parkingId)) {
            $parking  = Parking::findone($parkingId);
            $parkingList = empty($parking) ? null : [$parking];
        } else {
            $parkingList = Parking::getParkingArrAll($tenantId, $cityId);
        }

        $result = [];
        if (is_array($parkingList)) {
            foreach ($parkingList as $parking) {
                $result[] = [
                    'parking_id'                 => $parking['parking_id'],
                    'parking_name'               => $parking['name'],
                    'parking_polygone'           => json_decode($parking['polygon']),
                    'parking_type'               => $parking['type'],
                    'in_district'                => empty($parking['in_district']) ? 0 : $parking['in_district'],
                    'out_district'               => empty($parking['out_district']) ? 0 : $parking['out_district'],
                    'in_distr_coefficient_type'  => empty($parking['in_distr_coefficient_type']) ? null : $parking['in_distr_coefficient_type'],
                    'out_distr_coefficient_type' => empty($parking['out_distr_coefficient_type']) ? null : $parking['out_distr_coefficient_type'],
                ];
            }
        }

        return $result;
    }
}