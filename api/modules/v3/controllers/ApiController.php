<?php

namespace v3\controllers;

use api\components\orderApi\OrderApi;
use api\models\balance\WorkerTransaction;
use app\components\taxiPushComponent\TaxiPusherComponent;
use app\models\balance\Currency;
use app\models\order\OrderUpdateEvent;
use app\models\settings\DefaultSettings;
use app\models\worker\CarMileage;
use app\models\worker\Position;
use app\models\worker\Worker;
use app\models\worker\WorkerBlock;
use app\models\worker\WorkerHasPosition;
use app\models\worker\WorkerShift;
use bankCard\Card;
use api\components\curl\exceptions\RestException;
use Composer\Semver\Comparator;
use GuzzleHttp\Exception\ClientException;
use logger\ApiLogger;
use paymentGate\ProfileService;
use Ramsey\Uuid\Uuid;
use v3\components\CityService;
use v3\components\OrderService;
use v3\components\ParkingService;
use v3\components\PositionService;
use v3\components\processingErrors\ProcessingErrorsApiOrder;
use v3\components\services\access\AccessException;
use v3\components\services\access\worker\AccessWorkerService;
use v3\components\services\auth\SignCompany;
use v3\components\SettingService;
use v3\components\taxiApiRequest\TaxiApiRequest;
use v3\components\taxiApiRequest\TaxiErrorCode;
use v3\components\TenantService;
use v3\components\TimeHelper;
use v3\components\WorkerService;
use v3\components\WorkerShiftService;
use v3\components\WorkerTariffService;
use v3\exceptions\CityBlockedException;
use v3\exceptions\ExceptionConfidentialPage;
use v3\exceptions\ShiftIsClosedException;
use Yii;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\Module;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\tariff\TaxiTariff;
use app\models\client\Client;
use app\models\client\ClientPhone;
use app\models\address\City;
use app\models\order\Order;
use app\models\balance\Account;
use app\components\serviceEngine\ServiceEngine;
use app\models\parking\Parking;
use app\models\tenant\TenantSetting;
use app\models\tenant\Tenant;
use app\models\car\Car;
use app\models\order\OrderStatus;
use app\models\order\OrderChangeData;
use app\models\order\OrderDetailCost;
use app\models\order\RawOrderCalc;
use app\models\balance\Transaction;

/**
 * Class ApiController
 * @package v3\controllers
 */
class ApiController extends Controller
{
    const DEFAULT_VERSION = '3.0.0';

    public $headers;

    /**
     * @var PositionService
     */
    protected $positionService;

    /**
     * @var WorkerService
     */
    protected $workerService;

    /**
     * @var WorkerTariffService
     */
    protected $workerTariffService;

    /**
     * @var AccessWorkerService
     */
    protected $accessWorkerService;

    /**
     * @var ProfileService
     */
    private $profileService;

    /**
     * @var ApiLogger
     */
    protected $apiLogger;

    public function __construct(
        string $id,
        Module $module,
        AccessWorkerService $accessWorkerService,
        ProfileService $profileService,
        array $config = []
    )
    {
        $this->accessWorkerService = $accessWorkerService;
        $this->profileService = $profileService;

        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'auth_worker'         => ['get'],
                    'set_worker_position' => ['post'],

                    'get_worker_cities'    => ['get'],
                    'get_worker_positions' => ['get'],
                    'get_worker_profile'   => ['get'],
                    'get_worker_balance'   => ['get'],

                    'worker_start_work'          => ['post'],
                    'worker_end_work'            => ['post'],
                    'set_car_mileage'            => ['post'],
                    'get_parkings_list'          => ['get'],
                    'get_city_polygone'          => ['get'],
                    'set_parking_queue'          => ['post'],
                    'get_parkings_orders_queues' => ['get'],
                    'get_orders_open_list'       => ['get'],
                    'get_orders_pretime_list'    => ['get'],
                    'get_own_orders'             => ['get'],
                    'get_order_for_id'           => ['get'],
                    'set_order_status'           => ['post'],
                    'set_order_route'            => ['post'],
                    'set_order_route_file'       => ['post'],
                    'check_worker_on_work'       => ['get'],
                    'set_signal_sos'             => ['post'],
                    'unset_signal_sos'           => ['post'],
                    'get_city_list'              => ['get'],
                    'get_geoobjects_list'        => ['get'],
                    'set_worker_state'           => ['post'],
                    'get_client_tariff'          => ['get'],
                    'call_cost'                  => ['post'],
                    'create_order'               => ['post'],
                    'get_order_raw_calc'         => ['get'],
                    'set_order_raw_calc'         => ['post'],
                    'worker_order_unblock'       => ['post'],
                    'worker_order_block'         => ['post'],
                    'worker_pre_order_block'     => ['post'],
                    'worker_pre_order_unblock'   => ['post'],

                    'get_bank_cards'          => ['get'],
                    'create_bank_card'        => ['post'],
                    'check_bank_card'         => ['post'],
                    'delete_bank_card'        => ['post'],
                    'refill_account'          => ['post'],
                    'get_request_result'      => ['get'],
                    'get_confidential_page'   => ['get'],
                    'update_order_cost'       => ['post'],
                    'get_additional_options' => ['get'],

                    'get_order_detail_cost'  => ['get'],

                    'worker_statistic' => ['post'],
                    'worker_report' => ['post'],

                    'clear_information_after_worker_delete' => ['post'],


                    'get_own_orders_hospital'  => ['get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->positionService     = $this->module->positionService;
        $this->workerService       = $this->module->workerService;
        $this->workerService->lang = $this->getLang();
        $this->workerTariffService = $this->module->workerTariffService;
        $this->apiLogger           = \Yii::$app->get('apiLogger');

        parent::init();
    }

    /**
     * Getting worker service
     * @return WorkerService
     */
    private function getWorkerService()
    {
        return \Yii::$container->get('workerService');
    }

    /**
     * Getting worker tariff service
     * @return WorkerTariffService
     */
    private function getWorkerTariffService()
    {
        return \Yii::$container->get('workerTariffService');
    }

    /**
     * Getting worker shift service
     * @return WorkerShiftService
     */
    private function getWorkerShiftService()
    {
        return \Yii::$container->get('workerShiftService');
    }

    /**
     * Getting parking service
     * @return ParkingService
     */
    private function getParkingService()
    {
        return \Yii::$container->get('parkingService');
    }

    /**
     * Getting city service
     * @return CityService
     */
    private function getCityService()
    {
        return \Yii::$container->get('cityService');
    }

    /**
     * Getting order service
     * @return OrderService
     */
    private function getOrderService()
    {
        return \Yii::$container->get('orderService');
    }

    /**
     * Getting tenant service
     * @return TenantService
     */
    private function getTenantService()
    {
        return \Yii::$container->get('tenantService');
    }

    /**
     * Getting time helper
     * @return TimeHelper
     */
    private function getTimeHelper()
    {
        return \Yii::$container->get('timeHelper');
    }


    protected function dump($var, $depth = 10, $highlight = true)
    {
        return VarDumper::dump($var, $depth, $highlight);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Error action
     * @return array
     */
    public function actionError()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result                      = [
            "code" => "404",
            "info" => "METHOD_NOT_FOUND",
        ];

        return $result;
    }
    ///////////////////////////////МЕТОДЫ АПИ//////////////////////////////////////////////////////


    /**
     * API method static current worker current time
     * @return string JSON
     */
    public function actionWorker_statistic()
    {
        return $this->Worker_statistic($_POST);

    }
    public function Worker_statistic($params){
        $params = $this->getParams($params);

        $request = new TaxiApiRequest();

        $validateData = $this->checkParams($params, false);
        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = mb_strtolower($params['tenant_login']);
        $workerCallsign = (int)$params['worker_login'];

        $start_time = is_int(strtotime($params['start_date'])) ?  strtotime($params['start_date']) : time();
        $end_time = is_int(strtotime($params['end_date'])) ?  strtotime($params['end_date']) : time();


        $worker = $this->workerService->getWorker($tenantId, $workerCallsign);

        if (empty($worker)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_NOT_EXIST));
        }

        $result['data'] = [
            'earnings' => 0,
            'count_orders'      =>0,
            'working_hours'      => 0,
        ];
        try{
            $worker_id = $worker['worker_id'];
            $worker_statistics  =   $this->workerService->getStaticOfWorker($worker_id, $start_time, $end_time);

            $result['data'] = [
                'earnings' => $worker_statistics[0],
                'count_orders'      =>$worker_statistics[1],
                'working_hours'      => $worker_statistics[2],
            ];
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), $result);
        }catch (exception $e){
            return $request->getJsonAnswer([
                'code' => TaxiErrorCode::BAD_PARAM,
                'info' => TaxiErrorCode::ERROR_MESSAGES[TaxiErrorCode::BAD_PARAM] . ' worker_statistic',
            ]);
        }

    }


    public function actionWorker_report(){
        return $this->Worker_report($_POST);

    }
    public function Worker_report($params){
        $params = $this->getParams($params);

        $request = new TaxiApiRequest();

        $validateData = $this->checkParams($params, false);
        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = mb_strtolower($params['tenant_login']);
        $workerCallsign = (int)$params['worker_login'];

        $start_time = is_int(strtotime($params['start_date'])) ?  strtotime($params['start_date']) : time();
        $end_time = is_int(strtotime($params['end_date'])) ?  strtotime($params['end_date']) : time();
        $worker = $this->workerService->getWorker($tenantId, $workerCallsign);
        $page = (isset($params['page'])) ? $params['page'] : 0;


        if (empty($worker)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_NOT_EXIST));
        }

        try{
            $worker_id = $worker['worker_id'];
            $result  =   $this->workerService->getWorker_report($worker_id, $start_time, $end_time,$page);

            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), $result);
        }catch (exception $e){
            return $request->getJsonAnswer([
                'code' => TaxiErrorCode::BAD_PARAM,
                'info' => TaxiErrorCode::ERROR_MESSAGES[TaxiErrorCode::BAD_PARAM] . ' worker_report',
            ]);
        }
    }


    /**
     * API method for worker authorization
     * @return string JSON
     */
    public function actionAuth_worker()
    {
        return $this->authWorker($_GET);
    }

    /**
     * Worker authorization
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function authWorker($params)
    {
        $params = $this->getParams($params);

        // Не включать проверку сигнатуру в эттром методе, идет получение ключа
        $validateData = $this->checkParams($params, false);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $workerPassword = $params['worker_password'];
        $deviceToken    = $params['device_token'];
        $workerDevice   = $this->getWorkerDevice();

        if ($params['worker_login'] !== (string)$workerCallsign) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_NOT_EXIST));
        }

        $workerService = $this->getWorkerService();

        $worker = $workerService->getWorker($tenantId, $workerCallsign);

        try {
            $this->accessWorkerService->isAccessAuth($worker);
        } catch (AccessException $e) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData($e->getMessage()));
        }

        if (Yii::$app->security->validatePassword($workerPassword, $worker['password'])) {
            $workerId = $worker['worker_id'];
            $workerService->saveWorkerDeviceInfo($workerId, $workerDevice, $deviceToken);

            $signCompany = new SignCompany($worker);

            $result = [
                'auth_result'       => 1,
                'worker_secret_key' => TaxiApiRequest::getTenantApiKey($tenantLogin),
                'company_name'      => $signCompany->getCompanyName(),
                'company_logo'      => $signCompany->getCompanyLogo($tenantLogin),
            ];

            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), $result);
        }

        // Если неверный пароль
        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::ACCESS_DENIED));
    }


    /**
     * API method for setup worker position
     * @return string JSON
     */
    public function actionSet_worker_position()
    {
        return $this->setWorkerPosition($_POST);
    }

    /**
     * Set worker position
     *
     * @param $params
     *
     * @return string JSON
     */
    public function setWorkerPosition($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);
        $request      = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];

        $workerLat    = isset($params['worker_lat']) ? $params['worker_lat'] : null;
        $workerLon    = isset($params['worker_lon']) ? $params['worker_lon'] : null;
        $workerDegree = isset($params['worker_degree']) ? $params['worker_degree'] : null;
        $workerSpeed  = isset($params['worker_speed']) ? $params['worker_speed'] : null;
        $updateTime   = isset($params['update_time']) ? $params['update_time'] : null;
        $accuracy     = isset($params['coordinate_accuracy']) ? $params['coordinate_accuracy'] : null;

        $workerService = $this->getWorkerService();

        $worker = $workerService->getWorkerFromRedis($tenantId, $workerCallsign);
        if (empty($worker)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
        }

        $workerStatus = isset($worker['worker']['status']) ? $worker['worker']['status'] : null;

        $geo                = $worker['geo'];
        $geo['lat']         = $workerLat;
        $geo['lon']         = $workerLon;
        $geo['degree']      = $workerDegree;
        $geo['speed']       = $workerSpeed;
        $geo['update_time'] = $updateTime;
        $geo['accuracy']    = $accuracy;

        $worker = array_replace($worker, ['geo' => $geo]);

        $cityService = $this->getCityService();
        $cityId      = isset($worker['worker']['city_id']) ? $worker['worker']['city_id'] : null;
        $serverTime  = time();
        $cityTime    = $serverTime + $cityService->getCachedTimeOffset($cityId);

        if ($workerService->saveWorkerToRedis($tenantId, $workerCallsign, $worker)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'result'        => 1,
                'worker_status' => (string)$workerStatus,
                'server_time'   => $serverTime,
                'city_time'     => $cityTime,
            ]);
        }

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR));
    }


    /**
     * API method for getting worker cities
     * @return string JSON
     */
    public function actionGet_worker_cities()
    {
        return $this->getWorkerCities($_GET);
    }

    /**
     * Getting worker cities
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function getWorkerCities($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $workerCallsign = (int)$params['worker_login'];

        $worker = $this->workerService->getWorker($tenantId, $workerCallsign);

        if (empty($worker)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_NOT_EXIST));
        }

        $cities     = $this->workerService->getWorkerCities($tenantId, $worker['worker_id']);
        $lastCityId = $this->workerService->getLastActiveCityId($worker['worker_id']);

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::OK),
            [
                'cities'       => $cities,
                'last_city_id' => $lastCityId,
            ]);
    }


    /**
     * API method for getting worker positions
     * @return string JSON
     */
    public function actionGet_worker_positions()
    {
        return $this->getWorkerPositions($_GET);
    }

    /**
     * Getting worker positions
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function getWorkerPositions($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $workerCallsign = (int)$params['worker_login'];
        $cityId         = (int)$params['worker_city_id'];

        $worker = $this->workerService->getWorker($tenantId, $workerCallsign);

        if (empty($worker)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_NOT_EXIST));
        }

        $positions = $this->workerService->getWorkerPositions($tenantId, $worker['worker_id'], $cityId);

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::OK), ['positions' => $positions]);
    }


    /**
     * API method for getting worker profile
     * @return string JSON
     */
    public function actionGet_worker_profile()
    {
        return $this->getWorkerProfile($_GET);
    }

    /**
     * Getting worker profile
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function getWorkerProfile($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $workerCallsign = (int)$params['worker_login'];
        $workerCityId   = isset($params['worker_city_id']) ? (int)$params['worker_city_id'] : null;
        $positionId     = isset($params['position_id']) ? (int)$params['position_id'] : Position::POSITION_ID_DRIVER;

        $currentTime = time() + City::getTimeOffset($workerCityId);

        $workerService = $this->getWorkerService();
        try {
            $workerProfile = $workerService->getWorkerProfile($tenantId, $workerCallsign, $workerCityId, $positionId,
                $currentTime);
        } catch (CityBlockedException $ex) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::CITY_BLOCKED));
        }

        if (!$this->isCurrentVersionGreaterThanOrEqualTo('3.1.0')) {
            unset(
                $workerProfile['allow_work_without_gps'],
                $workerProfile['allow_refill_balance_by_bank_card'],
                $workerProfile['allow_cancel_order'],
                $workerProfile['allow_cancel_order_after_time'],
                $workerProfile['show_chat_with_dispatcher_before_start_shift'],
                $workerProfile['show_chat_with_dispatcher_on_shift'],
                $workerProfile['show_general_chat_on_shift']
            );
        }

        if (!$this->isCurrentVersionGreaterThanOrEqualTo('3.4.0')) {
            unset($workerProfile['start_time_by_order']);
        }

        if (!$this->isCurrentVersionGreaterThanOrEqualTo('3.9.0')) {
            unset($workerProfile['worker_arrival_time']);
        } elseif (!$this->isCurrentVersionGreaterThanOrEqualTo('3.14.0')) {
            unset(
                $workerProfile['show_estimation'],
                $workerProfile['allow_edit_order'],
                $workerProfile['show_urgent_order_time'],
                $workerProfile['show_worker_address']
            );
        } elseif (!$this->isCurrentVersionGreaterThanOrEqualTo('3.28.0')) {
            unset(
                $workerProfile['allow_worker_to_create_order'],
                $workerProfile['deny_fakegps'],
                $workerProfile['deny_setting_arrival_status_early'],
                $workerProfile['min_distance_to_set_arrival_status']
            );
        }

        if (empty($workerProfile)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_NOT_EXIST));
        } else {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), $workerProfile);
        }
    }


    /**
     * API method for getting worker balance
     * @return string JSON
     */
    public function actionGet_worker_balance()
    {
        return $this->getWorkerBalance($_GET);
    }

    /**
     * Получение html страницы о политике конфиденцальности заказчика
     *
     * Тип запроса: GET
     *
     * @params string current_time  - текущее время timestamp*.
     *
     * @return string HTML
     */
    public function actionGet_confidential_page()
    {
        $params       = $this->getParams($_GET);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);
        $request      = new TaxiApiRequest();

        if ($validateData['code'] !== TaxiErrorCode::OK) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $workerCallsign = (int)$params['worker_login'];
        $workerCityId   = (int)$params['worker_city_id'];
        $positionId     = (int)$params['position_id'];

        try {
            $confidentialPage = (new SettingService())
                ->getConfidentialPage($tenantId, $workerCallsign, $workerCityId, $positionId);

            $validateData['code'] = TaxiErrorCode::OK;
            $validateData['info'] = 'OK';
            $result               = ['result' => (string)$confidentialPage];

        } catch (ExceptionConfidentialPage $e) {

            $validateData['code'] = $e->getMessage();
            $validateData['info'] = TaxiErrorCode::ERROR_MESSAGES[$e->getMessage()];
            $result               = ['result' => ''];
        }

        return $request->getJsonAnswer($validateData, $result);

    }

    /**
     * Getting worker balance
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function getWorkerBalance($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $workerCallsign = (int)$params['worker_login'];
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCityId   = $params['worker_city_id'];

        $workerService = $this->getWorkerService();

        $worker = $workerService->getWorker($tenantId, $workerCallsign);

        if (empty($worker)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_NOT_EXIST));
        }

        $workerId = $worker['worker_id'];

        if ($workerCityId === null && !$this->isCurrentVersionGreaterThanOrEqualTo('3.15.0')) {
            $lastActiveCityId = $workerService->getLastActiveCityId($workerId);
            $workerCityId     = isset($worker['city_id']) ? $worker['city_id'] : $lastActiveCityId;
        }

        $currency   = TenantSetting::getTenantCurrencyCode($tenantId, $workerCityId);
        $currencyId = TenantSetting::getTenantCurrencyId($tenantId, $workerCityId);

        $account = Account::findOne([
            'owner_id'    => $workerId,
            'tenant_id'   => $tenantId,
            'acc_kind_id' => Account::WORKER_KIND,
            'currency_id' => $currencyId,
        ]);

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
            'worker_balance' => empty($account) ? 0 : $account->getFormattedBalance(),
            'currency'       => empty($currency) ? 'RUB' : $currency,
        ]);
    }


    /**
     * API method for start worker shift
     */
    public function actionWorker_start_work()
    {
        return $this->workerStartWork($_POST);
    }

    /**
     * Start worker shift
     *
     * @param array $params
     *
     * @return string JSON
     * @throws \yii\db\Exception
     * @throws \yii\di\NotInstantiableException
     * @throws InvalidConfigException
     * @throws \v3\exceptions\ShiftIsClosedException
     */
    public function workerStartWork($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();


        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        } else {
            $lang     = $this->getLang();
            $tenantId = $this->getTenantId($params);

            $workerCallsign = (int)$params['worker_login'];
            $tenantLogin    = $params['tenant_login'];
            $tenantLogin    = mb_strtolower($tenantLogin);
            $workerTariffId = $params['worker_tariff_id'];
            $workerCarId    = $params['worker_car_id'];
            $workerCityId   = $params['worker_city_id'];
            $deviceToken    = isset($params['device_token']) ? $params['device_token'] : null;
            $workerDevice   = $this->getWorkerDevice();

            // мое исправление
            //$params['app_version'] = 89;

            if (isset($params['app_version'])) {
                if (!$this->workerService->isValidAppVersion($params['app_version'], $workerDevice, $tenantId)) {
                    return $request->getJsonAnswer(
                        TaxiErrorCode::getErrorData(TaxiErrorCode::OLD_APP_VERSION));
                }
            }

            $workerData = $this->workerService->getWorker($tenantId, $workerCallsign);

            if (empty($workerData)) {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_NOT_EXIST));
            }

            if ($this->workerService->getWorkerFromRedis($tenantId, $workerCallsign)) {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_ALLREADY_ON_SHIFT));
            }


            $tenantService = $this->getTenantService();
            if ($tenantService->isCityBlocked($tenantId, $workerCityId)) {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::CITY_BLOCKED));
            }

            if ($this->workerService->isExceededOnlineWorkersLimit($tenantId)) {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_LIMIT));
            }


            $time     = time() + City::getTimeOffset($workerCityId);
            $workerId = $workerData['worker_id'];

            $workerTariff = $this->workerTariffService->getWorkerTariff($workerTariffId, $workerCityId);
            if (empty($workerTariff)) {
                $this->apiLogger->log("Worker tariff not found: tariffId={$workerTariffId}, cityId={$workerCityId}");

                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::BAD_TARIFF));
            }


            $periodType = isset($workerTariff['period_type']) ? $workerTariff['period_type'] : null;
            $period     = isset($workerTariff['period']) ? $workerTariff['period'] : null;
            if (!$this->workerTariffService->isValidPeriodTime($periodType, $period, $time)) {
                $cityTime = date('d.m.Y H:i:s', $time);
                $this->apiLogger->log("Invalid time for tariff period: periodType={$periodType}, period={$period}, time={$cityTime}");

                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::BAD_TARIFF));
            }

            $positionId     = $workerTariff['position_id'];
            $position       = $this->positionService->getPosition($positionId);
            $positionHasCar = $position['has_car'] == 1;

            if (!$this->workerService->isValidWorkerPosition($workerId, $positionId)) {
                $this->apiLogger->log('Invalid worker profession');

                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::BAD_TARIFF));
            }


            $carMileage = null;
            if ($positionHasCar) {
                if (!$this->workerService->isValidWorkerCar($workerId, $positionId, $workerCarId)) {
                    $this->apiLogger->log('Invalid worker car');

                    return $request->getJsonAnswer(
                        TaxiErrorCode::getErrorData(TaxiErrorCode::BAD_TARIFF));
                }

                $car                  = Car::findOne($workerCarId);
                $controlOwnCarMileage = TenantSetting::getSettingValue(
                    $tenantId, TenantSetting::SETTING_CONTROL_OWN_CAR_MILEAGE, $workerCityId, $positionId);

                if ((int)$controlOwnCarMileage === 1 && $car->isCompanyCar()
                    && $this->isCurrentVersionGreaterThanOrEqualTo('3.32.0')) {
                    $carMileage     = $params['car_mileage'] ?? null;
                    $currentMileage = CarMileage::getCurrentMileage((int)$workerCarId);

                    if ($carMileage === null || $currentMileage > $carMileage) {
                        $this->apiLogger->log('Invalid car mileage');

                        return $request->getJsonAnswer(
                            TaxiErrorCode::getErrorData(TaxiErrorCode::INVALID_CAR_MILEAGE),
                            ['current_mileage' => $currentMileage,]);
                    }
                }
            }


            if ($workerData['block'] == 1
                || $this->workerService->isWorkerBlocked(
                    $workerId, $positionId, WorkerBlock::TYPE_BLOCK_ORDER)
            ) {
                $result = ['set_result' => 0];

                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_BLOCKED), $result);
            }

            $hasActiveAboniment = $this->workerTariffService->hasActiveAboniment($workerId, $workerTariffId, $time);
            if (!$hasActiveAboniment) {
                if ($positionHasCar) {
                    if (!$this->workerTariffService->isValidCarForTariff($workerTariffId, $workerCarId, $workerCityId)) {
                        $this->apiLogger->log('Invalid car for this tariff');

                        return $request->getJsonAnswer(
                            TaxiErrorCode::getErrorData(TaxiErrorCode::INCORRECTLY_CAR_TARIF));
                    }
                } else {
                    if (!$this->workerTariffService->isValidPositionForTariff($workerId, $workerTariffId,
                        $positionId)
                    ) {
                        $this->apiLogger->log('Invalid position for this tariff');

                        return $request->getJsonAnswer(
                            TaxiErrorCode::getErrorData(TaxiErrorCode::BAD_TARIFF));
                    }
                }
            }


            $tariffOptionData = $this->workerTariffService->getTariffOptionData($workerTariffId, $time);

            if (empty($tariffOptionData)) {
                $this->apiLogger->log('Tariff option not found by getting params');

                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::BAD_TARIFF));
            }

            $baseCost = empty($workerTariff['cost']) ? 0 : $workerTariff['cost'];

            $discountForCarOptions = 0;
            if ($positionHasCar) {
                $discountForCarOptions = $this->workerTariffService->getDiscountForCarOptions(
                    $tenantId, $workerCityId, $tariffOptionData['option_id'], $workerCarId, $baseCost);
            }

            if ($hasActiveAboniment) {
                $tariffCost = 0;
            } else {
                $tariffCost = $baseCost - $discountForCarOptions;
            }

            if (!$this->workerService->isGoodWorkerBalance(
                $tenantId, $workerId, $workerCityId, $positionId, $tariffCost)
            ) {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::BAD_BALANCE));
            }


            if (!empty($workerCarId) && Car::isCarBusy($tenantId, $workerCarId)) {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::CAR_IS_BUSY));
            }

            $this->workerService->saveWorkerDeviceInfo($workerId, $workerDevice, $deviceToken, $lang);

            if ($tariffCost > 0) {
                $tariff = isset($workerTariff['name']) ? $workerTariff['name'] : null;

                $requestId    = \Yii::$app->request->headers->get('Request-Id') ?: Uuid::uuid4();
                $responseCode = $this->workerTariffService->buyWorkerTariff(
                    $tenantId, $workerId, $workerCityId, $tariff, $tariffCost, $requestId);

                if ($responseCode != Transaction::SUCCESS_RESPONSE) {
                    $errorCode = ($responseCode == Transaction::NO_MONEY_RESPONSE)
                        ? TaxiErrorCode::BAD_BALANCE : TaxiErrorCode::INTERNAL_ERROR;

                    \Yii::error("ERROR AT buyWorkerTariff. Error code: {$responseCode}");

                    $this->apiLogger->log('An error occurred while buying tariff (billing)');

                    return $request->getJsonAnswer(TaxiErrorCode::getErrorData($errorCode));
                }
            }

            if (!$this->workerService->isAllowedStartShiftByRating(
                $tenantId,
                $workerCityId,
                $positionId,
                $workerId,
                $workerCarId)
            ) {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_LOW_RATING));
            }


            if ($this->workerService->startShift(
                $tenantId, $tenantLogin, $workerId, $workerCallsign,
                $workerTariff, $workerCarId, $workerCityId, $hasActiveAboniment, $carMileage)
            ) {
                $shiftId = \Yii::$app->redis_worker_shift->executeCommand('GET',
                    [$tenantLogin . '_' . $workerCallsign]);

                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                    'result'          => 1,
                    'worker_shift_id' => $shiftId,
                ]);
            } else {
                \Yii::error("Internal Error. Method: startShift.");

                $this->apiLogger->log('An error occurred while starting shift');

                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR));
            }
        }
    }


    /**
     * API method to close worker shift
     * @return string JSON
     */
    public function actionWorker_end_work()
    {
        return $this->workerEndWork($_POST);
    }

    /**
     * Closing worker shift
     *
     * @param array $params
     *
     * @return string JSON
     * @throws InvalidConfigException
     */
    public function workerEndWork($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $workerCallsign = (int)$params['worker_login'];
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $carMileage     = isset($params['car_mileage']) ? (float)$params['car_mileage'] : null;

        $workerShiftService = $this->getWorkerShiftService();

        // check worker on order ore worker alreade end shift
        $status_worder =  $workerShiftService->workerOnOrder((int)$tenantId,(int)$workerCallsign );
        if($status_worder !== 0){

            // ДАННЫХ нет в редисе
            if($status_worder === TaxiErrorCode::SHIFT_NEED_CLOSED){
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                    'result' => 1,
                ]);
            }
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData($status_worder));
        }

        $shiftId     = $workerShiftService->getCurrentShiftId((int)$tenantId, (string)$tenantLogin,
            (int)$workerCallsign);
        $workerShift = WorkerShift::findOne($shiftId);


        if (!empty($workerShift->car_id) && $this->isCurrentVersionGreaterThanOrEqualTo('3.32.0')) {
            $car = Car::findOne($workerShift->car_id);

            $controlOwnCarMileage = TenantSetting::getSettingValue($tenantId,
                TenantSetting::SETTING_CONTROL_OWN_CAR_MILEAGE, $workerShift->city_id, $workerShift->position_id);
            if ((int)$controlOwnCarMileage === 1 && $car->isCompanyCar()) {
                try {
                    if (CarMileage::existsByShiftId((int)$shiftId)) {
                        if ($carMileage === null) {
                            throw new \Exception('Car mileage is required');
                        }
                        CarMileage::setEndValue((int)$car->car_id, (int)$shiftId, (float)$carMileage);
                    }
                } catch (\Exception $ex) {
                    $this->apiLogger->log("Invalid car mileage: code={$ex->getCode()}, error={$ex->getMessage()}");
                    $currentMileage = CarMileage::getCurrentMileage($car->car_id);

                    return $request->getJsonAnswer(
                        TaxiErrorCode::getErrorData(TaxiErrorCode::INVALID_CAR_MILEAGE),
                        ['current_mileage' => $currentMileage,]);
                }
            }
        }

        try {
            $result = $workerShiftService->closeWorkerShift($tenantId, $tenantLogin, $workerCallsign);
        } catch (ShiftIsClosedException $ex) {
            $result = true;
        }


        if ($result) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'result' => 1,
            ]);
        } else {
            Yii::error("Internal Error. Method:Worker_end_work.");

            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR));
        }
    }


    /**
     * API method to set car mileage
     * @return string JSON
     */
    public function actionSet_car_mileage()
    {
        return $this->setCarMileage($_POST);
    }

    /**
     * Set car mileage
     *
     * @param array $params
     *
     * @return string JSON
     * @throws InvalidConfigException
     */
    public function setCarMileage($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $workerCallsign = (int)$params['worker_login'];
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $shiftId        = $params['shift_id'];
        $carMileage     = $params['car_mileage'] ?? 0;

        $workerService = $this->getWorkerService();
        $worker        = $workerService->getWorker($tenantId, $workerCallsign);
        $workerShift   = WorkerShift::findOne($shiftId);

        if (empty($workerShift) || (int)$worker['worker_id'] !== (int)$workerShift->worker_id) {
            return $request->getJsonAnswer(TaxiErrorCode::getErrorData(TaxiErrorCode::EMPTY_DATA_IN_DATABASE));
        }

        $car = Car::findOne($workerShift->car_id);
        if (empty($car)) {
            return $request->getJsonAnswer(TaxiErrorCode::getErrorData(TaxiErrorCode::EMPTY_DATA_IN_DATABASE));
        }

        $controlOwnCarMileage = TenantSetting::getSettingValue($tenantId,
            TenantSetting::SETTING_CONTROL_OWN_CAR_MILEAGE, $workerShift->city_id, $workerShift->position_id);
        if ((int)$controlOwnCarMileage === 1 && $car->isCompanyCar()) {
            try {
                if (CarMileage::existsByShiftId((int)$shiftId)) {
                    $lastShiftWithMileage = CarMileage::getLastShiftWithMileage((int)$car->car_id);
                    if ((int)$shiftId !== $lastShiftWithMileage) {
                        return $request->getJsonAnswer(TaxiErrorCode::getErrorData(TaxiErrorCode::INVALID_SHIFT));
                    }

                    CarMileage::setEndValue((int)$car->car_id, (int)$shiftId, (float)$carMileage);
                }
            } catch (\Exception $ex) {
                $this->apiLogger->log("Invalid car mileage: code={$ex->getCode()}, error={$ex->getMessage()}");
                $currentMileage = CarMileage::getCurrentMileage($car->car_id);

                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::INVALID_CAR_MILEAGE),
                    ['current_mileage' => $currentMileage,]);
            }
        }

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
            'result' => 1,
        ]);
    }


    /**
     * API method to block worker order
     * @return string JSON
     */
    public function actionWorker_order_block()
    {
        return $this->workerOrderBlock($_POST);
    }

    /**
     * Worker order block
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function workerOrderBlock($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];

        $workerService = $this->getWorkerService();
        $timeHelper    = $this->getTimeHelper();

        if (empty($workerService->getWorker($tenantId, $workerCallsign))) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_NOT_EXIST));
        }

        if (!$workerService->isWorkerOnShift($tenantLogin, $workerCallsign)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
        }

        $worker = $workerService->getWorkerFromRedis($tenantId, $workerCallsign);
        if (!empty($worker['worker']['order_end_block'])
            && $timeHelper->getCurrentTime() < $worker['worker']['order_end_block']
        ) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_ALLREADY_BLOCKED), [
                'result' => 0,
            ]);
        }

        /**
         * @var $serviceEngine ServiceEngine
         */
        $serviceEngine = \Yii::$container->get(ServiceEngine::className());
        if ($serviceEngine->blockWorkerOrder($tenantId, $workerCallsign)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'result' => 1,
            ]);
        }

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR), [
            'result' => 0,
        ]);
    }


    /**
     * API method to unblock worker
     */
    public function actionWorker_order_unblock()
    {
        return $this->workerOrderUnblock($_POST);
    }

    /**
     * Unblock worker
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function workerOrderUnblock($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $blockId        = (int)$params['block_id'];

        $workerService = $this->getWorkerService();

        $worker = $workerService->getWorker($tenantId, $workerCallsign);
        if (empty($worker)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_NOT_EXIST));
        }

        /**
         * @var $serviceEngine ServiceEngine
         */
        $serviceEngine = \Yii::$container->get(ServiceEngine::className());
        if ($serviceEngine->unblockWorkerOrder($tenantId, $workerCallsign, $blockId)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'result' => 1,
            ]);
        }

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR), [
            'result' => 0,
        ]);
    }


    /**
     * API method to block worker pre-order
     * @return string JSON
     */
    public function actionWorker_pre_order_block()
    {
        return $this->workerPreOrderBlock($_POST);
    }

    /**
     * Worker pre-order block
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function workerPreOrderBlock($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];

        $workerService = $this->getWorkerService();
        $timeHelper    = $this->getTimeHelper();

        if (empty($workerService->getWorker($tenantId, $workerCallsign))) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_NOT_EXIST));
        }

        if (!$workerService->isWorkerOnShift($tenantLogin, $workerCallsign)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
        }

        $worker = $workerService->getWorkerFromRedis($tenantId, $workerCallsign);
        if (!empty($worker['worker']['preorder_end_block'])
            && $timeHelper->getCurrentTime() < $worker['worker']['preorder_end_block']
        ) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_ALLREADY_BLOCKED), [
                'result' => 0,
            ]);
        }

        /**
         * @var $serviceEngine ServiceEngine
         */
        $serviceEngine = \Yii::$container->get(ServiceEngine::className());
        if ($serviceEngine->blockWorkerPreOrder($tenantId, $workerCallsign)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'result' => 1,
            ]);
        }

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR), [
            'result' => 0,
        ]);
    }


    /**
     * API method to unblock worker
     */
    public function actionWorker_pre_order_unblock()
    {
        return $this->workerPreOrderUnblock($_POST);
    }

    /**
     * Unblock worker
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function workerPreOrderUnblock($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $blockId        = (int)$params['block_id'];

        $workerService = $this->getWorkerService();

        $worker = $workerService->getWorker($tenantId, $workerCallsign);
        if (empty($worker)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_NOT_EXIST));
        }

        /**
         * @var $serviceEngine ServiceEngine
         */
        $serviceEngine = \Yii::$container->get(ServiceEngine::className());
        if ($serviceEngine->unblockWorkerPreOrder($tenantId, $workerCallsign, $blockId)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'result' => 1,
            ]);
        }

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR), [
            'result' => 0,
        ]);
    }


    /**
     * API method for getting list of parking
     * @return string JSON
     */
    public function actionGet_parkings_list()
    {
        return $this->getParkingList($_GET);
    }

    /**
     * Getting list of parking
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function getParkingList($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $workerCityId   = $params['worker_city_id'];
        $parkingId      = isset($params['parking_id']) ? $params['parking_id'] : null;

        $workerService = $this->getWorkerService();
        if (!$workerService->isWorkerOnShift($tenantLogin, $workerCallsign)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
        }

        $parkingService = $this->getParkingService();
        $parkingList    = $parkingService->getParkingList($tenantId, $workerCityId, $parkingId);

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'parkings_list' => $parkingList,
            ]
        );
    }


    /**
     * API method for getting city polygon
     * @return string
     */
    public function actionGet_city_polygone()
    {
        return $this->getCityPolygon($_GET);
    }

    /**
     * Getting city polygon
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function getCityPolygon($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $workerCityId   = $params['worker_city_id'];

        $workerService = $this->getWorkerService();
        if (!$workerService->isWorkerOnShift($tenantLogin, $workerCallsign)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
        }

        $cityPolygon = Parking::getParkingArrBase($tenantId, $workerCityId);
        if (empty($cityPolygon)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'city_polygone' => null,
            ]);
        }

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
            'city_polygone' => [
                'parking_id'       => isset($cityPolygon['parking_id']) ? $cityPolygon['parking_id'] : null,
                'parking_name'     => isset($cityPolygon['name']) ? $cityPolygon['name'] : null,
                'parking_polygone' => isset($cityPolygon['polygon']) ? json_decode($cityPolygon['polygon']) : null,
                'parking_type'     => isset($cityPolygon['type']) ? $cityPolygon['type'] : null,
            ],
        ]);
    }


    /**
     * API method to set parking queue
     * @return string JSON
     */
    public function actionSet_parking_queue()
    {
        return $this->setParkingQueue($_POST);
    }

    /**
     * Set parking queue
     *
     * @param array $params
     *
     * @return string JSON
     * @throws \yii\db\Exception
     */
    public function setParkingQueue($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $parkingId      = empty($params['parking_id']) ? null : (int)$params['parking_id'];

        $workerService = $this->getWorkerService();
        if (!$workerService->isWorkerOnShift($tenantLogin, $workerCallsign)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
        }

        $worker     = $workerService->getWorkerFromRedis($tenantId, $workerCallsign);
        $workerId   = isset($worker['worker']['worker_id']) ? $worker['worker']['worker_id'] : null;
        $positionId = isset($worker['position']['position_id']) ? $worker['position']['position_id'] : null;

        if ($parkingId !== null) {
            $parking = Parking::findOne($parkingId);
            if (empty($parking)) {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::PARKING_NOT_EXIST));
            }
        }

        $needCheckWorkerRating = $parkingId !== null && TenantSetting::getSettingValue(
                $tenantId, TenantSetting::SETTING_CHECK_WORKER_RATING_TO_OFFER_ORDER, $parking->city_id, $positionId);

        $workerRating = 0;
        if ($needCheckWorkerRating) {
            $workerPosition = WorkerHasPosition::findOne([
                'worker_id'   => $workerId,
                'position_id' => $positionId,
                'active'      => 1,
            ]);

            if (!empty($workerPosition['rating'])) {
                $workerRating = $workerPosition['rating'];
            }
        }

        if ($workerService->setWorkerToParking(
            $tenantId, $workerCallsign, $parkingId, $workerRating, $needCheckWorkerRating)
        ) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'result' => '1',
            ]);
        }

        Yii::error("Internal Error . Method:Set_parking_queue . ");

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR));
    }


    /**
     * API method for getting order queues group by parking
     * @return string JSON
     */
    public function actionGet_parkings_orders_queues()
    {
        return $this->getOrderQueuesGroupByParking($_GET);
    }

    /**
     * Getting order queues group by parking
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function getOrderQueuesGroupByParking($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $workerCityId   = $params['worker_city_id'];
        $parkingId      = isset($params['parking_id']) ? $params['parking_id'] : null;

        if (!$this->workerService->isWorkerOnShift($tenantLogin, $workerCallsign)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
        }

        if (isset($parkingId)) {
            // Получаем одну парковку
            $parkingArr   = [];
            $parking      = Parking::find()
                ->where(['parking_id' => $parkingId])
                ->asArray()
                ->one();
            $parkingArr[] = $parking;
        } else {
            //Получаем все парковки в городе, если их нет то ошибка
            $parkingArr = Parking::getParkingArrAll($tenantId, $workerCityId);
        }

        if (empty($parkingArr)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::PARKING_NOT_EXIST));
        }

        $worker     = $this->workerService->getWorkerFromRedis($tenantId, $workerCallsign);
        $positionId = isset($worker['position']['position_id']) ? $worker['position']['position_id'] : null;

        $needCheckWorkerRating = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_CHECK_WORKER_RATING_TO_OFFER_ORDER, $workerCityId, $positionId);

        $workerParkingsQueueArr = [];
        foreach ($parkingArr as $parking) {
            $parkingId                          = $parking['parking_id'];
            $workerQueue                        = $this->workerService->getParkingWorkerQueue(
                $parkingId, $needCheckWorkerRating);
            $workerParkingsQueueArr[$parkingId] = $workerQueue;
        }

        $freeOrders  = Order::getFreeOrders($tenantId);
        $resultArray = [];
        foreach ($workerParkingsQueueArr as $key => $value) {
            $countFreeOrders = Parking::countFreeOrdersOnParking($key, $freeOrders);
            $resultArray[]   = [
                'parking_id'     => $key,
                'parkings_queue' => empty($value) ? null : $value,
                'parking_orders' => $countFreeOrders,
            ];
        }

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::OK), $resultArray);
    }


    /**
     * API method for getting open orders
     */
    public function actionGet_orders_open_list()
    {
        return $this->getOpenOrders($_GET);
    }

    /**
     * Get open (free) orders
     *
     * @param array $params
     *
     * @return string JSON
     * @throws InvalidConfigException
     */
    public function getOpenOrders($params)
    {
        $params       = $this->getParams($params);
        //$validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);
        $validateData = $this->checkParams($params, false);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }



        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $workerCityId   = isset($params['worker_city_id']) ? $params['worker_city_id'] : null;
        $workerLat      = isset($params['worker_lat']) ? $params['worker_lat'] : null;
        $workerLon      = isset($params['worker_lon']) ? $params['worker_lon'] : null;
        $parkingId      = isset($params['parking_id']) ? $params['parking_id'] : null;

        $workerService = $this->getWorkerService();
        if (!$workerService->isWorkerOnShift($tenantLogin, $workerCallsign)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
        }

        if (isset($parkingId) && !Parking::find()->where(['parking_id' => $parkingId])->exists()) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::PARKING_NOT_EXIST));
        }

        $worker = $workerService->getWorkerFromRedis($tenantId, $workerCallsign);

        $cities = isset($worker['worker']['cities'])
            ? $worker['worker']['cities'] : $workerService->getWorkerCities($tenantId, $worker['worker']['worker_id']);

        $currentCityId = $worker['worker']['city_id'];
        $currentCity   = current(array_filter($cities, function ($item) use ($currentCityId) {
            return $item['city_id'] == $currentCityId;
        }));

        if (empty($workerCityId) && !$this->isCurrentVersionGreaterThanOrEqualTo('3.16.0')) {
            $workerCityId = $currentCityId;
        }

        if (empty($workerLat) || empty($workerLon)) {
            $workerLat = isset($currentCity['lat']) ? $currentCity['lat'] : null;
            $workerLon = isset($currentCity['lon']) ? $currentCity['lon'] : null;
        }

        $orderService = $this->getOrderService();

        $result = [];
        foreach ($cities as $city) {
            if (!empty($workerCityId) && $city['city_id'] != $workerCityId) {
                continue;
            }

            $freeOrders         = $orderService->getFreeOrders($tenantId, $workerCallsign, $city['city_id'],
                $parkingId);
            $filteredFreeOrders = $orderService->getFilteredByDistanceOrders(
                $tenantId, $city['city_id'], $workerLat, $workerLon, $freeOrders);

            $filteredOrders = $orderService->getFilteredByExceptCarModel($worker, $filteredFreeOrders);
            $filteredOrders = $orderService->getFilteredOrders($filteredOrders);

            if (!empty($filteredOrders)) {
                $result = array_merge($result, $filteredOrders);
            }
        }


        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
            'orders_open_list' => $result,
        ]);
    }


    /**
     * API method for getting list of pre-orders
     * @return string JSON
     */
    public function actionGet_orders_pretime_list()
    {
        return $this->getPreOrders($_GET);
    }

    /**
     * Getting list of pre-orders
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function getPreOrders($params)
    {
        $params       = $this->getParams($params);
        //$validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);
        $validateData = $this->checkParams($params, false);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }


        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $workerCityId   = isset($params['worker_city_id'])
            ? $params['worker_city_id'] : null;

        $workerService = $this->getWorkerService();
        if (!$workerService->isWorkerOnShift($tenantLogin, $workerCallsign)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
        }

        $worker        = $workerService->getWorkerFromRedis($tenantId, $workerCallsign);
        $currentCityId = $worker['worker']['city_id'];
        $cities        = isset($worker['worker']['cities'])
            ? $worker['worker']['cities'] : $workerService->getWorkerCities($tenantId, $worker['worker']['worker_id']);

        $blocked = !empty($worker['worker']['preorder_end_block'])
            && $this->getTimeHelper()->getCurrentTime() < $worker['worker']['preorder_end_block'];

        if (empty($workerCityId) && !$this->isCurrentVersionGreaterThanOrEqualTo('3.16.0')) {
            $workerCityId = $currentCityId;
        }

        $orderService = $this->getOrderService();

        $freePreOrders   = [];
        $workerPreOrders = [];

        foreach ($cities as $city) {
            if (!empty($workerCityId) && $workerCityId != $city['city_id']) {
                continue;
            }

            $orders = $orderService->getFreePreOrders($tenantId, $workerCallsign, $city['city_id']);
            $orders = $orderService->getFilteredByExceptCarModel($worker, $orders);
            $orders = $orderService->getFilteredPreOrders($orders);
            if (!empty($orders)) {
                $freePreOrders = array_merge($freePreOrders, $orders);
            }

            $orders = $orderService->getWorkerPreOrders($tenantId, $workerCallsign, $city['city_id']);
            if (!empty($orders)) {
                $workerPreOrders = array_merge($workerPreOrders, $orders);
            }
        }

        return $request->getJsonAnswer(TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
            'orders_open_list'  => $blocked ? [] : $freePreOrders,
            'worker_pre_orders' => $workerPreOrders,
        ]);
    }


    /**
     * API method for getting order by id
     * @return string JSON
     */
    public function actionGet_order_for_id()
    {
        return $this->getOrderById($_GET);
    }

    /**
     * Getting order by id
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function getOrderById($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $workerCallsign = (int)$params['worker_login'];
        $orderId        = $params['order_id'];
        $lang           = $this->getLang();


        $workerService = $this->getWorkerService();
        $worker        = $workerService->getWorkerFromRedis($tenantId, $workerCallsign);
        if (empty($worker)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
        }

        $orderService = $this->getOrderService();

        $order = $orderService->getOrderFromRedis($orderId, $tenantId);
        if (!empty($order)) {
            $order = $orderService->filterOrder($order, $tenantId, $lang, $worker);

            if (!$this->isCurrentVersionGreaterThanOrEqualTo('3.15.0')) {
                if (isset($order['tariff']['auto_downtime'])) {
                    unset($order['tariff']['auto_downtime']);
                }
            }

            if (!$this->isCurrentVersionGreaterThanOrEqualTo('3.17.0')) {
                if (isset($order['costData']['city_time'])) {
                    unset($order['costData']['city_time']);
                }
                if (isset($order['costData']['city_distance'])) {
                    unset($order['costData']['city_distance']);
                }
                if (isset($order['costData']['city_cost'])) {
                    unset($order['costData']['city_cost']);
                }
                if (isset($order['costData']['out_city_time'])) {
                    unset($order['costData']['out_city_time']);
                }
                if (isset($order['costData']['out_city_distance'])) {
                    unset($order['costData']['out_city_distance']);
                }
                if (isset($order['costData']['out_city_cost'])) {
                    unset($order['costData']['out_city_cost']);
                }
            }

            $result = ['order_data' => $order];

            $this->apiLogger->log(json_encode($result, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

            return $request->getJsonAnswer(TaxiErrorCode::getErrorData(TaxiErrorCode::OK), $result);
        }

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::EMPTY_DATA_IN_DATABASE));
    }


    /**
     * API method to setup order status
     * @return string JSON
     */
    public function actionSet_order_status()
    {
        $params       = $this->getParams($_POST);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();


        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId        = $this->getTenantId($params);
        $tenantLogin     = $params['tenant_login'];
        $tenantLogin     = mb_strtolower($tenantLogin);
        $workerCallsign  = (int)$params['worker_login'];
        $orderId         = $params['order_id'];
        $statusNewId     = $params['status_new_id'];
        $lang            = $this->getLang();
        $timeToClient    = isset($params['time_to_client']) ? $params['time_to_client'] : null;
        $detailOrderData = isset($params['detail_order_data']) ? $params['detail_order_data'] : null;
        $orderRefuse = isset($params['order_refuse']) ? $params['order_refuse'] : "timer";

        return $this->setOrderStatus(
            $tenantId, $workerCallsign, $orderId, $statusNewId, $timeToClient, $detailOrderData, $orderRefuse, $lang);
    }

    /**
     * Setup order status
     *
     * @param int    $tenantId
     * @param int    $workerCallsign
     * @param int    $orderId
     * @param int    $statusId
     * @param int    $timeToClient
     * @param        string JSON $detailOrderData
     * @param string $lang
     *
     * @return string JSON
     */
    private function setOrderStatus(
        $tenantId,
        $workerCallsign,
        $orderId,
        $statusId,
        $timeToClient,
        $detailOrderData,
        $orderRefuse,
        $lang
    ) {
        $request = new TaxiApiRequest();

        if (in_array($statusId, [
            OrderStatus::STATUS_GET_WORKER,
            OrderStatus::STATUS_PRE_WORKER_ACCEPT_ORDER,
        ], false)) {
            $worker = $this->workerService->getWorkerFromRedis($tenantId, $workerCallsign);
            if (!empty($worker)) {
                if ($worker['worker']['status'] == Worker::STATUS_BLOCKED) {
                    return $request->getJsonAnswer(
                        TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_BLOCKED), [
                        'set_result' => 0,
                    ]);
                }

                if ($statusId == OrderStatus::STATUS_PRE_WORKER_ACCEPT_ORDER) {
                    $timeHelper = $this->getTimeHelper();
                    if (!empty($worker['worker']['preorder_end_block'])
                        && $timeHelper->getCurrentTime() < $worker['worker']['preorder_end_block']
                    ) {
                        return $request->getJsonAnswer(
                            TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_PRE_ORDER_BLOCKED), [
                            'unblock_time' => $worker['worker']['preorder_end_block']
                                + City::getTimeOffset($worker['worker']['city_id']),
                        ]);
                    }
                }
            }
        }


        if ($this->isCurrentVersionGreaterThanOrEqualTo('3.12.0')) {
            $orderService  = $this->getOrderService();
            $order         = $orderService->getOrderFromRedis($orderId, $tenantId);
            $orderStatusId = isset($order['status_id']) ? $order['status_id'] : null;
            $statusGroup   = isset($order['status']['status_group']) ? $order['status']['status_group'] : null;
            $callsign      = isset($order['worker']['callsign']) ? $order['worker']['callsign'] : null;


            if (in_array($statusId, [
                    OrderStatus::STATUS_GET_WORKER,
                    OrderStatus::STATUS_EXECUTION_PRE,
                ], false) &&
                (empty($order)
                    || in_array($statusGroup, [
                        OrderStatus::STATUS_GROUP_COMPLETED,
                        OrderStatus::STATUS_GROUP_REJECTED,
                    ], false))
            ) {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::EMPTY_DATA_IN_DATABASE), [
                    'set_result' => 0,
                ]);
            }

            if (empty($order)
                || ($orderStatusId == $statusId && $workerCallsign == $callsign)
                || in_array($statusGroup, [
                    OrderStatus::STATUS_GROUP_COMPLETED,
                    OrderStatus::STATUS_GROUP_REJECTED,
                ], false)
            ) {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                    'set_result' => 2,
                ]);
            }
        }

        $requestId = \Yii::$app->request->headers->get('Request-Id');

        /** @var $orderUpdateEvent OrderUpdateEvent */
        $orderUpdateEvent = new OrderUpdateEvent([
            'requestId' => $requestId,
            'tenantId'  => $tenantId,
            'orderId'   => $orderId,
            'senderId'  => $workerCallsign,
            'lang'      => $lang,
        ]);


        try {
            $orderUpdateEvent->addEvent([
                'status_id'         => $statusId,
                'time_to_client'    => $timeToClient,
                'detail_order_data' => $detailOrderData,
                'order_refuse' => $orderRefuse,
            ]);
        } catch (\Exception $ex) {
            $this->apiLogger->log('An error occurred while error adding order event to RabbitMQ queue');

            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR), [
                'set_result' => 0,
            ]);
        }

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
            'set_result' => 1,
        ]);
    }

    /**
     * API method to set order route
     * @return string JSON
     * @throws InvalidConfigException
     */
    public function actionSet_order_route()
    {
        $params       = $this->getParams($_POST);
        $validateData = $this->checkParams($params, false);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $orderId        = $params['order_id'];
        $orderRoute     = $params['order_route'];

        return $this->setOrderRoute($tenantId, $orderId, $orderRoute);
    }

    /**
     * API method to set order route (file)
     * @return string JSON
     * @throws InvalidConfigException
     */
    public function actionSet_order_route_file()
    {
        $params       = $this->getParams($_POST);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        if (!isset($_FILES['order_route'])) {
            return $request->getJsonAnswer([
                'code' => TaxiErrorCode::MISSING_INPUT_PARAMETER,
                'info' => TaxiErrorCode::ERROR_MESSAGES[TaxiErrorCode::MISSING_INPUT_PARAMETER] . ' order_route',
            ]);
        }

        $filename = $_FILES['order_route']['tmp_name'];
        if ($_FILES['order_route']['error'] !== 0 || !is_uploaded_file($filename)) {
            $this->apiLogger->log('File loading was failed (error=' . $_FILES['order_route']['error'] . ')');

            return $request->getJsonAnswer([
                'code' => TaxiErrorCode::INTERNAL_ERROR,
                'info' => TaxiErrorCode::ERROR_MESSAGES[TaxiErrorCode::INTERNAL_ERROR],
            ]);
        }

        $jsonString = file_get_contents($filename);
        if ($jsonString === false) {
            return $request->getJsonAnswer([
                'code' => TaxiErrorCode::BAD_PARAM,
                'info' => TaxiErrorCode::ERROR_MESSAGES[TaxiErrorCode::BAD_PARAM] . ' order_route',
            ]);
        }

        $orderRoute = json_decode($jsonString, true);
        if (empty($orderRoute)) {
            return $request->getJsonAnswer([
                'code' => TaxiErrorCode::BAD_PARAM,
                'info' => TaxiErrorCode::ERROR_MESSAGES[TaxiErrorCode::BAD_PARAM] . ' order_route',
            ]);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $orderId        = $params['order_id'];

        return $this->setOrderRoute($tenantId, $orderId, $jsonString);
    }

    /**
     * Set order route
     *
     * @param int    $tenantId
     * @param int    $orderId
     * @param string $orderRoute
     *
     * @return string JSON
     * @throws InvalidConfigException
     */
    public function setOrderRoute($tenantId, $orderId, $orderRoute)
    {
        $request = new TaxiApiRequest();

        $order = Order::find()
            ->select(['order_id', 'tenant_id'])
            ->where(['order_id' => $orderId])
            ->asArray()
            ->one();

        if (empty($order) || (int)$order['tenant_id'] !== $tenantId) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::EMPTY_DATA_IN_DATABASE), [
                'set_result' => 0,
            ]);
        }

        $orderService = $this->getOrderService();
        if (!empty($orderService->getOrderRoute($orderId))) {
            $this->apiLogger->log('Order track already exists in DB');

            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'set_result' => 1,
            ]);
        }

        if ($orderService->setOrderRoute($orderId, $orderRoute)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'set_result' => 1,
            ]);
        } else {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR), [
                'set_result' => 0,
            ]);
        }
    }


    /**
     * API method to setup order raw calc
     * @return string JSON
     */
    public function actionSet_order_raw_calc()
    {
        return $this->setOrderRawCalc($_POST);
    }

    /**
     * Set order raw calc
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function setOrderRawCalc($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $orderId        = (int)$params['order_id'];
        $orderCalcData  = (string)$params['order_calc_data'];

        if (empty(Order::findOne($orderId))) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::EMPTY_DATA_IN_DATABASE), [
                'set_result' => 0,
            ]);
        }

        $orderService = $this->getOrderService();
        if ($orderService->setRawOrderCalc($orderId, $orderCalcData)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'set_result' => 1,
            ]);
        } else {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR), [
                'set_result' => 0,
            ]);
        }
    }


    /**
     * API method for getting order raw calc
     * @return string JSON
     */
    public function actionGet_order_raw_calc()
    {
        return $this->getOrderRawCalcData($_GET);
    }

    /**
     * Getting order raw calc
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function getOrderRawCalcData($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $orderId        = $params['order_id'];

        $rawOrderCalc = RawOrderCalc::findone(['order_id' => $orderId]);

        $result = null;
        if (!empty($rawOrderCalc)) {
            $result = ['data' => json_decode($rawOrderCalc->raw_cacl_data)];
        }

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::OK), $result);
    }


    /**
     * API method for checking worker state
     * @return string JSON
     */
    public function actionCheck_worker_on_work()
    {
        return $this->checkWorkerOnWork($_GET);
    }

    /**
     * Check worker state
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function checkWorkerOnWork($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $workerCallsign = (int)$params['worker_login'];

        $workerService = $this->getWorkerService();
        $worker        = $workerService->getWorkerFromRedis($tenantId, $workerCallsign);

        if (!empty($worker)) {
            if (isset($worker['worker']['status'])) {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                    'worker_status' => (string)$worker['worker']['status'],
                ]);
            } else {
                Yii::error("Internal Error . Method:Check_worker_on_work . ");

                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR));
            }
        }

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
    }


    /**
     * API method to setup signal sos
     * @return string JSON
     */
    public function actionSet_signal_sos()
    {
        return $this->setSignalSos($_POST);
    }

    /**
     * Set signal sos
     *
     * @param $params
     *
     * @return string
     */
    public function setSignalSos($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $workerLat      = $params['worker_lat'];
        $workerLon      = $params['worker_lon'];

        $workerService = $this->getWorkerService();
        $worker        = $workerService->getWorkerFromRedis($tenantId, $workerCallsign);

        if (empty($worker)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
        }

        if (!empty($worker) && $worker['worker']['status'] == Worker::STATUS_BLOCKED) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_BLOCKED), [
                'sos_result' => 0,
            ]);
        }

        /**
         * @var $pusher TaxiPusherComponent
         */
        $pusher = Yii::$app->pusher;
        if ($pusher->setSignalSos($tenantId, $workerCallsign, $workerLat, $workerLon)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'sos_result' => 1,
            ]);
        } else {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::EMPTY_DATA_IN_DATABASE), [
                'sos_result' => 0,
            ]);
        }
    }


    /**
     * API method to unset signal sos
     * @return string JSON
     */
    public function actionUnset_signal_sos()
    {
        return $this->unsetSignalSos($_POST);
    }

    /**
     * Unset signal sos
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function unsetSignalSos($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $workerLat      = $params['worker_lat'];
        $workerLon      = $params['worker_lon'];

        $workerService = $this->getWorkerService();
        $worker        = $workerService->getWorkerFromRedis($tenantId, $workerCallsign);

        if (empty($worker)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
        }

        if (!empty($worker) && $worker['worker']['status'] == Worker::STATUS_BLOCKED) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_BLOCKED), [
                'sos_result' => 0,
            ]);
        }

        /**
         * @var $pusher TaxiPusherComponent
         */
        $pusher = Yii::$app->pusher;
        if ($pusher->unsetSignalSos($tenantId, $workerCallsign, $workerLat, $workerLon)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'sos_result' => 1,
            ]);
        } else {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::EMPTY_DATA_IN_DATABASE), [
                'sos_result' => 0,
            ]);
        }
    }


    /**
     * API method for getting city list
     * @return string JSON
     */
    public function actionGet_city_list()
    {
        return $this->getCityList($_GET);
    }

    /**
     * Getting city list
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function getCityList($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $cityPart       = $params['city_part'];
        $lang           = $this->getLang();
        $lang           = ($lang == 'ru' || empty($lang)) ? '' : '_' . $lang;

        $workerService = $this->getWorkerService();
        if (!$workerService->isWorkerOnShift($tenantLogin, $workerCallsign)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
        }

        if (mb_strlen($cityPart, 'utf-8') < 3) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'city_list' => null,
            ]);
        }

        $cityService = $this->getCityService();
        $cities      = $cityService->findCitiesCached($cityPart, $lang);

        if (empty($cities)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::EMPTY_DATA_IN_DATABASE));
        } else {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'city_list' => $cities,
            ]);
        }
    }


    /**
     * API method for getting list of geo objects
     */
    public function actionGet_geoobjects_list()
    {
        $params       = $this->getParams($_GET);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::EMPTY_DATA_IN_DATABASE));
    }

    /**
     * API method that set worker state
     * @return string JSON
     */
    public function actionSet_worker_state()
    {
        return $this->setWorkerState($_POST);
    }

    /**
     * Set worker state
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function setWorkerState($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $action         = $params['action'];

        if (!in_array($action, [Worker::ACTION_PAUSE, Worker::ACTION_WORK])) {
            $this->apiLogger->log('Action is not valid');

            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::BAD_PARAM));
        }

        $comment = isset($params['comment']) ? $params['comment'] : null;

        $workerService = $this->getWorkerService();

        if (!$workerService->isWorkerOnShift($tenantLogin, $workerCallsign)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
        }

        $worker = $workerService->getWorkerFromRedis($tenantId, $workerCallsign);
        if (empty($worker)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::EMPTY_DATA_IN_DATABASE), [
                'set_result' => 0,
            ]);
        }

        if ($worker['worker']['status'] == Worker::STATUS_BLOCKED) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_BLOCKED), [
                'set_result' => 0,
            ]);
        }

        if ($action === Worker::ACTION_PAUSE && $worker['worker']['status'] === Worker::STATUS_ON_ORDER) {
            $this->apiLogger->log("Can't set pause if worker already execute order: currentStatus={$worker['worker']['status']}");

            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::BAD_PARAM), [
                'set_result' => 0,
            ]);
        }

        switch ($action) {
            case Worker::ACTION_PAUSE:
                $worker['worker']['status']       = Worker::STATUS_ON_BREAK;
                $worker['worker']['break_reason'] = $comment;
                break;
            case Worker::ACTION_WORK:
                $worker['worker']['status']       = Worker::STATUS_FREE;
                $worker['worker']['break_reason'] = null;
                break;
        }

        if ($workerService->saveWorkerToRedis($tenantId, $workerCallsign, $worker)) {
            $workerShiftService = $this->getWorkerShiftService();
            $workerShiftService->setPauseData(
                $tenantId, $tenantLogin, $workerCallsign, $comment, $action);

            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                'set_result' => 1,
            ]);
        } else {
            Yii::error("Internal Error . Method:Set_worker_state . ");

            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR), [
                'set_result' => 0,
            ]);
        }
    }


    /**
     * API method to getting client tariffs
     * @return string JSON
     */
    public function actionGet_client_tariff()
    {
        $params       = $this->getParams($_GET);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        } else {
            $tenantId       = $this->getTenantId($params);
            $tenantLogin    = $params['tenant_login'];
            $tenantLogin    = mb_strtolower($tenantLogin);
            $workerCallsign = (int)$params['worker_login'];

            if (!$this->workerService->isWorkerOnShift($tenantLogin, $workerCallsign)) {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
            }

            $worker = $this->workerService->getWorkerFromRedis($tenantId, $workerCallsign);
            if (!empty($worker)) {
                $cityId    = isset($worker['worker']['city_id']) ? $worker['worker']['city_id'] : null;
                $classes   = isset($worker['position']['classes']) ? $worker['position']['classes'] : null;
                $positions = isset($worker['position']['positions']) ? $worker['position']['positions'] : null;

                if ($worker['position']['has_car'] == 1) {
                    $tariffs = TaxiTariff::getTariffsByCarClasses($tenantId, $classes, $cityId);
                } else {
                    $tariffs = TaxiTariff::getTariffsByPositions($tenantId, $positions, $cityId);
                }

                if (empty($tariffs)) {
                    Yii::error("Internal Error. Method:Get_client_tariff.");

                    $this->apiLogger->log('There are no client tariffs for the worker');

                    return $request->getJsonAnswer(
                        TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR));
                }

                $resultArr = [];
                foreach ($tariffs as $tariffData) {
                    $tariffId    = isset($tariffData['tariff_id']) ? $tariffData['tariff_id'] : null;
                    $tariffLabel = isset($tariffData['name']) ? $tariffData['name'] : null;
                    $positionId  = isset($tariffData['position_id']) ? $tariffData['position_id'] : null;

                    if ($tariffId) {
                        $now             = time() + City::getTimeOffset($cityId)
                            + Order::getPickUp($tenantId, $cityId, $positionId);
                        $tariffType      = TaxiTariff::getClientTariffTypeByTime($tariffId, $now);
                        $tariffDataCity  = TaxiTariff::getTariffData($tariffId, "CITY", $tariffType);
                        $tariffDataCity  = TaxiTariff::unSerializeIntervalData($tariffDataCity);
                        $tariffDataCity  = TaxiTariff::filterTariffDataAllowDayNigth($tariffDataCity);
                        $tariffDataTrack = TaxiTariff::getTariffData($tariffId, "TRACK", $tariffType);
                        $tariffDataTrack = TaxiTariff::unSerializeIntervalData($tariffDataTrack);
                        $tariffDataTrack = TaxiTariff::filterTariffDataAllowDayNigth($tariffDataTrack);
                        $isDay           = TaxiTariff::isTariffDay($tariffDataCity, $now);
                        $result          = [
                            "tariffInfo" => [
                                "tariffId"        => $tariffId,
                                "tariffType"      => $tariffType,
                                "tariffLabel"     => $tariffLabel,
                                "tariffDataCity"  => $tariffDataCity,
                                "tariffDataTrack" => $tariffDataTrack,
                                "isDay"           => $isDay,
                            ],
                        ];

                        $resultArr[] = $result;
                    }
                }

                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::OK), $resultArr);
            } else {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR));
            }
        }
    }

    public function actionGet_additional_options()
    {
        $params       = $this->getParams($_GET);
        $validateData = $this->checkParams($params, false);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        } else {
            $tenantId       = $this->getTenantId($params);
            $tenantLogin    = mb_strtolower($params['tenant_login']);
            $workerCallsign = (int)$params['worker_login'];
            $tariffId       = (int)$params['tariff_id'];


            if (!$this->workerService->isWorkerOnShift($tenantLogin, $workerCallsign)) {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
            }

            $worker = $this->workerService->getWorkerFromRedis($tenantId, $workerCallsign);


            if (!empty($worker)) {
                if (!$this->workerTariffService->isValidWorkerForBorderTariff($worker, $tariffId)) {
                    Yii::error("Internal Error. Method:Get_additional_options.");

                    $this->apiLogger->log('There are no client tariffs for the worker');

                    return $request->getJsonAnswer(
                        TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR));
                }

                $result = $this->workerService->getAdditionOptions($tariffId, $tenantId, $workerCallsign,
                    $this->getLang());


                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::OK), $result);
            } else {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR));
            }
        }
    }


    /**
     * API method to getting order cost
     * @return string JSON
     */
    public function actionCall_cost()
    {
        $params       = $this->getParams($_POST);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        } else {
            $tenantId       = $this->getTenantId($params);
            $tenantLogin    = $params['tenant_login'];
            $tenantLogin    = mb_strtolower($tenantLogin);
            $workerCallsign = (int)$params['worker_login'];
            $tariffId       = (int)$params['tariff_id'];
            $address        = $params['address'];
            $orderDate      = (int)$params['order_date'];
            $lang           = $this->getLang();

            if (!$this->workerService->isWorkerOnShift($tenantLogin, $workerCallsign)) {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
            }

            $addressArray = Order::filterAddress(json_decode($address, true));
            if (empty($addressArray)) {
                return $request->getJsonAnswer([
                    'code' => TaxiErrorCode::MISSING_INPUT_PARAMETER,
                    'info' => TaxiErrorCode::ERROR_MESSAGES[TaxiErrorCode::MISSING_INPUT_PARAMETER] . ' address',
                ]);
            }

            $worker = $this->workerService->getWorkerFromRedis($tenantId, $workerCallsign);

            if (!empty($worker)) {
                $cityId    = isset($worker['worker']['city_id']) ? $worker['worker']['city_id'] : null;
                $classes   = isset($worker['position']['classes']) ? $worker['position']['classes'] : null;
                $positions = isset($worker['position']['positions']) ? $worker['position']['positions'] : null;

                if ($worker['position']['has_car'] == 1) {
                    $tariffs = TaxiTariff::getTariffsByCarClasses($tenantId, $classes, $cityId);
                } else {
                    $tariffs = TaxiTariff::getTariffsByPositions($tenantId, $positions, $cityId);
                }

                if (!is_array($tariffs)
                    || !in_array($tariffId, ArrayHelper::getColumn($tariffs, 'tariff_id'), false)
                ) {
                    return $request->getJsonAnswer(
                        TaxiErrorCode::getErrorData(TaxiErrorCode::BAD_TARIFF));
                }

                if (empty($tenantId) || empty($cityId) || empty($tariffId)) {
                    Yii::error('Internal Error. Method:Call_cost.');

                    $this->apiLogger->log('One or more fields are empty (`tenant_id`, `city_id`, `tariff_id`)');

                    return $request->getJsonAnswer(
                        TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR));
                }

                $tariff = TaxiTariff::findOne($tariffId);

                $timeHelper = $this->getTimeHelper();
                $now        = $timeHelper->getCurrentTime()
                    + City::getTimeOffset($cityId) + Order::getPickUp($tenantId, $cityId, $tariff->position_id);
                $orderDate  = date('d-m-Y H:i:s', !empty($orderDate) ? $orderDate : $now);

                $taxiRouteAnalyzer = Yii::$app->routeAnalyzer;

                $geocoderType = TenantSetting::getSettingValue(
                    $tenantId, TenantSetting::SETTING_GEOCODER_TYPE, $cityId);
                if (empty($geocoderType)) {
                    $geocoderType = 'ru';
                }

                $routeCostInfo = $taxiRouteAnalyzer->analyzeRoute($tenantId, $cityId, $addressArray['address'], [],
                    $tariffId, $orderDate, $lang, $geocoderType);

                $costData = [];
                if (!empty($routeCostInfo)) {
                    $result                           = (array)$routeCostInfo;
                    $costData['additionals_cost']     = isset($result['additionalCost']) ? (string)$result['additionalCost'] : null;
                    $costData['summary_time']         = isset($result['summaryTime']) ? (string)$result['summaryTime'] : null;
                    $costData['summary_distance']     = isset($result['summaryDistance']) ? (string)$result['summaryDistance'] : null;
                    $costData['summary_cost']         = isset($result['summaryCost']) ? (string)$result['summaryCost'] : null;
                    $costData['city_time']            = isset($result['cityTime']) ? (string)$result['cityTime'] : null;
                    $costData['city_distance']        = isset($result['cityDistance']) ? (string)$result['cityDistance'] : null;
                    $costData['city_cost']            = isset($result['cityCost']) ? (string)$result['cityCost'] : null;
                    $costData['out_city_time']        = isset($result['outCityTime']) ? (string)$result['outCityTime'] : null;
                    $costData['out_city_distance']    = isset($result['outCityDistance']) ? (string)$result['outCityDistance'] : null;
                    $costData['out_city_cost']        = isset($result['outCityCost']) ? (string)$result['outCityCost'] : null;
                    $costData['is_fix']               = empty($result['isFix']) ? 0 : 1;
                    $costData['start_point_location'] = isset($result['startPointLocation']) ? (string)$result['startPointLocation'] : null;
                }

                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                    'cost_data' => $costData,
                ]);
            } else {
                Yii::error('Internal Error. Method:Call_cost.');

                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR));
            }
        }
    }

    /**
     * @param Order $order
     *
     * @throws \yii\base\ErrorException
     */
    private function sendOrderToEngine($order)
    {
        //Отправляем заказ в ноду
        $serviceEngine   = new ServiceEngine();
        try {
            $resultSendOrder = $serviceEngine->neworderAuto($order->order_id, $order->tenant_id);
        } catch (\Exception $e) {
            \Yii::error("Receive error neworderAuto: orderId={$order->order_id} (Error:{$e->getMessage()})");
            $resultSendOrder = false;
        }

        if (!$resultSendOrder) {
            \Yii::$app->redis_orders_active->executeCommand('hdel', [$order->tenant_id, $order->order_id]);
            $error = null;
            try {
                $isOrderDeleted = $order->delete();
            } catch (\Exception $ex) {
                $isOrderDeleted = false;
                $error          = $ex->getMessage();
            }

            if (empty($isOrderDeleted)) {
                \Yii::error("Error delete new order from MySql, after engine was failed (Error:{$error})", 'order');
                $this->apiLogger->log("Error delete new order from MySql, after engine was failed (Error:{$error})");
            }

            throw new ErrorException("An error occurred while sending order to NodeJS");
        }
    }

    /**
     * API method to create order
     * @return string JSON
     * @throws InvalidConfigException
     */
    public function actionCreate_order()
    {
        $params       = $this->getParams($_POST);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        } else {
            $clientId          = null;
            $tenantId          = $this->getTenantId($params);
            $tenantLogin       = $params['tenant_login'];
            $tenantLogin       = mb_strtolower($tenantLogin);
            $workerCallsign    = (int)$params['worker_login'];
            $tariffId          = (int)$params['tariff_id'];
            $address           = $params['address'];
            $phone             = isset($params['client_phone']) ? (int)$params['client_phone'] : null;
            $phone             = (string)$phone;

            $this->apiLogger->log($address);

            if (!$this->workerService->isWorkerOnShift($tenantLogin, $workerCallsign)) {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
            }


            $worker = $this->workerService->getWorkerFromRedis($tenantId, $workerCallsign);
            if ($worker) {

                $workerId = isset($worker['worker']['worker_id']) ? $worker['worker']['worker_id'] : null;
                $cityId   = isset($worker['worker']['city_id']) ? $worker['worker']['city_id'] : null;
                $carId    = isset($worker['car']['car_id']) ? $worker['car']['car_id'] : null;

                $hasCar    = isset($worker['position']['has_car']) ? $worker['position']['has_car'] : null;
                $classes   = isset($worker['position']['classes']) ? $worker['position']['classes'] : null;
                $positions = isset($worker['position']['positions']) ? $worker['position']['positions'] : null;

                if ($hasCar) {
                    $tariffs = TaxiTariff::getTariffsByCarClasses($tenantId, $classes, $cityId);
                } else {
                    $tariffs = TaxiTariff::getTariffsByPositions($tenantId, $positions, $cityId);
                }

                $filteredTariffs = array_filter($tariffs, function ($model) use ($tariffId) {
                    return $model['tariff_id'] == $tariffId;
                });

                if (empty($filteredTariffs)) {
                    return $request->getJsonAnswer(
                        TaxiErrorCode::getErrorData(TaxiErrorCode::BAD_TARIFF));
                }

                $tariff     = current($filteredTariffs);
                $positionId = $tariff['position_id'];

                //Создаем заказ
                $order              = new Order();
                $order->tenant_id   = $tenantId;
                $order->city_id     = $cityId;
                $order->tariff_id   = $tariffId;
                $order->parking_id  = isset($parkingId) ? $parkingId : null;
                $order->client_id   = $clientId;
                $order->phone       = $phone;
                $order->address     = json_decode($address, true)['address'];
                $order->worker_id   = $workerId;
                $order->car_id      = $carId;
                $order->device      = Order::DEVICE_WORKER;
                $order->position_id = $positionId;

                /** @var OrderApi $apiOrder */
                $apiOrder = Yii::$app->orderApi;
                /** @var ProcessingErrorsApiOrder $processingErrors */
                $processingErrors = Yii::createObject(ProcessingErrorsApiOrder::class);

                try {
                    $order = $apiOrder->createOrder($order);

                    return $request->getJsonAnswer(
                        TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
                        'order_id'     => $order->order_id,
                        'order_number' => $order->order_number,
                    ]);

                } catch (ClientException $ex) {
                    $processingErrors->setResponse(json_decode($ex->getResponse()->getBody(), true));

                    return $request->getJsonAnswer(
                        TaxiErrorCode::getErrorData($processingErrors->getError())
                    );
                } catch (\Exception $exc) {

                    \Yii::error('Ошибка сохранения заказа: ' . $exc->getMessage(), 'order');

                    return $request->getJsonAnswer(
                        TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR));
                }
            }
        }
    }

    /**
     * Getting list of bank cards
     *
     * @return string JSON
     * @throws InvalidConfigException
     */
    public function actionGet_bank_cards()
    {
        $params = $this->getParams($_GET);

        return $this->doCardAction( 'getCards', $params);
    }

    /**
     * Request to creating bank card
     *
     * @return string JSON
     * @throws InvalidConfigException
     */
    public function actionCreate_bank_card()
    {
        $params = $this->getParams($_POST);

        return $this->doCardAction('create', $params);
    }

    /**
     * Check bank card
     *
     * @return string JSON
     * @throws InvalidConfigException
     */
    public function actionCheck_bank_card()
    {
        $params = $this->getParams($_POST);

        return $this->doCardAction('check', $params);
    }

    /**
     * Delete bank card
     *
     * @return string JSON
     * @throws InvalidConfigException
     */
    public function actionDelete_bank_card()
    {
        $params = $this->getParams($_POST);

        return $this->doCardAction('delete', $params);
    }

    /**
     * Execute card action
     *
     * @param string $action
     * @param array $params
     *
     * @return mixed
     * @throws InvalidConfigException
     */
    protected function doCardAction($action, array $params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = (string)$this->getTenantId($params);
        $workerCallsign = (int)$params['worker_login'];

        $workerService = $this->getWorkerService();
        $worker        = $workerService->getWorker($tenantId, $workerCallsign);
        $workerId      = (string)$worker['worker_id'];

        /** @var $card Card */
        $card = \Yii::$container->get(Card::class,
            [\Yii::$app->restCurl, \Yii::$app->params['paymentGateApi.url'], $this->getLang()]);

        try {
            $cityId = $this->workerService->getLastActiveCityId($workerId);
            $profile = $this->profileService->getProfile($tenantId, $cityId);

            switch ($action) {
                case 'getCards':
                    $result = $card->getCards($tenantId, $profile, $workerId);
                    break;
                case 'create':
                    $response = $card->create($tenantId, $profile, $workerId);
                    $result   = empty($response['result']) ? null : $response['result'];
                    break;
                case 'check':
                    $response = $card->check($tenantId, $profile, $workerId, (string)$params['order_id']);
                    $result   = empty($response['result']) ? null : $response['result'];
                    break;
                case 'delete':
                    $response = $card->delete($tenantId, $profile, $workerId, (string)$params['pan']);
                    $result   = true;
                    break;
                default:
                    $result = null;
            }

            $statusCode = empty($response['statusCode']) ? null : $response['statusCode'];

            if ((int)$statusCode === 202 && $this->isCurrentVersionGreaterThanOrEqualTo('3.17.0')) {
                $code = TaxiErrorCode::REQUEST_PROCESSING;
            } else {
                $code = TaxiErrorCode::OK;
            }

            return $request->getJsonAnswer(TaxiErrorCode::getErrorData($code), $result);
        } catch (RestException $ex) {
            Yii::error($ex, 'bank-card');

            return $request->getJsonAnswer(TaxiErrorCode::getErrorData(TaxiErrorCode::PAYMENT_GATE_ERROR));
        } catch (\Exception $ex) {
            Yii::error($ex, 'bank-card');

            return $request->getJsonAnswer(TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR));
        }
    }

    /**
     * API method to update order
     */
    public function actionUpdate_order()
    {
        $params = $this->getParams($_POST);
        return $this->updateOrder($params);
    }

    public function actionUpdate_order_cost()
    {
        $params = $this->getParams($_POST);
        $res = $this->validationForUpdateOrder($params);

        if ($res !== true) {
            return $res;
        }

        $orderUpdateEvent = $this->createOrderUpdateEvent($params);

        $request = new TaxiApiRequest();

        try {
            $orderUpdateEvent->addEvent(['predv_price' => $params['predv_price']]);
        } catch (\Exception $ex) {
            $this->apiLogger->log('An error occurred while adding order update event to RabbitMQ');

            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR), [
                'set_result' => 0,
            ]);
        }

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
            'set_result' => 1,
        ]);
    }

    /**
     * Update order
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function updateOrder($params)
    {

        $res = $this->validationForUpdateOrder($params);

        if ($res !== true) {
            return $res;
        }

        $orderUpdateEvent = $this->createOrderUpdateEvent($params);

        $request = new TaxiApiRequest();

        try {
            $orderUpdateEvent->addEvent(['address' => $params['address']]);
        } catch (\Exception $ex) {
            $this->apiLogger->log('An error occurred while adding order update event to RabbitMQ');

            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR), [
                'set_result' => 0,
            ]);
        }

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
            'set_result' => 1,
        ]);

    }

    /**
     * API method to refill account
     */
    public function actionRefill_account()
    {
        return $this->refillAccount($_POST);
    }

    /**
     * @param $params
     *
     * @return OrderUpdateEvent
     */
    private function createOrderUpdateEvent($params)
    {

        $tenantId       = $this->getTenantId($params);
        $workerCallsign = (int)$params['worker_login'];
        $orderId        = $params['order_id'];
        $lang           = $this->getLang();

        $requestId = \Yii::$app->request->headers->get('Request-Id');

        return new OrderUpdateEvent([
            'requestId' => $requestId,
            'tenantId'  => $tenantId,
            'orderId'   => $orderId,
            'senderId'  => $workerCallsign,
            'lang'      => $lang,
        ]);
    }

    private function validationForUpdateOrder($params)
    {
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $workerCallsign = (int)$params['worker_login'];

        $workerService = $this->getWorkerService();

        $worker = $workerService->getWorkerFromRedis($tenantId, $workerCallsign);
        if (empty($worker)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
        } elseif ($worker['worker']['status'] == Worker::STATUS_BLOCKED) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_BLOCKED));
        } elseif (array_key_exists('predv_price', $params)) {
            if (TenantSetting::getSettingValue(
                    $tenantId,
                    TenantSetting::ALLOW_EDIT_COST_ORDER,
                    $worker['worker']['city_id'],
                    $worker['position']['position_id']
                ) != 1
            ) {
                return $request->getJsonAnswer(
                    TaxiErrorCode::getErrorData(TaxiErrorCode::FORBIDDEN_ACTION));
            }
        }

        return true;
    }

    /**
     * Refill account
     *
     * @param array $params
     *
     * @return string JSON
     */
    private function refillAccount($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $pan            = $params['pan'];
        $sum            = $params['sum'];
        $currency       = $params['currency'];
        $requestId      = \Yii::$app->request->headers->get('Request-Id', Uuid::uuid4()->toString());

        $workerService = $this->getWorkerService();

        $worker = $workerService->getWorker($tenantId, $workerCallsign);
        if (empty($worker)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::WORKER_NOT_EXIST));
        }

        $workerId   = $worker['worker_id'];
        $currencyId = Currency::getCurrencyIdByCode($currency);

        $transaction = new WorkerTransaction();
        $result      = $transaction->refillAccount(
            $tenantId, $workerId, $requestId, $pan, $sum, $currencyId);

        $resultCode = $result == Transaction::SUCCESS_RESPONSE
            ? TaxiErrorCode::OK : TaxiErrorCode::PAYMENT_GATE_ERROR;

        $account = Account::findOne([
            'owner_id'    => $workerId,
            'tenant_id'   => $tenantId,
            'acc_kind_id' => Account::WORKER_KIND,
            'currency_id' => $currencyId,
        ]);

        return $request->getJsonAnswer(
            TaxiErrorCode::getErrorData($resultCode), [
            'worker_balance' => empty($account) ? 0 : $account->getFormattedBalance(),
            'currency'       => $currency,
        ]);
    }

    public function actionGet_own_orders_hospital()
    {
        $params       = $this->getParams($_GET);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];


        $orderService = $this->getOrderService();
        $orders       = $orderService->getOwnOrdersHospital($tenantId, $workerCallsign);

        return $request->getJsonAnswer(TaxiErrorCode::getErrorData(TaxiErrorCode::OK), $orders);
    }

    /**
     * API method to getting list of own orders
     */
    public function actionGet_own_orders()
    {
        return $this->getOwnOrders($_GET);
    }

    /**
     * Getting list of own orders
     *
     * @param array $params
     *
     * @return string JSON
     */
    public function getOwnOrders($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];

        $workerService = $this->getWorkerService();
        $worker        = $workerService->getWorkerFromRedis($tenantId, $workerCallsign);

        if (empty($worker)) {
            return $request->getJsonAnswer(
                TaxiErrorCode::getErrorData(TaxiErrorCode::SHIFT_IS_CLOSED));
        }

        $orderService = $this->getOrderService();
        $orders       = $orderService->getOwnOrders($tenantId, $workerCallsign);

        return $request->getJsonAnswer(TaxiErrorCode::getErrorData(TaxiErrorCode::OK), $orders);
    }


    /**
     * API method to getting request result
     * @throws InvalidConfigException
     */
    public function actionGet_request_result()
    {
        return $this->getRequestResult($_GET);
    }

    /**
     * Getting request result
     *
     * @param array $params
     *
     * @return string JSON
     * @throws InvalidConfigException
     */
    public function getRequestResult($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];
        $requestId      = $params['request_id'];

        $orderService = $this->getOrderService();
        $responseData = json_decode($orderService->getOrderUpdateEventResult($requestId), true);

        $response = empty($responseData) ? null : [
            'code'   => isset($responseData['code']) ? $responseData['code'] : null,
            'info'   => isset($responseData['info']) ? $responseData['info'] : null,
            'result' => isset($responseData['result']) ? $responseData['result'] : null,
        ];

        return $request->getJsonAnswer(TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
            'set_result' => $response === null ? '0' : '1',
            'response'   => $response,
        ]);
    }


    /**
     * API method to clear information after worker was deleted from mobile application
     * @throws InvalidConfigException
     */
    public function actionClear_information_after_worker_delete()
    {
        return $this->clearInformationAfterWorkerDelete($_POST);
    }

    /**
     * Clear information after worker was deleted from mobile application
     *
     * @param array $params
     *
     * @return string JSON
     * @throws InvalidConfigException
     */
    public function clearInformationAfterWorkerDelete($params)
    {
        $params       = $this->getParams($params);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        $request = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        $tenantId       = $this->getTenantId($params);
        $tenantLogin    = $params['tenant_login'];
        $tenantLogin    = mb_strtolower($tenantLogin);
        $workerCallsign = (int)$params['worker_login'];

        $workerService = $this->getWorkerService();
        try {
            $workerService->clearInformationAfterWorkerDelete($tenantId, $workerCallsign);
        } catch (\Exception $ex) {
            \Yii::error($ex, 'clear_information_after_worker_delete');
            $this->apiLogger->log("Clear worker information error ({$ex->getMessage()})");

            return $request->getJsonAnswer(TaxiErrorCode::getErrorData(TaxiErrorCode::INTERNAL_ERROR), [
                'clear_result' => 0,
            ]);
        }

        return $request->getJsonAnswer(TaxiErrorCode::getErrorData(TaxiErrorCode::OK), [
            'clear_result' => 1,
        ]);
    }


    ///////////////////////////////Рабочие функции//////////////////////////////////////////////////

    /**
     * Функция перевода на другие языки
     *
     * @param type $category
     * @param type $message
     * @param type $params
     * @param type $language
     *
     * @return string
     */
    protected function t(
        $category,
        $message,
        $params = []
        ,
        $language = null
    ) {
        return Yii::t($category, $message, $params, $language);
    }


    /**
     * Проверка запроса на коорректность параметров и сигнатуры
     * @param $params
     * @param bool $needChekSign
     * @return mixed
     */
    protected function checkParams($params, $needChekSign = true)
    {
        $commandName   = $this->action->id;
        $request       = new TaxiApiRequest();
        $headersObject = Yii::$app->request->getHeaders();
        $headers       = $headersObject->toArray();
        $tenantLogin   = null;

        if (isset($params['tenant_login'])) {
            $tenantLogin = $params['tenant_login'];
            $tenantLogin = mb_strtolower($tenantLogin);
        } else {
            $tenantLogin = $this->getTenantLogin($params);
        }

        return $request->validateParams($commandName, $headers, $params, $needChekSign, $tenantLogin);
    }

    /**
     * Получить параметры запроса
     *
     * @param array $array
     *
     * @return array
     */
    protected function getParams($array)
    {
        $params = $array;
        unset($params['q']);
        $requset = new TaxiApiRequest();

        return $requset->filterParams($params);
    }

    /**
     * Получить device id устройства
     * @return string
     */
    protected function getDeviceId()
    {
        $headersObject = Yii::$app->request->getHeaders();
        $headers       = $headersObject->toArray();
        $deviceUdid    = $headers['deviceid']['0'];

        return $deviceUdid;
    }

    /**
     * Получить тип устройства исполнителя
     */
    protected function getWorkerDevice()
    {
        return \Yii::$app->request->headers->get('Worker-Device', null, true);
    }

    /**
     * Получить язык
     * @return string
     */
    protected function getLang()
    {
        $supportedLangs = Yii::$app->params['supportedLanguages'];
        $lang           = \Yii::$app->request->headers->get('Lang', null, true);

        if (!in_array($lang, $supportedLangs)) {
            $lang = 'en';
        }

        return $lang;
    }

    /**
     * Получить tenant_id по параметрам запроса
     *
     * @param array $params
     *
     * @return int
     */
    protected function getTenantId($params)
    {
        if (isset($params['tenant_login']
        )) {
            $domain = $params['tenant_login'];
            $domain = mb_strtolower($domain);
            $tenant = Tenant::find()
                ->where(['domain' => $domain])
                ->one();
            if (!empty($tenant)) {
                return $tenant->tenant_id;
            }
        }

        return null;
    }

    /**
     * Получить domain по парамтерам запроса.
     *
     * @param array $params
     *
     * @return string
     */
    protected function getTenantLogin($params)
    {
        if (isset($params['tenant_id'])) {
            $tenant_id = $params['tenant_id'];
            $tenant    = Tenant::find()
                ->where(['tenant_id' => $tenant_id])
                ->one();
            if (!empty($tenant)) {
                return $tenant->domain;
            }
        }

        return null;
    }

    /**
     * Check current version
     *
     * @param string $version
     *
     * @return bool
     */
    protected function isCurrentVersionGreaterThanOrEqualTo($version)
    {
        $currentVersion = \Yii::$app->request->headers->get('version', self::DEFAULT_VERSION);

        return Comparator::greaterThanOrEqualTo($currentVersion, $version);
    }
}