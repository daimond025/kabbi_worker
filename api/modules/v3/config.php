<?php

return [
    'components' => [
        'positionService'     => \v3\components\PositionService::className(),
        'workerService'       => \v3\components\WorkerService::className(),
        'workerTariffService' => \v3\components\WorkerTariffService::className(),
        'workerShiftService'  => \v3\components\WorkerShiftService::className(),
        'orderService'        => \v3\components\OrderService::className(),
        'parkingService'      => \v3\components\ParkingService::className(),
    ],
];