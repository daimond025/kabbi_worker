<?php

namespace logger;

use logger\handlers\BasicHandler;
use logger\handlers\LogHandlerInterface;

/**
 * Class LogHandlerFactory
 * @package logger
 */
class LogHandlerFactory
{
    /**
     * Getting log handler
     *
     * @param string $method
     *
     * @return LogHandlerInterface
     */
    public function getHandler($method)
    {
        switch ($method) {
            case '/v3/api/set_worker_position':
                return new BasicHandler([
                    'lat'        => ['name' => 'worker_lat'],
                    'lon'        => ['name' => 'worker_lon'],
                    'accuracy'   => ['name' => 'coordinate_accuracy'],
                    'updateTime' => ['name' => 'update_time'],
                ], [
                    'status'     => 'result.worker_status',
                    'serverTime' => 'result.server_time',
                    'cityTime'   => 'result.city_time',
                ]);
            case '/v3/api/get_worker_balance':
                return new BasicHandler([], [
                    'balance'  => 'result.worker_balance',
                    'currency' => 'result.currency',
                ]);
            case '/v3/api/worker_start_work':
                return new BasicHandler([
                    'tariff' => ['name' => 'worker_tariff_id'],
                    'car'    => ['name' => 'worker_car_id'],
                    'city'   => ['name' => 'worker_city_id'],
                    'app'    => ['name' => 'app_version'],
                    'device' => ['name' => 'Worker-Device', 'isHeader' => true],
                    'info'   => ['name' => 'user-agent', 'isHeader' => true],
                    'token'  => ['name' => 'device_token'],
                ], [
                    'shift' => 'result.worker_shift_id',
                ]);
            case '/v3/api/worker_order_unblock':
            case '/v3/api/worker_pre_order_unblock':
                return new BasicHandler([
                    'block' => ['name' => 'block_id'],
                ]);
            case '/v3/api/get_parkings_list':
            case '/v3/api/get_city_polygone':
            case '/v3/api/get_orders_pretime_list':
                return new BasicHandler([
                    'city' => ['name' => 'worker_city_id'],
                ]);
            case '/v3/api/get_worker_profile':
                return new BasicHandler([
                    'cityId'     => ['name' => 'worker_city_id'],
                    'positionId' => ['name' => 'position_id'],
                ]);
            case '/v3/api/set_parking_queue':
                return new BasicHandler([
                    'parking' => ['name' => 'parking_id'],
                ]);
            case '/v3/api/get_parkings_orders_queues':
                return new BasicHandler([
                    'city'    => ['name' => 'worker_city_id'],
                    'parking' => ['name' => 'parking_id'],
                ]);
            case '/v3/api/get_orders_open_list':
                return new BasicHandler([
                    'city'    => ['name' => 'worker_city_id'],
                    'lat'     => ['name' => 'worker_lat'],
                    'lon'     => ['name' => 'worker_lon'],
                    'parking' => ['name' => 'parking_id'],
                ]);
            case '/v3/api/get_order_for_id':
            case '/v3/api/set_order_route':
            case '/v3/api/set_order_route_file':
            case '/v3/api/set_order_raw_calc':
            case '/v3/api/get_order_raw_calc':
            case '/v3/api/update_order':
                return new BasicHandler([
                    'orderId' => ['name' => 'order_id'],
                ]);
            case '/v3/api/set_order_status':
                return new BasicHandler([
                    'orderId' => ['name' => 'order_id'],
                    'status'  => ['name' => 'status_new_id'],
                ]);
            case '/v3/api/check_worker_on_work':
                return new BasicHandler([], [
                    'status' => 'result.worker_status',
                ]);
            case '/v3/api/set_signal_sos':
            case '/v3/api/unset_signal_sos':
                return new BasicHandler([
                    'lat' => ['name' => 'worker_lat'],
                    'lon' => ['name' => 'worker_lon'],
                ]);
            case '/v3/api/get_city_list':
                return new BasicHandler([
                    'city' => ['name' => 'city_part'],
                    'lang' => ['name' => 'lang', 'isHeader' => true],
                ]);
            case '/v3/api/get_geoobjects_list':
                return new BasicHandler([
                    'city'   => ['name' => 'city_id'],
                    'street' => ['name' => 'street_part'],
                    'lang'   => ['name' => 'lang', 'isHeader' => true],
                ]);
            case '/v3/api/set_worker_state':
                return new BasicHandler([
                    'action'  => ['name' => 'action'],
                    'comment' => ['name' => 'comment'],
                ]);
            case '/v3/api/call_cost':
                return new BasicHandler([
                    'tariff' => ['name' => 'tariff_id'],
                    'date'   => ['name' => 'order_date'],
                ], [
                    'cost' => 'result.cost_data.summary_cost',
                ]);
            case '/v3/api/create_order':
                return new BasicHandler([
                    'tariff' => ['name' => 'tariff_id'],
                    'client' => ['name' => 'client_phone'],
                ], [
                    'orderId' => 'result.order_id',
                    'order#'  => 'result.order_number',
                ]);
            case '/v3/api/create_bank_card':
                return new BasicHandler([], [
                    'paymentOrderId' => ['name' => 'result.orderId'],
                ]);
            case '/v3/api/check_bank_card':
                return new BasicHandler([
                    'orderId' => ['name' => 'order_id'],
                ]);
            case '/v3/api/delete_bank_card':
                return new BasicHandler([
                    'pan' => ['name' => 'pan'],
                ]);
            case '/v3/api/refill_account':
                return new BasicHandler([
                    'pan'      => ['name' => 'pan'],
                    'sum'      => ['name' => 'sum'],
                    'currency' => ['name' => 'currency'],
                ]);
            default:
                return new BasicHandler();
        }
    }
}