<?php

namespace logger\handlers;

use yii\helpers\ArrayHelper;
use yii\web\Request;
use yii\web\Response;

/**
 * Class BasicHandler
 * @package logger\handlers
 */
class BasicHandler implements LogHandlerInterface
{
    const DEFAULT_REQUEST_PARAMS = [
        't'  => ['name' => 'tenant_login'],
        'w'  => ['name' => 'worker_login'],
        'id' => ['name' => 'Request-Id', 'isHeader' => true],
        'v'  => ['name' => 'Version', 'isHeader' => true],
    ];

    /**
     * @var array
     */
    private $requestParams;

    /**
     * @var array
     */
    private $responseParams;

    /**
     * DefaultHandler constructor.
     *
     * @param array $requestParams
     * @param array $responseParams
     */
    public function __construct(array $requestParams = [], array $responseParams = [])
    {
        $this->requestParams  = $requestParams;
        $this->responseParams = $responseParams;
    }

    /**
     * Getting prepared request params
     *
     * @param Request $request
     *
     * @return string
     */
    private function getPreparedRequestParams(Request $request)
    {
        $requestParams = ArrayHelper::merge(self::DEFAULT_REQUEST_PARAMS, $this->requestParams);

        $result = [];
        foreach ($requestParams as $key => $param) {
            if (empty($param['isHeader'])) {
                $value = $request->isGet
                    ? $request->getQueryParam($param['name']) : $request->getBodyParam($param['name']);
            } else {
                $value = $request->headers->get($param['name']);
            }

            $result[] = "{$key}=\"{$value}\"";
        }

        return implode(',', $result);
    }

    /**
     * Getting prepared response params
     *
     * @param Response $response
     *
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    private function getPreparedResponseParams(Response $response)
    {
        $content = json_decode($response->content, true);

        $code = isset($content['code']) ? $content['code'] : '';
        $info = isset($content['info']) ? $content['info'] : '';

        $result = ["{$code} {$info}"];
        if ($response->statusCode == 200 && $code == 0) {
            foreach ($this->responseParams as $key => $param) {
                $value    = ArrayHelper::getValue($content, $param);
                $result[] = "{$key}=\"{$value}\"";
            }
        }

        return implode(',', $result);
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function process(Request $request, Response $response)
    {
        $preparedRequestParams  = $this->getPreparedRequestParams($request);
        $preparedResponseParams = $this->getPreparedResponseParams($response);

        return "({$preparedRequestParams} => {$preparedResponseParams})";
    }
}