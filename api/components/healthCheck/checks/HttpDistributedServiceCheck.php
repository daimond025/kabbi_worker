<?php

namespace healthCheck\checks;

use GuzzleHttp\Client;
use healthCheck\exceptions\HealthCheckException;

/**
 * Class HttpDistributedServiceCheck
 * @package healthCheck\checks
 */
class HttpDistributedServiceCheck extends BaseCheck
{

    /**
     * @var string name of the service in consul
     */
    private $consulServiceName;

    /**
     * @var string  check method name
     */
    private $checkMethodName;

    /**
     * @var int hhtp status code
     */
    private $statusCode;

    /**
     * HttpDistributedServiceCheck constructor.
     *
     * @param string $name
     * @param string $consulServiceName
     * @param string $checkMethodName
     * @param int $statusCode
     */
    public function __construct($name, $consulServiceName, $checkMethodName, $statusCode = 200)
    {

        $this->consulServiceName = $consulServiceName;
        $this->checkMethodName   = $checkMethodName;
        $this->statusCode        = $statusCode;
        parent::__construct($name);
    }

    /**
     * Run check
     */
    public function run()
    {
        try {
            $client           = new Client();
            $response         = $client->get($this->getUrl($this->consulServiceName, $this->checkMethodName));
            $actualStatusCode = (string)$response->getStatusCode();
            if ($actualStatusCode !== (string)$this->statusCode) {
                throw new HealthCheckException("Incorrect status code [{$actualStatusCode}]");
            }
        } catch (\Exception $ex) {
            throw new HealthCheckException(
                'Check service exception: error=' . $ex->getMessage() . ', code=' . $ex->getCode(), 0, $ex);
        }
    }


    /**
     * @param $consulServiceName
     * @param $checkMethodName
     * @return mixed
     */
    private function getUrl($consulServiceName, $checkMethodName)
    {
        try {
            return \Yii::$app->get('consulService')->createMethodUrl($consulServiceName, $checkMethodName);
        } catch (\Exception $ex) {
            throw new HealthCheckException(
                'Error of getting location from consul for this service: error=' . $ex->getMessage() . ', code=' . $ex->getCode(), 0, $ex);
        }

    }
}