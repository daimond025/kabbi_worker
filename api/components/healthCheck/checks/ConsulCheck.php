<?php


namespace healthCheck\checks;

use app\components\consul\ConsulService;
use healthCheck\exceptions\HealthCheckException;

/**
 * Class ConsulCheck
 * @package healthCheck\checks
 */
class ConsulCheck extends BaseCheck
{
    private $consulService;

    public function __construct($name, ConsulService $consulService)
    {
        $this->consulService = $consulService;
        parent::__construct($name);
    }

    public function run()
    {
        try {
            $this->consulService->getAgentInfo();
        } catch (HealthCheckException $ex) {
            throw new HealthCheckException(
                'Check ConsulCheck exception: error=' . $ex->getMessage() . ', code=' . $ex->getCode(), 0, $ex);
        }
    }
}