<?php

namespace api\components\orderApi;

use api\components\orderApi\interfaces\createOrder\ApiOrderInterface;
use app\models\order\Order;
use GuzzleHttp\Client;
use api\components\orderApi\exceptions\OrderApiException;
use GuzzleHttp\Exception\ClientException;
use yii\base\Object;

/**
 * Class OrderApi
 * @package frontend\components
 */
class OrderApi extends Object
{
    const STATUS_USER_ERROR_400 = 400;
    const STATUS_USER_ERROR_422 = 422;

    /* @var string */
    public $baseUrl;

    /* @var int */
    public $timeout = 15;

    /* @var int */
    public $connectionTimeout = 15;

    /* @var Client */
    private $httpClient;

    /**
     * @inherited
     */
    public function init()
    {
        parent::init();

        $this->httpClient = new Client([
            'base_uri'          => $this->baseUrl,
            'connectionTimeout' => $this->connectionTimeout,
            'timeout'           => $this->timeout,
        ]);
    }

    public function getInfoPromoCode($codeId)
    {

        try {
            $response = $this->httpClient->request('GET', "promocode/promo/promo_code_info?code_id={$codeId}");

            return json_decode($response->getBody(), true);
        } catch (\Exception $ex) {
            throw new OrderApiException(
                'An error occurred while retrieving promotional code information', 0, $ex);
        }

    }

    public function createOrder(ApiOrderInterface $order)
    {
        try {
            $response = $this->httpClient->request('POST', 'v1/order/create', [
                'form_params' => [
                    'city_id'                 => $order->getCityId(),
                    'order_now'               => $order->getOrderNow(),
                    'order_date'              => $order->getOrderDate(),
                    'order_hours'             => $order->getOrderHours(),
                    'order_minutes'           => $order->getOrderMinutes(),
                    'phone'                   => $order->getPhone(),
                    'client_id'               => $order->getClientId(),
                    'comment'                 => $order->getComment(),
                    'bonus_payment'           => $order->getBonusPayment(),
                    'payment'                 => $order->getPayment(),
                    'position_id'             => $order->getPositionId(),
                    'tariff_id'               => $order->getTariffId(),
                    'predv_price'             => $order->getPredvPrice(),
                    'predv_distance'          => $order->getPredvDistance(),
                    'predv_time'              => $order->getPredvTime(),
                    'predv_price_no_discount' => $order->getPredvPriceNoDiscount(),
                    'is_fix'                  => $order->getIsFix(),
                    'orderAction'             => $order->getOrderAction(),
                    'parking_id'              => $order->getParkingId(),
                    'company_id'              => $order->getCompanyId(),
                    'device'                  => $order->getDevice(),
                    'tenant_id'               => $order->getTenantId(),
                    'user_modifed'            => $order->getUserModifedId(),
                    'user_create'             => $order->getUserCreatedId(),
                    'call_id'                 => $order->getCallId(),
                    'worker_id'               => $order->getWorkerId(),
                    'car_id'                  => $order->getCarId(),

                    'address' => $order->getAddressOriginal(),
                ],
            ]);

            $res = json_decode($response->getBody(), true);

            return new Order($res);
        } catch (\Exception $ex) {
            $this->throwValidateErrors($ex);

            throw new OrderApiException('Error create order', 0, $ex);
        }
    }

    protected function throwValidateErrors($ex)
    {
        if ($this->isUserErrors($ex)) {
            throw $ex;
        }
    }

    protected function isUserErrors(\Exception $exception)
    {
        $isInstance = $exception instanceof ClientException;

        /** @var ClientException $exception */
        return $isInstance && in_array($exception->getResponse()->getStatusCode(), [
                self::STATUS_USER_ERROR_400,
                self::STATUS_USER_ERROR_422,
            ]);
    }
}
