<?php

namespace api\components\orderApi\exceptions;

use yii\base\Exception;

/**
 * Class OrderApiException
 */
class OrderApiException extends Exception
{

}
