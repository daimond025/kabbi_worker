<?php

namespace api\components\orderApi\interfaces\createOrder;

interface ApiOrderInterface
{
    public function getCityId();
    public function getOrderNow();
    public function getOrderDate();
    public function getOrderHours();
    public function getOrderMinutes();
    public function getPhone();
    public function getClientId();
    public function getComment();
    public function getPayment();
    public function getBonusPayment();
    public function getPositionId();
    public function getTariffId();
    public function getTenantId();
    public function getUserModifedId();
    public function getUserCreatedId();
    public function getCallId();
    public function getWorkerId();
    public function getCarId();

    public function getPredvPrice();
    public function getPredvDistance();
    public function getPredvTime();
    public function getPredvPriceNoDiscount();

    public function getIsFix();
    public function getOrderAction();
    public function getParkingId();
    public function getCompanyId();
    public function getDevice();
    public function getAdditionalOption();

    public function getAddressOriginal();
}
