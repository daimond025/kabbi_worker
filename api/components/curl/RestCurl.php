<?php

namespace api\components\curl;

use api\components\curl\Curl;
use api\components\curl\CurlResponse;
use api\components\curl\exceptions\RestException;

/**
 * Rest wrapper of Curl component
 */
class RestCurl extends \yii\base\Object
{
    /**
     * @var Curl
     */
    private $curl;

    public function init()
    {
        $this->curl = new Curl();
        parent::init();
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers = [])
    {
        $this->curl->headers = $headers;
    }

    /**
     * Execute get request
     *
     * @param string $url
     *
     * @throws RestException
     * @return array
     */
    public function get($url)
    {
        /* @var $response CurlResponse|boolean */
        $response = $this->curl->get($url);

        if ($response === false) {
            throw new RestException($this->curl->error());
        } else {
            $result     = json_decode($response->body, true);
            $statusCode = $response->headers['Status-Code'];

            switch ($statusCode) {
                case 200:
                    return compact('result', 'statusCode');
                case 404:
                    return null;
                default:
                    $message = empty($result['message'])
                        ? $this->curl->error() : $result['message'];
                    throw new RestException($message, $statusCode, $result);
            }
        }
    }

    /**
     * Execute create request
     *
     * @param string $url
     * @param array  $params
     *
     * @return array|null
     * @throws RestException
     */
    public function post($url, $params = null)
    {
        /* @var $response CurlResponse|boolean */
        $response = $this->curl->post($url, $params);

        if ($response === false) {
            throw new RestException($this->curl->error());
        } else {
            $result     = json_decode($response->body, true);
            $statusCode = $response->headers['Status-Code'];

            switch ($statusCode) {
                case 200:
                case 201:
                case 202:
                case 204:
                    return compact('result', 'statusCode');
                default:
                    $message = empty($result['message'])
                        ? $this->curl->error() : $result['message'];
                    throw new RestException($message, $statusCode, $result);
            }
        }
    }

    /**
     * Execute update request
     *
     * @param string $url
     * @param array  $params
     *
     * @return array|null
     * @throws RestException
     */
    public function put($url, $params = null)
    {
        /* @var $response CurlResponse|boolean */
        $response = $this->curl->put($url, $params);

        if ($response === false) {
            throw new RestException($this->curl->error());
        } else {
            $result     = json_decode($response->body, true);
            $statusCode = $response->headers['Status-Code'];

            switch ($statusCode) {
                case 200:
                case 201:
                case 202:
                case 204:
                    return compact('result', 'statusCode');
                default:
                    $message = empty($result['message'])
                        ? $this->curl->error() : $result['message'];
                    throw new RestException($message, $statusCode, $result);
            }
        }
    }

    /**
     * Execute delete request
     *
     * @param string $url
     * @param array  $params
     *
     * @return boolean
     * @throws RestException
     */
    public function delete($url, $params = null)
    {
        /* @var $response CurlResponse|boolean */
        $response = $this->curl->delete($url, $params);

        if ($response === false) {
            throw new RestException($this->curl->error());
        } else {
            $result     = json_decode($response->body, true);
            $statusCode = $response->headers['Status-Code'];

            switch ($statusCode) {
                case 200:
                case 201:
                case 202:
                case 204:
                    return compact('result', 'statusCode');
                default:
                    $message = empty($result['message'])
                        ? $this->curl->error() : $result['message'];
                    throw new RestException($message, $statusCode, $result);
            }
        }
    }

}