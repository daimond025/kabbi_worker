<?php


namespace api\components;


use DateTime;
use DateTimeZone;
use yii\base\Object;

class TimeZone extends Object
{
    private $_timezones = null;

    /**
     * Getting timezone map
     * @return array|null
     */
    public function getTimeZoneMap()
    {
        if ($this->_timezones === null) {
            $this->_timezones = [];
            $offsets          = [];
            $now              = new DateTime();

            foreach (DateTimeZone::listIdentifiers() as $timezone) {
                $now->setTimezone(new DateTimeZone($timezone));
                $offsets[]                   = $offset = $now->getOffset();
                $this->_timezones[$timezone] = '(' . $this->formatGmtOffset($offset) . ') ' . $this->formatTimezoneName($timezone);
            }

            array_multisort($offsets, $this->_timezones);
        }

        return $this->_timezones;
    }

    /**
     * @param string $timezone
     *
     * @return int
     */
    public function getLocalTimestamp($timezone)
    {
        $dateTimeZone = new DateTimeZone($timezone);
        $dateTime     = new DateTime("now", $dateTimeZone);

        return $dateTime->getTimestamp() + $dateTime->getOffset();
    }

    public function getTimeOffsetByTimezone($timezone)
    {
        return $this->getDateTime($timezone)->getOffset();
    }

    /**
     * @param int    $timestamp Local timestamp
     * @param string $timezone
     * @param bool   $inHours
     *
     * @return int
     */
    public function getTimeOffsetByLocalTimestamp($timestamp, $timezone, $inHours = false)
    {
        $dateTime = $this->getDateTime($timezone);
        $dateTime->setTimestamp($timestamp);
        $timeOffset = $dateTime->getOffset();

        return $inHours ? $timeOffset / 3600 : $timeOffset;
    }

    /**
     * @param string $timezone
     *
     * @return DateTime
     */
    public function getDateTime($timezone)
    {
        $dateTimeZone = new DateTimeZone($timezone);

        return new DateTime("now", $dateTimeZone);
    }

    /**
     * @param int $offset Time offset in seconds
     *
     * @return string
     */
    private function formatGmtOffset($offset)
    {
        $hours   = intval($offset / 3600);
        $minutes = abs(intval($offset % 3600 / 60));

        return 'GMT' . ($offset ? sprintf('%+03d:%02d', $hours, $minutes) : '');
    }

    /**
     * @param string $name
     *
     * @return string
     */
    private function formatTimezoneName($name)
    {
        $name = str_replace('/', ', ', $name);
        $name = str_replace('_', ' ', $name);
        $name = str_replace('St ', 'St. ', $name);

        return $name;
    }
}