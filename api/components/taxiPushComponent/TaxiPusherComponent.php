<?php

namespace app\components\taxiPushComponent;

use yii\base\Object;
use Yii;

/**
 * Компонент  для работы  c сервисом пушей
 * @author Сергей К.
 */
class TaxiPusherComponent extends Object
{
    /**
     * @var string
     */
    public $url;

    /**
     * taxi-push-api notification
     * @var string
     */
    public $controller = "notification";

    /**
     * Timeout of requst to taxi-push-api
     * @var int
     */
    public $timeout = 15;

    public function init()
    {
        parent::init();
    }

    /**
     *  Send SOS to workers
     *
     * @param int    $tenantId
     * @param int    $workerCallsign
     * @param string $workerLat
     * @param string $workerLon
     *
     * @return boolean
     */
    public function setSignalSos($tenantId, $workerCallsign, $workerLat, $workerLon)
    {
        $params = [
            'tenant_id'       => $tenantId,
            'worker_callsign' => $workerCallsign,
            'worker_lat'      => $workerLat,
            'worker_lon'      => $workerLon,
        ];
        $result = $this->sendGetRequest('set_signal_sos', $params);

        return isset($result['result']) && $result['result'] == 1;
    }

    /**
     *  Unset SOS to workers
     *
     * @param int    $tenantId
     * @param int    $workerCallsign
     * @param string $workerLat
     * @param string $workerLon
     *
     * @return boolean
     */
    public function unsetSignalSos($tenantId, $workerCallsign, $workerLat, $workerLon)
    {
        $params = [
            'tenant_id'       => $tenantId,
            'worker_callsign' => $workerCallsign,
            'worker_lat'      => $workerLat,
            'worker_lon'      => $workerLon,
        ];
        $result = $this->sendGetRequest('unset_signal_sos', $params);

        return isset($result['result']) && $result['result'] == 1;
    }

    /**
     * Send GET request to taxi-push-api
     *
     * @param string $method
     * @param array  $params
     *
     * @return bool|mixed
     */
    private function sendGetRequest($method, $params)
    {
        if (is_array($params)) {
            $params = http_build_query($params);
        }

        if (strlen($params) > 0) {
            $url = $this->getUrl($method) . "?" . $params;
        } else {
            $url = $this->getUrl($method);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        $result    = curl_exec($ch);
        $errorCode = curl_errno($ch);
        curl_close($ch);

        return ($errorCode == CURLE_OK) ? json_decode($result, true) : false;
    }

    /**
     * Send POST request  taxi-push-api
     *
     * @param string $method
     * @param array  $params
     *
     * @return bool|mixed
     */
    private function sendPostRequest($method, $params)
    {
        $url = $this->getUrl($method);
        $ch  = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $result    = curl_exec($ch);
        $errorCode = curl_errno($ch);
        curl_close($ch);

        return ($errorCode == CURLE_OK) ? json_decode($result, true) : false;
    }

    /**
     * Get url of taxi-push-api  method
     *
     * @param string $method
     *
     * @return string
     */
    private function getUrl($method)
    {
        return "{$this->url}{$this->controller}/{$method}";
    }

}
