<?php

namespace app\components\consul;

use yii\base\Component;
use SensioLabs\Consul\ServiceFactory;
use app\components\consul\exceptions\ConsulServiceException;

class ConsulService extends Component
{
    /**
     * @var ServiceFactory
     */
    private $serviceFactory;

    /**
     * @var string $consulAgentHost
     */
    public $consulAgentHost;

    /**
     * @var integer $consulAgentPort
     */
    public $consulAgentPort;

    public function init()
    {
        $this->serviceFactory = new ServiceFactory(['base_uri' => 'http://' . $this->consulAgentHost . ':' . $this->consulAgentPort]);
        parent::init();
    }

    public function getAgentInfo()
    {
        $agent = $this->serviceFactory->get('agent');
        if (!json_decode($agent->self()->getBody(true), true)) {
            throw new ConsulServiceException('Error of getting agent info');
        }
    }


    /**
     * Get 'catalog' service
     * @return mixed
     */
    protected function getCatalogService()
    {
        return $this->serviceFactory->get('catalog');
    }

    /**
     * Get 'health' service
     * @return mixed
     */
    protected function getHealthService()
    {
        return $this->serviceFactory->get('health');
    }

    /**
     * Lists the nodes in a given service
     * @param string $serviceName
     * @return array
     */
    public function getCatalogOfServicesByName($serviceName)
    {
        $catalog = $this->getCatalogService();
        return json_decode($catalog->service($serviceName)->getBody(true), true);
    }

    /**
     * Lists the healthy in a given service
     * @param string $serviceName
     * @return array
     */
    public function getHealthOfServicesByName($serviceName)
    {
        $health = $this->getHealthService();
        return json_decode($health->service($serviceName)->getBody(true), true);
    }

    /**
     * Get healthy services location
     * @param string $serviceName
     * @return array
     * @throws ConsulServiceException
     */
    public function getHealthyServicesLocation($serviceName)
    {
        $listofHealthyServicesLocation = [];
        try {
            $listOfServices = $this->getHealthOfServicesByName($serviceName);
        } catch (\Exception $ex) {
            throw new ConsulServiceException('Error of getting helthy services wiht name:' . $serviceName, 0, $ex);
        }

        foreach ($listOfServices as $serviceInfo) {
            $checksCount = count($serviceInfo['Checks']);
            $checksGoodCount = 0;
            foreach ($serviceInfo['Checks'] as $check) {
                if ($check["Status"] == "passing") {
                    $checksGoodCount++;
                }
            }
            if ($checksGoodCount == $checksCount) {
                $listofHealthyServicesLocation[] = ['host' => $serviceInfo['Service']['Address'], 'port' => $serviceInfo['Service']['Port']];
            }
        }
        return $listofHealthyServicesLocation;
    }

    /**
     * Get random healthy service location
     * @param $serviceName
     * @return mixed
     * @throws ConsulServiceException
     */
    public function getRandomHealthyServiceLocation($serviceName)
    {
        $serviceLocations = $this->getHealthyServicesLocation($serviceName);
        $location = $serviceLocations[array_rand($serviceLocations)];
        if (!isset($location['host'])) {
            throw new ConsulServiceException('No healthy nodes for service:' . $serviceName);
        }
        return $location;
    }

    /**
     * Create method url
     * @param string $serviceName
     * @param string $method
     * @return string
     */
    public function createMethodUrl($serviceName, $method)
    {
        $location = $this->getRandomHealthyServiceLocation($serviceName);
        return "http://" . $location['host'] . ":" . $location["port"] . "/" . $method;
    }


}