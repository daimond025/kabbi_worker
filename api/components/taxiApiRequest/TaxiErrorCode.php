<?php

namespace app\components\taxiApiRequest;

use yii\base\Object;
use Yii;
use DateTime;

class TaxiErrorCode extends Object
{
    const OK = 0;
    const INTERNAL_ERROR = 1;
    const INCORRECTLY_SIGNATURE = 2;
    const UNKNOWN_REQUEST = 3;
    const BAD_PARAM = 4;
    const MISSING_INPUT_PARAMETER = 5;
    const EMPTY_VALUE = 6;
    const EMPTY_DATA_IN_DATABASE = 7;
    const BLACK_LIST = 8;
    const ACCESS_DENIED = 9;
    const DRIVER_NOT_EXIST = 10;
    const DRIVER_ALLREADY_ON_SHIFT = 40;
    const BAD_BALANCE = 41;
    const CAR_IS_BUSY = 42;
    const BAD_TARIFF = 44;
    const SHIFT_IS_CLOSED = 50;
    const PARKING_NOT_EXIST = 60;
    const ORDER_IS_BUSY = 70;
    const DRIVER_BLOCKED = 80;
    const DRIVER_PRE_ORDER_BLOCKED = 81;
    const OLD_APP_VERSION = 82;
    const DRIVER_LIMIT = 83;
    const CITY_BLOCKED = 84;
    const DRIVER_ALLREADY_BLOCKED = 85;

    public $errorCodeData = array(
        '0'  => 'OK',
        '1'  => 'INTERNAL_ERROR',
        '2'  => 'INCORRECTLY_SIGNATURE',
        '3'  => 'UNKNOWN_REQUEST',
        '4'  => 'BAD_PARAM',
        '5'  => 'MISSING_INPUT_PARAMETER',
        '6'  => 'EMPTY_VALUE',
        '7'  => 'EMPTY_DATA_IN_DATABASE',
        '8'  => 'BLACK_LIST',
        '9'  => 'ACCESS_DENIED',
        '10' => 'DRIVER_NOT_EXIST',
        '40' => 'DRIVER_ALLREADY_ON_SHIFT',
        '41' => 'BAD_BALANCE',
        '42' => 'CAR_IS_BUSY',
        '44' => 'BAD_TARIFF',
        '50' => 'SHIFT_IS_CLOSED',
        '60' => 'PARKING_NOT_EXIST',
        '70' => 'ORDER_IS_BUSY',
        '80' => 'DRIVER_BLOCKED',
        '81' => 'DRIVER_PRE_ORDER_BLOCKED',
        '82' => 'OLD_APP_VERSION',
        '83' => 'DRIVER_LIMIT',
        '84' => 'CITY_BLOCKED',
        '85' => 'DRIVER_ALLREADY_BLOCKED'
    );

}