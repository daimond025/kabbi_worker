<?php

namespace app\components\taxiApiRequest;

use yii\base\Object;
use Yii;
use DateTime;
use app\components\taxiApiRequest\TaxiApiMethods;
use app\components\taxiApiRequest\TaxiErrorCode;
use yii\db\ActiveRecord;

/**
 * Класс обеспечивающий стандартизацию ответа от апи
 * Анализатор маршрута
 * @author Сергей К.
 */
class TaxiApiRequest extends Object
{

    public $methods;
    public $errCode;
    public $dataBaseLayer;
    public $validateData;

    public function __construct($config = [])
    {
        $this->methods = new TaxiApiMethods();
        $this->errCode = new TaxiErrorCode();
        $this->validateData['code'] = null;
        $this->validateData['info'] = null;
        parent::__construct($config);
    }

    private function setValidateDataFull($code, $param = null)
    {
        $this->validateData['code'] = $code;
        $this->validateData['info'] = $this->errCode->errorCodeData[$code];
        if (!empty($param)) {
            $this->validateData['info'] = $this->validateData['info'] . " $param";
        }
        return $this->validateData;
    }

    public function validateParams($commandName, $headers, $params, $needChekSign, $tenantLogin)
    {
        $signature = isset($headers['signature']['0']) ? $headers['signature']['0'] : null;

        Yii::info("HEEEADERS!: " . json_encode($headers));
        if ($needChekSign) {
            if (empty($signature)) {
                return $this->setValidateDataFull(TaxiErrorCode::INCORRECTLY_SIGNATURE);
            }
            if (!(TaxiErrorCode::OK == $this->validateSignature($signature, $tenantLogin, $params))) {
                return $this->setValidateDataFull(TaxiErrorCode::INCORRECTLY_SIGNATURE);
            }
        }
        $validateResult = $this->methods->validateParams($commandName, $params);
        return $this->setValidateDataFull($validateResult['code'], $validateResult['param']);
    }

    public function filterParams($params)
    {
        $paramsFiltered = array();
        foreach ($params as $paramKey => $paramVal) {
            if (is_string($paramVal)) {
                $paramVal = urldecode($paramVal);
                $paramVal = str_replace('\\', '/', $paramVal);
                $paramVal = trim(strip_tags(stripcslashes($paramVal)));
            }
            $paramsFiltered[$paramKey] = $paramVal;
        }
        return $paramsFiltered;
    }

    public function getJsonAnswer($validateData, $result = null)
    {
        $response = array(
            'code'   => $validateData['code'],
            'info'   => $validateData['info'],
            'result' => $result
        );
        return json_encode($response);
    }

    private function validateSignature($signature, $tenant_login, $params)
    {
        $apiKey = $this->getTenantApiKey($tenant_login);
        $params = http_build_query($params, '', '&', PHP_QUERY_RFC3986);
        $sign = md5($params . $apiKey);
        Yii::info("PARAMS!: $params");
        Yii::info("API KEY!: $apiKey");
        Yii::info("MY SIGNATURE!: $sign");
        Yii::info("OUT SIGNATURE!: $signature");
        if ($sign == $signature) {
            return TaxiErrorCode::OK;
        } else {
            Yii::error("INCORRECTLY_SIGNATURE");
            Yii::error("PARAMS!: $params");
            Yii::error("API KEY!: $apiKey");
            Yii::error("MY SIGNATURE!: $sign");
            Yii::error("OUT SIGNATURE!: $signature");
            return TaxiErrorCode::INCORRECTLY_SIGNATURE;
        }
    }

    public static function getTenantApiKey($tenant_login)
    {
        $tenantData = \app\models\tenant\Tenant::find()
                ->where(['domain' => $tenant_login])
                ->asArray()
                ->one();
        $tenantId = $tenantData['tenant_id'];
        $tenantSettings = \app\models\tenant\TenantSetting::find()
                ->where(['tenant_id' => $tenantId])
                ->andWhere(['name' => 'WORKER_API_KEY'])
                ->one();
        return $tenantSettings->value;
    }

}
