<?php

namespace app\components\taxiApiRequest;

use yii\base\Object;
use Yii;
use DateTime;
use app\components\taxiApiRequest\TaxiErrorCode;

/*
 * Для общей работы с набором методов АПИ, их параметров
 */

class TaxiApiMethods extends Object
{
    const REQUARED = '!REQ!';

    public $validateData;

    /**
     * Карта для исправления и автозаполнения пареметров коммнад апи
     * @var array
     */
    private $_fixParamsMap = array(
        'auth_driver'                => array(
            'tenant_login'    => TaxiApiMethods::REQUARED,
            'driver_login'    => TaxiApiMethods::REQUARED,
            'driver_password' => TaxiApiMethods::REQUARED,
            'device_token'    => TaxiApiMethods::REQUARED,
        ),
        'set_driver_position'        => array(
            'tenant_login'  => TaxiApiMethods::REQUARED,
            'driver_login'  => TaxiApiMethods::REQUARED,
            'driver_lat'    => TaxiApiMethods::REQUARED,
            'driver_lon'    => TaxiApiMethods::REQUARED,
            'driver_degree' => TaxiApiMethods::REQUARED,
            'driver_speed'  => TaxiApiMethods::REQUARED,
        ),
        'get_driver_profile'         => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
        ),
        'get_driver_balance'         => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
        ),
        'driver_start_work'          => array(
            'tenant_login'     => TaxiApiMethods::REQUARED,
            'driver_login'     => TaxiApiMethods::REQUARED,
            'driver_tariff_id' => TaxiApiMethods::REQUARED,
            'driver_car_id'    => TaxiApiMethods::REQUARED,
            'driver_city_id'   => TaxiApiMethods::REQUARED,
            'device_token'     => null,
            'app_version'      => null,
        ),
        'driver_end_work'            => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
        ),
        'get_parkings_list'          => array(
            'tenant_login'   => TaxiApiMethods::REQUARED,
            'driver_login'   => TaxiApiMethods::REQUARED,
            'driver_city_id' => TaxiApiMethods::REQUARED,
            'parking_id'     => null,
        ),
        'get_city_polygone'          => array(
            'tenant_login'   => TaxiApiMethods::REQUARED,
            'driver_login'   => TaxiApiMethods::REQUARED,
            'driver_city_id' => TaxiApiMethods::REQUARED,
        ),
        'set_parking_queue'          => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
            'parking_id'   => TaxiApiMethods::REQUARED,
        ),
        'get_parkings_orders_queues' => array(
            'tenant_login'   => TaxiApiMethods::REQUARED,
            'driver_login'   => TaxiApiMethods::REQUARED,
            'driver_city_id' => TaxiApiMethods::REQUARED,
            'parking_id'     => null,
        ),
        'get_orders_open_list'       => array(
            'tenant_login'   => TaxiApiMethods::REQUARED,
            'driver_login'   => TaxiApiMethods::REQUARED,
            'driver_city_id' => TaxiApiMethods::REQUARED,
            'parking_id'     => null,
        ),
        'get_orders_pretime_list'    => array(
            'tenant_login'   => TaxiApiMethods::REQUARED,
            'driver_login'   => TaxiApiMethods::REQUARED,
            'driver_city_id' => TaxiApiMethods::REQUARED,
        ),
        'get_order_for_id'           => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
            'order_id'     => TaxiApiMethods::REQUARED,
        ),
        'set_order_status'           => array(
            'tenant_login'      => TaxiApiMethods::REQUARED,
            'driver_login'      => TaxiApiMethods::REQUARED,
            'order_id'          => TaxiApiMethods::REQUARED,
            'status_new_id'     => TaxiApiMethods::REQUARED,
            'time_to_client'    => null,
            'detail_order_data' => null,
        ),
        'set_order_route'            => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
            'order_id'     => TaxiApiMethods::REQUARED,
            'order_route'  => TaxiApiMethods::REQUARED,
        ),
        'check_driver_on_work'       => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
        ),
        'set_signal_sos'             => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
            'driver_lat'   => TaxiApiMethods::REQUARED,
            'driver_lon'   => TaxiApiMethods::REQUARED,
        ),
        'unset_signal_sos'           => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
            'driver_lat'   => TaxiApiMethods::REQUARED,
            'driver_lon'   => TaxiApiMethods::REQUARED,
        ),
        'get_city_list'              => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
            'city_part'    => TaxiApiMethods::REQUARED,
        ),
        'get_geoobjects_list'        => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
            'city_id'      => TaxiApiMethods::REQUARED,
            'street_part'  => TaxiApiMethods::REQUARED,
        ),
        'set_driver_state'           => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
            'action'       => TaxiApiMethods::REQUARED,
            'comment'      => null,
        ),
        'get_client_tariff'          => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
        ),
        'call_cost'                  => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
            'address'      => TaxiApiMethods::REQUARED,
        ),
        'create_order'               => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
            'address'      => TaxiApiMethods::REQUARED,
            'client_phone' => TaxiApiMethods::REQUARED,
        ),
        'set_order_raw_calc'         => array(
            'tenant_login'    => TaxiApiMethods::REQUARED,
            'driver_login'    => TaxiApiMethods::REQUARED,
            'order_id'        => TaxiApiMethods::REQUARED,
            'order_calc_data' => TaxiApiMethods::REQUARED,
        ),
        'get_order_raw_calc'         => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
            'order_id'     => TaxiApiMethods::REQUARED,
        ),
        'driver_order_unblock'       => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
        ),
        'driver_order_block'         => array(
            'tenant_login' => TaxiApiMethods::REQUARED,
            'driver_login' => TaxiApiMethods::REQUARED,
            'block_min'    => TaxiApiMethods::REQUARED,
        )
    );

    public function __construct()
    {
        $this->validateData['code'] = null;
        $this->validateData['param'] = null;
        parent::__construct();
    }
    /*
     * GETTERS AND SETTERS
     */

    /*
     * METHODS
     */

    /**
     * Получить порядок по умолчанию для параметров метода, с дефолтными значениями
     * @param string $commandName - метод
     * @return array - array('name' => '', 'type' => null, 'orderId' => 0, ...)
     */
    private function defaultOrder($commandName)
    {
        $map = $this->_fixParamsMap[$commandName];
        return $map;
    }

    private function setValidateDataResult($code, $param = null)
    {
        $this->validateData['code'] = $code;
        $this->validateData['param'] = $param;
        return $this->validateData;
    }

    /**
     * Проверяет имена параметров на корректность
     * @param type $commandName
     * @param type $params
     * @return int TaxiErrorCode
     */
    private function validateParamName($commandName, $params)
    {
        $defaultOrder = $this->defaultOrder($commandName);
        foreach ($params as $param => $value) {
            if (!key_exists($param, $defaultOrder)) {
                return $this->setValidateDataResult(TaxiErrorCode::BAD_PARAM, $param);
            }
        }
        return $this->setValidateDataResult(TaxiErrorCode::OK);
    }

    /**
     * Проверка, заполнены ли обязательные параметры
     * @param type $commandName
     * @param type $params
     * @return int TaxiErrorCode
     */
    private function validateParamRequared($commandName, $params)
    {
        $defaultOrder = $this->defaultOrder($commandName);
        foreach ($defaultOrder as $defaultParam => $defaultValue) {
            if ($defaultValue == TaxiApiMethods::REQUARED) {
                if (!key_exists($defaultParam, $params)) {
                    return $this->setValidateDataResult(TaxiErrorCode::MISSING_INPUT_PARAMETER, $defaultParam);
                }
                if (key_exists($defaultParam, $params) && empty($params[$defaultParam])) {
                    return $this->setValidateDataResult(TaxiErrorCode::EMPTY_VALUE, $defaultParam);
                }
            }
        }
        return TaxiErrorCode::OK;
    }

    /**
     * Проверка входных парамтеров команды на корректность,
     * Если одна из проверок не проходит то возвращается код ошибки в
     * соответствующем методе
     * @param strin $commandName - команда
     * @param array $params - параметры
     * @return int TaxiErrorCode
     */
    public function validateParams($commandName, $params)
    {
        if (key_exists($commandName, $this->_fixParamsMap)) {
            //Проверяем параметры запроса на корректное название
            $validateData = $this->validateParamName($commandName, $params);
            if (!empty($validateData['code'])) {
                return $validateData;
            }
            //Проверяем, заполнены ли обязательные параметры
            $validateData = $this->validateParamRequared($commandName, $params);
            if (!empty($validateData['code'])) {
                return $validateData;
            }

            if (empty($validateData['code'])) {
                return $this->setValidateDataResult(TaxiErrorCode::OK);
            }
        } else {
            //Неизвестный запрос
            return $this->setValidateDataResult(TaxiErrorCode::UNKNOWN_REQUEST);
        }
    }
}