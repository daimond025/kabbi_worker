<?php

declare(strict_types=1);

namespace addressHistory\models;

/**
 * Class Client
 * @package addressHistory
 */
class Client
{
    /**
     * @var int
     */
    private $clientId;

    /**
     * Client constructor.
     *
     * @param int $clientId
     */
    public function __construct(int $clientId)
    {
        $this->clientId = $clientId;
    }

    /**
     * @return int
     */
    public function getClientId(): int
    {
        return $this->clientId;
    }
}