<?php

declare(strict_types=1);

namespace addressHistory\models;

/**
 * Class CityId
 * @package addressHistory\models
 */
class CityId
{
    /**
     * @var int
     */
    private $cityId;

    /**
     * CityId constructor.
     *
     * @param int $cityId
     */
    public function __construct(int $cityId)
    {
        $this->cityId = $cityId;
    }

    /**
     * @return int
     */
    public function getCityId(): int
    {
        return $this->cityId;
    }
}