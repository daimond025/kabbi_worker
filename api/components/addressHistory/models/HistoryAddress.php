<?php

declare(strict_types=1);

namespace addressHistory\models;

/**
 * Class HistoryAddress
 * @package addressHistory\models
 */
class HistoryAddress
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var Address
     */
    private $address;

    /**
     * HistoryAddress constructor.
     *
     * @param Client  $client
     * @param Address $address
     * @param int     $id
     */
    public function __construct(Client $client, Address $address, ?int $id = null)
    {
        $this->id      = $id;
        $this->client  = $client;
        $this->address = $address;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

}