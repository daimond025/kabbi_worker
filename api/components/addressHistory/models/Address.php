<?php

declare(strict_types=1);

namespace addressHistory\models;

/**
 * Class Address
 * @package addressHistory
 */
class Address
{
    /**
     * @var CityId
     */
    private $cityId;
    /**
     * @var string
     */
    private $city;
    /**
     * @var string
     */
    private $street;
    /**
     * @var string
     */
    private $house;
    /**
     * @var string
     */
    private $lat;
    /**
     * @var string
     */
    private $lon;

    /**
     * Address constructor.
     *
     * @param CityId $cityId
     * @param string $city
     * @param string $street
     * @param string $house
     * @param string $lat
     * @param string $lon
     */
    public function __construct(CityId $cityId, string $city, string $street, string $house, string $lat, string $lon)
    {
        $this->cityId = $cityId;
        $this->city   = $city;
        $this->street = $street;
        $this->house  = $house;
        $this->lat    = $lat;
        $this->lon    = $lon;
    }

    /**
     * @return CityId
     */
    public function getCityId(): CityId
    {
        return $this->cityId;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @return string
     */
    public function getHouse(): string
    {
        return $this->house;
    }

    /**
     * @return string
     */
    public function getLat(): string
    {
        return $this->lat;
    }

    /**
     * @return string
     */
    public function getLon(): string
    {
        return $this->lon;
    }

    /**
     * @param Address $address
     *
     * @return bool
     */
    public function equals(Address $address): bool
    {
        return $this->cityId === $address->getCityId()
            && $this->city === $address->getCity()
            && $this->street === $address->getStreet()
            && $this->house === $address->getHouse()
            && $this->lat === $address->getLat()
            && $this->lon === $address->getLon();
    }
}