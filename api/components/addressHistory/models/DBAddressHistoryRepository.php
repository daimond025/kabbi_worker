<?php

declare(strict_types=1);

namespace addressHistory\models;

use addressHistory\exceptions\AddressHistoryException;
use addressHistory\models\ar\ClientAddressHistory;
use yii\db\Exception;

/**
 * Class DBAddressHistoryRepository
 * @package addressHistory\models
 */
class DBAddressHistoryRepository implements RepositoryInterface
{
    /**
     * @param int $clientId
     * @param int $cityId
     *
     * @return array
     */
    private function getHistoryData(int $clientId, int $cityId): array
    {
        $data = ClientAddressHistory::find()
            ->select(['history_id', 'client_id', 'city_id', 'city', 'street', 'house', 'lat', 'lon'])
            ->where([
                'client_id' => $clientId,
                'city_id'   => $cityId,
            ])
            ->orderBy(['history_id' => SORT_DESC])
            ->asArray()
            ->all();

        return empty($data) ? [] : $data;
    }

    /**
     * @param int $id
     *
     * @throws AddressHistoryException
     */
    private function deleteHistoryAddress(int $id)
    {
        try {
            $model = ClientAddressHistory::findOne($id);

            if ($model !== null) {
                $model->delete();
            }
        } catch (\Exception $e) {
            throw new AddressHistoryException('Delete address error. Error=' . $e->getMessage(), 0, $e);
        }
    }

    /**
     * @param int    $clientId
     * @param int    $cityId
     * @param string $city
     * @param string $street
     * @param string $house
     * @param string $lat
     * @param string $lon
     *
     * @throws AddressHistoryException
     */
    private function insertHistoryAddress(
        int $clientId,
        int $cityId,
        string $city,
        string $street,
        string $house,
        string $lat,
        string $lon
    ) {
        try {
            $model = new ClientAddressHistory([
                'client_id' => $clientId,
                'city_id'   => $cityId,
                'city'      => $city,
                'street'    => $street,
                'house'     => $house,
                'lat'       => $lat,
                'lon'       => $lon,
            ]);

            if (!$model->save()) {
                throw new Exception(implode('; ', $model->getFirstErrors()));
            }
        } catch (\Exception $e) {
            throw new AddressHistoryException('Insert address error. Error=' . $e->getMessage(), 0, $e);
        }
    }

    /**
     * @param Client $client
     * @param CityId $cityId
     *
     * @return array
     */
    public function get(Client $client, CityId $cityId): array
    {
        return array_map(function ($item) use ($client, $cityId) {
            $city   = (string)($item['city'] ?? '');
            $street = (string)($item['street'] ?? '');
            $house  = (string)($item['house'] ?? '');
            $lat    = (string)($item['lat'] ?? '');
            $lon    = (string)($item['lon'] ?? '');

            return new HistoryAddress($client, new Address($cityId, $city, $street, $house, $lat, $lon),
                (int)$item['history_id']);
        }, $this->getHistoryData($client->getClientId(), $cityId->getCityId()));
    }

    /**
     * @param HistoryAddress $historyAddress
     *
     * @throws AddressHistoryException
     */
    public function delete(HistoryAddress $historyAddress): void
    {
        $this->deleteHistoryAddress($historyAddress->getId());
    }

    /**
     * @param HistoryAddress $historyAddress
     *
     * @throws AddressHistoryException
     */
    public function create(HistoryAddress $historyAddress): void
    {
        $address = $historyAddress->getAddress();

        $this->insertHistoryAddress(
            $historyAddress->getClient()->getClientId(),
            $address->getCityId()->getCityId(),
            $address->getCity(),
            $address->getStreet(),
            $address->getHouse(),
            $address->getLat(),
            $address->getLon());
    }
}