<?php

declare(strict_types=1);

namespace addressHistory\models;

/**
 * Interface RepositoryInterface
 * @package addressHistory\models
 */
interface RepositoryInterface
{
    public function get(Client $client, CityId $cityId): array;

    public function delete(HistoryAddress $historyAddress): void;

    public function create(HistoryAddress $historyAddress): void;

}