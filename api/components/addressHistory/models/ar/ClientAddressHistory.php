<?php

declare(strict_types=1);

namespace addressHistory\models\ar;

use yii\db\ActiveRecord;

/**
 * Class ClientAddressHistory
 * @package addressHistory\models\ar
 *
 * @property int    $history_id
 * @property int    $client_id
 * @property int    $city_id
 * @property string $city
 * @property string $street
 * @property string $house
 * @property string $lat
 * @property string $lon
 * @property string $create_time
 */
class ClientAddressHistory extends ActiveRecord
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['client_id', 'city_id'], 'required'],
            [['city', 'street', 'house'], 'string', 'max' => 255],
            [['lat', 'lon'], 'double'],
        ];
    }

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return '{{%client_address_history}}';
    }

    /**
     * @param bool $insert
     *
     * @return bool
     */
    public function beforeSave($insert): bool
    {
        $this->create_time = date('Y-m-d H:i:s');

        return parent::beforeSave($insert);
    }
}