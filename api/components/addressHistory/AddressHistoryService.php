<?php

declare(strict_types=1);

namespace addressHistory;

use addressHistory\models\Address;
use addressHistory\models\HistoryAddress;
use addressHistory\models\CityId;
use addressHistory\models\Client;
use addressHistory\models\RepositoryInterface;

/**
 * Class AddressHistoryService
 * @package addressHistory
 */
class AddressHistoryService
{
    const DEFAULT_HISTORY_LIMIT = 20;

    /**
     * @var RepositoryInterface
     */
    private $repository;

    /**
     * AddressHistoryService constructor.
     *
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array   $history
     * @param Address $address
     *
     * @return array
     */
    private function removeDoublesFromHistory(array $history, Address $address): array
    {
        $cleanedHistory = [];
        foreach ($history as $item) {
            /* @var $item HistoryAddress */
            if ($item->getAddress()->equals($address)) {
                $this->repository->delete($item);
            } else {
                $cleanedHistory[] = $item;
            }
        }

        return $cleanedHistory;
    }

    /**
     * @param array $history
     */
    private function removeHistoryExcess(array $history): void
    {
        $excessAddresses = array_slice($history, self::DEFAULT_HISTORY_LIMIT - 1);

        foreach ($excessAddresses as $item) {
            $this->repository->delete($item);
        }
    }

    /**
     * @param HistoryAddress $historyAddress
     */
    public function save(HistoryAddress $historyAddress): void
    {
        $history = $this->repository->get($historyAddress->getClient(), $historyAddress->getAddress()->getCityId());

        $cleanedHistory = $this->removeDoublesFromHistory($history, $historyAddress->getAddress());
        $this->removeHistoryExcess($cleanedHistory);

        $this->repository->create($historyAddress);
    }

    /**
     * @param Client $client
     * @param CityId $cityId
     *
     * @return array
     */
    public function getHistory(Client $client, CityId $cityId): array
    {
        return $this->repository->get($client, $cityId);
    }

}
