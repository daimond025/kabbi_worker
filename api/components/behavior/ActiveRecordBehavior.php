<?php
/**
 * This behavior contains common methods for AR models.
 */
namespace app\components\behavior;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class ActiveRecordBehavior extends Behavior
{
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert',
        ];
    }

    public function beforeInsert($event)
    {
        //$this->owner->tenant_id = user()->tenant_id;
    }
}