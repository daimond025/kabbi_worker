<?php

namespace app\components\serviceEngine;

use app\components\consul\exceptions\ConsulServiceException;
use yii\base\Object;
use Yii;

/**
 * Class ServiceEngine
 * @package app\components\serviceEngine
 */
class ServiceEngine extends Object
{
    const SHIFT_END_REASON = 'worker_service';
    const SHIFT_END_EVENT_SENDER_TYPE = 'worker';
    const CONSUL_SERVICE_NAME = 'service_engine';


    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * Send order to service_engine
     *
     * @param $orderId
     * @param $tenantId
     * @return bool
     */
    public function neworderAuto($orderId, $tenantId)
    {

        $params = [
            'order_id'  => (string)$orderId,
            'tenant_id' => (string)$tenantId,
        ];

        $response = $this->sendRequst('neworder_auto', $params);

        return isset($response['result']) && $response['result'] == 1;
    }

    /**
     * Send to engine new worker shift
     *
     * @param string $tenantId
     * @param string $tenantLogin
     * @param string $workerCallsign
     * @param string $shiftId
     * @param string $shiftLifeTime
     *
     * @return boolean
     */
    public function watchWorkerShift($tenantId, $tenantLogin, $workerCallsign, $shiftId, $shiftLifeTime)
    {
        $result = $this->sendRequst("watch_worker_shift", [
            'tenant_login'    => (string)$tenantLogin,
            'worker_callsign' => (string)$workerCallsign,
            'shift_life_time' => (string)$shiftLifeTime,
            'tenant_id'       => (string)$tenantId,
            'shift_id'        => (string)$shiftId,
        ]);

        return isset($result['result']) && $result['result'] == 1;
    }


    /**
     * Close worker shift
     *
     * @param int $tenantId
     * @param string $tenantLogin
     * @param string $workerId
     * @param int $workerCallsign
     * @param int $shiftId
     *
     *
     * @return bool
     */
    public function closeWorkerShift($tenantId, $tenantLogin, $workerId,$workerCallsign,  $shiftId)
    {
        $result = $this->sendRequst('close_worker_shift', [
            'tenant_id'                   => (string)$tenantId,
            'tenant_login'                => (string)$tenantLogin,
            'worker_callsign'             => (string)$workerCallsign,
            'shift_end_reason'            => (string)self::SHIFT_END_REASON,
            'shift_end_event_sender_type' => (string)self::SHIFT_END_EVENT_SENDER_TYPE,
            'shift_end_event_sender_id'   => (string)$workerId,
            'shift_id'                    => (string)$shiftId,
        ]);
        if (isset($result['error'])) {
            \Yii::error("Error in NodeJS method `close_worker_shift` (tenant_id={$tenantId}, tenant_login={$tenantLogin},worker_callsign={$workerCallsign}, shift_id={$shiftId}): . {$result['error']} . ");
        }
        return isset($result['result']) && $result['result'] == 1;
    }

    /**
     * Block worker order
     *
     * @param $tenantId
     * @param $workerCallsign
     *
     * @return bool
     */
    public function blockWorkerOrder($tenantId, $workerCallsign)
    {
        $result = $this->sendRequst('block_worker_order', [
            'tenant_id'       => (string)$tenantId,
            'worker_callsign' => (string)$workerCallsign,
        ]);
        if (isset($result['error'])) {
            \Yii::error("Error in NodeJS method `block_worker_order` (tenant_id={$tenantId}, worker_callsign={$workerCallsign}): {$result['error']}");
        }

        return isset($result['result']) && $result['result'] == 1;
    }


    /**
     * Unblock worker order
     *
     * @param $tenantId
     * @param $workerCallsign
     * @param $blockId
     *
     * @return bool
     */
    public function unblockWorkerOrder($tenantId, $workerCallsign, $blockId)
    {
        $result = $this->sendRequst('unblock_worker_order', [
            'tenant_id'       => (string)$tenantId,
            'worker_callsign' => (string)$workerCallsign,
            'block_id'        => (string)$blockId,
        ]);
        if (isset($result['error'])) {
            \Yii::error("Error in NodeJS method `unblock_worker_order` (tenant_id={$tenantId}, worker_callsign={$workerCallsign}, block_id={$blockId}): . {$result['error']} . ");
        }

        return isset($result['result']) && $result['result'] == 1;
    }


    /**
     * Block worker pre-order
     *
     * @param $tenantId
     * @param $workerCallsign
     *
     * @return bool
     */
    public function blockWorkerPreOrder($tenantId, $workerCallsign)
    {
        $result = $this->sendRequst('block_worker_preorder', [
            'tenant_id'       => (string)$tenantId,
            'worker_callsign' => (string)$workerCallsign,
        ]);
        if (isset($result['error'])) {
            \Yii::error("Error in NodeJS method `block_worker_preorder` (tenant_id={$tenantId}, worker_callsign={$workerCallsign}): {$result['error']}");
        }

        return isset($result['result']) && $result['result'] == 1;
    }


    /**
     * Unblock worker pre-order
     *
     * @param $tenantId
     * @param $workerCallsign
     * @param $blockId
     *
     * @return bool
     */
    public function unblockWorkerPreOrder($tenantId, $workerCallsign, $blockId)
    {
        $result = $this->sendRequst('unblock_worker_preorder', [
            'tenant_id'       => (string)$tenantId,
            'worker_callsign' => (string)$workerCallsign,
            'block_id'        => (string)$blockId,
        ]);
        if (isset($result['error'])) {
            \Yii::error("Error in NodeJS method `unblock_worker_preorder` (tenant_id={$tenantId}, worker_callsign={$workerCallsign}, block_id={$blockId}): . {$result['error']} . ");
        }

        return isset($result['result']) && $result['result'] == 1;
    }

    /**
     * Send request
     * @param $method
     * @param $params
     * @return bool|mixed
     */
    private function sendRequst($method, $params)
    {
        if (is_array($params)) {
            $params = http_build_query($params);
        }
        $url = $this->getUrl($method) . "?" . $params;
        if (!$url) {
            return false;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/x-www-form-urlencode',
        ]);
        $result    = curl_exec($ch);
        $errorCode = curl_errno($ch);
        curl_close($ch);
        return ($errorCode == CURLE_OK) ? json_decode($result, true) : false;
    }

    /**
     * Get url
     * @param $method
     * @return bool|string
     */
    private function getUrl($method)
    {
        try {
            return Yii::$app->get('consulService')->createMethodUrl(self::CONSUL_SERVICE_NAME, $method);
        } catch (ConsulServiceException $ex) {
            Yii::error('Ошибка получения location из consul  для:' . self::CONSUL_SERVICE_NAME . ". Ошибка: " . $ex->getMessage());
            return false;
        }

    }


}