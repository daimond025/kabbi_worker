<?php

namespace bonusSystem\exceptions;

use yii\base\Exception;

/**
 * Class InvalidClientTariffException
 * @package bonusSystem\exceptions
 */
class InvalidClientTariffException extends Exception
{

}