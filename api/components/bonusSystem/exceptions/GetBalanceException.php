<?php

namespace bonusSystem\exceptions;

use yii\base\Exception;

/**
 * Class GetBalanceException
 * @package bonusSystem\exceptions
 */
class GetBalanceException extends Exception
{

}