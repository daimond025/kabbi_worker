<?php

namespace bonusSystem\exceptions;

use yii\base\Exception;

/**
 * Class CreatePurchaseOperationException
 * @package bonusSystem\exceptions
 */
class CreatePurchaseOperationException extends Exception
{

}