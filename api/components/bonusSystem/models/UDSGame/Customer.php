<?php

namespace bonusSystem\models\UDSGame;

/**
 * Class Customer
 * @package bonusSystem\models\UDSGame
 */
class Customer
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $surname;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var float
     */
    private $scores;

    /**
     * Customer constructor.
     *
     * @param int    $id
     * @param string $name
     * @param string $surname
     * @param string $phone
     * @param float  $scores
     */
    public function __construct($id, $name, $surname, $phone, $scores)
    {
        $this->id      = $id;
        $this->name    = $name;
        $this->surname = $surname;
        $this->phone   = $phone;
        $this->scores  = $scores;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return float
     */
    public function getScores()
    {
        return $this->scores;
    }

}