<?php

namespace bonusSystem\models\UDSGame;

/**
 * Class MarketingSettings
 * @package bonusSystem\models\UDSGame
 */
class MarketingSettings
{
    /**
     * @var float
     */
    private $discountBase;

    /**
     * @var float
     */
    private $discountLevel1;

    /**
     * @var float
     */
    private $discountLevel2;

    /**
     * @var float
     */
    private $discountLevel3;

    /**
     * @var float
     */
    private $maxScoresDiscount;

    /**
     * MarketingSettings constructor.
     *
     * @param float $discountBase
     * @param float $discountLevel1
     * @param float $discountLevel2
     * @param float $discountLevel3
     * @param float $maxScoresDiscount
     */
    public function __construct(
        $discountBase,
        $discountLevel1,
        $discountLevel2,
        $discountLevel3,
        $maxScoresDiscount
    ) {
        $this->discountBase      = $discountBase;
        $this->discountLevel1    = $discountLevel1;
        $this->discountLevel2    = $discountLevel2;
        $this->discountLevel3    = $discountLevel3;
        $this->maxScoresDiscount = $maxScoresDiscount;
    }

    /**
     * @return float
     */
    public function getDiscountBase()
    {
        return $this->discountBase;
    }

    /**
     * @return float
     */
    public function getDiscountLevel1()
    {
        return $this->discountLevel1;
    }

    /**
     * @return float
     */
    public function getDiscountLevel2()
    {
        return $this->discountLevel2;
    }

    /**
     * @return float
     */
    public function getDiscountLevel3()
    {
        return $this->discountLevel3;
    }

    /**
     * @return float
     */
    public function getMaxScoresDiscount()
    {
        return $this->maxScoresDiscount;
    }

}