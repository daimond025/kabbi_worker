<?php

namespace bonusSystem;

use bonusSystem\exceptions\ClientIsNotRegisteredException;
use bonusSystem\exceptions\CreatePurchaseOperationException;
use bonusSystem\exceptions\GetBalanceException;
use bonusSystem\exceptions\GetCustomerException;
use bonusSystem\exceptions\GetPaymentStrategyException;
use bonusSystem\exceptions\InvalidClientTariffException;
use bonusSystem\exceptions\OrderIsNotRegisteredException;
use bonusSystem\exceptions\PaymentStrategyNotFoundException;
use bonusSystem\exceptions\RegisterClientException;
use bonusSystem\exceptions\RegisterOrderException;
use bonusSystem\models\ar\BonusSystem as ARBonusSystem;
use bonusSystem\models\BonusSystemType;
use bonusSystem\models\GootaxService;
use bonusSystem\models\PaymentStrategy;
use bonusSystem\models\UDSGame\UDSGameApi;
use bonusSystem\models\UDSGameService;

/**
 * Class BonusSystem
 * @package bonusSystem
 */
class BonusSystem
{
    const BONUS_SYSTEM_ID_GOOTAX = 1;
    const BONUS_SYSTEM_ID_UDS_GAME = 2;

    /**
     * @var BonusSystemType
     */
    private $bonusSystemType;

    /**
     * @var GootaxService
     */
    private $gootaxService;

    /**
     * @var UDSGameService
     */
    private $udsGameService;

    /**
     * BonusSystemFacade constructor.
     *
     * @param int $tenantId
     */
    public function __construct($tenantId)
    {
        $this->bonusSystemType = $this->getBonusSystemTypeByTenantId($tenantId);

        if ($this->bonusSystemType->getId() === self::BONUS_SYSTEM_ID_UDS_GAME) {
            $api = new UDSGameApi(
                isset(\Yii::$app->params['bonusSystem.UDSGame.baseUrl']) ? \Yii::$app->params['bonusSystem.UDSGame.baseUrl'] : null,
                $this->bonusSystemType->getApiKey(),
                isset(\Yii::$app->params['bonusSystem.UDSGame.connectionTimeout']) ? \Yii::$app->params['bonusSystem.UDSGame.connectionTimeout'] : null,
                isset(\Yii::$app->params['bonusSystem.UDSGame.timeout']) ? \Yii::$app->params['bonusSystem.UDSGame.timeout'] : null
            );

            $this->udsGameService = new UDSGameService($api);
        } else {
            $this->gootaxService = new GootaxService();
        }
    }

    /**
     *  Getting current type of bonus system
     * 
     * @return BonusSystemType
     */
    public function getBonusSystemType() 
    {
        return $this->bonusSystemType;
    }

    /**
     * Getting bonus system type
     *
     * @param int $tenantId
     *
     * @return BonusSystemType
     */
    private function getBonusSystemTypeByTenantId($tenantId)
    {
        $model = ARBonusSystem::find()
            ->select(['t.id', 't.name', 'ts.api_key'])
            ->alias('t')
            ->innerJoinWith('tenantHasBonusSystem as ts')
            ->where(['ts.tenant_id' => $tenantId])
            ->asArray()
            ->one();

        if (empty($model)) {
            $model = ARBonusSystem::find()
                ->select(['id', 'name'])
                ->where(['id' => self::BONUS_SYSTEM_ID_GOOTAX])
                ->asArray()
                ->one();
        }

        return new BonusSystemType(
            (int)$model['id'],
            (string)$model['name'],
            (string)array_key_exists('api_key', $model) ? $model['api_key'] : null
        );
    }

    /**
     * Is UDS Game bonus system
     *
     * @return bool
     */
    public function isUDSGameBonusSystem()
    {
        return $this->bonusSystemType->getId() === self::BONUS_SYSTEM_ID_UDS_GAME;
    }

    /**
     * Getting balance
     *
     * @param int $clientId
     * @param int $currencyId
     *
     * @return float
     * @throws GetBalanceException
     * @throws ClientIsNotRegisteredException
     */
    public function getBalance($clientId, $currencyId)
    {
        return $this->isUDSGameBonusSystem()
            ? $this->udsGameService->getBalance($clientId, $currencyId)
            : $this->gootaxService->getBalance($clientId, $currencyId);
    }

    /**
     * Client registration
     *
     * @param int    $clientId
     * @param string $promoCode
     *
     * @throws RegisterClientException
     * @throws GetCustomerException
     */
    public function registerClient($clientId, $promoCode)
    {
        $this->udsGameService->registerClient($clientId, $promoCode);
    }

    /**
     * Is registered client?
     *
     * @param int $clientId
     *
     * @return bool
     */
    public function isRegisteredClient($clientId)
    {
        return $this->udsGameService->isRegisteredClient($clientId);
    }

    /**
     * Order registration
     *
     * @param int    $orderId
     * @param string $promoCode
     *
     * @throws GetCustomerException
     * @throws RegisterOrderException
     * @throws ClientIsNotRegisteredException
     * @throws InvalidClientTariffException
     */
    public function registerOrder($orderId, $promoCode = null)
    {
        $this->udsGameService->registerOrder($orderId, $promoCode);
    }

    /**
     * Register order payment
     *
     * @param int   $orderId
     * @param float $sum
     * @param float $bonus
     *
     * @throws OrderIsNotRegisteredException
     * @throws CreatePurchaseOperationException
     */
    public function registerOrderPayment($orderId, $sum, $bonus)
    {
        $this->udsGameService->registerOrderPayment($orderId, $sum, $bonus);
    }

    /**
     * Is registered order?
     *
     * @param int $orderId
     *
     * @return bool
     */
    public function isRegisteredOrder($orderId)
    {
        return $this->udsGameService->isRegisteredOrder($orderId);
    }

    /**
     * Is registered order for write-off?
     *
     * @param int $orderId
     *
     * @return bool
     */
    public function isRegisteredOrderForWriteOff($orderId)
    {
        return $this->udsGameService->isRegisteredOrderForWriteOff($orderId);
    }

    /**
     * Getting payment strategy
     *
     * @param int $tariffId
     *
     * @return PaymentStrategy
     * @throws PaymentStrategyNotFoundException
     * @throws GetPaymentStrategyException
     */
    public function getPaymentStrategy($tariffId)
    {
        return $this->isUDSGameBonusSystem()
            ? $this->udsGameService->getPaymentStrategy($tariffId)
            : $this->gootaxService->getPaymentStrategy($tariffId);
    }

}