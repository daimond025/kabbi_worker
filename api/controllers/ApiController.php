<?php

namespace app\controllers;

use healthCheck\CheckResult;
use healthCheck\checks\BaseCheck;
use healthCheck\checks\CustomCheck;
use healthCheck\checks\HttpServiceCheck;
use healthCheck\checks\MongoDBCheck;
use healthCheck\checks\MySqlCheck;
use healthCheck\checks\RedisCheck;
use healthCheck\checks\ConsulCheck;
use healthCheck\HealthCheckService;
use healthCheck\checks\HttpDistributedServiceCheck;
use app\components\serviceEngine\ServiceEngine;
use yii\base\Application;
use yii\base\InvalidConfigException;
use yii\base\Module;
use yii\helpers\Html;
use yii\web\Controller;

/**
 * Class ApiController
 * @package api\controllers
 */
class ApiController extends Controller
{
    const SERVICE_OPERATIONAL = 'SERVICE OPERATIONAL';
    const SERVICE_NOT_OPERATIONAL = 'SERVICE NOT OPERATIONAL';

    const STATUS_OK = 'ok';
    const STATUS_ERROR = 'error';

    const STATUS_LABELS = [
        self::STATUS_OK    => '[ OK    ]',
        self::STATUS_ERROR => '[ ERROR ]',
    ];

    const MIME_TYPE_TEXT_PLAIN = 'text/plain';

    /**
     * @var Application
     */
    private $application;

    /**
     * ApiController constructor.
     *
     * @param string $id
     * @param Module $module
     * @param array  $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->application = \Yii::$app;
    }

    /**
     * @return string
     */
    private function getApiVersion()
    {
        $application = \Yii::$app->id;
        $version     = \Yii::$app->params['version'];

        return "{$application} v{$version}";
    }

    /**
     * @return string
     */
    private function getPHPVersion()
    {
        $php = PHP_VERSION;

        return "work on php v{$php}";
    }

    /**
     * @return BaseCheck[]
     * @throws InvalidConfigException
     */
    private function getHealthChecks()
    {
        $application = $this->application;

        return [
            new MySqlCheck('database "db"', $application->get('db')),
            new MongoDBCheck('database "mongodb"', $application->get('mongodb')),
            new RedisCheck('database "redis"', $application->get('redis_main_check_status')),
            new RedisCheck('database "redis (cache)"', $application->get('redis_cache_check_status')),
            new CustomCheck('service "gearman"',
                function () use ($application) {
                    $application->get('gearman');
                }),
            new CustomCheck('service "rabbitmq"',
                function () use ($application) {
                    $application->get('amqp');
                }),
            new ConsulCheck('service "consulAgent"', $application->get('consulService')),
            new HttpDistributedServiceCheck('service "engine"',ServiceEngine::CONSUL_SERVICE_NAME,'version'),
            new HttpServiceCheck('service "push"', $application->get('pusher')->url . 'version'),
            new HttpServiceCheck('api "paygate"', $application->params['paymentGateApi.url'] . 'version'),
            new HttpServiceCheck('api "service"', $application->get('routeAnalyzer')->url . 'version'),
            new HttpServiceCheck('api "order"', $application->get('orderApi')->baseUrl . 'version'),
        ];
    }

    /**
     * @return bool
     */
    private function isTextFormatRequested()
    {
        return array_key_exists(self::MIME_TYPE_TEXT_PLAIN, $this->application->request->acceptableContentTypes);
    }

    public function actionVersion()
    {
        $result = implode(PHP_EOL, [$this->getApiVersion(), $this->getPHPVersion()]);

        return $this->isTextFormatRequested()
            ? $result : Html::tag('pre', Html::encode($result),
                ['style' => 'word - wrap: break-word; white - space: pre - wrap']);
    }

    public function actionStatus()
    {
        $status = self::STATUS_OK;
        $lines  = [$this->getApiVersion(), $this->getPHPVersion(), ''];

        $checkResults = (new HealthCheckService($this->getHealthChecks()))->getCheckResults();
        foreach ($checkResults as $checkResult) {
            /* @var $checkResult CheckResult */
            if ($checkResult->isSuccessful()) {
                $lines[] = self::STATUS_LABELS[self::STATUS_OK] . " {$checkResult->getName()}";
            } else {
                $lines[] = self::STATUS_LABELS[self::STATUS_ERROR] . " {$checkResult->getName()} ({$checkResult->getErrorMessage()})";
                $status  = self::STATUS_ERROR;
            }
        }

        $lines[] = '';
        $lines[] = $status === self::STATUS_OK ? self::SERVICE_OPERATIONAL : self::SERVICE_NOT_OPERATIONAL;

        $result = implode(PHP_EOL, $lines);

        return $this->isTextFormatRequested()
            ? $result : Html::tag('pre', Html::encode($result),
                ['style' => 'word - wrap: break-word; white - space: pre - wrap']);
    }

}